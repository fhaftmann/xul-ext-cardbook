import { cardbookBGRepo } from "./chrome/content/BG/utils/cardbookBGRepo.mjs";
import { cardbookBGInit } from "./chrome/content/BG/utils/cardbookBGInit.mjs";
import { cardbookBGLog } from "./chrome/content/BG/utils/cardbookBGLog.mjs";
import { cardbookBGUtils } from "./chrome/content/BG/utils/cardbookBGUtils.mjs";
import { cardbookBGActions } from "./chrome/content/BG/utils/cardbookBGActions.mjs";
import { cardbookBGSynchronization } from "./chrome/content/BG/utils/cardbookBGSynchronization.mjs";
import { cardbookBGSynchronizationGoogle2 } from "./chrome/content/BG/utils/cardbookBGSynchronizationGoogle2.mjs";
import { cardbookBGSynchronizationCARDDAV } from "./chrome/content/BG/utils/cardbookBGSynchronizationCARDDAV.mjs";

import { cardbookBGCollection } from "./chrome/content/BG/utils/cardbookBGCollection.mjs";
import { cardbookBGAttachVCards } from "./chrome/content/BG/utils/cardbookBGAttachVCards.mjs";
import { cardbookBGBirthdays } from "./chrome/content/BG/utils/cardbookBGBirthdays.mjs";
import { cardbookBGDuplicate } from "./chrome/content/BG/utils/cardbookBGDuplicate.mjs";
import { cardbookBGFiles } from "./chrome/content/BG/utils/cardbookBGFiles.mjs";
import { cardbookBGColumns } from "./chrome/content/BG/utils/cardbookBGColumns.mjs";
import { cardbookBGPreferences } from "./chrome/content/BG/utils/cardbookBGPreferences.mjs";

import { cardbookIDBMailPop } from "./chrome/content/BG/indexedDB/cardbookIDBMailPop.mjs";
import { cardbookIDBDuplicate } from "./chrome/content/BG/indexedDB/cardbookIDBDuplicate.mjs";
import { cardbookIDBImage } from "./chrome/content/BG/indexedDB/cardbookIDBImage.mjs";
import { cardbookIDBCard } from "./chrome/content/BG/indexedDB/cardbookIDBCard.mjs";
import { cardbookIDBCat } from "./chrome/content/BG/indexedDB/cardbookIDBCat.mjs";
import { cardbookIDBSearch } from "./chrome/content/BG/indexedDB/cardbookIDBSearch.mjs";
import { cardbookIDBPrefDispName } from "./chrome/content/BG/indexedDB/cardbookIDBPrefDispName.mjs";
import { cardbookIDBUndo } from "./chrome/content/BG/indexedDB/cardbookIDBUndo.mjs";

import { cardbookIDBEncryptor } from "./chrome/content/BG/indexedDB/cardbookIDBEncryptor.mjs";
import { cardbookIndexedDB } from "./chrome/content/BG/indexedDB/cardbookIndexedDB.mjs";

import { cardbookHTMLUtils } from "./chrome/content/HTML/utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookHTMLTypes } from "./chrome/content/HTML/utils/scripts/cardbookHTMLTypes.mjs";
import { cardbookHTMLFormulas } from "./chrome/content/HTML/utils/scripts/cardbookHTMLFormulas.mjs";
import { cardbookHTMLPluralRules } from "./chrome/content/HTML/utils/scripts/cardbookHTMLPluralRules.mjs";
import { cardbookHTMLDates } from "./chrome/content/HTML/utils/scripts/cardbookHTMLDates.mjs";

import { cardbookCardParser } from "./chrome/content/BG/utils/cardbookCardParser.mjs";
import { cardbookCategoryParser } from "./chrome/content/BG/utils/cardbookCategoryParser.mjs";
import { cardbookListConversion } from "./chrome/content/BG/utils/cardbookListConversion.mjs";
import { cardbookFetchParser } from "./chrome/content/BG/utils/cardbookFetchParser.mjs";

import { simpleMailRedirection } from "./chrome/content/BG/external/simpleMailRedirection.js";

async function main() {

	var processDataDisplayId = {};

	messenger.NotifyTools.onNotifyBackground.addListener(async (info) => {
		switch (info.query) {
			case "cardbook.attachments.getAttachmentContent": {
					let tabs = await browser.tabs.query({type: "mail"});
					let displayedMessages = await browser.messageDisplay.getDisplayedMessages(tabs[0].id);
					let displayedMessage = displayedMessages[0];
					let attachments = await browser.messages.listAttachments(displayedMessage.id);
					let file = await browser.messages.getAttachmentFile(displayedMessage.id, info.attachmentId);
					let content = await file.text();
					return content;
				}
			case "cardbook.attachments.importCardsFromFile": {
				let actionId = cardbookBGActions.startAction(cardbookBGRepo, "cardsImportedFromFile", [info.filename]);
				cardbookBGRepo.importConflictChoice[actionId] = {};
				cardbookBGRepo.currentAction[actionId]["mode"] = "import";
				cardbookBGRepo.currentAction[actionId]["status"] = "STARTED";
				cardbookBGRepo.currentAction[actionId]["params"] = {};
				let cards = cardbookBGFiles.getCardsFromFileContent(cardbookBGRepo, info.content, null, info.dirPrefId);
				cardbookBGRepo.currentAction[actionId].totalEstimatedCards = cards.length;
				for (let card of cards) {
					let version = cardbookBGPreferences.getVCardVersion(info.dirPrefId);
					let dateFormat = cardbookHTMLUtils.getDateFormat(info.dirPrefId, version);
					let sourceDateFormat = cardbookHTMLUtils.getDateFormat(card.dirPrefId, card.version);
					let defaultChoice = "";
					await importCard(card, info.dirPrefId, version, sourceDateFormat, dateFormat,
						actionId, defaultChoice);
				}
				cardbookBGRepo.currentAction[actionId]["status"] = "FINISHED";
				let targetName = cardbookBGPreferences.getName(info.dirPrefId);
				await endAction(actionId, null, {source: "XUL", name: info.filename, length: cardbookBGRepo.currentAction[actionId].doneCards, targetName: targetName});
				break;
			}
			case "cardbook.clipboardSetText":
				await navigator.clipboard.writeText(info.text);
				break;
			case "cardbook.clipboardSetImage":
				// info.type is png, jpg, etc...
				// setImageData only accepts png and jpeg
				let imageType = (info.type.toLowerCase() == "jpg") ? "jpeg" : info.type.toLowerCase();
				const byteCharacters = atob(info.b64);
				const byteNumbers = new Uint8Array(byteCharacters.length);
				for (let i = 0; i < byteCharacters.length; i++) {
					byteNumbers[i] = byteCharacters.charCodeAt(i);
				}
				await browser.clipboard.setImageData(byteNumbers.buffer, imageType);
				// does not work
				// const byteCharacters = atob(info.b64);
				// const byteNumbers = new Array(byteCharacters.length);
				// for (let i = 0; i < byteCharacters.length; i++) {
				// 	byteNumbers[i] = byteCharacters.charCodeAt(i);
				// }
				// const byteArray = new Uint8Array(byteNumbers);
				// const blob = new Blob([byteArray], {type: info.type});
				// let data = [new ClipboardItem({ [info.type]: blob })];
				// await navigator.clipboard.write(data);
				break;
			case "cardbook.sharevCards":
				let tab1 = await messenger.compose.beginNew();
				for (let vCard of info.vCards) {
					let blob = new Blob([vCard.vCard], {type: "text;charset=utf-8"});
					let file = new File([blob], vCard.filename);
					await messenger.compose.addAttachment(tab1.id, {file: file, name: vCard.filename});
				}
				break;
			case "cardbook.emailCards":
				let tab2 = await messenger.compose.beginNew();
				let composeDetails = {};
				for (let compField of info.compFields) {
					composeDetails[compField.field] = compField.value;
				}
				messenger.compose.setComposeDetails(tab2.id, composeDetails);
				break;
			case "cardbook.conf.addProgressBar":
				await messenger.runtime.sendMessage({query: info.query, type: info.type, total: info.total, done: info.done});
				break;
			case "cardbook.pref.getPref":
				return cardbookBGPreferences.getPref(info.pref);
			case "cardbook.pref.getReadOnly":
				return cardbookBGPreferences.getReadOnly(info.dirPrefId);
			case "cardbook.pref.getColor":
				return cardbookBGPreferences.getColor(info.dirPrefId);
			case "cardbook.pref.getName":
				return cardbookBGPreferences.getName(info.dirPrefId);
			case "cardbook.pref.getDBCached":
				return cardbookBGPreferences.getDBCached(info.dirPrefId);
			case "cardbook.pref.getAllRestrictions":
				return cardbookBGPreferences.getAllRestrictions();
			case "cardbook.pref.getAllPrefs":
				return cardbookBGPreferences.getAllPrefs();
			case "cardbook.pref.removePrefs":
				await cardbookBGPreferences.removePrefs(info.keys);
				break;
			case "cardbook.pref.setPref":
				await cardbookBGPreferences.setPref(info.key, info.value);
				break;
			case "cardbook.pref.migrateClear":
				try {
					await cardbookBGPreferences.clear();
					return "OK";
				} catch {return "KO"};
			case "cardbook.pref.migrateString":
				try {
					await cardbookBGPreferences.setPref(info.key, info.value);
					return "OK";
				} catch {return "KO"};
			case "cardbook.QFB.getAllPrefIds": {
				let result = [];
				for (let dirPrefId of cardbookBGPreferences.getAllPrefIds()) {
					let type = cardbookBGPreferences.getType(dirPrefId);
					let enabled = cardbookBGPreferences.getEnabled(dirPrefId);
					if (enabled && (type !== "SEARCH")) {
						let name = cardbookBGPreferences.getName(dirPrefId);
						let color = cardbookBGPreferences.getColor(dirPrefId);
						result.push([dirPrefId, name, color]);
					}
				}
				return result;
			}
			case "cardbook.displayEvents":
				await messenger.runtime.sendMessage({query: info.query});
				break;
			case "cardbook.processData.cardsLoaded":
				await messenger.runtime.sendMessage({query: info.query, winId: info.winId, displayId: info.displayId, cardsLoaded: info.cardsLoaded});
				break;
			case "cardbook.processData.toDo":
				await messenger.runtime.sendMessage({query: info.query, winId: info.winId, displayId: info.displayId, toDo: info.toDo});
				break;
			case "cardbook.processData.rowDone":
				await messenger.runtime.sendMessage({query: info.query, winId: info.winId, displayId: info.displayId, rowDone: info.rowDone});
				break;
			case "cardbook.formatData.displayCardLineTels":
				await messenger.runtime.sendMessage({query: info.query, winId: info.winId, displayId: info.displayId, record: info.record});
				break;
			case "cardbook.formatData.displayCardLineEmail":
				await messenger.runtime.sendMessage({query: info.query, winId: info.winId, displayId: info.displayId, record: info.record});
				break;
			case "cardbook.formatData.displayCardLineFields":
				await messenger.runtime.sendMessage({query: info.query, winId: info.winId, displayId: info.displayId, record: info.record});
				break;
			case "cardbook.findDuplicates.finishMergeAction":
				await messenger.runtime.sendMessage({query: info.query, duplicateWinId: info.duplicateWinId, duplicateDisplayId: info.duplicateDisplayId, duplicateLineId: info.duplicateLineId});
				break;
			case "cardbook.getPrefAddressFromCard":
				return cardbookHTMLUtils.getPrefAddressFromCard(info.card, info.type, info.pref);
			case "cardbook.getCardFromEmail":
				return cardbookBGRepo.getCardFromEmail(info.email, info.dirPrefId);
			case "cardbook.getMailFormatFromCard":
				return cardbookHTMLUtils.getMailFormatFromCard(info.card);
			case "cardbook.getImage": {
				let image = await cardbookIDBImage.getImage(cardbookBGRepo.statusInformation, info.field, info.dirName, info.cardId, info.cardName);
				return image;
			}
			case "cardbook.deleteCardsAndValidate": {
				await deleteCardsAndValidate(info.listOfCardsId);
				break;
			}
			case "cardbook.deleteCards":
				let actionId = cardbookBGActions.startAction(cardbookBGRepo, "cardsDeleted", info.cards, null, info.cards.length, info.cards.length);
				await cardbookBGRepo.asyncDeleteCards(info.cards, actionId);
				await endAction(actionId, true);
				break;
			case "cardbook.createContact": {
				let newCard = new cardbookCardParser();
				newCard.dirPrefId = info.dirPrefId;
				await openEditionWindow(newCard, "CreateContact");
				break;
			}
			case "cardbook.createList": {
				let newCard = new cardbookCardParser();
				newCard.dirPrefId = info.dirPrefId;
				newCard.isAList = true;
				await openEditionWindow(newCard, "CreateList");
				break;
			}
			case "cardbook.editOrViewContact": {
				let outCard = new cardbookCardParser();
				await cardbookBGUtils.cloneCard(info.card, outCard);
				let type = outCard.isAList ? "List" : "Contact";
				let mode = cardbookBGPreferences.getReadOnly(info.card.dirPrefId) ? `View${type}` : `Edit${type}`;
				await openEditionWindow(info.card, mode);
				break;
			}
			case "cardbook.addEmailToCardBook": {
				let card = new cardbookCardParser("", "", "", info.dirPrefId);
				await openEditionWindow(card, "AddEmail", null, info.addEmail);
				break;
			}
			case "cardbook.writePossibleCustomFields":
				cardbookBGRepo.writePossibleCustomFields(info.possibleCustomFields);
				break;
			case "cardbook.getPossibleCustomFields":
				return cardbookBGRepo.possibleCustomFields;
			case "cardbook.setCardbookServerCardSyncTotal":
				cardbookBGRepo.cardbookServerCardSyncTotal[info.dirPrefId] = info.count;
				break;
			case "cardbook.getCardbookCardParser":
				return new cardbookCardParser(info.data);
			case "cardbook.importCard":{
				await cardbookBGRepo.saveCardFromUpdate({}, info.card, "", true);
				cardbookBGRepo.cardbookServerCardSyncDone[info.dirPrefId]++;
				break;
			}
			case "cardbook.importCardsError":{
				cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, info.field + " error : " + info.message, "Error");
				cardbookBGRepo.cardbookServerCardSyncError[info.dirPrefId]++;
				cardbookBGRepo.cardbookServerCardSyncDone[info.dirPrefId]++;
				break;
			}
			case "cardbook.addMailPop": {
				cardbookIDBMailPop.updateMailPop(cardbookBGRepo, info.email, info.value);
			}
			case "cardbook.removePrefDispName":
				cardbookIDBPrefDispName.removePrefDispName(cardbookBGRepo, info.email);
				break;
			case "cardbook.addPrefDispName":
				cardbookIDBPrefDispName.addPrefDispName(cardbookBGRepo, { email: info.email });
				break;
			case "cardbook.isEmailRegistered":
				return cardbookBGRepo.isEmailRegistered(info.email, info.identity);
			case "cardbook.doesListExist": {
				for (let j in cardbookBGRepo.cardbookCards) {
					let card = cardbookBGRepo.cardbookCards[j];
					if (card.isAList && card.fn == info.name) {
						return true;
					}
				}
				return false;
			}
			case "cardbook.filters.filtersInitialized": {
				if (info.value) {
					cardbookBGRepo.filtersInitialized = info.value;
				} else {
					return cardbookBGRepo.filtersInitialized;
				}
				break;
			}
			case "cardbook.filters.addCards": {
				let category = "";
				let sepPosition = info.actionValue.indexOf("::",0);
				if (sepPosition != -1) {
					category = info.actionValue.substr(sepPosition+2, info.actionValue.length);
					info.actionValue = info.actionValue.substr(0, sepPosition);
				}
				let actionId = cardbookBGActions.startAction(cardbookBGRepo, "emailCollectedByFilter");
				for (let address of info.emails) {
					await cardbookBGRepo.addCardFromDisplayAndEmail(info.actionValue, address.name, address.email, category, actionId);
				}
				await endAction(actionId, true);
				break;
			}
			case "cardbook.filters.deleteCards": {
				let category = "";
				let sepPosition = info.actionValue.indexOf("::",0);
				if (sepPosition != -1) {
					category = info.actionValue.substr(sepPosition+2, info.actionValue.length);
					info.actionValue = info.actionValue.substr(0, sepPosition);
				}
				let actionId = cardbookBGActions.startAction(cardbookBGRepo, "emailDeletedByFilter");
				for (let address of info.emails) {
					let email = address.email.toLowerCase();
					if (cardbookBGRepo.cardbookCardEmails[info.actionValue]) {
						if (cardbookBGRepo.cardbookCardEmails[info.actionValue][email]) {
							for (let k = 0; k < cardbookBGRepo.cardbookCardEmails[info.actionValue][email].length; k++) {
								let card = cardbookBGRepo.cardbookCardEmails[info.actionValue][email][k];
								if (category != "") {
									let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
									if (category == uncat) {
										if (card.categories == "") {
											await cardbookBGRepo.asyncDeleteCards([card], actionId);
										}
									} else {
										if (card.categories.includes(category)) {
											await cardbookBGRepo.asyncDeleteCards([card], actionId);
										}
									}
								} else if (category == "") {
									await cardbookBGRepo.asyncDeleteCards([card], actionId);
								}
							}
						}
					}
				}
				await endAction(actionId, true);
				break;
			}
			case "cardbook.getAllCustomFields":
				return cardbookBGPreferences.getAllCustomFields();
			case "cardbook.getAllColumns":
				return cardbookHTMLUtils.allColumns;
			case "cardbook.getDateFields":
				return cardbookHTMLDates.dateFields;
			case "cardbook.getMultilineFields":
				return cardbookHTMLUtils.multilineFields;
			case "cardbook.getCard":
				return cardbookBGRepo.cardbookCards[info.cbid];
			case "cardbook.getAccounts":
				return cardbookBGRepo.cardbookAccounts;
			case "cardbook.getAccountsCategories":
				return cardbookBGRepo.cardbookAccountsCategories[info.dirPrefId] || [];
			case "cardbook.getCardbookNodeColors":
				return cardbookBGPreferences.getNodeColors();
			case "cardbook.getMimeEmailsFromCardsAndLists":
				return cardbookBGUtils.getMimeEmailsFromCardsAndLists(cardbookBGRepo, info.listOfCards, info.identity, info.useOnlyEmail);
			case "cardbook.getListConversion":
				return new cardbookListConversion(cardbookBGRepo, info.email, info.identity, true);
			case "cardbook.getPopularity":
				if (cardbookBGRepo.cardbookMailPopularityIndex[info.email]) {
					return cardbookBGRepo.cardbookMailPopularityIndex[info.email].count;
				} else {
					return 0;
				}
			case "cardbook.getTypesFromDirPrefId":
				return cardbookHTMLTypes.getTypesFromDirPrefId(info.type, info.dirPrefId);
			case "cardbook.getIMPPCode":
				return cardbookHTMLTypes.getIMPPCode(info.types);
			case "cardbook.getIMPPProtocol":
				return cardbookHTMLTypes.getIMPPProtocol(info.value);
			case "cardbook.getOnlyTypesFromTypes":
				return cardbookHTMLUtils.getOnlyTypesFromTypes(info.types);
			case "cardbook.getIMPPLineForCode":
				return cardbookHTMLTypes.getIMPPLineForCode(info.protocol);
			case "cardbook.getIMPPLineForProtocol":
				return cardbookHTMLTypes.getIMPPLineForProtocol(info.protocol);
			case "cardbook.whichLabelTypeShouldBeChecked":
				return cardbookHTMLTypes.whichLabelTypeShouldBeChecked(info.type, info.dirPrefId, info.types);
			case "cardbook.whichCodeTypeShouldBeChecked":
				return cardbookHTMLTypes.whichCodeTypeShouldBeChecked(info.type, info.dirPrefId, info.inputTypes, info.sourceTypes);
			case "cardbook.getFormattedDateForDateString":
				return cardbookHTMLDates.getFormattedDateForDateString(info.dateString, info.target);
			case "cardbook.getPrefBooleanFromTypes":
				return cardbookHTMLUtils.getPrefBooleanFromTypes(info.types);
			case "cardbook.getEventsFromCard":
				return cardbookHTMLUtils.getEventsFromCard(info.cardNote, info.cardOthers);
			case "cardbook.getCardValueByField":
				return cardbookHTMLUtils.getCardValueByField(info.card, info.column, info.includePref);
			case "cardbook.getCardValueByField":
				return cardbookHTMLUtils.formatAddress(info.address, info.formula);
			case "cardbook.getDisplayCards":
				return cardbookBGRepo.cardbookDisplayCards[info.accountId].cards;
			case "cardbook.isMyAccountRemote": {
				let type = cardbookBGPreferences.getType(info.dirPrefId);
				return cardbookBGUtils.isMyAccountRemote(type);
			}
			case "cardbook.isMyAccountSyncing":
				if (cardbookBGRepo.cardbookSyncMode[info.dirPrefId] && cardbookBGRepo.cardbookSyncMode[info.dirPrefId] == 1) {
					return true;
				}
				return false;
			case "cardbook.openCBWindow": {
				openCBWindow();
				break;
			}
			case "cardbook.addAddressbook": {
				await cardbookBGRepo.addAddressbook(info.action, info.dirPrefId, info.accountsToAdd);
				break;
			}
			case "cardbook.getLongSearchString":
				return cardbookBGUtils.getLongSearchString(info.card);
			case "cardbook.autocomplete.getCards": {
				let search = cardbookBGPreferences.getPref("autocompleteRestrictSearch");
				if (search == true) {
					return cardbookBGRepo.cardbookCardShortSearch[info.dirPrefId];
				} else {
					return cardbookBGRepo.cardbookCardLongSearch[info.dirPrefId];
				}
			}
			case "cardbook.contactssidebar.getCards": {
				return cardbookBGRepo.cardbookCardLongSearch[info.dirPrefId];
			}
			case "cardbook.contactssidebar.searchRemote":
				await cardbookBGSynchronization.searchRemote(cardbookBGRepo, info.dirPrefId, info.value);
				break;
			case "cardbook.contactssidebar.setLastSearch":
				await cardbookBGPreferences.setLastSearch(info.dirPrefId, info.value);
				break;
			case "cardbook.removeNode": {
				await removeNode(info.node);
				break;
			}
			case "cardbook.renameNode": {
				await renameNode(info.node);
				break;
			}
			case "cardbook.formatStringForOutput":
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, info.string, info.values, info.error);
				break;
			case "cardbook.syncBirthdays.sendResults":
				await messenger.runtime.sendMessage({query: info.query, results: info.results});
				break;
			case "cardbook.getCardBookDisplayNameFromEmail": 
				return cardbookBGColumns.getCardBookDisplayNameFromEmail(info.email, info.defaultDisplay, cardbookBGRepo);
			case "cardbook.CardBookXULNotifyListenerReady": {
				if (cardbookBGRepo.firstLoad) {
					return;
				}
				await cardbookBGInit.startup(cardbookBGRepo);
				cardbookBGRepo.firstLoad = true;

				let spaceButtonLabel = messenger.i18n.getMessage("cardbookTitle");
				let spaceButtonURL = "chrome/content/HTML/mainWindow/wdw_mainWindow.html";
				let spaceButtonProp = { "defaultIcons": "chrome/content/skin/cardbook_16_16.svg" };
				let CBspace = await browser.spaces.create(spaceButtonLabel, spaceButtonURL, spaceButtonProp);
				CBspace.key = "CardBookKey";
			
				let firstRun = cardbookBGPreferences.getPref("firstRun");
				if (firstRun) {
					await cardbookBGRepo.addAddressbook("first");
					await cardbookBGPreferences.setPref("firstRun", false);
				}
				break;
			}
			case "cardbook.openWindow":
				let url = browser.runtime.getURL(info.url) + "*";
				let tabs = await browser.tabs.query({url});
				if (tabs.length) {
					await browser.windows.update(tabs[0].windowId, {focused: true});
					return tabs[0].windowId;
				} else {
					let state = {};
					try {
						let winPath = info.url.split("?")[0];
						let winName = winPath.split("/").pop();
						let prefName = `window.${winName}.state`;
						state = cardbookBGPreferences.getPref(prefName);
					} catch(e) {}
					let winParams = { ...state, type: info.type, url: info.url, allowScriptsToClose: true};
					let win = await browser.windows.create(winParams);
					return win.id;
				}
			case "cardbook.openTab": {
				let url = browser.runtime.getURL(info.url);
				let tabs = await browser.tabs.query({url});
				if (tabs.length) {
					await browser.tabs.update(tabs[0].id, {active: true});
					return tabs[0].id;
				} else {
					let tabParams = {url: url};
					let tab = await browser.tabs.create(tabParams);
					return tab.id;
				}
			}
		}
	});

	async function onMessageListenerFunction(info) {
		switch (info.query) {
			case "cardbook.clipboardSetImage":
				// info.type is png, jpg, etc...
				// setImageData only accepts png and jpeg
				let imageType = (info.type.toLowerCase() == "jpg") ? "jpeg" : info.type.toLowerCase();
				const byteCharacters = atob(info.b64);
				const byteNumbers = new Uint8Array(byteCharacters.length);
				for (let i = 0; i < byteCharacters.length; i++) {
					byteNumbers[i] = byteCharacters.charCodeAt(i);
				}
				await browser.clipboard.setImageData(byteNumbers.buffer, imageType);
				// does not work
				// const byteCharacters = atob(info.b64);
				// const byteNumbers = new Array(byteCharacters.length);
				// for (let i = 0; i < byteCharacters.length; i++) {
				// 	byteNumbers[i] = byteCharacters.charCodeAt(i);
				// }
				// const byteArray = new Uint8Array(byteNumbers);
				// const blob = new Blob([byteArray], {type: info.type});
				// let data = [new ClipboardItem({ [info.type]: blob })];
				// await navigator.clipboard.write(data);
				break;

			case "cardbook.pref.setPref":
				await cardbookBGPreferences.setPref(info.key, info.state);
				break;
			case "cardbook.openExternalURL":
				await messenger.NotifyTools.notifyExperiment({query: info.query, link: info.link});
				break;
			case "cardbook.emailCards":
				let tab2 = await messenger.compose.beginNew();
				let composeDetails = {};
				for (let compField of info.compFields) {
					composeDetails[compField.field] = compField.value;
				}
				messenger.compose.setComposeDetails(tab2.id, composeDetails);
				break;
			case "cardbook.findEmails":
				await messenger.NotifyTools.notifyExperiment({query: info.query, card: info.card, email: info.email});
				break;
			case "cardbook.searchForThKeyEdit": {
				let key = await messenger.NotifyTools.notifyExperiment({query: info.query, card: info.card, email: info.email});
				return key;
			}
			case "cardbook.connectIMPP": {
				let card = cardbookBGRepo.cardbookCards[info.cbid];
				let imppRow = card.impp[info.index];
				let serviceCode = cardbookHTMLTypes.getIMPPCode(imppRow[1]);
				let serviceProtocol = cardbookHTMLTypes.getIMPPProtocol(imppRow[0]);
				if (serviceCode != "") {
					let serviceLine = [];
					serviceLine = cardbookHTMLTypes.getIMPPLineForCode(serviceCode)
					if (serviceLine[0]) {
						let regExp = new RegExp("^" + serviceLine[2] + ":");
						let address = imppRow[0][0].replace(regExp, "");
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.openExternalURL", link: cardbookHTMLUtils.formatIMPPForOpenning(serviceLine[2] + ":" + address)})
					}
				} else if (serviceProtocol != "") {
					let serviceLine = [];
					serviceLine = cardbookHTMLTypes.getIMPPLineForProtocol(serviceProtocol)
					if (serviceLine[0]) {
						let regExp = new RegExp("^" + serviceLine[2] + ":");
						let address = imppRow[0][0].replace(regExp, "");
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.openExternalURL", link: cardbookHTMLUtils.formatIMPPForOpenning(serviceLine[2] + ":" + address)})
					}
				}
				break;
			}
			case "cardbook.connectTel": {
				let telProtocol = "callto";
				let telProtocolLine = cardbookBGPreferences.getPref("tels.0");
				if (telProtocolLine) {
					let telProtocolLineArray = telProtocolLine.split(':');
					if (telProtocolLineArray[2]) {
						telProtocol = telProtocolLineArray[2];
					}
				}
				let value = cardbookHTMLUtils.formatTelForOpenning(info.tel);
				if (telProtocol != "url") {
					let result = `${telProtocol}:${value}`;
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.openExternalURL", link: result})
				} else {
					let url = cardbookBGPreferences.getPref("URLPhoneURL").replace("{{1}}", value).replace("$1", value);
					let background = cardbookBGPreferences.getPref("URLPhoneBackground");
					if (background) {
						let user = cardbookBGPreferences.getPref("URLPhoneUser");
						let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: user, url: url});
						let req = new cardbookFetchParser(user, password);
						let request = req.fetch(url, {
							method: "GET",
							signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
						});
					} else {
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.openExternalURL", link: url})
					}
				}
				break;
			}
			case "cardbook.pref.preferencesChanged":
				await messenger.NotifyTools.notifyExperiment({query: "cardbook.notifyObserver", value: info.query});
				break;
			case "cardbook.notifyObserver":
				await messenger.NotifyTools.notifyExperiment({query: info.query, value: info.value, params: info.params});
				break;
			case "cardbook.callDirPicker":
				return messenger.NotifyTools.notifyExperiment({query: info.query, id: info.id, title: info.title});
			case "cardbook.callFilePicker":
				return messenger.NotifyTools.notifyExperiment({query: info.query, id: info.id, result: info.result, title: info.title, mode: info.mode, type: info.type, defaultFileName: info.defaultFileName, 
								defaultDir: info.defaultDir, content: info.content, utf8: info.utf8});
			case "cardbook.getSystemPref":
				let pref = await messenger.NotifyTools.notifyExperiment({query: info.query, type: info.type, value: info.value});
				return pref;
			case "cardbook.getDensity":
				let density = await messenger.NotifyTools.notifyExperiment({query: info.query});
				return density;
			case "cardbook.getCollectedStandardAB":
				let collectedAB = await messenger.NotifyTools.notifyExperiment({query: info.query});
				return collectedAB;
			case "cardbook.getLDAPStandardAB":
				let localAB = await messenger.NotifyTools.notifyExperiment({query: info.query});
				return localAB;
			case "cardbook.getRemoteStandardAB":
				let remoteAB = await messenger.NotifyTools.notifyExperiment({query: info.query});
				return remoteAB;
			case "cardbook.getPassword":
				let url = info.dirPrefId ? cardbookBGPreferences.getUrl(info.dirPrefId) : info.url;
				let pwd = await messenger.NotifyTools.notifyExperiment({query: info.query, user: info.user, url: url});
				return pwd;
			case "cardbook.getDomainPassword":
				let domain = await messenger.NotifyTools.notifyExperiment({query: info.query, domain: info.domain});
				return domain;
			case "cardbook.removePassword":
				await messenger.NotifyTools.notifyExperiment({query: info.query, user: info.user, url: info.url});
				break;
			case "cardbook.rememberPassword":
				await messenger.NotifyTools.notifyExperiment({query: info.query, user: info.user, url: info.url, pwd: info.pwd, save: info.save});
				break;
			case "cardbook.getTimezoneIds":
				let timezoneIds = await messenger.NotifyTools.notifyExperiment({query: info.query});
				return timezoneIds;
			case "cardbook.getEvents":
				let events = await messenger.NotifyTools.notifyExperiment({query: info.query, emails: info.emails, column: info.column, order: info.order});
				return events;
			case "cardbook.editEvent":
				await messenger.NotifyTools.notifyExperiment({query: info.query, eventId: info.eventId, calendarId: info.calendarId});
				break;
			case "cardbook.createEvent":
				await messenger.NotifyTools.notifyExperiment({query: info.query, contacts: info.contacts});
				break;
			case "cardbook.createToDo":
				await messenger.NotifyTools.notifyExperiment({query: info.query, title: info.title, description: info.description});
				break;
			case "cardbook.getAllCalendars":
				let calendars = await messenger.NotifyTools.notifyExperiment({query: info.query});
				return calendars;
			case "cardbook.getLeafName":
				return await messenger.NotifyTools.notifyExperiment({query: info.query, url: info.url});
			case "cardbook.sharevCards":
				let tab1 = await messenger.compose.beginNew();
				for (let vCard of info.vCards) {
					let blob = new Blob([vCard.vCard], {type: "text;charset=utf-8"});
					let file = new File([blob], vCard.filename);
					await messenger.compose.addAttachment(tab1.id, {file: file, name: vCard.filename});
				}
				break;
			case "cardbook.emptyUnCachedAccountFromRepository": {
				let name = cardbookBGPreferences.getName(info.dirPrefId);
				await cardbookBGRepo.emptyUnCachedAccountFromRepository(info.dirPrefId, name);
			}
			case "cardbook.getMimeEmailsFromCardsAndLists":
				return cardbookBGUtils.getMimeEmailsFromCardsAndLists(cardbookBGRepo, info.listOfCards, info.identity, info.useOnlyEmail);
			case "cardbook.getWrongBirthdayCalendars":
				return await messenger.NotifyTools.notifyExperiment({query: info.query, calendars: info.calendars});
			case "cardbook.getImageFromURI":
				return await messenger.NotifyTools.notifyExperiment({query: info.query, URI: info.URI});
			case "cardbook.getCardsFromFileContent":
				return cardbookBGFiles.getCardsFromFileContent(cardbookBGRepo, info.content, null, info.dirPrefId);
			case "cardbook.getCardsWithSearch":
				let searchString = cardbookBGUtils.makeSearchString(info.search);
				let cards = [];
				if (!cardbookBGRepo.cardbookDisplayCards[info.displayId]) {
					return [ cards, info.searchId ];
				}
				if (searchString) {
					for (let card of cardbookBGRepo.cardbookDisplayCards[info.displayId].cards) {
						if (cardbookBGUtils.getLongSearchString(card).indexOf(searchString) >= 0) {
							cards.push(card);
						}
					}
				} else {
					for (let card of cardbookBGRepo.cardbookDisplayCards[info.displayId].cards) {
						cards.push(card);
					}
				}
				return [ cards, info.searchId ];
			case "cardbook.dragCards":
				let nodeArray = cardbookHTMLUtils.escapeStringSemiColon(info.target).split("::");
				let dirPrefId = nodeArray[0];
				if (!dirPrefId) {
					return;
				}	
				let nodeType = nodeArray[1];
				let nodeName = nodeArray[nodeArray.length-1];
				nodeArray.shift();
				nodeArray.shift();
				let orgNode = JSON.parse(JSON.stringify(nodeArray));
				let version = cardbookBGPreferences.getVCardVersion(dirPrefId);
				let dateFormat = cardbookHTMLUtils.getDateFormat(dirPrefId, version);

				let ids = info.ids.split(" , ");
				let choice = cardbookBGRepo.importConflictChoiceImportValues.join("::");
				if (!cardbookBGRepo.importConflictChoice[info.actionId]) {
					cardbookBGRepo.importConflictChoice[info.actionId] = {};
				}
				if (!cardbookBGRepo.importConflictChoice[info.actionId][choice]) {
					cardbookBGRepo.importConflictChoice[info.actionId][choice] = {};
				}
				for (let id of ids) {
					let card = cardbookBGRepo.cardbookCards[id];
					if (dirPrefId == card.dirPrefId) {
						if (nodeType == "categories" && card.categories.includes(nodeName)) {
							continue;
						} else if (nodeType == "org" && orgNode.join("::") == card.org.join("::")) {
							continue;
						} else if (!cardbookHTMLUtils.possibleNodes.includes(nodeType)) {
							continue;
						} else {
							cardbookBGRepo.importConflictChoice[info.actionId][choice].result = "update";
						}
					}
					let sourceDateFormat = cardbookHTMLUtils.getDateFormat(card.dirPrefId, card.version);
					await importCard(card, info.target, version, sourceDateFormat, dateFormat, info.actionId);
					if (dirPrefId != card.dirPrefId &&
						cardbookBGRepo.importConflictChoice[info.actionId] &&
						cardbookBGRepo.importConflictChoice[info.actionId][choice] &&
						cardbookBGRepo.importConflictChoice[info.actionId][choice].result != "cancel") {
						if (!info.ctrlKey) {
							cardbookBGRepo.currentAction[info.actionId].totalCards++;
							await cardbookBGRepo.asyncDeleteCards([card], info.actionId);
						}
					}
				}
				break;
			case "cardbook.searchRemote":
				await cardbookBGSynchronization.searchRemote(cardbookBGRepo, info.dirPrefId, info.value);
				break;
			case "cardbook.validateWithoutDiscovery":
				await cardbookBGSynchronizationCARDDAV.validateWithoutDiscovery(cardbookBGRepo, info.connection, info.type, info.params);
				break;
			case "cardbook.discoverPhase1":
				await cardbookBGSynchronizationCARDDAV.discoverPhase1(cardbookBGRepo, info.connection, info.type, info.params);
				break;
			case "cardbook.initDiscoveryOperations":
				cardbookBGSynchronization.initDiscoveryOperations(cardbookBGRepo, info.dirPrefId);
				break;
			case "cardbook.stopDiscoveryOperations":
				cardbookBGSynchronization.stopDiscoveryOperations(cardbookBGRepo, info.dirPrefId);
				break;
			case "cardbook.removePeriodicSync":
				cardbookBGSynchronization.removePeriodicSync(cardbookBGRepo, info.dirPrefId, info.name);
				break;
			case "cardbook.addPeriodicSync":
				cardbookBGSynchronization.addPeriodicSync(cardbookBGRepo, info.dirPrefId, info.name, info.interval);
				break;
			case "cardbook.askPassword":
				if (cardbookBGRepo.askPasswordChoice[info.username] && cardbookBGRepo.askPasswordChoice[info.username][info.url]) {
					cardbookBGRepo.askPasswordChoice[info.username][info.url] = info.password;
				}
				if (info.save) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.rememberPassword", user: info.username, url: info.url, pwd: info.password, save: info.save});
				}
				break;
			case "cardbook.syncAccount":
				cardbookBGSynchronization.syncAccount(cardbookBGRepo, info.dirPrefId);
				break;
			case "cardbook.getNewRefreshTokenForGooglePeople":
				await cardbookBGSynchronizationGoogle2.getNewRefreshTokenForGooglePeople(cardbookBGRepo, info.connection, info.callback, info.followAction);
				break;
			case "cardbook.updateStatusProgressInformation":
				cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, info.string, info.error);
				break;
			case "cardbook.getABs": {
				let sortedAddressBooks = [];
				for (let account of cardbookBGRepo.cardbookAccounts) {
					if ((info.includeDisabled || account[2])
							&& (info.includeReadOnly || !account[4])
							&& (info.includeSearch || (account[3] !== "SEARCH"))) {
						if (info.exclRestrictionList && info.exclRestrictionList[account[1]]) {
							continue;
						}
						if (info.inclRestrictionList && info.inclRestrictionList.length > 0) {
							if (info.inclRestrictionList[account[1]]) {
								sortedAddressBooks.push([account[0], account[1], cardbookBGUtils.getABIconType(account[3])]);
							}
						} else {
							sortedAddressBooks.push([account[0], account[1], cardbookBGUtils.getABIconType(account[3])]);
						}
					}
				}
				if (!info.exclusive) {
					for (let addrbook of MailServices.ab.directories) {
						// remote LDAP directory
						if (addrbook.isRemote && addrbook.dirType === 0) {
							continue;
						}
						if (info.inclRestrictionList && info.inclRestrictionList.length > 0) {
							if (info.inclRestrictionList[addrbook.dirPrefId]) {
								sortedAddressBooks.push([addrbook.dirName, addrbook.dirPrefId, "standard-abook"]);
							}
						} else {
							sortedAddressBooks.push([addrbook.dirName, addrbook.dirPrefId, "standard-abook"]);
						}
					}
				}
				cardbookHTMLUtils.sortMultipleArrayByString(sortedAddressBooks, 0, 1);
				return sortedAddressBooks;
			}
			case "cardbook.addAddressbook": {
				await cardbookBGRepo.addAddressbook(info.action, info.dirPrefId, info.accountsToAdd);
				break;
			}
			case "cardbook.removeAddressbook": {
				await cardbookBGRepo.removeAddressbook(info.dirPrefId, info.winId);
				break;
			}
			case "cardbook.printCards": {
				await cardbookBGRepo.notifyObservers(info.query, info.listOfCards);
				break;
			}
			case "cardbook.decryptDBs":
				await cardbookIndexedDB.decryptDBs(cardbookBGRepo);
				await messenger.runtime.sendMessage({query: "cardbook.conf.finishProgressBar", type: "crypto"});
				break;
			case "cardbook.encryptDBs":
				await cardbookIndexedDB.encryptDBs(cardbookBGRepo);
				await messenger.runtime.sendMessage({query: "cardbook.conf.finishProgressBar", type: "crypto"});
				break;
			case "cardbook.finishCSV": {
				await cardbookHTMLUtils.saveWindowSize(info.winName, info.winState);
				cardbookBGRepo.currentAction[info.actionId] = null;
				break;
			}
			case "cardbook.prepareCSVImport": {
				cardbookBGRepo.currentAction[info.actionId]["mode"] = "import";
				cardbookBGRepo.currentAction[info.actionId]["status"] = "STARTED";
				cardbookBGRepo.currentAction[info.actionId]["target"] = info.target;
				cardbookBGRepo.currentAction[info.actionId]["content"] = info.content;
				break;
			}
			case "cardbook.importCSVFile": {
				let content = cardbookBGRepo.currentAction[info.params.actionId].content;
				let target = cardbookBGRepo.currentAction[info.params.actionId].target;
				let dirPrefId = cardbookBGUtils.getAccountId(target);
				let targetName = cardbookBGPreferences.getName(dirPrefId);
				let version = cardbookBGPreferences.getVCardVersion(dirPrefId);
				let dateFormat = cardbookHTMLUtils.getDateFormat(dirPrefId, version);

				let parser = d3.dsvFormat(info.params.columnSeparator);
				let result = parser.parseRows(content)
				let start = 0;
				if (info.params.lineHeader) {
					start = 1;
				}
				let fields = info.params.fields.split("|");
				for (let i = start; i < result.length; i++) {
					let card = new cardbookCardParser();
					try {
						card.dirPrefId = dirPrefId;
						for (var j = 0; j < result[i].length; j++) {
							if (fields[j]) {
								cardbookHTMLUtils.setCardValueByField(card, fields[j], result[i][j]);
							}
						}
						card.version = version;
						if (card.fn == "") {
							cardbookHTMLFormulas.getDisplayedName(card, card.dirPrefId,
																[card.prefixname, card.firstname, card.othername, card.lastname, card.suffixname, card.nickname],
																[card.org, card.title, card.role]);
						}
					}
					catch (e) {
						if (e.message == "") {
							cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "parsingCardError", [targetName, messenger.i18n.getMessage(e.code), result[i]], "Error");
						} else {
							cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "parsingCardError", [targetName, e.message, result[i]], "Error");
						}
						continue;
					}
					await importCard(card, target, version, dateFormat, dateFormat, info.params.actionId);
				}
				await endAction(info.params.actionId, null, {name: info.params.filename, length: cardbookBGRepo.currentAction[info.params.actionId].doneCards, targetName: targetName});
				cardbookBGSynchronization.finishMultipleOperations(cardbookBGRepo, dirPrefId);
				break;
			}
			case "cardbook.setUndoAndRedoLabel": {
				setUndoAndRedoLabel();
				break;
			}
			case "cardbook.undo": {
				let actionId = await cardbookIDBUndo.executeUndoItem(cardbookBGRepo);
				if (actionId) {
					await endAction(actionId, true);
				}
				break;
			}
			case "cardbook.redo": {
				let actionId = await cardbookIDBUndo.executeRedoItem(cardbookBGRepo);
				if (actionId) {
					await endAction(actionId, true);
				}
				break;
			}
			case "cardbook.importCardsFromDir": {
				cardbookBGRepo.importConflictChoice[info.actionId] = {};
				cardbookBGRepo.currentAction[info.actionId]["mode"] = "import";
				cardbookBGRepo.currentAction[info.actionId]["status"] = "STARTED";
				cardbookBGRepo.currentAction[info.actionId]["params"] = {};
				let cards = await cardbookBGFiles.getCardsFromDir(cardbookBGRepo, info.url, info.dirPrefId);
				cardbookBGRepo.currentAction[info.actionId].totalEstimatedCards = cards.length;
				for (let card of cards) {
					let version = cardbookBGPreferences.getVCardVersion(info.dirPrefId);
					let dateFormat = cardbookHTMLUtils.getDateFormat(info.dirPrefId, version);
					let sourceDateFormat = cardbookHTMLUtils.getDateFormat(card.dirPrefId, card.version);
					let defaultChoice = "";
					await importCard(card, info.target, version, sourceDateFormat, dateFormat,
						info.actionId, defaultChoice);
				}
				cardbookBGRepo.currentAction[info.actionId]["status"] = "FINISHED";
				let targetName = cardbookBGPreferences.getName(info.dirPrefId);
				await endAction(info.actionId, null, {name: info.directoryname, length: cardbookBGRepo.currentAction[info.actionId].doneCards, targetName: targetName});
				cardbookBGSynchronization.finishMultipleOperations(cardbookBGRepo, info.dirPrefId);
				break;
			}
			case "cardbook.importCardsFromFile": {
				cardbookBGRepo.importConflictChoice[info.actionId] = {};
				cardbookBGRepo.currentAction[info.actionId]["mode"] = "import";
				cardbookBGRepo.currentAction[info.actionId]["status"] = "STARTED";
				cardbookBGRepo.currentAction[info.actionId]["params"] = {};
				let cards = cardbookBGFiles.getCardsFromFileContent(cardbookBGRepo, info.content, null, info.dirPrefId);
				cardbookBGRepo.currentAction[info.actionId].totalEstimatedCards = cards.length;
				for (let card of cards) {
					let version = cardbookBGPreferences.getVCardVersion(info.dirPrefId);
					let dateFormat = cardbookHTMLUtils.getDateFormat(info.dirPrefId, version);
					let sourceDateFormat = cardbookHTMLUtils.getDateFormat(card.dirPrefId, card.version);
					let defaultChoice = "";
					await importCard(card, info.target, version, sourceDateFormat, dateFormat,
						info.actionId, defaultChoice);
				}
				cardbookBGRepo.currentAction[info.actionId]["status"] = "FINISHED";
				let targetName = cardbookBGPreferences.getName(info.dirPrefId);
				await endAction(info.actionId, null, {name: info.filename, length: cardbookBGRepo.currentAction[info.actionId].doneCards, targetName: targetName});
				cardbookBGSynchronization.finishMultipleOperations(cardbookBGRepo, info.dirPrefId);
				break;
			}
			case "cardbook.generateFn": {
				let dirPrefId = cardbookBGUtils.getAccountId(info.accountId);
				let name = cardbookBGPreferences.getName(dirPrefId);
				let cards = JSON.parse(JSON.stringify(cardbookBGRepo.cardbookDisplayCards[info.accountId].cards));
				let counter = 0;
				for (let card of cards) {
					let outCard = new cardbookCardParser();
					await cardbookBGUtils.cloneCard(card, outCard);
					let fn = outCard.fn;
					cardbookHTMLFormulas.getDisplayedName(outCard, outCard.dirPrefId,
														[outCard.prefixname, outCard.firstname, outCard.othername, outCard.lastname, outCard.suffixname, outCard.nickname],
														[outCard.org, outCard.title, outCard.role]);
					if (fn != outCard.fn && outCard.fn != "") {
						await cardbookBGRepo.saveCardFromUpdate(card, outCard, info.actionId, false);
						counter++;
					}
				}
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "displayNameGenerated", [name, counter]);
				break;
			}
			case "cardbook.modifyNode": {
				await modifyNode(info.node);
				break;
			}
			case "cardbook.createCategory": {
				await createCategory(info.category);
				break;
			}
			case "cardbook.closeAddressbooksEdit": {
				cardbookBGSynchronization.finishMultipleOperations(cardbookBGRepo, info.dirPrefId);
				await cardbookBGRepo.notifyObservers("cardbook.syncFinished", info.dirPrefId);
				await cardbookHTMLUtils.saveWindowSize(info.winName, info.winState);
				break;
			}
			case "cardbook.createAddressbook": {
				await createAddressbook(info.account);
				break;
			}
			case "cardbook.modifySearchAddressbook": {
				await modifySearchAddressbook(info.account);
				break;
			}
			case "cardbook.modifyAddressbook": {
				await modifyAddressbook(info.account);
				break;
			}
			case "cardbook.updateCurrentAction": {
				if (cardbookBGRepo.currentAction[info.actionId]) {
					cardbookBGRepo.currentAction[info.actionId].doneCards = info.doneCards;
				}
				break;
			}
			case "cardbook.notifyObservers": {
				await cardbookBGRepo.notifyObservers(info.topic, info.params);
				break;
			}
			case "cardbook.finishMultipleOperations":
				cardbookBGSynchronization.finishMultipleOperations(cardbookBGRepo, info.dirPrefId);
				await cardbookBGRepo.notifyObservers("cardbook.syncFinished", info.dirPrefId);
				break;
			case "cardbook.alertUser": {
				if (info.action == "removeAddressbook") {
					if (info.response === true) {
						let name = cardbookBGPreferences.getName(info.lineId);
						let type = cardbookBGPreferences.getType(info.lineId);
						let url = cardbookBGPreferences.getUrl(info.lineId);
						if (type !== "SEARCH") {
							cardbookBGSynchronization.removePeriodicSync(cardbookBGRepo, info.lineId, name);
							await cardbookBGRepo.removeAccountFromComplexSearch(info.lineId, true);
							await cardbookBGRepo.removeAccountFromRepository(info.lineId);
						} else {
							await cardbookBGRepo.removeComplexSearchFromRepository(info.lineId);
							await cardbookIDBSearch.removeSearch(cardbookBGRepo.statusInformation, info.lineId);
						}
						await cardbookIndexedDB.removeAccount(cardbookBGRepo.statusInformation, info.lineId, name);
						cardbookBGPreferences.delAccount(info.lineId);
						cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "addressbookDeleted", [name]);
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.addActivity", code: "addressbookDeleted", array: [name], icon: "deleteMail"});
						await cardbookBGRepo.notifyObservers("addressbookDeleted", info.lineId);
						if (info.checkboxAction == true) {
							cardbookBGLog.updateStatusProgressInformationWithDebug2(cardbookBGRepo.statusInformation, "debug mode : deleting : " + url);
							await messenger.NotifyTools.notifyExperiment({query: "cardbook.deleteFile", url: url});
						}
					}
					cardbookBGSynchronization.finishMultipleOperations(cardbookBGRepo, info.lineId);
					await cardbookBGRepo.notifyObservers("cardbook.syncFinished", info.lineId);
				} else if (info.winId == "background" && info.action == "deleteCard" && info.response === true) {
					return new Promise(async (resolve, reject) => {
						let listOfSelectedId = info.lineId.split(",");
						let cards = [];
						for (let id of listOfSelectedId) {
							cards.push(cardbookBGRepo.cardbookCards[id]);
						}
						let actionId = cardbookBGActions.startAction(cardbookBGRepo, "cardsDeleted", cards, null, cards.length, cards.length);
						await cardbookBGRepo.deleteCards(cards, actionId);
						await endAction(actionId, true);
						resolve();
					});
				} else if (info.action == "removeNode" && info.response === true) {
					let tmpArray = info.lineId.split("::");
					let nodeId = info.lineId;
					let dirPrefId = tmpArray[0];
					let nodeType = tmpArray[1];
					let nodeName = tmpArray[2];

					let cards = JSON.parse(JSON.stringify(cardbookBGRepo.cardbookDisplayCards[nodeId].cards));
					let topic = (nodeType != "categories") ? "nodeDeleted" : "categoryDeleted";
					let actionId = cardbookBGActions.startAction(cardbookBGRepo, topic, [nodeName], null, cards.length);

					let allDirPrefIds = cardbookHTMLUtils.arrayUnique(Array.from(cards, item => item.dirPrefId));
					for (let card of cards) {
						// as it is possible to remove a category from a virtual folder
						// should avoid to modify cards belonging to a read-only address book
						if (cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
							continue;
						}
						let outCard = new cardbookCardParser();
						await cardbookBGUtils.cloneCard(card, outCard);
						if (nodeType == "categories") {
							cardbookHTMLUtils.removeCategoryFromCard(outCard, nodeName);
						} else if (nodeType == "org") {
							cardbookHTMLUtils.removeOrgFromCard(outCard, nodeId);
						}
						await cardbookBGRepo.saveCardFromUpdate(card, outCard, actionId, false);
					}
					if (nodeType == "categories") {
						async function delCat(dirPrefId1) {
							await cardbookBGRepo.deleteCategories([cardbookBGRepo.cardbookCategories[`${dirPrefId1}::${nodeName}`]], actionId);
						}
						if (allDirPrefIds.length) {
							for (let dirPrefId of allDirPrefIds) {
								if (cardbookBGRepo.cardbookDisplayCards[nodeId].cards.length == 0) {
									await delCat(dirPrefId);
								}
							}
						} else {
							await delCat(dirPrefId);
						}
					}
					await endAction(actionId, true);
				}
				break;
			}
			case "cardbook.writeCardsToDir": {
				for (let card of info.cards) {
					let filename = cardbookBGUtils.getFileNameForCard(info.url, card.fn, "vcf");
					let data = await cardbookBGUtils.cardToVcardData(cardbookBGRepo.statusInformation, card);
					let result = await messenger.NotifyTools.notifyExperiment({query: "cardbook.writeContentToFile", url: filename, data: data, utf8: "UTF8"});
					cardbookBGRepo.currentAction[info.actionId].doneCards++;
					if (result == "OK") {
						cardbookBGLog.updateStatusProgressInformationWithDebug2(cardbookBGRepo.statusInformation, "debug mode : file rewritten : " + filename);
					} else {
						cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookXULUtils.writeContentToFile error : filename : " + filename, "Error");
					}
				}
				break;
			}
			case "cardbook.provider": {
				let cards = [];
				let searchArray = cardbookBGRepo.cardbookCardLongSearch;
				if (cardbookBGPreferences.getPref("autocompleteRestrictSearch")) {
					searchArray = cardbookBGRepo.cardbookCardShortSearch;
				}
				let newSearchString = cardbookBGUtils.makeSearchString(info.searchString);
				for (let account of cardbookBGRepo.cardbookAccounts) {
					if (account[2] && account[3] != "SEARCH") {
						let dirPrefId = account[1];
						for (var j in searchArray[dirPrefId]) {
							if (j.indexOf(newSearchString) >= 0 || newSearchString == "") {
								for (let card of searchArray[dirPrefId][j]) {
									let emails = cardbookBGUtils.getEmailsFromCard(card, true);
									let displayName = card.fn;
									let primaryEmail = emails.length ? emails[0] : "";
									let secondEmail = emails.length > 1 ? emails[1] : "";
									cards.push({ displayName, primaryEmail, secondEmail});
								}
							}
						}
					}
				}
				return cards;
			}
			case "cardbook.pasteCards": {
				let nodeArray = cardbookHTMLUtils.escapeStringSemiColon(info.target).split("::");
				let dirPrefId = nodeArray[0];
				let nodeType = nodeArray[1];
				let nodeName = nodeArray[nodeArray.length-1];
				nodeArray.shift();
				nodeArray.shift();
				let orgNode = cardbookHTMLUtils.unescapeStringSemiColon(nodeArray.join(";"));
				let version = cardbookBGPreferences.getVCardVersion(dirPrefId);
				let dateFormat = cardbookHTMLUtils.getDateFormat(dirPrefId, version);
	
				for (let data of info.ids) {
					if (cardbookBGRepo.cardbookCards[data]) {
						let card = cardbookBGRepo.cardbookCards[data];
						let defaultChoice = "";
						if (dirPrefId == card.dirPrefId) {
							if (nodeType == "categories" && card.categories.includes(nodeName)) {
								defaultChoice = "duplicate";
							} else if (nodeType == "org" && orgNode == card.org) {
								defaultChoice = "duplicate";
							} else if (!cardbookHTMLUtils.possibleNodes.includes(nodeType)) {
								defaultChoice = "duplicate";
							} else {
								defaultChoice = "update";
							}
						}
						let sourceDateFormat = cardbookHTMLUtils.getDateFormat(card.dirPrefId, card.version);
						await importCard(card, info.target, version, sourceDateFormat, dateFormat,
										info.actionId, defaultChoice);
						let choice = cardbookBGRepo.importConflictChoiceImportValues.join("::");
						if (dirPrefId != card.dirPrefId && 
							cardbookBGRepo.importConflictChoice[info.actionId] &&
							cardbookBGRepo.importConflictChoice[info.actionId][choice] &&
							cardbookBGRepo.importConflictChoice[info.actionId][choice].result != "cancel") {
							if (info.mode == "CUT") {
								cardbookBGRepo.currentAction[info.actionId].totalCards++;
								await cardbookBGRepo.asyncDeleteCards([card], info.actionId);
							}
						}
					} else {
						cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "clipboardWrong");
					}
				}
				await endAction(info.actionId, true);
				break;
			}
			case "cardbook.askUser.sendChoice":
				await cardbookHTMLUtils.saveWindowSize(info.winName, info.winState);
				if (cardbookBGRepo.importConflictChoice[info.dirPrefId] &&
					cardbookBGRepo.importConflictChoice[info.dirPrefId][info.buttons] &&
					cardbookBGRepo.importConflictChoice[info.dirPrefId][info.buttons][info.message]) {
					if (info.confirm == true) {
						await messenger.runtime.sendMessage({query: "cardbook.askUser.importConflictChoicePersist", dirPrefId: info.dirPrefId, buttons: info.buttons});
						cardbookBGRepo.importConflictChoice[info.dirPrefId][info.buttons].result = info.result;
					} else {
						cardbookBGRepo.importConflictChoice[info.dirPrefId][info.buttons][info.message].result = info.result;
					}
				}
				break;
			case "cardbook.saveCardFromUpdate":
				await cardbookBGRepo.saveCardFromUpdate(info.card, info.outCard, info.actionId, info.checkCategory);
				break;
			case "cardbook.updateServerSyncRequest":
				cardbookBGRepo.cardbookServerSyncRequest[info.dirPrefId]++;
				break;
			case "cardbook.initServerValidation":
				cardbookBGRepo.cardbookServerValidation[info.dirPrefId] = {length: 0, user: info.user};
				break;
			case "cardbook.fromValidationToArray":
				return cardbookBGRepo.fromValidationToArray(info.dirPrefId, info.type);
			case "cardbook.getListConversion":
				return new cardbookListConversion(cardbookBGRepo, info.email, info.identity, true);
			case "cardbook.getFn": {
				let fn = "";
				if (cardbookBGRepo.cardbookCards[info.id]) {
					fn = cardbookBGRepo.cardbookCards[info.id].fn;
				}
				return fn;
			}
			case "cardbook.formatStringForOutput":
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, info.string, info.values, info.error);
				break;
			case "cardbook.getCurrentActions":
				return cardbookBGRepo.currentAction;
			case "cardbook.getEncryptorVersion":
				return String(cardbookIDBEncryptor.VERSION);
			case "cardbook.applyFormulaToAllAB":
				for (let account of cardbookBGRepo.cardbookAccounts) {
					if ((account[1] == info.dirPrefId) || ("allAddressBooks" == info.dirPrefId)) {
						await cardbookBGPreferences.setFnFormula(account[1], info.formula);
					}
				}
				break;
			case "cardbook.removePrefDispName":
				cardbookIDBPrefDispName.removePrefDispName(cardbookBGRepo, info.email);
				break;
			case "cardbook.addPrefDispName":
				cardbookIDBPrefDispName.addPrefDispName(cardbookBGRepo, { email: info.email });
				break;
			case "cardbook.mergeCards.addCardToTemp": {
				let uid = info.ids.split(",")[0].split("::")[1];
				let dirPrefId = info.ids.split(",")[0].split("::")[0];
				// temporary card
				cardbookBGRepo.addCardToTemp(info.card);
				cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid] = {};
				cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid].tempCard = info.card;
				break;
			}
			case "cardbook.mergeCards.mergeFinished": {
				await cardbookHTMLUtils.saveWindowSize(info.winName, info.winState);
				// source : MERGE, DUPLICATE, SYNC, IMPORT
				let uid = info.ids.split(",")[0].split("::")[1];
				let dirPrefId = info.ids.split(",")[0].split("::")[0];
				if (info.action == "CANCEL") {
					if (cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid]) {
						// remove the temporary card
						cardbookBGRepo.cardbookServerGetCardForMergeResponse[dirPrefId]++;
						if (cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid].tempCard) {
							let tempCard = cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid].tempCard;
							cardbookBGRepo.removeCardFromTemp(tempCard);
							cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid] = null;
						}
						cardbookBGRepo.cardbookServerCardSyncDone[dirPrefId]++;
					}
					if (info.source == "IMPORT") {
						cardbookBGRepo.currentAction[info.actionId].params[info.mergeId].status = "FINISHED";
					}
					return;
				}
				if (info.source != "SYNC") {
					var mergeActionId = info.actionId || cardbookBGActions.startAction(cardbookBGRepo, "cardsMerged", null, null);
					switch (info.action) {
						case "CREATEANDREPLACE":
							for (let cbid of info.ids.split(",")) {
								if (cardbookBGRepo.cardbookCards[cbid]) {
									let card = cardbookBGRepo.cardbookCards[cbid];
									await cardbookBGRepo.deleteCards([card], mergeActionId);
									cardbookBGRepo.currentAction[mergeActionId].totalCards++;
								} else {
									// remove the temporary card
									let card = await cardbookIDBCard.getCard(cardbookBGRepo.statusInformation, cbid);
									cardbookBGRepo.removeCardFromTemp(card);
								}
							}
						case "CREATE":
							await cardbookBGRepo.saveCardFromUpdate({}, info.cardOut, mergeActionId, true);
							break;
					}
					if (info.source == "DUPLICATE" || info.source == "MERGE") {
						await endAction(mergeActionId, true);
					} else {
						cardbookBGRepo.currentAction[mergeActionId].params[info.mergeId].status = "FINISHED";
					}
				}
				if (info.action == "CREATE" || info.action == "CREATEANDREPLACE") {
					if (info.source == "DUPLICATE") {
						await messenger.runtime.sendMessage({query: "cardbook.findDuplicates.finishMergeAction", duplicateWinId: info.duplicateWinId, duplicateDisplayId: info.duplicateDisplayId, duplicateLineId: info.duplicateLineId});
					} else if (info.source == "SYNC" || info.source == "IMPORT") {
						cardbookBGRepo.cardbookServerGetCardForMergeResponse[dirPrefId]++;
						if (info.source == "SYNC") {
							info.cardOut.uid = uid;
							let etag = cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid].etag;
							cardbookBGUtils.addEtag(info.cardOut, etag);
							cardbookBGUtils.setCalculatedFields(info.cardOut);
							cardbookBGRepo.cardbookServerUpdatedCardRequest[dirPrefId]++;
							let connection = cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid].connection;
							let prefIdType = cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid].prefIdType;
							let cacheCard = cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid].cacheCard;
							if (prefIdType == "GOOGLE2") {
								cardbookBGSynchronizationGoogle2.serverUpdateCard(cardbookBGRepo, connection, info.cardOut);
							} else if (prefIdType == "OFFICE365") {
								// not possible any more
								let a;
							} else {
								await cardbookBGSynchronizationCARDDAV.serverUpdateCard(cardbookBGRepo, connection, cacheCard, info.cardOut, prefIdType);
							}
						}
						// remove the temporary card
						let tempCard = cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid].tempCard;
						cardbookBGRepo.removeCardFromTemp(tempCard);
						cardbookBGRepo.cardbookServerSyncMerge[dirPrefId][uid] = null;
					}
				}
				break;
				}
			case "cardbook.getImage": {
				let image = await cardbookIDBImage.getImage(cardbookBGRepo.statusInformation, info.field, info.dirName, info.cardId, info.cardName);
				return image;
			}
			case "cardbook.processData.setDisplayId":
				processDataDisplayId[info.winId] = info.displayId;
				break;
			case "cardbook.findDuplicates.getCards": {
				let cardArray = [];
				let gResults = [];
				let gResultsDirPrefId = [];
				if (info.dirPrefId) {
					for (let card of cardbookBGRepo.cardbookDisplayCards[info.dirPrefId].cards) {
						if (processDataDisplayId[info.winId] != info.displayId) {
							return
						}
						if (!card.isAList) {
							cardArray.push([cardbookBGDuplicate.generateCardArray(card), card, true]);
						}
					}
				} else {
					for (let i in cardbookBGRepo.cardbookCards) {
						if (processDataDisplayId[info.winId] != info.displayId) {
							return
						}
						let card = cardbookBGRepo.cardbookCards[i];
						if (!card.isAList) {
							cardArray.push([cardbookBGDuplicate.generateCardArray(card), card, true]);
						}
					}
				}
				await messenger.runtime.sendMessage({query: "cardbook.processData.toDo", winId: info.winId, displayId: info.displayId, toDo: cardArray.length});
				let rowDuplicateDoneGet = 0;
				for (var i = 0; i < cardArray.length; i++) {
					rowDuplicateDoneGet++;
					await messenger.runtime.sendMessage({query: "cardbook.processData.rowDone", winId: info.winId, displayId: info.displayId, rowDone: rowDuplicateDoneGet});
					if (processDataDisplayId[info.winId] != info.displayId) {
						return
					}
					if (cardArray[i][2] === false) {
						continue
					}
					let tmpResult = [cardArray[i][1]];
					for (var j = i+1; j < cardArray.length; j++) {
						if (processDataDisplayId[info.winId] != info.displayId) {
							return
						}
						let added = false;
						if (cardArray[j][2] && cardbookBGDuplicate.compareCardArraySure(cardArray[i][0].resultSure, cardArray[j][0].resultSure)) {
							tmpResult.push(cardArray[j][1]);
							cardArray[j][2] = false;
							added = true;
						} else if (cardArray[j][2] && info.state == "more" && cardbookBGDuplicate.compareCardArrayTry(cardArray[i][0].resultTry, cardArray[j][0].resultTry)) {
							tmpResult.push(cardArray[j][1]);
							cardArray[j][2] = false;
							added = true;
						}
						if (added && !gResultsDirPrefId.includes(cardArray[i][1].dirPrefId)) {
							gResultsDirPrefId.push(cardArray[i][1].dirPrefId)
						}
						if (added && !gResultsDirPrefId.includes(cardArray[j][1].dirPrefId)) {
							gResultsDirPrefId.push(cardArray[j][1].dirPrefId)
						}
					}
					if (tmpResult.length > 1) {
						// necessary to sort for the excluded duplicates
						cardbookHTMLUtils.sortMultipleArrayByString(tmpResult, "uid", 1);
						gResults.push(tmpResult);
					}
				}
				await messenger.runtime.sendMessage({query: "cardbook.findDuplicates.ABs", winId: info.winId, displayId: info.displayId, ABids: gResultsDirPrefId});
				await messenger.runtime.sendMessage({query: "cardbook.findDuplicates.cards", winId: info.winId, displayId: info.displayId, cards: gResults});
				break;
			}
			case "cardbook.findDuplicates.getDuplidateIndex": {
				let duplicate = await cardbookIDBDuplicate.loadDuplicate();
				return duplicate;
			}
			case "cardbook.findDuplicates.addDuplidateIndex": {
				let uid0 = info.uids[0];
				for (let i = 1; i < info.uids.length; i++) {
					await cardbookIDBDuplicate.addDuplicate(cardbookBGRepo.statusInformation, uid0, info.uids[i]);
				}
				break;
			}
			case "cardbook.findDuplicates.mergeOne":
				await cardbookBGDuplicate.mergeOne(cardbookBGRepo, info.record, info.sourceCat, info.targetCat, info.actionId);
				break;
			case "cardbook.formatData.getCards": {
				let fieldType = "";
				if (info.fields != "tel" && info.fields != "email") {
					if (cardbookHTMLUtils.allColumns.personal.includes(info.fields) || cardbookHTMLUtils.allColumns.org.includes(info.fields) || cardbookHTMLUtils.adrElements.includes(info.fields)) {
						fieldType = "string";
					} else {
						let customFields = cardbookBGPreferences.getAllCustomFields();
						let personalCB = customFields.personal.filter(x => x[0] == info.fields);
						let orgCB = customFields.org.filter(x => x[0] == info.fields);
						if (personalCB.length || orgCB.length) {
							fieldType = "custom";
						} else {
							let orgStructure = cardbookBGPreferences.getPref("orgStructure");
							if (orgStructure.includes(info.fields.replace(/^org\./, ""))) {
								fieldType ="customOrg";
							}
						}
					}
				}
				let data = JSON.parse(JSON.stringify(cardbookBGRepo.cardbookDisplayCards[info.dirPrefId].cards));
				await messenger.runtime.sendMessage({query: "cardbook.processData.toDo", winId: info.winId, displayId: info.displayId, toDo: data.length});
				let rowFormatDoneGet = 0;
				let lines = 0;
				for (let card of data) {
					if (processDataDisplayId[info.winId] != info.displayId) {
						return
					}
					let i = 0;
					switch(info.fields) {
						case "postOffice":
						case "extendedAddr":
						case "street":
						case "locality":
						case "region":
						case "postalCode":
						case "country":
							let adrIndex = cardbookHTMLUtils.adrElements.indexOf(info.fields);
							for (let adrLine of card.adr) {
								if (processDataDisplayId[info.winId] != info.displayId) {
									return
								}
								await messenger.runtime.sendMessage({query: "cardbook.formatData.displayCardLineFields", winId: info.winId, displayId: info.displayId, record: [lines, card.cbid, card.fn, adrLine[0][adrIndex], adrLine[0][adrIndex], i]});
								lines++;
								i++;
							}
							rowFormatDoneGet++;
							await messenger.runtime.sendMessage({query: "cardbook.processData.rowDone", winId: info.winId, displayId: info.displayId, rowDone: rowFormatDoneGet});
							break;
						case "tel":
							let country = cardbookHTMLUtils.getCardRegion(card);
							for (let telLine of card.tel) {
								if (processDataDisplayId[info.winId] != info.displayId) {
									return
								}
								await messenger.runtime.sendMessage({query: "cardbook.formatData.displayCardLineTels", winId: info.winId, displayId: info.displayId, record: [lines, card.cbid, card.fn, country.toLowerCase(), telLine[0][0], telLine[0][0], i]});
								lines++;
								i++;
							}
							rowFormatDoneGet++;
							await messenger.runtime.sendMessage({query: "cardbook.processData.rowDone", winId: info.winId, displayId: info.displayId, rowDone: rowFormatDoneGet});
							break;
						case "email":
							for (let emailLine of card.email) {
								if (processDataDisplayId[info.winId] != info.displayId) {
									return
								}
								await messenger.runtime.sendMessage({query: "cardbook.formatData.displayCardLineEmail", winId: info.winId, displayId: info.displayId, record: [lines, card.cbid, card.fn, emailLine[0][0], emailLine[0][0], i]});
								lines++;
								i++;
							}
							rowFormatDoneGet++;
							await messenger.runtime.sendMessage({query: "cardbook.processData.rowDone", winId: info.winId, displayId: info.displayId, rowDone: rowFormatDoneGet});
							break;
						default:
							if (processDataDisplayId[info.winId] != info.displayId) {
								return
							}
							let field = info.fields;
							let value = "";
							if (fieldType == "string") {
								if (typeof card[field] !== "undefined") {
									value = card[field];
								}
							} else if (fieldType == "custom") {
								let tmpArray = card.others.filter( x => x.startsWith(field + ":"));
								if (tmpArray.length) {
									let regexp = new RegExp("^" + field + ":");
									value = tmpArray[0].replace(regexp, "");
								}
							} else if (fieldType == "customOrg") {
								let orgStructure = cardbookBGPreferences.getPref("orgStructure");
								let index = orgStructure.indexOf(field.replace(/^org\./, ""));
								if (card.org[index]) {
									value = card.org[index];
								}
							}
							await messenger.runtime.sendMessage({query: "cardbook.formatData.displayCardLineFields", winId: info.winId, displayId: info.displayId, record: [lines, card.cbid, card.fn, value, value]});
							lines++;
							rowFormatDoneGet++;
							await messenger.runtime.sendMessage({query: "cardbook.processData.rowDone", winId: info.winId, displayId: info.displayId, rowDone: rowFormatDoneGet});
							break;
						}
				}
				data = null;
				await messenger.runtime.sendMessage({query: "cardbook.processData.cardsLoaded", winId: info.winId, displayId: info.displayId, cardsLoaded: true});
				processDataDisplayId[info.winId] = null;
				break;
			}
			case "cardbook.formatData.saveCards": {
				let fieldTypeSave = "";
				if (info.fields != "tel" && info.fields != "email") {
					if (cardbookHTMLUtils.allColumns.personal.includes(info.fields) || cardbookHTMLUtils.allColumns.org.includes(info.fields) || cardbookHTMLUtils.adrElements.includes(info.fields)) {
						fieldTypeSave = "string";
					} else {
						let customFields = cardbookBGPreferences.getAllCustomFields();
						let personalCB = customFields.personal.filter(x => x[0] == info.fields);
						let orgCB = customFields.org.filter(x => x[0] == info.fields);
						if (personalCB.length || orgCB.length) {
							fieldTypeSave = "custom";
						} else {
							let orgStructure = cardbookBGPreferences.getPref("orgStructure");
							if (orgStructure.includes(info.fields.replace(/^org\./, ""))) {
								fieldTypeSave ="customOrg";
							}
						}
					}
				}
				let valueTypeName = messenger.i18n.getMessage(`${info.fields}Label`);
				let dirPrefId = info.dirPrefId;
				let formatActionId = cardbookBGActions.startAction(cardbookBGRepo, "cardsFormatted", [info.scopeName, valueTypeName], dirPrefId);
				let rowFormatDoneSave = 0;
				for (let id in info.results) {
					if (cardbookBGRepo.cardbookCards[id]) {
						rowFormatDoneSave++;
						await messenger.runtime.sendMessage({query: "cardbook.processData.rowDone", winId: info.winId, displayId: info.displayId, rowDone: rowFormatDoneSave});
						let card = cardbookBGRepo.cardbookCards[id];
						if (cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
							continue;
						}
						let outCard = new cardbookCardParser();
						await cardbookBGUtils.cloneCard(card, outCard);
						let deleted = 0;
						for (let line of info.results[id]) {
							let index = line[0] - deleted;
							let value = line[1];
							if (value) {
								if (info.fields == "tel" || info.fields == "email") {
									outCard[info.fields][index][0] = [value];
								} else if (cardbookHTMLUtils.adrElements.includes(info.fields)) {
									let adrIndex = cardbookHTMLUtils.adrElements.indexOf(info.fields);
									outCard.adr[index][0][adrIndex] = value;
								} else {
									if (fieldTypeSave == "string") {
										outCard[info.fields] = value;
									} else if (fieldTypeSave == "custom") {
										outCard.others = outCard.others.filter(x => !x.startsWith(info.fields + ":"));
										outCard.others.push(info.fields + ":"+ value);
									} else if (fieldTypeSave == "customOrg") {
										let orgStructure = cardbookBGPreferences.getPref("orgStructure");
										let index = orgStructure.indexOf(info.fields.replace(/^org\./, ""));
										outCard.org[index] = cardbookHTMLUtils.escapeStringSemiColon(value.trim());
										outCard.org = cardbookHTMLUtils.unescapeStringSemiColon(outCard.org.join(";"));
									}
								}
							} else {
								if (info.fields == "tel" || info.fields == "email") {
									outCard[info.fields].splice(index, 1);
									deleted++;
								} else {
									if (fieldTypeSave == "string") {
										outCard[info.fields] = "";
									} else if (fieldTypeSave == "custom") {
										outCard.others = outCard.others.filter(x => !x.startsWith(info.fields + ":"));
									} else if (fieldTypeSave == "customOrg") {
									}
								}
							}
						}
						await cardbookBGRepo.saveCardFromUpdate(card, outCard, formatActionId, false);
					}
				}
				await messenger.runtime.sendMessage({query: "cardbook.processData.cardsLoaded", winId: info.winId, displayId: info.displayId, cardsLoaded: true});
				await endAction(formatActionId, true);
				break;
			}
			case "cardbook.convertVCards": {
				let convertTopic = "cardsConverted";
				let convertActionId = cardbookBGActions.startAction(cardbookBGRepo, convertTopic);
				let targetVersion = cardbookBGPreferences.getVCardVersion(info.dirPrefId);
				let targetName = cardbookBGPreferences.getName(info.dirPrefId);
				// the date format is no longer stored
				let newDateFormat = cardbookHTMLUtils.getDateFormat(info.dirPrefId, info.initialVCardVersion);
				let counter = 0;

				let taskHandle;
				let currentTaskNumber = 0;
				let taskList = [];
				async function convertVCard(card) {
					let tempCard = new cardbookCardParser();
					await cardbookBGUtils.cloneCard(card, tempCard);
					let result = await cardbookHTMLUtils.convertVCard(tempCard, targetName, targetVersion, newDateFormat, newDateFormat, cardbookBGRepo.possibleCustomFields);
					cardbookBGRepo.writePossibleCustomFields(result.possibleCustomFields);
					if (result.converted) {
						await cardbookBGRepo.saveCardFromUpdate(card, tempCard, convertActionId, false);
						counter++;
					}
				}

				async function runTaskQueue(deadline) {
					while ((deadline.timeRemaining() > 0 || deadline.didTimeout) && taskList.length) {
						const task = taskList.shift();
						currentTaskNumber++;
						await task.handler(task.data);
					}
					if (taskList.length) {
						taskHandle = requestIdleCallback(runTaskQueue, { timeout: 1000 });
					} else {
						taskHandle = 0;
						cardbookBGRepo.writePossibleCustomFields();
						try {
							cardbookBGPreferences.delBranch(`${cardbookBGPreferences.prefCardBookData}${info.dirPrefId}.dateFormat`);
						} catch(e) {}
						cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, convertTopic, [targetName, targetVersion, counter]);
						await endAction(convertActionId, true);
					}
				}
				for (let card of cardbookBGRepo.cardbookDisplayCards[info.dirPrefId].cards) {
					taskList.push({ handler: convertVCard, data: card });
				}
				if (!taskHandle) {
					taskHandle = requestIdleCallback(runTaskQueue, { timeout: 1000 });
				}
				break;
			}
			case "cardbook.writePossibleCustomFields":
				cardbookBGRepo.writePossibleCustomFields(info.possibleCustomFields);
				break;
			case "cardbook.getPossibleCustomFields":
				return cardbookBGRepo.possibleCustomFields;
			case "cardbook.cardbookPreferDisplayNameIndex":
				if (cardbookBGRepo.cardbookPreferDisplayNameIndex[info.email]) {
					return cardbookBGRepo.cardbookPreferDisplayNameIndex[info.email];
				} else {
					return null;
				}
			case "cardbook.searchForWrongCards": {
				try {
					cardbookBGPreferences.delBranch(`${cardbookBGPreferences.prefCardBookData}${info.dirPrefId}.dateFormat`);
				} catch(e) {}
				let found = false;
				let version = cardbookBGPreferences.getVCardVersion(info.dirPrefId);
				for (let card of cardbookBGRepo.cardbookDisplayCards[info.dirPrefId].cards) {
					if (card.version != version) {
						found = true;
						break;
					}
				}
				return found;
			}
			case "cardbook.changePrefEmail": {
				let total = Object.keys(cardbookBGRepo.cardbookCards).length;
				await messenger.runtime.sendMessage({query: "cardbook.conf.addProgressBar", type: "changePrefEmail", total: total});
				let taskHandle;
				let currentTaskNumber = 0;
				let taskList = [];
				async function changePrefEmail(card) {
					if (!card.isAList) {
						let newEmails = cardbookHTMLUtils.getPrefAddressFromCard(card, "email", info.value);
						if (newEmails.join(",") != card.emails.join(",")) {
							let tmpCard = new cardbookCardParser();
							await cardbookBGUtils.cloneCard(card, tmpCard);
							await cardbookBGRepo.saveCardFromMove(card, tmpCard, null, false);
						}
					}
				}

				async function runTaskQueue(deadline) {
					while ((deadline.timeRemaining() > 0 || deadline.didTimeout) && taskList.length) {
						const task = taskList.shift();
						currentTaskNumber++;
						await task.handler(task.data);
						if (currentTaskNumber % 100 == 0) {
							messenger.runtime.sendMessage({query: "cardbook.conf.addProgressBar", type: "changePrefEmail", done: currentTaskNumber});
						}
					}
					if (taskList.length) {
						taskHandle = requestIdleCallback(runTaskQueue, { timeout: 1000 });
					} else {
						taskHandle = 0;
						messenger.runtime.sendMessage({query: "cardbook.conf.addProgressBar", type: "changePrefEmail", done: total});
					}
				}
				for (let i in cardbookBGRepo.cardbookCards) {
					taskList.push({ handler: changePrefEmail, data: cardbookBGRepo.cardbookCards[i] });
				}
				if (!taskHandle) {
					taskHandle = requestIdleCallback(runTaskQueue, { timeout: 1000 });
				}
				break;
			}
			case "cardbook.disableShortSearch":
				cardbookBGRepo.cardbookCardShortSearch = {};
				break;
			case "cardbook.enableShortSearch": {
				let total = Object.keys(cardbookBGRepo.cardbookCards).length;
				await messenger.runtime.sendMessage({query: "cardbook.conf.addProgressBar", type: "enableShortSearch", total: total});
				let taskHandle;
				let currentTaskNumber = 0;
				let taskList = [];
				function runTaskQueue(deadline) {
					while ((deadline.timeRemaining() > 0 || deadline.didTimeout) && taskList.length) {
						const task = taskList.shift();
						currentTaskNumber++;
						task.handler(task.data);
						if (currentTaskNumber % 100 == 0) {
							messenger.runtime.sendMessage({query: "cardbook.conf.addProgressBar", type: "enableShortSearch", done: currentTaskNumber});
						}
					}
					if (taskList.length) {
						taskHandle = requestIdleCallback(runTaskQueue, { timeout: 1000 });
					} else {
						taskHandle = 0;
						messenger.runtime.sendMessage({query: "cardbook.conf.addProgressBar", type: "enableShortSearch", done: total});
					}
				}
				for (let i in cardbookBGRepo.cardbookCards) {
					taskList.push({ handler: cardbookBGRepo.addCardToShortSearch, data: cardbookBGRepo.cardbookCards[i] });
				}
				if (!taskHandle) {
					taskHandle = requestIdleCallback(runTaskQueue, { timeout: 1000 });
				}
				break;
			}
			case "cardbook.addBirthdaysToCalendar":
				await messenger.NotifyTools.notifyExperiment({query: info.query, birthdayList: info.birthdayList, prefs: info.prefs, sendResults: info.sendResults});
				break;
			case "cardbook.getBirthdays":
				return cardbookBGBirthdays.getBirthdays(cardbookBGRepo, info.days);
			case "cardbook.getMembersFromCard":
				return cardbookBGUtils.getMembersFromCard(cardbookBGRepo, info.card);
			case "cardbook.cardToVcardData":
				return await cardbookBGUtils.cardToVcardData(cardbookBGRepo.statusInformation, info.card, info.mediaFromDB); 
			case "cardbook.getvCard":
				return await cardbookBGUtils.getvCardForEmail(cardbookBGRepo.statusInformation, cardbookBGRepo.cardbookCards[info.dirPrefId+"::"+info.contactId]);
			case "cardbook.getPopularity":
				if (cardbookBGRepo.cardbookMailPopularityIndex[info.email]) {
					return cardbookBGRepo.cardbookMailPopularityIndex[info.email].count;
				} else {
					return 0;
				}
			case "cardbook.getCardFromEmail":
				return cardbookBGRepo.getCardFromEmail(info.email, info.dirPrefId);
			case "cardbook.changeOrgForAddressBook":
				await changeOrgForAddressBook();
				break;
			case "cardbook.setNextAndPreviousCard": {
				// for next and previous in edition
				cardbookBGRepo.displayedIds = {};
				let index = 0;
				for (let cbid of info.cbids) {
					cardbookBGRepo.displayedIds[index] = cbid;
					index++;
				}
				break;
			}
			case "cardbook.getNextAndPreviousCard": {
				let result = {};
				let previous = 0;
				let next = 0;
				for (let index in cardbookBGRepo.displayedIds) {
					if (cardbookBGRepo.displayedIds[index] == info.cbid) {
						previous = parseInt(index)-1;
						next = parseInt(index)+1;
						break;
					}
				}
				if (cardbookBGRepo.displayedIds[previous]) {
					result.previous = cardbookBGRepo.cardbookCards[cardbookBGRepo.displayedIds[previous]];
				}
				if (cardbookBGRepo.displayedIds[next]) {
					result.next = cardbookBGRepo.cardbookCards[cardbookBGRepo.displayedIds[next]];
				}
				return result;
			}
			case "cardbook.getStatusInformation":
				return cardbookBGRepo.statusInformation;
			case "cardbook.setStatusInformation":
				while (cardbookBGRepo.statusInformation.length > info.value) {
					cardbookBGRepo.statusInformation.splice(0, 1);
				}
				break;
			case "cardbook.flushStatusInformation":
				cardbookBGRepo.statusInformation = [];
				break;
			case "cardbook.getAccounts":
				return cardbookBGRepo.cardbookAccounts;
			case "cardbook.getAccountsCategories":
				return cardbookBGRepo.cardbookAccountsCategories;
			case "cardbook.updateMailPop":
				cardbookIDBMailPop.updateMailPop(cardbookBGRepo, info.email, info.value);
				break;
			case "cardbook.isMyAccountSyncing":
				if (cardbookBGRepo.cardbookSyncMode[info.dirPrefId] && cardbookBGRepo.cardbookSyncMode[info.dirPrefId] == 1) {
					return true;
				}
				return false;
			case "cardbook.getModifiedCards":
				return cardbookBGRepo.cardbookDisplayCards[info.account].modified;
			case "cardbook.startAction": {
				let actionId = cardbookBGActions.startAction(cardbookBGRepo, info.actionCode, info.array, info.refresh, info.totalEstimated, info.total)
				return actionId;
			}
			case "cardbook.endAction":
				await endAction(info.actionId, info.refresh, info.params);
				break;
			case "cardbook.displayBulkWindow": {
				if (info.actionId && cardbookBGRepo.currentAction[info.actionId] && cardbookBGRepo.currentAction[info.actionId].totalEstimatedCards) {
					if (cardbookBGRepo.currentAction[info.actionId].totalEstimatedCards < 10) {
						return false;
					}
				}
				return true;
			}
			case "cardbook.removeNode": {
				await removeNode(info.node);
				break;
			}
			case "cardbook.renameNode": {
				await renameNode(info.node);
				break;
			}
			case "cardbook.deleteCardsAndValidate": {
				await deleteCardsAndValidate(info.listOfCardsId);
				break;
			}
			case "cardbook.deleteCards":
				await cardbookBGRepo.asyncDeleteCards(info.cards, info.actionId);
				await endAction(info.actionId, true);
				break;
			case "cardbook.createListFromNode": {
				let allDirPrefIds = {};
				let listOfUids = [];
				for (let card of cardbookBGRepo.cardbookDisplayCards[info.nodeId].cards) {
					let dirPrefId = card.dirPrefId;
					if (!allDirPrefIds[dirPrefId]) {
						let newList = new cardbookCardParser();
						newList.dirPrefId = dirPrefId;
						cardbookBGUtils.setCardUUID(newList);
						newList.version = cardbookBGPreferences.getVCardVersion(dirPrefId);
						newList.fn = info.name;
						cardbookHTMLUtils.addOrgToCard(newList, info.nodeId);
						allDirPrefIds[dirPrefId] = {};
						allDirPrefIds[dirPrefId].list = newList;
						allDirPrefIds[dirPrefId].members = [];
					}
					if (card.isAList) {
						listOfUids = cardbookBGUtils.getUidsFromList(cardbookBGRepo, card);
						for (let uid of listOfUids) {
							allDirPrefIds[dirPrefId].members.push("urn:uuid:" + uid);
						}
					} else {
						allDirPrefIds[dirPrefId].members.push("urn:uuid:" + card.uid);
					}
				}
		
				for (let i in allDirPrefIds) {
					cardbookHTMLUtils.addMemberstoCard(allDirPrefIds[i].list, allDirPrefIds[i].members, "group");
					await cardbookBGRepo.saveCardFromUpdate({}, allDirPrefIds[i].list, info.aActionId, true);
				}
				break;
			}
			case "cardbook.convertNodeToList": {
				let cards = JSON.parse(JSON.stringify(cardbookBGRepo.cardbookDisplayCards[info.nodeId].cards));
				let allDirPrefIds = {};
				for (let card of cards) {
					// as it is possible to remove a category from a virtual folder
					// should avoid to modify cards belonging to a read-only address book
					if (cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
						continue;
					}
					let newList = new cardbookCardParser();
					if (!allDirPrefIds[card.dirPrefId]) {
						newList.dirPrefId = card.dirPrefId;
						cardbookBGUtils.setCardUUID(newList);
						newList.version = cardbookBGPreferences.getVCardVersion(card.dirPrefId);
						newList.fn = info.nodeName;
						allDirPrefIds[card.dirPrefId] = {};
						allDirPrefIds[card.dirPrefId].list = newList;
						allDirPrefIds[card.dirPrefId].members = [];
					}
					allDirPrefIds[card.dirPrefId].members.push("urn:uuid:" + card.uid);
		
					let outCard = new cardbookCardParser();
					await cardbookBGUtils.cloneCard(card, outCard);
					if (info.nodeType == "categories") {
						cardbookHTMLUtils.removeCategoryFromCard(outCard, info.nodeName);
					}
					await cardbookBGRepo.saveCardFromUpdate(card, outCard, info.actionId, true);
				}
				for (let i in allDirPrefIds) {
					cardbookHTMLUtils.addMemberstoCard(allDirPrefIds[i].list, allDirPrefIds[i].members, "group");
					await cardbookBGRepo.saveCardFromUpdate({}, allDirPrefIds[i].list, info.actionId, true);
				}
				break;
			}
			case "cardbook.getCategories": {
				let sortedCategories = [];
				if (cardbookBGRepo.cardbookAccountsCategories[info.defaultPrefId]) {
					for (let category of cardbookBGRepo.cardbookAccountsCategories[info.defaultPrefId]) {
						if (info.exclRestrictionList && info.exclRestrictionList[info.defaultPrefId] && info.exclRestrictionList[info.defaultPrefId][category]) {
							continue;
						}
						let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
						if (info.noUncat && info.noUncat === true && category == uncat) {
							continue;
						}
						if (info.inclRestrictionList && info.inclRestrictionList[info.defaultPrefId]) {
							if (info.inclRestrictionList[info.defaultPrefId][category]) {
								sortedCategories.push([category, info.defaultPrefId+"::categories::"+category]);
							}
						} else {
							sortedCategories.push([category, info.defaultPrefId+"::categories::"+category]);
						}
					}
				}
				cardbookHTMLUtils.sortMultipleArrayByString(sortedCategories,0,1);
				return sortedCategories;
			}
			case "cardbook.getCardWithId":
				return cardbookBGRepo.cardbookCards[info.cbid];
			case "cardbook.getCards":
					let cardslist = [];
					for (let cbid of info.cbids) {
						if (cardbookBGRepo.cardbookCards[cbid]) {
							cardslist.push(cardbookBGRepo.cardbookCards[cbid]);
						}
						if (cardbookBGRepo.cardbookTempCards[cbid]) {
							cardslist.push(cardbookBGRepo.cardbookTempCards[cbid]);
						}
					}
				return cardslist;
			case "cardbook.initMultipleOperations":
				cardbookBGSynchronization.initMultipleOperations(cardbookBGRepo, info.dirPrefId);
				await cardbookBGRepo.notifyObservers("cardbook.syncRunning", info.dirPrefId);
				break;
			case "cardbook.getSupportedConnections":
				return cardbookBGRepo.supportedConnections;
			case "cardbook.getCardbookComplexSearch":
				return cardbookBGRepo.cardbookComplexSearch;
			case "cardbook.getCardbookServerValidation":
				return cardbookBGRepo.cardbookServerValidation;
			case "cardbook.getCardbookServerDiscoveryRequest":
				return cardbookBGRepo.cardbookServerDiscoveryRequest;
			case "cardbook.getCardbookServerDiscoveryResponse":
				return cardbookBGRepo.cardbookServerDiscoveryResponse;
			case "cardbook.getCardbookServerDiscoveryError":
				return cardbookBGRepo.cardbookServerDiscoveryError;
			case "cardbook.getCardbookRefreshTokenRequest":
				return cardbookBGRepo.cardbookRefreshTokenRequest;
			case "cardbook.getCardbookRefreshTokenResponse":
				return cardbookBGRepo.cardbookRefreshTokenResponse;
			case "cardbook.getCardbookRefreshTokenError":
				return cardbookBGRepo.cardbookRefreshTokenError;
			case "cardbook.saveEditionWindow": {
				let actionId = await cardbookBGRepo.saveEditionWindow(info.cardIn, info.cardOut, info.editionMode, info.action);
				if (actionId) {
					await endAction(actionId, true);
				}
				break;
			}
			case "cardbook.openWindow": {
				let url = browser.runtime.getURL(info.url) + "*";
				let tabs = await browser.tabs.query({url});
				if (tabs.length) {
					await browser.windows.update(tabs[0].windowId, {focused:true});
					return tabs[0].windowId;
				} else {
					let state = {};
					try {
						let winPath = info.url.split("?")[0];
						let winName = winPath.split("/").pop();
						let prefName = `window.${winName}.state`;
						state = cardbookBGPreferences.getPref(prefName);
					} catch(e) {}
					let winParams = { ...state, type: info.type, url: info.url, allowScriptsToClose: true};
					let win = await browser.windows.create(winParams);
					return win.id;
				}
			}
			case "cardbook.openTab": {
				let url = browser.runtime.getURL(info.url);
				let tabs = await browser.tabs.query({url});
				if (tabs.length) {
					await browser.tabs.update(tabs[0].id, {active: true});
					return tabs[0].id;
				} else {
					let tabParams = {url: url};
					let tab = await browser.tabs.create(tabParams);
					return tab.id;
				}
			}
		}
	};

	messenger.runtime.onMessage.addListener( (info) => {
		return onMessageListenerFunction(info);
	});

	// init WindowListener
	messenger.WindowListener.registerChromeUrl([ ["content", "cardbook", "chrome/content/"] ]);
	
	// register a script which is called upon add-on unload (to unload any JSM loaded via overlays)
	messenger.WindowListener.registerShutdownScript("chrome://cardbook/content/scripts/unload.js");
	
	// master password
	await messenger.WindowListener.waitForMasterPassword();
	
	// about message (contextual menus)
	messenger.WindowListener.registerWindow("about:message", "chrome://cardbook/content/XUL/AboutMessage/wl_cardbookAboutMessage.js");
	
	// about 3 panes (quick filter bar)
	messenger.WindowListener.registerWindow("about:3pane", "chrome://cardbook/content/XUL/About3Panes/wl_cardbookAbout3Pane.js");
	
	// support for CardBook, yellow stars, creation from emails, formatting email fields
	messenger.WindowListener.registerWindow("chrome://messenger/content/messenger.xhtml", "chrome://cardbook/content/XUL/messenger/wl_cardbookMessenger.js");

	// support for the message window
	messenger.WindowListener.registerWindow("chrome://messenger/content/messageWindow.xhtml", "chrome://cardbook/content/XUL/messenger/wl_cardbookMessenger.js");
	
	// support for Lightning attendees
	messenger.WindowListener.registerWindow("chrome://calendar/content/calendar-event-dialog-attendees.xhtml", "chrome://cardbook/content/XUL/calendar/wl_calendar.js");
	
	// support for Contacts sidebar
	// support for attaching a vCard
	// support for attaching lists
	// support for CardBook menu in composition window
	messenger.WindowListener.registerWindow("chrome://messenger/content/messengercompose/messengercompose.xhtml", "chrome://cardbook/content/XUL/composeMsg/wl_composeMsg.js");
	
	// // support for filter messages
	messenger.DomContentScript.registerWindow("chrome://messenger/content/FilterEditor.xhtml", "chrome://cardbook/content/XUL/filters/cardbookFilterPicker.js");
	messenger.DomContentScript.registerWindow("chrome://messenger/content/SearchDialog.xhtml", "chrome://cardbook/content/XUL/filters/cardbookFilterPicker.js");
	messenger.DomContentScript.registerWindow("chrome://messenger/content/mailViewSetup.xhtml", "chrome://cardbook/content/XUL/filters/cardbookFilterPicker.js");
	messenger.DomContentScript.registerWindow("chrome://messenger/content/virtualFolderProperties.xhtml", "chrome://cardbook/content/XUL/filters/cardbookFilterPicker.js");

	messenger.WindowListener.startListening();

	async function externalListener(message, sender, sendResponse) {
		if (message?.query) {
			switch (message.query) {
				// mailmerge@example.net
				case "mailmerge.getAddressBooks":
					return new Promise((resolve, reject) => {
						let addressbooks = cardbookBGRepo.cardbookAccounts;
						addressbooks = addressbooks.map((e) => { return { name: e[0], id: e[1], enabled: e[2], type: e[3], readonly: e[4] } });
						resolve(addressbooks);
					});
				case "mailmerge.getContacts":
					return new Promise((resolve, reject) => {
						let contacts = cardbookBGRepo.cardbookDisplayCards[message.id].cards;
						resolve(contacts);
					});
				// simplemailredirection@ggbs.de
				case "simpleMailRedirection.version":
					return simpleMailRedirection.version();
					break;
				case "simpleMailRedirection.getAddressBooks":
					return simpleMailRedirection.addressbooks(cardbookBGRepo);
					break;
				case "simpleMailRedirection.lists":
					return simpleMailRedirection.lists(cardbookBGRepo, message.id);  //all lists if exists, else emails of list with id
					break;
				case "simpleMailRedirection.contacts":
					return simpleMailRedirection.contacts(cardbookBGRepo, message.search, message.books);
					break;
				case "simpleMailRedirection.openBook":
					let m3p = Services.wm.getMostRecentWindow("mail:3pane");
					if (m3p) {
						let tabmail = m3p.document.getElementById("tabmail");
						if (tabmail) {
							//m3p.focus();
							tabmail.openTab("cardbook", { title: messenger.i18n.getMessage("cardbookTitle") });
						}
					}
					break;
				// smarttemplate4@thunderbird.extension
				case "smartTemplates.getContactsFromMail":
					for (let account of cardbookBGRepo.cardbookAccounts) {
						if (account[1] && account[2] && account[3] != "SEARCH") {
							let dirPrefId = account[1];
							if (message.dirPrefId) {
								if (dirPrefId != message.dirPrefId) {
									continue;
								}
							}
							if (cardbookBGRepo.cardbookCardEmails[dirPrefId]) {
								let mail = message.mail.toLowerCase();
								if (cardbookBGRepo.cardbookCardEmails[dirPrefId][mail]) {
									return cardbookBGRepo.cardbookCardEmails[dirPrefId][mail];
								}
							}
						}
					}
					return [];
				case "smartTemplates.getAccounts":
					return cardbookBGRepo.cardbookAccounts;
				case "smartTemplates.getContactsFromSearch":
					return new Promise(async (resolve, reject) => {
						if (!message.value) {
							resolve([]);
						}
						let field = message.field ? message.field : "fn";
						let term = message.term ? message.term : "Contains";
						let casse = message.case ? message.case : "dig";
						let searchAB = "allAddressBooks";
						let matchAll = true;
						let rules = [ { case: casse, field, term, value: message.value } ];
						let search = { searchAB , matchAll, rules };
						await cardbookIDBCard.openCardDB({});
						let [ cards, searchId ] = await cardbookIDBCard.getCards("1", "1", search, "", 1);
						resolve(cards);
					});
				case "myphoneexplorer.getAddressBooks": {
					let t = cardbookBGPreferences.getAllPrefIds();
					for (let i = 0; i < t.length; i++) {
						if (cardbookBGPreferences.getType(t[i]) == "SEARCH"){
							t[i] = '';
						} else{
							let u = cardbookBGPreferences.getUrl(t[i]);
							if (u != ""){u = "\t" + u;}
							t[i] = "Cardbook - " + cardbookBGPreferences.getName(t[i]) + "\t" + "cardbook-" + t[i] + u + "\r\n";
						}
					}
					return t;
				}
				case "myphoneexplorer.refreshCBView":
					await cardbookBGRepo.notifyObservers("cardbook.syncFinished", message.dirPrefId);
					break;
				case "myphoneexplorer.getContacts": {
					return new Promise(async (resolve, reject) => {
						if (!message.dirPrefId) {
							resolve([]);
						}
						await cardbookIDBCard.openCardDB({});
						let [ cards, searchId ] = await cardbookIDBCard.getCards(message.dirPrefId, message.dirPrefId, "", "", 1);

						let results = [];
						for (let card of cards) {
							let vCard = await cardbookBGUtils.cardToVcardData({}, card); 
							vCard = vCard.replace(/\nUID:/g,"\nUID:" + message.dirPrefId + "|");
							results.push(vCard);
						}
						resolve(results);
					});
				}
				case "myphoneexplorer.addContact": {
					let newcard = new cardbookCardParser(message.vCard, "", "", message.dirPrefId);
					let actionId = cardbookBGActions.startAction(cardbookBGRepo, "cardCreated", [newcard.fn], message.dirPrefId);
					await cardbookBGRepo.saveCardFromUpdate({}, newcard, actionId, true);
					return actionId;
				}
				case "myphoneexplorer.modifyContact": {
					if (!cardbookBGRepo.cardbookCards[message.dirPrefId+"::"+message.uid]) {
						return
					}
					let oldcard = cardbookBGRepo.cardbookCards[message.dirPrefId+"::"+message.uid];
					let newcard = new cardbookCardParser(message.vCard, "", "", message.dirPrefId);
					newcard.uid = message.uid;
					let actionId = cardbookBGActions.startAction(cardbookBGRepo, "cardModified", [newcard.fn], message.dirPrefId);
					await cardbookBGRepo.saveCardFromUpdate(oldcard, newcard, actionId, true);
					return actionId;
				}
				case "myphoneexplorer.deleteContact": {
					if (!cardbookBGRepo.cardbookCards[message.dirPrefId+"::"+message.uid]) {
						return
					}
					let cards = [];
					cards.push(cardbookBGRepo.cardbookCards[message.dirPrefId+"::"+message.uid]);
					let actionId = cardbookBGActions.startAction(cardbookBGRepo, "cardsDeleted", cards, null, cards.length, cards.length);
					await cardbookBGRepo.asyncDeleteCards(cards, actionId);
					await endAction(actionId, true);
					}
			}
			return {};
		}
	}
	messenger.runtime.onMessageExternal.addListener(externalListener);

	messenger.compose.onBeforeSend.addListener(async (tab, details) => {
		for (let field of ["to", "cc", "bcc"]) {
			if (details[field].length) {
				for (let address of details[field]) {
					let actionId = await cardbookBGCollection.addCollectedContact(cardbookBGRepo, details.identityId, address);
					if (actionId) {
						await endAction(actionId, true);
					}
					await cardbookBGCollection.addCollectedPopularity(cardbookBGRepo, address);
				}
			}
		}

		let vCards = await cardbookBGAttachVCards.getVCards(cardbookBGRepo, details.identityId);
		for (let vCard of vCards) {
			let blob = new Blob([vCard.vCard], {type: "text;charset=utf-8"});
			let file = new File([blob], vCard.filename);
			await messenger.compose.addAttachment(tab.id, {file: file, name: vCard.filename});
		}
	});

	messenger.compose.onIdentityChanged.addListener(async (tab, identityId) => {
		messenger.NotifyTools.notifyExperiment({query: "cardbook.identityChanged", windowId: tab.windowId, identityId: identityId});
	});

	browser.browserAction.onClicked.addListener(() => {
		openCBWindow();
	});

	browser.composeAction.onClicked.addListener(() => {
		openCBWindow();
	});

	messenger.messageDisplay.onMessagesDisplayed.addListener(async (tab, message) => {
		function addSubMenus(aMenuId, aABs) {
			for (let account of aABs) {
				let menuProps = {
					onclick: async (event) => {
						let subMenuId = event.menuItemId;
						let parentMenuId = event.parentMenuItemId;
						let dirPrefId = subMenuId.replace(`${parentMenuId}-`, "")
						if (event.linkUrl && event.linkUrl.includes("mailto:")) {
							let email = event.linkUrl.replace("mailto:", "");
							let fn = event.linkText;
							let card = new cardbookCardParser("", "", "", dirPrefId);
							let addEmail = {fn : fn, email: email.toLowerCase()};
							if (fn == email) {
								addEmail.fn = email.substr(0, email.indexOf("@")).replace("."," ").replace("_"," ");
							}
							let fnArray = addEmail.fn.split(" ");
							if (fnArray.length > 1) {
								addEmail.lastname = fnArray[fnArray.length - 1];
								let removed = fnArray.splice(fnArray.length - 1, 1);
								addEmail.firstname = fnArray.join(" ");
							}
							await openEditionWindow(card, "AddEmail", null, addEmail);
						} else if (event.attachments) {
							for (let attachment of event.attachments) {
								await messenger.NotifyTools.notifyExperiment({query: "cardbook.addAttachments", attachment: attachment, dirPrefId: dirPrefId});
							}
						}
					},
					enabled: true,
					id: `${aMenuId}-${account[1]}`,
					parentId: aMenuId,
					title: account[0]
				}
				messenger.menus.create(menuProps);
			}
		}

		let tabs = await browser.tabs.query({type: "mail"});
		let displayedMessages = await browser.messageDisplay.getDisplayedMessages(tabs[0].id);
		let displayedMessage = displayedMessages[0];
		let accountId = displayedMessage.folder.accountId;
		let accounts = await messenger.accounts.list();
		let identity = accounts.filter(x => x.id == accountId)[0].identities[0]?.email;

		let ABInclRestrictions = {};
		let ABExclRestrictions = {};
		let catInclRestrictions = {};
		let catExclRestrictions = {};
		function _loadRestrictions(aIdentityKey) {
			let result = [];
			result = cardbookBGPreferences.getAllRestrictions();
			ABInclRestrictions = {};
			ABExclRestrictions = {};
			catInclRestrictions = {};
			catExclRestrictions = {};
			if (!aIdentityKey) {
				ABInclRestrictions["length"] = 0;
				return;
			}
			for (var i = 0; i < result.length; i++) {
				var resultArray = result[i];
				if ((resultArray[0] == "true") && (resultArray[3] != "") && ((resultArray[2] == aIdentityKey) || (resultArray[2] == "allMailAccounts"))) {
					if (resultArray[1] == "include") {
						ABInclRestrictions[resultArray[3]] = 1;
						if (resultArray[4]) {
							if (!(catInclRestrictions[resultArray[3]])) {
								catInclRestrictions[resultArray[3]] = {};
							}
							catInclRestrictions[resultArray[3]][resultArray[4]] = 1;
						}
					} else {
						if (resultArray[4]) {
							if (!(catExclRestrictions[resultArray[3]])) {
								catExclRestrictions[resultArray[3]] = {};
							}
							catExclRestrictions[resultArray[3]][resultArray[4]] = 1;
						} else {
							ABExclRestrictions[resultArray[3]] = 1;
						}
					}
				}
			}
			ABInclRestrictions["length"] = cardbookBGUtils.sumElements(ABInclRestrictions);
		};
		_loadRestrictions(identity);
		let ABsWithIdentity = [];
		for (let account of cardbookBGRepo.cardbookAccounts) {
			if (account[2] && !account[4] && (account[3] != "SEARCH")) {
				let name = account[0];
				let dirPrefId = account[1];
				if (cardbookBGRepo.verifyABRestrictions(dirPrefId, "allAddressBooks", ABExclRestrictions, ABInclRestrictions)) {
					ABsWithIdentity.push([name, dirPrefId]);
				}
			}
		}
		cardbookHTMLUtils.sortMultipleArrayByString(ABsWithIdentity,0,1);

		let menuId = "addToCardBookMenu";
		let menuAttId = "addAttToCardBookMenu";
		let menuAllAttId = "addAllAttToCardBookMenu";
		messenger.menus.remove(menuId);
		messenger.menus.remove(menuAttId);
		messenger.menus.remove(menuAllAttId);
		if (!ABsWithIdentity.length) {
			return;
		}

		// add contacts for links
		let menuLabel = messenger.i18n.getMessage("addToCardBookMenuLabel");
		let menuProps = {
			contexts: ["link"],
			icons: "chrome/content/skin/cardbook_16_16.svg",
			enabled: true,
			id: menuId,
			title: menuLabel
		}
		messenger.menus.create(menuProps);
		addSubMenus(menuId, ABsWithIdentity);

		// add attachment import
		let attachments = await browser.messages.listAttachments(displayedMessage.id);
		let interestingAtt = attachments.some(x => {
			let extension = x.name.split(".").pop();
			return (extension.toLowerCase() == "vcf")
		});
		if (!interestingAtt) {
			return;
		}

		let menuAttLabel = messenger.i18n.getMessage("addAttachementToCardBookMenuLabel");
		let menuAttProps = {
			contexts: ["message_attachments"],
			icons: "chrome/content/skin/cardbook_16_16.svg",
			enabled: true,
			id: menuAttId,
			title: menuAttLabel
		}
		messenger.menus.create(menuAttProps);
		addSubMenus(menuAttId, ABsWithIdentity);

		let menuAllAttLabel = messenger.i18n.getMessage("addAllAttachementsToCardBookMenuLabel");
		let menuAllAttProps = {
			contexts: ["all_message_attachments"],
			icons: "chrome/content/skin/cardbook_16_16.svg",
			enabled: true,
			id: menuAllAttId,
			title: menuAllAttLabel
		}
		messenger.menus.create(menuAllAttProps);
		addSubMenus(menuAllAttId, ABsWithIdentity);
	});

	// update on Thunderbird restart
	messenger.runtime.onUpdateAvailable.addListener(async () => { /* nothing */ });


	// custom columns part
	// Register the onCellEntriesShown listener before adding the column, to make
	// sure we are not missing the initial event.
	messenger.cc.onCellEntriesShown.addListener(async (columnId, entries) => {
		let cellUpdates = [];
		for (let entry of entries) {
			cellUpdates.push(await cardbookBGColumns.getCustomColumnCellValue(columnId, entry.messageHeader, cardbookBGRepo));
		}
		await messenger.cc.setCellData(columnId, cellUpdates)
	})

	// Force an update of the threadPane, after a folder has been changed. This
	// is optional and helps minimizing showing empty cells on scroll, but might
	// have a performance impact on large folders.
	messenger.mailTabs.onDisplayedFolderChanged.addListener(async (tab, folder) => {
		cardbookBGColumns.updateFolderDisplay(tab, folder, cardbookBGRepo);
	});

	// test messenger.runtime.onMessage.addListener((msg, sender, sendResponse)=>{
	// test   sendResponse();
	// test   if (msg=="prefsChanged") doRefresh();
	// test });

	messenger.accounts.onCreated.addListener( () => cardbookBGColumns.doRefresh(cardbookBGRepo) );
	messenger.accounts.onDeleted.addListener( () => cardbookBGColumns.doRefresh(cardbookBGRepo) );
	messenger.accounts.onUpdated.addListener( () => cardbookBGColumns.doRefresh(cardbookBGRepo) );
	messenger.identities.onCreated.addListener( () => cardbookBGColumns.doRefresh(cardbookBGRepo) );
	messenger.identities.onDeleted.addListener( () => cardbookBGColumns.doRefresh(cardbookBGRepo) );
	messenger.identities.onUpdated.addListener( () => cardbookBGColumns.doRefresh(cardbookBGRepo) );


	/*messenger.addressBooks.provider.onSearchRequest.addListener(
		async (node, searchString, query) => {
			return {
				isCompleteResult: true,
				// Return an array of ContactProperties as results.
				results: await messenger.runtime.sendMessage({query: "cardbook.provider", searchString: searchString})
			};
		},
		{
			addressBookName: "CardBook",
			isSecure: true,
		}
	);*/

	async function openCBWindow() {
		let sourceUrl = "chrome/content/HTML/mainWindow/wdw_mainWindow.html";
		let url = browser.runtime.getURL(sourceUrl);
		let tabs = await browser.tabs.query({url});
		if (tabs.length) {
			await browser.tabs.update(tabs[0].id, {active: true});
			return tabs[0].id;
		} else {
			let tabParams = {url: url};
			let tab = await browser.tabs.create(tabParams);
			return tab.id;
		}
	};

	async function openEditionWindow(aCard, aMode, aCardContent, aAddEmail, aIds) {
		let sourceUrl = "chrome/content/HTML/cardEdition/wdw_cardEdition.html";
		let params = new URLSearchParams();
		if (aCard) {
			params.set("cbIdIn", aCard.cbid);
		}
		if (aAddEmail) {
			if (aAddEmail.fn) {
				params.set("fn", aAddEmail.fn);
			}
			if (aAddEmail.lastname) {
				params.set("lastname", aAddEmail.lastname);
			}
			if (aAddEmail.firstname) {
				params.set("firstname", aAddEmail.firstname);
			}
			if (aAddEmail.email) {
				params.set("email", aAddEmail.email);
			}
		}
		if (aCard.categories.length == 1) {
			params.set("category", aCard.categories[0]);
		}
		if (aIds) {
			params.set("ids", aIds);
		}
		params.set("editionMode", aMode);
		if (aCardContent) {
			params.set("cardContent", aCardContent);
		}

		let url = `${sourceUrl}?${params.toString()}`;
		let finalUrl = browser.runtime.getURL(url) + "*";
		let tabs = await browser.tabs.query({url: finalUrl});
		if (tabs.length) {
			await browser.windows.update(tabs[0].windowId, {focused:true});
			return tabs[0].windowId;
		} else {
			let state = {};
			try {
				let winPath = url.split("?")[0];
				let winName = winPath.split("/").pop();
				let prefName = `window.${winName}.state`;
				state = cardbookBGPreferences.getPref(prefName);
			} catch(e) {}
			let winParams = { ...state, type: "popup", url: url, allowScriptsToClose: true};
			let win = await browser.windows.create(winParams);
			return win.id;
		}
	};

	async function importCard(aCard, aTarget, aTargetVersion, aDateFormatSource, aDateFormatTarget, aActionId, aDefaultChoice) {
		let targetPrefId = cardbookBGUtils.getAccountId(aTarget);
		let targetName = cardbookBGPreferences.getName(targetPrefId);

		let choice = cardbookBGRepo.importConflictChoiceImportValues.join("::");
		if (!cardbookBGRepo.importConflictChoice[aActionId]) {
			cardbookBGRepo.importConflictChoice[aActionId] = {};
		}
		if (!cardbookBGRepo.importConflictChoice[aActionId][choice]) {
			cardbookBGRepo.importConflictChoice[aActionId][choice] = {};
		}
		if (aDefaultChoice) {
			cardbookBGRepo.importConflictChoice[aActionId][choice].result = aDefaultChoice;
		}
		let newCard = new cardbookCardParser();
		await cardbookBGUtils.cloneCard(aCard, newCard);
		newCard.dirPrefId = targetPrefId;

		// conversion ?
		let result = await cardbookHTMLUtils.convertVCard(newCard, targetName, aTargetVersion, aDateFormatSource, aDateFormatTarget, cardbookBGRepo.possibleCustomFields);
		cardbookBGRepo.writePossibleCustomFields(result.possibleCustomFields);
		await cardbookBGUtils.changeMediaFromFileToContent(newCard);

		let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
		let nodeType = "";
		let nodeName = "";
		let sepPosition = aTarget.indexOf("::",0);
		if (sepPosition != -1) {
			let nodeArray = aTarget.split("::");
			nodeType = nodeArray[1];
			nodeName = nodeArray[nodeArray.length-1];
			if (nodeType == "categories") {
				if (nodeName != uncat) {
					cardbookHTMLUtils.addCategoryToCard(newCard, nodeName);
				} else {
					newCard.categories = [];
				}
			} else if (nodeType == "org") {
				if (nodeName != uncat) {
					nodeArray.shift();
					nodeArray.shift();
					newCard.org = JSON.parse(JSON.stringify(nodeArray));
				} else {
					newCard.org = [];
				}
			}
		}

		if (cardbookBGRepo.cardbookCards[`${targetPrefId}::${newCard.uid}`]) {
			let message = messenger.i18n.getMessage("cardAlreadyExists", [targetName, newCard.fn]);
			let conflictResult = await cardbookBGRepo.askUser("card", aActionId, message, cardbookBGRepo.importConflictChoiceImportValues);
			switch (conflictResult) {
				case "cancel":
				case "keep":
					cardbookBGRepo.cardbookServerCardSyncDone[targetPrefId]++;
					break;
				case "duplicate":
					newCard.cardurl = "";
					newCard.fn = newCard.fn + " " + messenger.i18n.getMessage("fnDuplicatedMessage");
					cardbookBGUtils.setCardUUID(newCard);
					await cardbookBGRepo.saveCardFromUpdate({}, newCard, aActionId, true);
					cardbookBGRepo.cardbookServerCardSyncDone[targetPrefId]++;
					break;
				case "write":
					await cardbookBGRepo.saveCardFromMove({}, newCard, aActionId, true);
					cardbookBGRepo.cardbookServerCardSyncDone[targetPrefId]++;
					break;
				case "update":
					if (cardbookBGRepo.cardbookCards[`${targetPrefId}::${newCard.uid}`]) {
						let targetCard = cardbookBGRepo.cardbookCards[`${targetPrefId}::${newCard.uid}`];
						await cardbookBGRepo.saveCardFromMove(targetCard, newCard, aActionId, true);
					} else {
						await cardbookBGRepo.saveCardFromMove({}, newCard, aActionId, true);
					}
					cardbookBGRepo.cardbookServerCardSyncDone[targetPrefId]++;
					break;
				case "merge":
					cardbookBGRepo.cardbookServerGetCardForMergeRequest[targetPrefId]++;
					let targetCard = cardbookBGRepo.cardbookCards[`${targetPrefId}::${newCard.uid}`];
					await cardbookBGRepo.mergeCardsFromSync(targetCard, newCard, null, null, "IMPORT", aActionId);
					break;
				default:
					break;
				}
		} else {
			let targetCard = {};
			if (cardbookBGRepo.cardbookCards[`${targetPrefId}::${newCard.uid}`]) {
				targetCard = cardbookBGRepo.cardbookCards[`${targetPrefId}::${newCard.uid}`];
			}
			await cardbookBGRepo.saveCardFromMove(targetCard, newCard, aActionId, true);
			cardbookBGRepo.cardbookServerCardSyncDone[targetPrefId]++;
		}

		// inside same account to a category
		if (aTarget != aCard.dirPrefId && nodeType == "categories") {
			if (nodeName && nodeName != uncat) {
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "cardAddedToCategory", [targetName, newCard.fn, nodeName]);
			} else if (nodeName && nodeName == uncat) {
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "cardDeletedFromAllCategory", [targetName, newCard.fn]);
			}
		}
	};

	async function deleteCardsAndValidate(aListOfCardsId) {
		let confirmTitle = messenger.i18n.getMessage("confirmTitle");
		let cardsCount = aListOfCardsId.length;
		if (!cardsCount) {
			return
		}
		let confirmMsg = cardbookHTMLPluralRules.getPluralMessage("selectedCardsDeletionConfirmMessagePF", cardsCount);
		let sourceUrl = "chrome/content/HTML/alertUser/wdw_cardbookAlertUser.html";
		let params = new URLSearchParams();
		params.set("type", "confirm");
		params.set("action", "deleteCard");
		params.set("winId", "background");
		params.set("lineId", aListOfCardsId.join(","));
		params.set("title", confirmTitle);
		params.set("message", confirmMsg);

		let url = `${sourceUrl}?${params.toString()}`;
		let finalUrl = browser.runtime.getURL(url) + "*";
		let tabs = await browser.tabs.query({url: finalUrl});
		if (tabs.length) {
			await browser.windows.update(tabs[0].windowId, {focused:true});
			return tabs[0].windowId;
		} else {
			let state = {};
			try {
				let winPath = url.split("?")[0];
				let winName = winPath.split("/").pop();
				let prefName = `window.${winName}.state`;
				state = cardbookBGPreferences.getPref(prefName);
			} catch(e) {}
			let winParams = { ...state, type: "popup", url: url, allowScriptsToClose: true};
			let win = await browser.windows.create(winParams);
		}
	};

	async function removeNode(aNode) {
		let tmpArray = aNode.split("::");
		let dirPrefId = tmpArray[0];
		let nodeType = tmpArray[1];
		let nodeName = tmpArray[2];
		if (nodeName == cardbookBGPreferences.getPref("uncategorizedCards") || cardbookBGPreferences.getReadOnly(dirPrefId)) {
			return;
		}
		let cardsCount = cardbookBGRepo.cardbookDisplayCards[aNode].cards.length;
		let confirmTitle = messenger.i18n.getMessage("confirmTitle");
		let message = (nodeType != "categories") ? "nodeDeletionsConfirmMessagePF" : "catDeletionsConfirmMessagePF";
		let confirmMsg = cardbookHTMLPluralRules.getPluralMessage(message, cardsCount, [cardsCount, nodeName]);

		let sourceUrl = "chrome/content/HTML/alertUser/wdw_cardbookAlertUser.html";
		let params = new URLSearchParams();
		params.set("type", "confirm");
		params.set("action", "removeNode");
		params.set("winId", "background");
		params.set("lineId", aNode);
		params.set("title", confirmTitle);
		params.set("message", confirmMsg);

		let url = `${sourceUrl}?${params.toString()}`;
		let finalUrl = browser.runtime.getURL(url) + "*";
		let tabs = await browser.tabs.query({url: finalUrl});
		if (tabs.length) {
			await browser.windows.update(tabs[0].windowId, {focused:true});
			return tabs[0].windowId;
		} else {
			let state = {};
			try {
				let winPath = url.split("?")[0];
				let winName = winPath.split("/").pop();
				let prefName = `window.${winName}.state`;
				state = cardbookBGPreferences.getPref(prefName);
			} catch(e) {}
			let winParams = { ...state, type: "popup", url: url, allowScriptsToClose: true};
			let win = await browser.windows.create(winParams);
		}
	};

	async function renameNode(aNode) {
		let tmpArray = aNode.split("::");
		let dirPrefId = tmpArray[0];
		let nodeType = tmpArray[1];
		let nodeName = tmpArray[tmpArray.length-1];
		if (cardbookBGPreferences.getReadOnly(dirPrefId)) {
			return;
		}
		let lowerCat = nodeName.toLowerCase();
		let mailPop = (cardbookBGRepo.cardbookMailPopularityIndex[lowerCat]) ? cardbookBGRepo.cardbookMailPopularityIndex[lowerCat].count : 0;
		let context = (nodeType == "categories") ? "EditCat" : "EditNode";
		let sourceUrl = "chrome/content/HTML/renameField/wdw_cardbookRenameField.html";
		let params = new URLSearchParams();
		params.set("dirPrefId", dirPrefId);
		params.set("type", nodeType);
		params.set("context", context);
		params.set("showColor", "true");
		let nodeColors = cardbookBGPreferences.getNodeColors();
		if (nodeColors[nodeName]) {
			params.set("color", nodeColors[nodeName]);
		}
		params.set("name", nodeName);
		params.set("id", aNode);
		params.set("mailpop", mailPop);

		let url = `${sourceUrl}?${params.toString()}`;
		let finalUrl = browser.runtime.getURL(url) + "*";
		let tabs = await browser.tabs.query({url: finalUrl});
		if (tabs.length) {
			await browser.windows.update(tabs[0].windowId, {focused:true});
			return tabs[0].windowId;
		} else {
			let state = {};
			try {
				let winPath = url.split("?")[0];
				let winName = winPath.split("/").pop();
				let prefName = `window.${winName}.state`;
				state = cardbookBGPreferences.getPref(prefName);
			} catch(e) {}
			let winParams = { ...state, type: "popup", url: url, allowScriptsToClose: true};
			let win = await browser.windows.create(winParams);
		}
	};

	async function createAddressbook(aAccount) {
		if (aAccount.type === "SEARCH") {
			aAccount.search.dirPrefId = aAccount.dirPrefId;
			await cardbookIDBSearch.addSearch(cardbookBGRepo.statusInformation, aAccount.search);
			await cardbookBGRepo.addAccountToRepository(aAccount.dirPrefId, aAccount.name, aAccount.type, "", aAccount.username, aAccount.color,
														aAccount.enabled, aAccount.vcard, aAccount.dateFormat, false, false,
														"", aAccount.DBcached, false, "0", true);
			await cardbookBGSynchronization.loadComplexSearchAccount(cardbookBGRepo, aAccount.dirPrefId, false, aAccount.search);
		} else if (cardbookBGUtils.isMyAccountRemote(aAccount.type)) {
			await cardbookBGRepo.addAccountToRepository(aAccount.dirPrefId, aAccount.name, aAccount.type, aAccount.url, aAccount.username, aAccount.color,
														true, aAccount.vcard, aAccount.dateFormat, aAccount.readonly, aAccount.urnuuid,
														aAccount.sourceDirPrefId, aAccount.DBcached, true, "60", true);
			cardbookBGSynchronization.syncAccount(cardbookBGRepo, aAccount.dirPrefId);	
		} else if (aAccount.type === "STANDARD") {
			if (aAccount.collected) {
				await cardbookBGRepo.addAccountToCollected(aAccount.dirPrefId);
			}
			await cardbookBGRepo.addAccountToRepository(aAccount.dirPrefId, aAccount.name, "LOCALDB", "", aAccount.username, aAccount.color,
														true, aAccount.vcard, aAccount.dateFormat, aAccount.readonly, false,
														"", aAccount.DBcached, true, "60", true);
			cardbookBGSynchronization.initMultipleOperations(cardbookBGRepo, aAccount.dirPrefId);
			let kindCustom = cardbookBGPreferences.getPref("kindCustom");
			let memberCustom = cardbookBGPreferences.getPref("memberCustom");
			let dateFormat = cardbookHTMLUtils.getDateFormat(aAccount.dirPrefId, aAccount.vcard);
			await messenger.NotifyTools.notifyExperiment({query: "cardbook.importCards", sourceId: aAccount.sourceDirPrefId, targetId: aAccount.dirPrefId, version: aAccount.vcard, dateFormat: dateFormat, kindCustom: kindCustom, memberCustom: memberCustom});
			cardbookBGSynchronization.finishMultipleOperations(cardbookBGRepo, aAccount.dirPrefId);
			await cardbookBGRepo.notifyObservers("cardbook.syncFinished", info.dirPrefId);
			// if the first proposed import of standard address books is finished OK
			// then set CardBook as exclusive
			if (aAccount.firstAction) {
				await cardbookBGPreferences.setPref("exclusive", true);
			}
		} else if (aAccount.type === "LOCALDB") {
			await cardbookBGRepo.addAccountToRepository(aAccount.dirPrefId, aAccount.name, aAccount.type, "", aAccount.username, aAccount.color,
														true, aAccount.vcard, aAccount.dateFormat, aAccount.readonly, false,
														"", aAccount.DBcached, true, "60", true);
		} else if (aAccount.type === "FILE" || aAccount.type === "DIRECTORY") {
			await cardbookBGRepo.addAccountToRepository(aAccount.dirPrefId, aAccount.name, aAccount.type, aAccount.filepath, aAccount.username, aAccount.color,
														true, aAccount.vcard, aAccount.dateFormat, aAccount.readonly, false,
														"", aAccount.DBcached, true, "60", true);
			await cardbookBGSynchronization.loadAccount(cardbookBGRepo, aAccount.dirPrefId, false, false, false);
		}
		cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "addressbookCreated", [aAccount.name]);
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.addActivity", code: "addressbookCreated", array: [aAccount.name], icon: "addItem"});
		await cardbookBGRepo.notifyObservers("addressbookCreated", aAccount.dirPrefId);
	};

	async function modifyAddressbook(aAccount) {
		let changed = false;
		let activityAdded = false;
		for (let account of cardbookBGRepo.cardbookAccounts) {
			if (account[1] === aAccount.dirPrefId) {
				if (aAccount.name != account[0]) {
					await cardbookBGPreferences.setName(aAccount.dirPrefId, aAccount.name);
					account[0] = aAccount.name;
					changed = true;
				}
				if (aAccount.readonly != account[4]) {
					await cardbookBGPreferences.setReadOnly(aAccount.dirPrefId, aAccount.readonly);
					account[4] = aAccount.readonly;
					if (aAccount.readonly) {
						await cardbookBGRepo.removeAccountFromCollected(aAccount.dirPrefId);
						cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "addressbookReadOnly", [aAccount.name]);
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.addActivity", code: "addressbookReadOnly", array: [aAccount.name], icon: "editItem"});
					} else {
						cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "addressbookReadWrite", [aAccount.name]);
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.addActivity", code: "addressbookReadWrite", array: [aAccount.name], icon: "editItem"});
					}
					activityAdded = true;
					changed = true;
				}
				if (aAccount.enabled != account[2]) {
					await cardbookBGPreferences.setEnabled(aAccount.dirPrefId, aAccount.enabled);
					account[2] = aAccount.enabled;
					await cardbookBGRepo.enableOrDisableAccountFromCollected(aAccount.dirPrefId, aAccount.enabled);
					await cardbookBGRepo.enableOrDisableAccountFromRestrictions(aAccount.dirPrefId, aAccount.enabled);
					await cardbookBGRepo.enableOrDisableAccountFromBirthday(aAccount.dirPrefId, aAccount.enabled);
					await cardbookBGRepo.enableOrDisableAccountFromVCards(aAccount.dirPrefId, aAccount.enabled);
					if (!aAccount.enabled) {
						await cardbookBGRepo.removeAccountFromDiscovery(aAccount.dirPrefId);
					}
					let type = cardbookBGPreferences.getType(aAccount.dirPrefId);
					cardbookBGSynchronization.setPeriodicSyncs(cardbookBGRepo, aAccount.dirPrefId);
					if (aAccount.enabled) {
						if (type == "SEARCH") {
							await cardbookBGSynchronization.loadComplexSearchAccount(cardbookBGRepo, aAccount.dirPrefId, false);
						} else {
							await cardbookBGSynchronization.loadAccount(cardbookBGRepo, aAccount.dirPrefId, true, false);
						}
						cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "addressbookEnabled", [aAccount.name]);
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.addActivity", code: "addressbookEnabled", array: [aAccount.name], icon: "editItem"});
					} else {
						cardbookBGSynchronization.initMultipleOperations(cardbookBGRepo, aAccount.dirPrefId);
						if (type != "SEARCH") {
							await cardbookBGRepo.removeAccountFromComplexSearch(aAccount.dirPrefId, false);
							await cardbookBGRepo.emptyAccountFromRepository(aAccount.dirPrefId, false);
						} else {
							await cardbookBGRepo.emptyComplexSearchFromRepository(aAccount.dirPrefId, false);
						}
						cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "addressbookDisabled", [aAccount.name]);
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.addActivity", code: "addressbookDisabled", array: [aAccount.name], icon: "editItem"});
					}
					activityAdded = true;
					changed = true;
				}
				if ("color" in aAccount) {
					await cardbookBGPreferences.setColor(aAccount.dirPrefId, aAccount.color);
					changed = true;
				}
				if ("node" in aAccount) {
					await cardbookBGPreferences.setNode(aAccount.dirPrefId, aAccount.node);
					await changeOrgForAddressBook(aAccount.dirPrefId);
					changed = true;
				}
		
				if (changed) {
					cardbookHTMLUtils.sortMultipleArrayByString(cardbookBGRepo.cardbookAccounts,0,1);
					if (!activityAdded){
						cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "addressbookModified", [aAccount.name]);
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.addActivity", code: "addressbookModified", array: [aAccount.name], icon: "editItem"});
					}
					await cardbookBGRepo.notifyObservers("addressbookModified", aAccount.dirPrefId);
				}
				break;
			}
		}
		cardbookBGSynchronization.finishMultipleOperations(cardbookBGRepo, aAccount.dirPrefId);
		await cardbookBGRepo.notifyObservers("cardbook.syncFinished", aAccount.dirPrefId);
	};

	async function changeOrgForAddressBook(aDirPrefId) {
		let allDirPrefIds = cardbookBGPreferences.getAllPrefIds();
		if (aDirPrefId) {
			allDirPrefIds = allDirPrefIds.filter(x  => x == aDirPrefId);
		}
		for (let dirPrefId of allDirPrefIds) {
			let node = cardbookBGPreferences.getNode(dirPrefId);
			for (let card of cardbookBGRepo.cardbookDisplayCards[dirPrefId].cards) {
				await cardbookBGRepo.removeCardFromOrg(card, dirPrefId, true);
				if (node == "org") {
					await cardbookBGRepo.addCardToOrg(card, dirPrefId, true);
				}
			}
		}
	};

	async function modifySearchAddressbook(aAccount) {
		if (cardbookBGPreferences.getName(aAccount.dirPrefId) == "") {
			return
		}
		await cardbookBGPreferences.setName(aAccount.dirPrefId, aAccount.name);
		await cardbookBGPreferences.setColor(aAccount.dirPrefId, aAccount.color);
		await cardbookBGPreferences.setVCardVersion(aAccount.dirPrefId, aAccount.vcard);
		await cardbookBGPreferences.setReadOnly(aAccount.dirPrefId, aAccount.readonly);
		await cardbookBGPreferences.setUrnuuid(aAccount.dirPrefId, aAccount.urnuuid);
		for (let account of cardbookBGRepo.cardbookAccounts) {
			if (account[1] === aAccount.dirPrefId) {
				account[0] = aAccount.name;
				account[4] = aAccount.readonly;
				break;
			}
		}
		aAccount.search.dirPrefId = aAccount.dirPrefId;
		await cardbookIDBSearch.addSearch(cardbookBGRepo.statusInformation, aAccount.search);
		cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "addressbookModified", [aAccount.name]);
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.addActivity", code: "addressbookModified", array: [aAccount.name], icon: "editItem"});
		await cardbookBGRepo.notifyObservers("addressbookModified", aAccount.dirPrefId);

		await cardbookBGRepo.emptyComplexSearchFromRepository(aAccount.dirPrefId, true);
		await cardbookBGSynchronization.loadComplexSearchAccount(cardbookBGRepo, aAccount.dirPrefId, false, aAccount.search);
	};

	async function createCategory(aCategoryData) {
		if (cardbookBGRepo.cardbookAccountsCategories[aCategoryData.dirPrefId].includes(aCategoryData.name)) {
			return
		}
		let actionId = cardbookBGActions.startAction(cardbookBGRepo, "categoryCreated", [aCategoryData.name], `${aCategoryData.dirPrefId}::categories::${aCategoryData.name}`);
		let newCat = new cardbookCategoryParser(aCategoryData.name, aCategoryData.dirPrefId);
		cardbookBGUtils.addTagCreated(newCat);
		await cardbookBGRepo.saveCategory({}, newCat, actionId);
		await endAction(actionId, true);
		if (aCategoryData.color) {
			let nodeColors = cardbookBGPreferences.getNodeColors();
			nodeColors[aCategoryData.name] = aCategoryData.color;
			await cardbookBGPreferences.setNodeColors(nodeColors);
		}
		if (aCategoryData.mailpop) {
			let value = parseInt(aCategoryData.mailpop) || 0;
			cardbookIDBMailPop.updateMailPop(cardbookBGRepo, aCategoryData.name, value);
		}
	};

	async function modifyNode(aNodeData) {
		let uncategorized = (aNodeData.oldName == cardbookBGPreferences.getPref("uncategorizedCards"));
		let nameChanged = aNodeData.oldName != aNodeData.name;
		let colorChanged = aNodeData.oldColor != aNodeData.color;
		let mailpopChanged = aNodeData.oldMailpop != aNodeData.mailpop;

		let topic = (aNodeData.type != "categories") ? "nodeRenamed" : "categoryRenamed";
		let actionId = cardbookBGActions.startAction(cardbookBGRepo, topic, [aNodeData.oldName], `${aNodeData.dirPrefId}::${aNodeData.type}::${aNodeData.name}`);
		if (nameChanged) {
			let cards = JSON.parse(JSON.stringify(cardbookBGRepo.cardbookDisplayCards[aNodeData.id].cards));
			let allDirPrefIds = Array.from(cards, item => item.dirPrefId);
			// aNodeData.dirPrefId might be a complex search
			allDirPrefIds.push(aNodeData.dirPrefId);
			allDirPrefIds = cardbookHTMLUtils.arrayUnique(allDirPrefIds);
			if (aNodeData.type == "categories" && !uncategorized) {
				async function saveCat(aDirPrefId1) {
					let oldCat = cardbookBGRepo.cardbookCategories[`${aDirPrefId1}::${aNodeData.oldName}`];
					let newCat = new cardbookCategoryParser();
					cardbookBGUtils.cloneCategory(oldCat, newCat);
					newCat.cbid = `${aNodeData.dirPrefId}::${aNodeData.name}`;
					newCat.name = aNodeData.name;
					if (newCat.uid == aNodeData.oldName) {
						newCat.uid = aNodeData.name;
					}
					await cardbookBGRepo.saveCategory(oldCat, newCat, actionId);
				}
				if (allDirPrefIds.length) {
					for (let dirPrefId of allDirPrefIds) {
						await saveCat(dirPrefId);
					}
				} else {
					await saveCat(aNodeData.dirPrefId);
				}
			}
			if (uncategorized) {
				await cardbookBGRepo.renameUncategorized(aNodeData.oldName, aNodeData.name);
				await cardbookBGRepo.notifyObservers(topic, `forceAccount::${aNodeData.dirPrefId}::${aNodeData.type}::${aNodeData.name}`);
			} else {
				for (let card of cards) {
					// as it is possible to rename a category from a virtual folder
					// should avoid to modify cards belonging to a read-only address book
					if (cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
						continue;
					}
					let outCard = new cardbookCardParser();
					await cardbookBGUtils.cloneCard(card, outCard);
					if (aNodeData.type == "categories") {
						cardbookBGRepo.renameCategoryFromCard(outCard, aNodeData.oldName, aNodeData.name);
					} else if (aNodeData.type == "org") {
						cardbookBGRepo.renameOrgFromCard(outCard, aNodeData.id, aNodeData.name);
					}
					await cardbookBGRepo.saveCardFromUpdate(card, outCard, actionId, false);
				}
			}
			let nodeColors = cardbookBGPreferences.getNodeColors();
			if (aNodeData.oldName in nodeColors) {
				nodeColors[aNodeData.name] = nodeColors[aNodeData.oldName];
				delete nodeColors[aNodeData.oldName];
				await cardbookBGPreferences.setNodeColors(nodeColors);
			}
		}
		if (colorChanged) {
			let nodeColors = cardbookBGPreferences.getNodeColors();
			if (nameChanged) {
				delete nodeColors[aNodeData.oldName];
			}
			if (aNodeData.color) {
				nodeColors[aNodeData.name] = aNodeData.color;
			} else {
				delete nodeColors[aNodeData.name];
			}
			await cardbookBGPreferences.setNodeColors(nodeColors);
			if (!nameChanged) {
				await cardbookBGRepo.notifyObservers("addressbookModified", `forceAccount::${aNodeData.dirPrefId}::${aNodeData.type}::${aNodeData.name}`);
			}
		}
		if (nameChanged) {
			cardbookIDBMailPop.updateMailPop(cardbookBGRepo, aNodeData.oldName, "0");
		}
		if (mailpopChanged) {
			let value = parseInt(aNodeData.mailpop) || "0";
			cardbookIDBMailPop.updateMailPop(cardbookBGRepo, aNodeData.name, value);
		}
		if (nameChanged && !uncategorized) {
			await endAction(actionId, true);
		}
	};

	async function endAction(aActionId, aForceRefresh, aParams) {
		if (cardbookBGRepo.currentAction[aActionId]) {
			let action = cardbookBGRepo.currentAction[aActionId];
			if ((action.refresh != "") || (aForceRefresh == true)) {
				await cardbookBGRepo.notifyObservers(action.actionCode, "forceAccount::" + action.refresh);
			} else {
				await cardbookBGRepo.notifyObservers(action.actionCode);
			}
			if (cardbookBGPreferences.getPref("syncAfterChange")) {
				let accounts = cardbookHTMLUtils.arrayUnique(action.files);
				cardbookBGSynchronization.syncAccounts(cardbookBGRepo, accounts);
			}
			await cardbookBGActions.endAction(cardbookBGRepo, aActionId, aParams);
			await setUndoAndRedoLabel();
		}
	};

	async function setUndoAndRedoLabel() {
		await cardbookIDBUndo.openUndoDB(cardbookBGRepo);
		let currentUndoId = cardbookBGRepo.currentUndoId;
		let undoLabel = await cardbookIDBUndo.getChangeLabel(cardbookBGRepo, "undo", currentUndoId);
		let setUndo = messenger.runtime.sendMessage({query: "cardbook.setUndoAndRedoLabel", code: "undo", value: undoLabel});
		await setUndo.catch(() => {});
		currentUndoId++;
		let redoLabel = await cardbookIDBUndo.getChangeLabel(cardbookBGRepo, "redo", currentUndoId);
		let setRedo = messenger.runtime.sendMessage({query: "cardbook.setUndoAndRedoLabel", code: "redo", value: redoLabel});
		await setRedo.catch(() => {});
	};

};

await main();
