import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { MimeAddressParser } from "../mimeParser/MimeJSComponents.sys.mjs";

export var cardbookBGColumns = {
    refreshRunning: true,
    showCondensedAddresses: true,
	messageCache: new Map(),
	myAddrArr: [],

	// Helper function for easy looping over message lists. See
	// https://webextension-api.thunderbird.net/en/latest/how-to/messageLists.html
	getMessages: async function* (list) {
		let page = await list;
		for (let message of page.messages) {
			yield message;
		}

		while (page.id) {
			page = await messenger.messages.continueList(page.id);
			for (let message of page.messages) {
				yield message;
			}
		}
	},

	//Find all email addresses which count as mine
	getMyEmails: async function () {
        cardbookBGColumns.showCondensedAddresses = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getSystemPref", type: "boolean", value: "mail.showCondensedAddresses"});
		cardbookBGColumns.myAddrArr = [];
        for (let account of await messenger.accounts.list()) {
            for (let identity of account.identities) {
                cardbookBGColumns.myAddrArr.push(identity);
            }
        }
	},

    getIdentityForEmail: function(aEmail) {
        let emailAddress = aEmail.toLowerCase();
        let result = cardbookBGColumns.myAddrArr.map(x => x.email).filter(x => x == emailAddress);
        if (result[0]) {
            return result[0];
        } else {
            return null;
        }
    },
    
    getCardBookDisplayNameFromEmail: function(aEmail, aDefaultDisplay, aRepo) {
        var found = false;
        var myResult = "";
        if (aEmail) {
            var myTestString = aEmail.toLowerCase();
            if (aRepo.cardbookPreferDisplayNameIndex[myTestString]) {
                myResult = aDefaultDisplay;
                found = true;
            } else {
                for (let account of aRepo.cardbookAccounts) {
                    if (account[2] && account[3] != "SEARCH") {
                        var myDirPrefId = account[1];
                        if (aRepo.cardbookCardEmails[myDirPrefId]) {
                            if (aRepo.cardbookCardEmails[myDirPrefId][myTestString]) {
                                myResult = aRepo.cardbookCardEmails[myDirPrefId][myTestString][0].fn;
                                found = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (found) {
                if (myResult) {
                    return {found: found, result: myResult};
                } else {
                    return {found: found, result: aEmail};
                }
            } else {
                if (aDefaultDisplay) {
                    return {found: found, result: aDefaultDisplay};
                } else {
                    return {found: found, result: aEmail};
                }
            }
        } else {
            return {found: found, result: aDefaultDisplay};
        }
    },

    getDisplayNameColumn: async function(aEmails, aRepo) {
		let exclusive = cardbookBGPreferences.getPref("exclusive");
        let results = [];
        let myCardBookResult = {};
        let addresses = MimeAddressParser.prototype.parseEncodedHeaderW(aEmails);
        for (let address of addresses) {
            if (cardbookBGColumns.showCondensedAddresses ==  true) {
                myCardBookResult = cardbookBGColumns.getCardBookDisplayNameFromEmail(address.email, address.name, aRepo);
                if (exclusive) {
                    if (!myCardBookResult.found) {
                        let identity = cardbookBGColumns.getIdentityForEmail(address.email);
                        if (identity && identity.name) {
                            results.push(identity.name);
                        } else {
                            if (address.name) {
                                results.push(address.name);
                            } else {
                                results.push(address.email);
                            }
                        }
                    } else {
                        results.push(myCardBookResult.result);
                    }
                } else {
                    if (!myCardBookResult.found) {
                        let displayName =  await messenger.NotifyTools.notifyExperiment({query: "cardbook.getStandardNameForEmail", email: address.email});
                        if (displayName) {
                            results.push(displayName);
                        } else {
                            let identity = cardbookBGColumns.getIdentityForEmail(address.email);
                            if (identity && identity.name) {
                                results.push(identity.name);
                            } else {
                                if (address.name) {
                                    results.push(address.name);
                                } else {
                                    results.push(address.email);
                                }
                            }
                        }
                    } else {
                        results.push(myCardBookResult.result);
                    }
                }
            } else {
                if (address.name) {
                    results.push(address.name);
                } else {
                    results.push(address.email);
                }
            }
        }
        return results.join(", ");
    },

	getCustomColumnCellValue: async function (columnId, message, repo) {
		let msg = cardbookBGColumns.messageCache.get(message.id);
		if (!msg) {
            let acct = message.folder.accountId;
            let author = await cardbookBGColumns.getDisplayNameColumn(message.author, repo);
            let authorEmail = MimeAddressParser.prototype.parseEncodedHeaderW(message.author);
			let recipients = await cardbookBGColumns.getDisplayNameColumn(message.recipients.join(", "), repo);
            let received = (acct.type == "nntp") ? "in" : (cardbookBGColumns.myAddrArr.map(x => x.email).includes(authorEmail[0].email) ? "out" : "in");
			msg = {
				received: received,
				corr: received == "out" ? recipients : author,
				from: author,
				to: recipients
			}
			cardbookBGColumns.messageCache.set(message.id, msg);
        }
        switch (columnId) {
            case "from":
                return {
                    messageId: message.id,
                    data: { text: msg.from }
                }
            case "to":
                return {
                    messageId: message.id,
                    data: { text: msg.to }
                }
            case "correspondent":
                return {
                    messageId: message.id,
                    data: { text: msg.corr }
                }
            default: throw new Error(`Unknown column: ${columnId}`);
        }
    },

    addColumns: async function () {
		await messenger.cc.addColumn("from", {
			name: messenger.i18n.getMessage("fromLabel") + " (CB)",
			sortable: "byTextContent",
			hidden: false,
			resizable: true
		})
		await messenger.cc.addColumn("to", {
			name: messenger.i18n.getMessage("recipientsLabel") + " (CB)",
			sortable: "byTextContent",
			hidden: false,
			resizable: true
		})
		await messenger.cc.addColumn("correspondent", {
			name: messenger.i18n.getMessage("correspondentsLabel") + " (CB)",
			sortable: "byTextContent",
			hidden: false,
			resizable: true
		})
	},

    updateFolderDisplay: async function (tab, folder, repo) {
        if (cardbookBGColumns.refreshRunning == false) {
            cardbookBGColumns.refreshRunning = true;
            let cellUpdatesCorr = [];
            let cellUpdatesTo = [];
            let cellUpdatesFrom = [];
            let messages;
            try { //since TB121
                messages = cardbookBGColumns.getMessages(messenger.mailTabs.getListedMessages(tab.id));
            } catch(e) {  //TB115
                messages = cardbookBGColumns.getMessages(messenger.messages.list(folder));
            }
            for await (let message of messages) {
                cellUpdatesCorr.push(await cardbookBGColumns.getCustomColumnCellValue("correspondent", message, repo));
                cellUpdatesTo.push(await cardbookBGColumns.getCustomColumnCellValue("to", message, repo));
                cellUpdatesFrom.push(await cardbookBGColumns.getCustomColumnCellValue("from", message, repo));
            }
            await messenger.cc.setCellData("correspondent", cellUpdatesCorr);
            await messenger.cc.setCellData("to", cellUpdatesTo);
            await messenger.cc.setCellData("from", cellUpdatesFrom);
            cardbookBGColumns.refreshRunning = false;
        }
	},

	doRefresh: async function (repo) {
		cardbookBGColumns.messageCache.clear();
		await cardbookBGColumns.getMyEmails();
		const tabs=await messenger.mailTabs.query();
		for (const tab of tabs) {
			cardbookBGColumns.updateFolderDisplay(tab, tab.displayedFolder, repo);
		}
	},

	init: async function (repo) {
        await cardbookBGColumns.addColumns();
        await cardbookBGColumns.doRefresh(repo);
    },

};
