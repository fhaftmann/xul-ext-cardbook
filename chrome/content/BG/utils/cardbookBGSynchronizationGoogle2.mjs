import { cardbookBGLog } from "./cardbookBGLog.mjs";
import { cardbookBGUtils } from "./cardbookBGUtils.mjs";
import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookBGSynchronizationUtils } from "./cardbookBGSynchronizationUtils.mjs";
import { cardbookCardParser } from "./cardbookCardParser.mjs";
import { cardbookCategoryParser } from "./cardbookCategoryParser.mjs";

import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs"
import { cardbookHTMLDates } from "../../HTML/utils/scripts/cardbookHTMLDates.mjs"
import { cardbookHTMLTypes } from "../../HTML/utils/scripts/cardbookHTMLTypes.mjs"
import { cardbookHTMLFormulas } from "../../HTML/utils/scripts/cardbookHTMLFormulas.mjs"

export var cardbookBGSynchronizationGoogle2 = {
	skippedLabels: {},
	labels: {},
	labelsList: {},
	contacts: {},
	googleOAuthData: {"GOOGLE2": {
			EMAIL_TYPE:                 "@gmail.com",
			VCARD_VERSIONS:             [ "3.0" ],
			CLIENT_ID:                  "779554755808-957jloa2c3c8n0rrm1a5304fkik7onf0.apps.googleusercontent.com",
			CLIENT_SECRET:              "h3NUkhofCKAW2E1X_NKSn4C_",
			RESPONSE_TYPE:              "code",
			SCOPE_CONTACTS:             "https://www.googleapis.com/auth/contacts",
			AUTH_PREFIX_CONTACTS:       "chrome://cardbook/oauth/people",
			LABELS:                     "contactGroups",
			LABELS_URL:                 "https://people.googleapis.com/v1/contactGroups",
			LABELS_URL_SIZE:            "1000",
			CONTACTS_URL:               "https://people.googleapis.com/v1/people/me/connections",
			CONTACTS_URL_SIZE:          "1000",
			CONTACT_URL:                "https://people.googleapis.com/v1/people",
			BATCH_GET_URL:              "https://people.googleapis.com/v1/people:batchGet",
			BATCH_GET_URL_SIZE:         "50",
			SYNC_PERSON_FIELDS:         "metadata,memberships",
			GET_PERSON_FIELDS:          "addresses,biographies,birthdays,emailAddresses,events,imClients,memberships,metadata,names,nicknames,organizations,phoneNumbers,photos,urls,userDefined",
			UPDATE_PERSON_FIELDS:       "addresses,biographies,birthdays,emailAddresses,events,imClients,memberships,names,nicknames,organizations,phoneNumbers,urls,userDefined",
			CREATE_PERSON_FIELDS:       "addresses,biographies,birthdays,emailAddresses,events,imClients,memberships,metadata,names,nicknames,organizations,phoneNumbers,urls,userDefined",
			OBJECT_TYPE:				"CONTACT",
			UPDATEPHOTO_PERSON_FIELDS:  "metadata",
			DELETEPHOTO_PERSON_FIELDS:  "metadata",
			OAUTH_URL:                  "https://accounts.google.com/o/oauth2/auth",
			TOKEN_REQUEST_URL:          "https://accounts.google.com/o/oauth2/token",
			TOKEN_REQUEST_TYPE:         "POST",                                                                      
			TOKEN_REQUEST_GRANT_TYPE:   "authorization_code",
			REFRESH_REQUEST_URL:        "https://accounts.google.com/o/oauth2/token",
			REFRESH_REQUEST_TYPE:       "POST",
			REFRESH_REQUEST_GRANT_TYPE: "refresh_token",
			ROOT_API:                   "https://www.googleapis.com"},
		"GOOGLE3": {
			VCARD_VERSIONS:             [ "3.0" ],
			CLIENT_ID:                  "779554755808-957jloa2c3c8n0rrm1a5304fkik7onf0.apps.googleusercontent.com",
			CLIENT_SECRET:              "h3NUkhofCKAW2E1X_NKSn4C_",
			RESPONSE_TYPE:              "code",
			SCOPE_CONTACTS:             "https://www.googleapis.com/auth/directory.readonly",
			AUTH_PREFIX_CONTACTS:       "chrome://cardbook/oauth/directory",
			LABELS:                     "contactGroups",
			LABELS_URL:                 "https://people.googleapis.com/v1/contactGroups",
			LABELS_URL_SIZE:            "1000",
			CONTACTS_URL:               "https://people.googleapis.com/v1/people:listDirectoryPeople",
			CONTACTS_URL_SIZE:          "1000",
			CONTACTS_SOURCES:			"DIRECTORY_SOURCE_TYPE_DOMAIN_PROFILE",
			OBJECT_TYPE:				"DOMAIN_PROFILE",
			CONTACT_URL:                "https://people.googleapis.com/v1/people",
			BATCH_GET_URL:              "https://people.googleapis.com/v1/people:batchGet",
			BATCH_GET_URL_SIZE:         "50",
			SYNC_PERSON_FIELDS:         "metadata,memberships",
			GET_PERSON_FIELDS:          "addresses,biographies,birthdays,emailAddresses,events,imClients,memberships,metadata,names,nicknames,organizations,phoneNumbers,photos,urls,userDefined",
			UPDATE_PERSON_FIELDS:       "addresses,biographies,birthdays,emailAddresses,events,imClients,memberships,names,nicknames,organizations,phoneNumbers,urls,userDefined",
			CREATE_PERSON_FIELDS:       "addresses,biographies,birthdays,emailAddresses,events,imClients,memberships,metadata,names,nicknames,organizations,phoneNumbers,urls,userDefined",
			UPDATEPHOTO_PERSON_FIELDS:  "metadata",
			DELETEPHOTO_PERSON_FIELDS:  "metadata",
			OAUTH_URL:                  "https://accounts.google.com/o/oauth2/auth",
			TOKEN_REQUEST_URL:          "https://accounts.google.com/o/oauth2/token",
			TOKEN_REQUEST_TYPE:         "POST",                                                                      
			TOKEN_REQUEST_GRANT_TYPE:   "authorization_code",
			REFRESH_REQUEST_URL:        "https://accounts.google.com/o/oauth2/token",
			REFRESH_REQUEST_TYPE:       "POST",
			REFRESH_REQUEST_GRANT_TYPE: "refresh_token",
			ROOT_API:                   "https://www.googleapis.com"}
			},

	getGoogleOAuthURLForGooglePeople: function (aEmail, aType, aRedirectURL) {
		return cardbookBGSynchronizationGoogle2.googleOAuthData[aType].OAUTH_URL +
			"?response_type=" + cardbookBGSynchronizationGoogle2.googleOAuthData[aType].RESPONSE_TYPE +
			"&client_id=" + cardbookBGSynchronizationGoogle2.googleOAuthData[aType].CLIENT_ID +
			"&redirect_uri=" + aRedirectURL +
			"&scope=" + cardbookBGSynchronizationGoogle2.googleOAuthData[aType].SCOPE_CONTACTS +
			"&login_hint=" + aEmail;
	},

	getNewRefreshTokenForGooglePeople: async function (aRepo, aConnection, aCallback, aFollowAction) {
		aRepo.cardbookRefreshTokenRequest[aConnection.connPrefId]++;
		let redirectURI = await browser.identity.getRedirectURL();
		let hostname = (new URL(redirectURI)).hostname;
		let redirectId = hostname.split(".")[0];
		let redirectURL = `http://127.0.0.1/mozoauth2/${redirectId}`;
		let url = cardbookBGSynchronizationGoogle2.getGoogleOAuthURLForGooglePeople(aConnection.connUser, aConnection.connType, redirectURL);
		let getCode = new Promise(async (resolve, reject) => {
			function validateState(url) {
				if (url) {
					let searchParams = url.replace(redirectURL, "");
					let urlParams = new URLSearchParams(searchParams);
					let code = urlParams.get("code");
					resolve(code);
				} else {
					resolve("");
				}
			}
			let authorizingURL = await browser.identity.launchWebAuthFlow({interactive: true, url: url}, validateState);
		});
		let code = await getCode;
		if (code) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "googleNewRefreshTokenOK", [aConnection.connDescription, code]);
			let connection = {connUser: aConnection.connUser, connType: aConnection.connType, connUrl: cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].TOKEN_REQUEST_URL, connPrefId: aConnection.connPrefId, connDescription: aConnection.connDescription};
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "googleRequestRefreshToken", [connection.connDescription, connection.connUrl]);
			let headers = { "Content-Type": "application/x-www-form-urlencoded", "GData-Version": "3" };
			let params = {};
			params["code"] = code;
			params["client_id"] = cardbookBGSynchronizationGoogle2.googleOAuthData[connection.connType].CLIENT_ID;
			params["client_secret"] = cardbookBGSynchronizationGoogle2.googleOAuthData[connection.connType].CLIENT_SECRET;
			params["redirect_uri"] = redirectURL;
			params["grant_type"] = cardbookBGSynchronizationGoogle2.googleOAuthData[connection.connType].TOKEN_REQUEST_GRANT_TYPE;
			let data = cardbookBGSynchronizationUtils.encodeParams(params);

			let request = fetch(connection.connUrl, {
				method: cardbookBGSynchronizationGoogle2.googleOAuthData[connection.connType].TOKEN_REQUEST_TYPE,
				headers: headers,
				body: data,
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});

			request.then(async (response) => {
				if (response.ok) {
					try {
						let responseJSON = await response.json();
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "googleRefreshTokenOK", [connection.connDescription, cardbookBGSynchronizationUtils.cleanWebObject(responseJSON)]);
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.rememberPassword", user: connection.connUser, url: cardbookBGSynchronizationGoogle2.googleOAuthData[connection.connType].AUTH_PREFIX_CONTACTS, pwd: responseJSON.refresh_token, save: true});
						if (aCallback) {
							aCallback(aRepo, connection, aFollowAction);
						}
					}
					catch(e) {
						cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, connection.connDescription + " : cardbookBGSynchronizationGoogle2.getNewRefreshTokenForGooglePeople error : " + e, "Error");
						aRepo.cardbookRefreshTokenError[aConnection.connPrefId]++;
						aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
					}
				} else {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [connection.connDescription, "getNewRefreshTokenForGooglePeople", connection.connUrl, response.status], "Error");
					aRepo.cardbookRefreshTokenError[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				}
				aRepo.cardbookRefreshTokenResponse[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [connection.connDescription, "getNewRefreshTokenForGooglePeople", connection.connUrl, response.status], "Error");
				aRepo.cardbookRefreshTokenError[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			});
		} else {
			aRepo.cardbookRefreshTokenError[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			aRepo.cardbookRefreshTokenResponse[aConnection.connPrefId]++;
		}
	},

	getNewAccessTokenForGooglePeople: async function(aRepo, aConnection, aFollowAction) {
		aRepo.cardbookAccessTokenRequest[aConnection.connPrefId]++;
		aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].REFRESH_REQUEST_URL
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "googleRequestAccessToken", [aConnection.connDescription, aConnection.connUrl]);
		let code = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url: cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].AUTH_PREFIX_CONTACTS});
		let headers = { "Content-Type": "application/x-www-form-urlencoded", "GData-Version": "3" };
		let params = {};
		params["refresh_token"] = code;
		params["client_id"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CLIENT_ID;
		params["client_secret"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CLIENT_SECRET;
		params["grant_type"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].REFRESH_REQUEST_GRANT_TYPE;
		let data = cardbookBGSynchronizationUtils.encodeParams(params);
		let request = fetch(aConnection.connUrl, {
			method: cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].REFRESH_REQUEST_TYPE,
			headers: headers,
			body: data,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});
		request.then(async (response) => {
			if (response.ok) {
				try {
					let responseJSON = await response.json();
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "googleAccessTokenOK", [aConnection.connDescription, cardbookBGSynchronizationUtils.cleanWebObject(responseJSON)]);
					aConnection.accessToken = responseJSON.token_type + " " + responseJSON.access_token;
					aFollowAction(aRepo, aConnection);
				}
				catch(e) {
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
					aRepo.cardbookAccessTokenError[aConnection.connPrefId]++;
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, aConnection.connDescription + " : cardbookBGSynchronizationGoogle2.getNewAccessTokenForGooglePeople error : " + e, "Error");
				}
			} else {
				if (response.status == 400 || response.status == 401) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "getNewAccessTokenForGooglePeople", aConnection.connUrl, response.status]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "googleGetNewRefreshToken", [aConnection.connDescription, aConnection.connUrl]);
					cardbookBGSynchronizationGoogle2.getNewRefreshTokenForGooglePeople(aRepo, aConnection, cardbookBGSynchronizationGoogle2.getNewAccessTokenForGooglePeople, aFollowAction);
				} else {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "getNewAccessTokenForGooglePeople", aConnection.connUrl, response.status], "Error");
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
					aRepo.cardbookAccessTokenError[aConnection.connPrefId]++;
				}
			}
			aRepo.cardbookAccessTokenResponse[aConnection.connPrefId]++;
		}).catch(async (response) => {
			console.log(response);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "getNewAccessTokenForGooglePeople", aConnection.connUrl, response.status], "Error");
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			aRepo.cardbookAccessTokenError[aConnection.connPrefId]++;
			aRepo.cardbookAccessTokenResponse[aConnection.connPrefId]++;
		});
	},

	getCategoriesNumber: function (aRepo, aDirPrefId) {
		aRepo.cardbookCategoriesFromCache[aDirPrefId] = {};
		if (!aRepo.cardbookFileCacheCategories[aDirPrefId]) {
			aRepo.cardbookFileCacheCategories[aDirPrefId] = {}
		}
		if (aRepo.cardbookCategoriesFromCache[aDirPrefId]) {
			aRepo.cardbookCategoriesFromCache[aDirPrefId] = JSON.parse(JSON.stringify(aRepo.cardbookFileCacheCategories[aDirPrefId]));
		}
		let length = 0;
		if (aRepo.cardbookFileCacheCategories[aDirPrefId]) {
			for (let i in aRepo.cardbookFileCacheCategories[aDirPrefId]) {
				length++;
			}
		}
		return length;
	},

	googleSyncLabelsInit: function (aRepo, aConnection) {
		cardbookBGSynchronizationGoogle2.skippedLabels[aConnection.connPrefId] = [];
		cardbookBGSynchronizationGoogle2.labels[aConnection.connPrefId] = [];
		cardbookBGSynchronizationGoogle2.labelsList[aConnection.connPrefId] = [];
		cardbookBGSynchronizationGoogle2.googleSyncLabels(aRepo, aConnection);
	},

	googleSyncLabels: function(aRepo, aConnection, aNextPageToken) {
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationSearchingCategories", [aConnection.connDescription]);
		let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken};
		let params = {};
		params["pageSize"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].LABELS_URL_SIZE;
		params["groupFields"] = "name,groupType";
		if (aNextPageToken != null) {
			params.pageToken = aNextPageToken;
		}
		let data = cardbookBGSynchronizationUtils.encodeParams(params);
		aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].LABELS_URL + "?" + data;
		let request = fetch(aConnection.connUrl, {
			method: "GET",
			headers: headers,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});

		request.then(async (response) => {
			if (response.ok) {
				aRepo.cardbookServerSyncHandleRemainingCatTotal[aConnection.connPrefId] = cardbookBGSynchronizationGoogle2.getCategoriesNumber(aRepo, aConnection.connPrefId);
				let responseJSON = await response.json();
				for (let contactGroup of responseJSON.contactGroups) {
					if (contactGroup.groupType == "SYSTEM_CONTACT_GROUP") {
						continue;
					}
					cardbookBGSynchronizationGoogle2.labelsList[aConnection.connPrefId].push(contactGroup.name);
				}
				for (let contactGroup of responseJSON.contactGroups) {
					let tmpArray = contactGroup.resourceName.split("/");
					let uid = tmpArray[tmpArray.length - 1];
					if (contactGroup.groupType == "SYSTEM_CONTACT_GROUP") {
						cardbookBGSynchronizationGoogle2.skippedLabels[aConnection.connPrefId].push(uid);
						continue;
					}
					let href = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].LABELS_URL + "/" + uid;
					
					aRepo.cardbookServerCatSyncTotal[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncCompareCatWithCacheTotal[aConnection.connPrefId]++;
					let aCategory = new cardbookCategoryParser(contactGroup.name, aConnection.connPrefId);
					aCategory.etag = contactGroup.etag;
					aCategory.href = href;
					aCategory.uid = uid;
					cardbookBGSynchronizationGoogle2.labels[aConnection.connPrefId].push(aCategory);
				}
				if (responseJSON.nextPageToken) {
					aRepo.cardbookServerSyncRequest[aConnection.connPrefId]++;
					cardbookBGSynchronizationGoogle2.googleSyncLabels(aRepo, aConnection, responseJSON.nextPageToken)
				} else {
					for (let category of cardbookBGSynchronizationGoogle2.labels[aConnection.connPrefId]) {
						let aCatConnection = {accessToken: aConnection.accessToken, connPrefId: aConnection.connPrefId, connUrl: category.href, connDescription: aConnection.connDescription,
												connUser: aConnection.connUser, connType: aConnection.connType};
						await cardbookBGSynchronizationGoogle2.compareServerCatWithCache(aRepo, aCatConnection, category, cardbookBGSynchronizationGoogle2.labelsList[aConnection.connPrefId]);
						if (aRepo.cardbookCategoriesFromCache[aCatConnection.connPrefId][category.href]) {
							delete aRepo.cardbookCategoriesFromCache[aCatConnection.connPrefId][category.href];
							aRepo.cardbookServerSyncHandleRemainingCatTotal[aCatConnection.connPrefId]--;
						}
					}
					await cardbookBGSynchronizationGoogle2.handleRemainingCatCache(aRepo, aConnection, cardbookBGSynchronizationGoogle2.labelsList[aConnection.connPrefId]);
				}
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			} else {
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "googleSyncLabels", aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerCatSyncError[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			}
		}).catch(async (response) => {
			console.log(response);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "googleSyncLabels", aConnection.connUrl, response.status], "Error");
			aRepo.cardbookServerCatSyncError[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
		});
	},

	compareServerCatWithCache: async function (aRepo, aCatConnection, aCategory, aServerList) {
		if (aRepo.cardbookFileCacheCategories[aCatConnection.connPrefId] && aRepo.cardbookFileCacheCategories[aCatConnection.connPrefId][aCategory.href]) {
			var myCacheCat = aRepo.cardbookFileCacheCategories[aCatConnection.connPrefId][aCategory.href];
			var myServerCat = new cardbookCategoryParser();
			cardbookBGUtils.cloneCategory(myCacheCat, myServerCat);
			cardbookBGUtils.addEtag(myServerCat, aCategory.etag);
			if (myCacheCat.etag == aCategory.etag) {
				if (myCacheCat.deleted) {
					// "DELETEDONDISK"
					aRepo.cardbookServerDeletedCatRequest[aCatConnection.connPrefId]++;
					aRepo.cardbookServerSyncDeletedCatOnDisk[aCatConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryDeletedOnDisk", [aCatConnection.connDescription, myCacheCat.name]);
					cardbookBGSynchronizationGoogle2.serverDeleteCategory(aRepo, aCatConnection, myCacheCat);
				} else if (myCacheCat.updated) {
					// "UPDATEDONDISK"
					aRepo.cardbookServerSyncUpdatedCatOnDisk[aCatConnection.connPrefId]++;
					if (aServerList.includes(myCacheCat.name)) {
						aRepo.cardbookServerCatSyncDone[aCatConnection.connPrefId]++;
						await aRepo.removeCategoryFromRepository(myCacheCat, true, aCatConnection.connPrefId);
					} else {
						aRepo.cardbookServerUpdatedCatRequest[aCatConnection.connPrefId]++;
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryUpdatedOnDisk", [aCatConnection.connDescription, myCacheCat.name]);
						var aUpdateConnection = JSON.parse(JSON.stringify(aCatConnection));
						cardbookBGSynchronizationGoogle2.serverUpdateCategory(aRepo, aUpdateConnection, myCacheCat);
					}
				} else {
					// "NOTUPDATED"
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryAlreadyGetFromCache", [aCatConnection.connDescription, myCacheCat.name]);
					aRepo.cardbookServerCatSyncDone[aCatConnection.connPrefId]++;
					aRepo.cardbookServerSyncNotUpdatedCat[aCatConnection.connPrefId]++;
				}
			} else if (myCacheCat.deleted) {
				// "DELETEDONDISKUPDATEDONSERVER"
				aRepo.cardbookServerSyncDeletedCatOnDiskUpdatedCatOnServer[aCatConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryDeletedOnDiskUpdatedOnServer", [aCatConnection.connDescription, myCacheCat.name]);
				var solveConflicts = cardbookBGPreferences.getPref("solveConflicts");
				if (solveConflicts === "Local") {
					var conflictResult = "delete";
				} else if (solveConflicts === "Remote") {
					var conflictResult = "keep";
				} else {
					var message = messenger.i18n.getMessage("categoryDeletedOnDiskUpdatedOnServer", [aCatConnection.connDescription, myCacheCat.name]);
					var conflictResult = await aRepo.askUser("category", aCatConnection.connPrefId, message, aRepo.importConflictChoiceSync1Values);
				}
				
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aCatConnection.connDescription + " : debug mode : conflict resolution : ", conflictResult);
				switch (conflictResult) {
					case "keep":
						if (aRepo.cardbookAccountsCategories[aCatConnection.connPrefId].includes(aCategory.name)) {
							// new category created on CardBook with the same name
							if (aRepo.cardbookCategories[aCatConnection.connPrefId+"::"+aCategory.name]) {
								let myOldCategory = aRepo.cardbookCategories[aCategory.dirPrefId+"::"+aCategory.name];
								await aRepo.removeCategoryFromRepository(myOldCategory, true, aCatConnection.connPrefId);
							// another category was updated on CardBook to the same name
							} else {
								for (let i in aRepo.cardbookCategories) {
									let myCategory = aRepo.cardbookCategories[i];
									if (myCategory.name == aCategory.name) {
										await aRepo.removeCategoryFromRepository(myCategory, true, aCatConnection.connPrefId);
										break;
									}
								}
							}
						}
						await aRepo.removeCategoryFromRepository(myCacheCat, true, aCatConnection.connPrefId);
						await aRepo.addCategoryToRepository(aCategory, true, aCatConnection.connPrefId);
						aRepo.cardbookServerCatSyncDone[aCatConnection.connPrefId]++;
						break;
					case "delete":
						aRepo.cardbookServerDeletedCatRequest[aCatConnection.connPrefId]++;
						myCacheCat.etag = aCategory.etag;
						cardbookBGSynchronizationGoogle2.serverDeleteCategory(aRepo, aCatConnection, myCacheCat);
						break;
					default:
						aRepo.cardbookServerCatSyncDone[aCatConnection.connPrefId]++;
						break;
				}
			} else if (myCacheCat.updated) {
				// "UPDATEDONBOTH"
				aRepo.cardbookServerSyncUpdatedCatOnBoth[aCatConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryUpdatedOnBoth", [aCatConnection.connDescription, myCacheCat.name]);
				var solveConflicts = cardbookBGPreferences.getPref("solveConflicts");
				if (solveConflicts === "Local") {
					var conflictResult = "local";
				} else if (solveConflicts === "Remote") {
					var conflictResult = "remote";
				} else {
					var message = messenger.i18n.getMessage("categoryUpdatedOnBoth", [aCatConnection.connDescription, myCacheCat.name]);
					var conflictResult = await aRepo.askUser("category", aCatConnection.connPrefId, message, aRepo.importConflictChoiceSync3Values);
				}
				
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aCatConnection.connDescription + " : debug mode : conflict resolution : ", conflictResult);
				switch (conflictResult) {
					case "local":
						if (aServerList.includes(myCacheCat.name)) {
							aRepo.cardbookServerCatSyncDone[aCatConnection.connPrefId]++;
							await aRepo.removeCategoryFromRepository(myCacheCat, true, aCatConnection.connPrefId);
						} else {
							aRepo.cardbookServerUpdatedCatRequest[aCatConnection.connPrefId]++;
							var aUpdateConnection = JSON.parse(JSON.stringify(aCatConnection));
							myCacheCat.etag = aCategory.etag;
							cardbookBGSynchronizationGoogle2.serverUpdateCategory(aRepo, aUpdateConnection, myCacheCat);
						}
						break;
					case "remote":
						if (aRepo.cardbookAccountsCategories[aCatConnection.connPrefId].includes(aCategory.name)) {
							// new category created on CardBook with the same name
							if (aRepo.cardbookCategories[aCatConnection.connPrefId+"::"+aCategory.name]) {
								let myOldCategory = aRepo.cardbookCategories[aCategory.dirPrefId+"::"+aCategory.name];
								await aRepo.removeCategoryFromRepository(myOldCategory, true, aCatConnection.connPrefId);
							// another category was updated on CardBook to the same name
							} else {
								for (let i in aRepo.cardbookCategories) {
									let myCategory = aRepo.cardbookCategories[i];
									if (myCategory.name == aCategory.name) {
										await aRepo.removeCategoryFromRepository(myCategory, true, aCatConnection.connPrefId);
										break;
									}
								}
							}
						}
						await aRepo.updateCategoryFromRepository(aCategory, myCacheCat, aCatConnection.connPrefId);
						aRepo.cardbookServerCatSyncDone[aCatConnection.connPrefId]++;
						break;
					default:
						aRepo.cardbookServerCatSyncDone[aCatConnection.connPrefId]++;
						break;
				}
			} else {
				// "UPDATEDONSERVER"
				if (aRepo.cardbookAccountsCategories[aCatConnection.connPrefId].includes(aCategory.name)) {
					// new category created on CardBook with the same name
					if (aRepo.cardbookCategories[aCatConnection.connPrefId+"::"+aCategory.name]) {
						let myOldCategory = aRepo.cardbookCategories[aCategory.dirPrefId+"::"+aCategory.name];
						await aRepo.removeCategoryFromRepository(myOldCategory, true, aCatConnection.connPrefId);
					// another category was updated on CardBook to the same name
					} else {
						for (let i in aRepo.cardbookCategories) {
							let myCategory = aRepo.cardbookCategories[i];
							if (myCategory.name == aCategory.name) {
								await aRepo.removeCategoryFromRepository(myCategory, true, aCatConnection.connPrefId);
								break;
							}
						}
					}
				}
				aRepo.cardbookServerSyncUpdatedCatOnServer[aCatConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryUpdatedOnServer", [aCatConnection.connDescription, myCacheCat.name, aCategory.etag, myCacheCat.etag]);
				await aRepo.updateCategoryFromRepository(aCategory, myCacheCat, aCatConnection.connPrefId);
				aRepo.cardbookServerCatSyncDone[aCatConnection.connPrefId]++;
			}
		} else {
			// "NEWONSERVER"
			if (aRepo.cardbookAccountsCategories[aCatConnection.connPrefId].includes(aCategory.name)) {
				// new category created on CardBook with the same name
				if (aRepo.cardbookCategories[aCatConnection.connPrefId+"::"+aCategory.name]) {
					let myOldCategory = aRepo.cardbookCategories[aCategory.dirPrefId+"::"+aCategory.name];
					await aRepo.removeCategoryFromRepository(myOldCategory, true, aCatConnection.connPrefId);
				// another category was updated on CardBook to the same name
				} else {
					for (let i in aRepo.cardbookCategories) {
						let myCategory = aRepo.cardbookCategories[i];
						if (myCategory.name == aCategory.name) {
							await aRepo.removeCategoryFromRepository(myCategory, true, aCatConnection.connPrefId);
							break;
						}
					}
				}
			}
			aRepo.cardbookServerSyncNewCatOnServer[aCatConnection.connPrefId]++;
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryNewOnServer", [aCatConnection.connDescription]);
			await aRepo.addCategoryToRepository(aCategory, true, aCatConnection.connPrefId);
			aRepo.cardbookServerCatSyncDone[aCatConnection.connPrefId]++;
		}
		aRepo.cardbookServerSyncCompareCatWithCacheDone[aCatConnection.connPrefId]++;
	},

	handleRemainingCatCache: async function (aRepo, aConnection, aServerList) {
		if (aRepo.cardbookCategoriesFromCache[aConnection.connPrefId]) {
			for (var i in aRepo.cardbookCategoriesFromCache[aConnection.connPrefId]) {
				var aCategory = aRepo.cardbookCategoriesFromCache[aConnection.connPrefId][i];
				let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
				if (aCategory.name == uncat){
					aRepo.cardbookServerCatSyncTotal[aConnection.connPrefId]++;
					aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
				} else if (aServerList.includes(aCategory.name)){
					aRepo.cardbookServerCatSyncTotal[aConnection.connPrefId]++;
					aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
				} else {
					if (aCategory.created) {
						// "NEWONDISK"
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryNewOnDisk", [aConnection.connDescription, aCategory.name]);
						aRepo.cardbookServerCreatedCatRequest[aConnection.connPrefId]++;
						aRepo.cardbookServerCatSyncTotal[aConnection.connPrefId]++;
						aRepo.cardbookServerSyncNewCatOnDisk[aConnection.connPrefId]++;
						var aCreateConnection = JSON.parse(JSON.stringify(aConnection));
						cardbookBGSynchronizationGoogle2.serverCreateCategory(aRepo, aCreateConnection, aCategory);
					} else if (aCategory.updated) {
						// "UPDATEDONDISKDELETEDONSERVER";
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryUpdatedOnDiskDeletedOnServer", [aConnection.connDescription, aCategory.name]);
						aRepo.cardbookServerCatSyncTotal[aConnection.connPrefId]++;
						aRepo.cardbookServerSyncUpdatedCatOnDiskDeletedCatOnServer[aConnection.connPrefId]++;
						var solveConflicts = cardbookBGPreferences.getPref("solveConflicts");
						if (solveConflicts === "Local") {
							var conflictResult = "keep";
						} else if (solveConflicts === "Remote") {
							var conflictResult = "delete";
						} else {
							var message = messenger.i18n.getMessage("categoryUpdatedOnDiskDeletedOnServer", [aConnection.connDescription, aCategory.name]);
							var conflictResult = await aRepo.askUser("category", aConnection.connPrefId, message, aRepo.importConflictChoiceSync1Values);
						}
						
						cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aConnection.connDescription + " : debug mode : conflict resolution : ", conflictResult);
						switch (conflictResult) {
							case "keep":
								cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryNewOnDisk", [aConnection.connDescription, aCategory.name]);
								aRepo.cardbookServerCreatedCatRequest[aConnection.connPrefId]++;
								var aCreateConnection = JSON.parse(JSON.stringify(aConnection));
								cardbookBGSynchronizationGoogle2.serverCreateCategory(aRepo, aCreateConnection, aCategory);
								break;
							case "delete":
								cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryDeletedOnServer", [aConnection.connDescription, aCategory.name]);
								await aRepo.removeCardsFromCategory(aConnection.connPrefId, aCategory.name);
								await aRepo.removeCategoryFromRepository(aCategory, true, aConnection.connPrefId);
								aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
								break;
							default:
								aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
								break;
						}
					} else {
						// "DELETEDONSERVER"
						aRepo.cardbookServerCatSyncTotal[aConnection.connPrefId]++;
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryDeletedOnServer", [aConnection.connDescription, aCategory.name]);
						await aRepo.removeCardsFromCategory(aConnection.connPrefId, aCategory.name);
						await aRepo.removeCategoryFromRepository(aCategory, true, aConnection.connPrefId);
						aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
						aRepo.cardbookServerSyncDeletedCatOnServer[aConnection.connPrefId]++;
					}
				}
				aRepo.cardbookServerSyncHandleRemainingCatDone[aConnection.connPrefId]++;
			}
		}
	},

	serverDeleteCategory: function(aRepo, aConnection, aCategory) {
		if (cardbookBGSynchronizationUtils.getModifsPushed(aRepo, aConnection.connPrefId) <= cardbookBGPreferences.getMaxModifsPushed()) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategorySendingDeletion", [aConnection.connDescription, aCategory.name]);
			let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken, "If-Match": aCategory.etag };
			let request = fetch(aConnection.connUrl, {
				method: "DELETE",
				headers: headers,
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});
			request.then(async (response) => {
				if (response.ok) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategoryDeletedFromServer", [aConnection.connDescription, aCategory.name]);
					await aRepo.removeCategoryFromRepository(aCategory, true, aConnection.connPrefId);
				} else if (response.status == 404) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategoryNotExistServer", [aConnection.connDescription, aCategory.name]);
					await aRepo.removeCategoryFromRepository(aCategory, true, aConnection.connPrefId);
				} else {
					aRepo.cardbookServerDeletedCatError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategoryDeleteFailed", [aConnection.connDescription, aCategory.name, aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
				aRepo.cardbookServerDeletedCatResponse[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerDeletedCatError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategoryDeleteFailed", [aConnection.connDescription, aCategory.name, aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
				aRepo.cardbookServerDeletedCatResponse[aConnection.connPrefId]++;
			});
		} else {
			aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
			aRepo.cardbookServerDeletedCatResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerNotPushed[aConnection.connPrefId]++;
		}
	},

	serverUpdateCategory: function(aRepo, aConnection, aCategory) {
		if (cardbookBGSynchronizationUtils.getModifsPushed(aRepo, aConnection.connPrefId) <= cardbookBGPreferences.getMaxModifsPushed()) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategorySendingUpdate", [aConnection.connDescription, aCategory.name]);
			aConnection.connUrl = aCategory.href;
			let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken };
			if (aCategory.etag && aCategory.etag != "0") {
				headers["If-Match"] = aCategory.etag;
			} else {
				headers["If-None-Match"] = "*";
			}
			let params = {};
			params["updateGroupFields"] = "name,groupType";
			params["readGroupFields"] = "name,groupType";
			params["contactGroup"] = {name: aCategory.name, etag: aCategory.etag, resourceName: cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].LABELS + "/" + aCategory.uid};
			let data = JSON.stringify(params);

			let request = fetch(aConnection.connUrl, {
				method: "PUT",
				headers: headers,
				body: data,
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});
			request.then(async (response) => {
				if (response.ok) {
					let responseJSON = await response.json();
					if (aRepo.cardbookCategories[aCategory.dirPrefId+"::"+aCategory.uid]) {
						let myOldCategory = aRepo.cardbookCategories[aCategory.dirPrefId+"::"+aCategory.uid];
						await aRepo.removeCategoryFromRepository(myOldCategory, true, aConnection.connPrefId);
					}
					let tmpArray = responseJSON.resourceName.split("/");
					let uid = tmpArray[tmpArray.length - 1];
					aCategory.uid = uid;
					cardbookBGUtils.addEtag(aCategory, responseJSON.etag);
					aCategory.href = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].LABELS_URL + "/" + uid;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategoryUpdatedOnServerWithEtag", [aConnection.connDescription, aCategory.name, responseJSON.etag]);
					cardbookBGUtils.nullifyTagModification(aCategory);
					await aRepo.addCategoryToRepository(aCategory, true, aConnection.connPrefId);
				} else {
					aRepo.cardbookServerUpdatedCatError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategoryUpdateFailed", [aConnection.connDescription, aCategory.name, aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
				aRepo.cardbookServerUpdatedCatResponse[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerUpdatedCatError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategoryUpdateFailed", [aConnection.connDescription, aCategory.name, aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
				aRepo.cardbookServerUpdatedCatResponse[aConnection.connPrefId]++;
			});
		} else {
			aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
			aRepo.cardbookServerUpdatedCatResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerNotPushed[aConnection.connPrefId]++;
		}
	},

	serverCreateCategory: function(aRepo, aConnection, aCategory) {
		if (cardbookBGSynchronizationUtils.getModifsPushed(aRepo, aConnection.connPrefId) <= cardbookBGPreferences.getMaxModifsPushed()) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategorySendingCreate", [aConnection.connDescription, aCategory.name]);
			aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].LABELS_URL;
			let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken };
			let params = {};
			params["readGroupFields"] = "name,groupType";
			params["contactGroup"] = {name: aCategory.name};
			let data = JSON.stringify(params);

			let request = fetch(aConnection.connUrl, {
				method: "POST",
				headers: headers,
				body: data,
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});
			request.then(async (response) => {
				if (response.ok) {
					let responseJSON = await response.json();
					if (aRepo.cardbookCategories[aCategory.dirPrefId+"::"+aCategory.uid]) {
						let myOldCategory = aRepo.cardbookCategories[aCategory.dirPrefId+"::"+aCategory.uid];
						await aRepo.removeCategoryFromRepository(myOldCategory, true, aConnection.connPrefId);
					}
					let tmpArray = responseJSON.resourceName.split("/");
					let uid = tmpArray[tmpArray.length - 1];
					aCategory.uid = uid;
					cardbookBGUtils.addEtag(aCategory, responseJSON.etag);
					aCategory.href = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].LABELS_URL + "/" + uid;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategoryCreatedOnServerWithEtag", [aConnection.connDescription, aCategory.name, responseJSON.etag]);
					cardbookBGUtils.nullifyTagModification(aCategory);
					await aRepo.addCategoryToRepository(aCategory, true, aConnection.connPrefId);
				} else {
					cardbookBGUtils.addTagCreated(aCategory);
					aRepo.cardbookServerCreatedCatError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategoryCreateFailed", [aConnection.connDescription, aCategory.name, aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerCreatedCatResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				cardbookBGUtils.addTagCreated(aCategory);
				aRepo.cardbookServerCreatedCatError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCategoryCreateFailed", [aConnection.connDescription, aCategory.name, aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerCreatedCatResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
			});
		} else {
			aRepo.cardbookServerCatSyncDone[aConnection.connPrefId]++;
			aRepo.cardbookServerCreatedCatResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerNotPushed[aConnection.connPrefId]++;
		}
	},

	getCardsNumber: function (aRepo, aPrefId) {
		aRepo.cardbookCardsFromCache[aPrefId] = {};
		if (!aRepo.cardbookFileCacheCards[aPrefId]) {
			aRepo.cardbookFileCacheCards[aPrefId] = {}
		}
		if (aRepo.cardbookFileCacheCards[aPrefId]) {
			aRepo.cardbookCardsFromCache[aPrefId] = JSON.parse(JSON.stringify(aRepo.cardbookFileCacheCards[aPrefId]));
		}
		var length = 0;
		if (aRepo.cardbookFileCacheCards[aPrefId]) {
			for (var i in aRepo.cardbookFileCacheCards[aPrefId]) {
				length++;
			}
		}
		return length;
	},

	googleSyncContactsInit: function (aRepo, aConnection) {
		cardbookBGSynchronizationGoogle2.contacts[aConnection.connPrefId] = {};
		cardbookBGSynchronizationGoogle2.googleSyncContacts(aRepo, aConnection);
	},

	googleSyncContacts: function(aRepo, aConnection, aNextPageToken) {
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationSearchingCards", [aConnection.connDescription]);
		let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken};
		let params = {};
		params["pageSize"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACTS_URL_SIZE;
		params["personFields"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].SYNC_PERSON_FIELDS;
		if (aNextPageToken != null) {
			params.pageToken = aNextPageToken;
		}
		let data = cardbookBGSynchronizationUtils.encodeParams(params);
		aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACTS_URL + "?" + data;
		let request = fetch(aConnection.connUrl, {
			method: "GET",
			headers: headers,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});

		request.then(async (response) => {
			if (response.ok) {
				let responseJSON = await response.json();
				aRepo.cardbookServerSyncHandleRemainingCardTotal[aConnection.connPrefId] = cardbookBGSynchronizationGoogle2.getCardsNumber(aRepo, aConnection.connPrefId);
				if (responseJSON.connections) {
					for (let resource of responseJSON.connections) {
						let tmpArray = resource.resourceName.split("/");
						let uid = tmpArray[tmpArray.length - 1];
						let href = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACT_URL + "/" + uid;
						let etag = resource.metadata.sources[0].etag;
						// Google sometimes answers the same contacts
						if (typeof cardbookBGSynchronizationGoogle2.contacts[aConnection.connPrefId][uid] == "undefined") {
							cardbookBGSynchronizationGoogle2.contacts[aConnection.connPrefId][uid] = { href: href, etag: etag, memberships: resource.memberships, resourceEtag: resource.etag};
							aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId]++;
							aRepo.cardbookServerSyncCompareCardWithCacheTotal[aConnection.connPrefId]++;
						}
					}
				}
				if (responseJSON.nextPageToken) {
					aRepo.cardbookServerSyncRequest[aConnection.connPrefId]++;
					cardbookBGSynchronizationGoogle2.googleSyncContacts(aRepo, aConnection, responseJSON.nextPageToken)
				} else {
					for (let uid in cardbookBGSynchronizationGoogle2.contacts[aConnection.connPrefId]) {
						let card = cardbookBGSynchronizationGoogle2.contacts[aConnection.connPrefId][uid];
						let aCardConnection = {accessToken: aConnection.accessToken, connPrefId: aConnection.connPrefId, connUrl: card.href, connDescription: aConnection.connDescription,
												connUser: aConnection.connUser, connType: aConnection.connType};
						await cardbookBGSynchronizationGoogle2.compareServerCardWithCache(aRepo, aCardConnection, uid, card.etag, uid);
						if (aRepo.cardbookCardsFromCache[aCardConnection.connPrefId][uid]) {
							delete aRepo.cardbookCardsFromCache[aCardConnection.connPrefId][uid];
							aRepo.cardbookServerSyncHandleRemainingCardTotal[aCardConnection.connPrefId]--;
						}
					}
					await cardbookBGSynchronizationGoogle2.handleRemainingCardCache(aRepo, aConnection);
				}
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			} else {
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "googleSyncContacts", aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncHandleRemainingCardDone[aConnection.connPrefId] = aRepo.cardbookServerSyncHandleRemainingCardTotal[aConnection.connPrefId];
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] = aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId];
			}
		}).catch(async (response) => {
			console.log(response);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "googleSyncContacts", aConnection.connUrl, response.status], "Error");
			aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncHandleRemainingCardDone[aConnection.connPrefId] = aRepo.cardbookServerSyncHandleRemainingCardTotal[aConnection.connPrefId];
			aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] = aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId];
		});
	},

	googleSyncWorkspaceInit: function (aRepo, aConnection) {
		cardbookBGSynchronizationGoogle2.contacts[aConnection.connPrefId] = {};
		cardbookBGSynchronizationGoogle2.googleSyncWorkspace(aRepo, aConnection);
	},

	googleSyncWorkspace: function(aRepo, aConnection, aNextPageToken) {
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationSearchingCards", [aConnection.connDescription]);
		let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken};
		let params = {};
		params["sources"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACTS_SOURCES;
		params["pageSize"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACTS_URL_SIZE;
		params["readMask"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].SYNC_PERSON_FIELDS;
		if (aNextPageToken != null) {
			params.pageToken = aNextPageToken;
		}
		let data = cardbookBGSynchronizationUtils.encodeParams(params);
		aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACTS_URL + "?" + data;
		let request = fetch(aConnection.connUrl, {
			method: "GET",
			headers: headers,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});

		request.then(async (response) => {
			if (response.ok) {
				let responseJSON = await response.json();
				aRepo.cardbookServerSyncHandleRemainingCardTotal[aConnection.connPrefId] = cardbookBGSynchronizationGoogle2.getCardsNumber(aRepo, aConnection.connPrefId);
				if (responseJSON.people) {
					for (let resource of responseJSON.people) {
						let tmpArray = resource.resourceName.split("/");
						let uid = tmpArray[tmpArray.length - 1];
						let href = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACT_URL + "/" + uid;
						let etag = resource.metadata.sources[0].etag;
						// Google sometimes answers the same contacts
						if (typeof cardbookBGSynchronizationGoogle2.contacts[aConnection.connPrefId][uid] == "undefined") {
							cardbookBGSynchronizationGoogle2.contacts[aConnection.connPrefId][uid] = { href: href, etag: etag, memberships: resource.memberships, resourceEtag: resource.etag};
							aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId]++;
							aRepo.cardbookServerSyncCompareCardWithCacheTotal[aConnection.connPrefId]++;
						}
					}
				}
				if (responseJSON.nextPageToken) {
					aRepo.cardbookServerSyncRequest[aConnection.connPrefId]++;
					cardbookBGSynchronizationGoogle2.googleSyncWorkspace(aRepo, aConnection, responseJSON.nextPageToken)
				} else {
					for (let uid in cardbookBGSynchronizationGoogle2.contacts[aConnection.connPrefId]) {
						let card = cardbookBGSynchronizationGoogle2.contacts[aConnection.connPrefId][uid];
						let aCardConnection = {accessToken: aConnection.accessToken, connPrefId: aConnection.connPrefId, connUrl: card.href, connDescription: aConnection.connDescription,
							connUser: aConnection.connUser, connType: aConnection.connType};
						await cardbookBGSynchronizationGoogle2.compareServerCardWithCache(aRepo, aCardConnection, uid, card.etag, uid);
						if (aRepo.cardbookCardsFromCache[aCardConnection.connPrefId][uid]) {
							delete aRepo.cardbookCardsFromCache[aCardConnection.connPrefId][uid];
							aRepo.cardbookServerSyncHandleRemainingCardTotal[aCardConnection.connPrefId]--;
						}
					}
					await cardbookBGSynchronizationGoogle2.handleRemainingCardCache(aRepo, aConnection);
				}
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			} else {
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "googleSyncWorkspace", aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncHandleRemainingCardDone[aConnection.connPrefId] = aRepo.cardbookServerSyncHandleRemainingCardTotal[aConnection.connPrefId];
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] = aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId];
			}
		}).catch(async (response) => {
			console.log(response);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "googleSyncWorkspace", aConnection.connUrl, response.status], "Error");
			aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncHandleRemainingCardDone[aConnection.connPrefId] = aRepo.cardbookServerSyncHandleRemainingCardTotal[aConnection.connPrefId];
			aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] = aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId];
		});
	},

	compareServerCardWithCache: async function (aRepo, aCardConnection, aUrl, aEtag, aId) {
		if (aRepo.cardbookFileCacheCards[aCardConnection.connPrefId] && aRepo.cardbookFileCacheCards[aCardConnection.connPrefId][aUrl]) {
			var myCacheCard = aRepo.cardbookFileCacheCards[aCardConnection.connPrefId][aUrl];
			var myServerCard = new cardbookCardParser();
			await cardbookBGUtils.cloneCard(myCacheCard, myServerCard);
			cardbookBGUtils.addEtag(myServerCard, aEtag);
			if (myCacheCard.etag == aEtag) {
				if (myCacheCard.deleted) {
					// "DELETEDONDISK";
					aRepo.cardbookServerDeletedCardRequest[aCardConnection.connPrefId]++;
					aRepo.cardbookServerSyncDeletedCardOnDisk[aCardConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardDeletedOnDisk", [aCardConnection.connDescription, myCacheCard.fn]);
					cardbookBGSynchronizationGoogle2.serverDeleteCard(aRepo, aCardConnection, myCacheCard);
				} else if (myCacheCard.updated) {
					// "UPDATEDONDISK";
					aRepo.cardbookServerUpdatedCardRequest[aCardConnection.connPrefId]++;
					aRepo.cardbookServerSyncUpdatedCardOnDisk[aCardConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardUpdatedOnDisk", [aCardConnection.connDescription, myCacheCard.fn]);
					cardbookBGSynchronizationGoogle2.serverUpdateCard(aRepo, aCardConnection, myCacheCard);
				} else {
					// "NOTUPDATED";
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardAlreadyGetFromCache", [aCardConnection.connDescription, myCacheCard.fn]);
					aRepo.cardbookServerCardSyncDone[aCardConnection.connPrefId]++;
					aRepo.cardbookServerSyncNotUpdatedCard[aCardConnection.connPrefId]++;
				}
			} else if (myCacheCard.deleted) {
				// "DELETEDONDISKUPDATEDONSERVER";
				aRepo.cardbookServerSyncDeletedCardOnDiskUpdatedCardOnServer[aCardConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardDeletedOnDiskUpdatedOnServer", [aCardConnection.connDescription, myCacheCard.fn]);
				var solveConflicts = cardbookBGPreferences.getPref("solveConflicts");
				if (solveConflicts === "Local") {
					var conflictResult = "delete";
				} else if (solveConflicts === "Remote") {
					var conflictResult = "keep";
				} else {
					var message = messenger.i18n.getMessage("cardDeletedOnDiskUpdatedOnServer", [aCardConnection.connDescription, myCacheCard.fn]);
					var conflictResult = await aRepo.askUser("card", aCardConnection.connPrefId, message, aRepo.importConflictChoiceSync1Values);
				}
				
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aCardConnection.connDescription + " : debug mode : conflict resolution : ", conflictResult);
				switch (conflictResult) {
					case "keep":
						await aRepo.removeCardFromRepository(myCacheCard, true);
						aRepo.cardbookServerMultiGetArray[aCardConnection.connPrefId].push(aId);
						break;
					case "delete":
						aRepo.cardbookServerDeletedCardRequest[aCardConnection.connPrefId]++;
						cardbookBGSynchronizationGoogle2.serverDeleteCard(aRepo, aCardConnection, myCacheCard);
						break;
					default:
						aRepo.cardbookServerCardSyncDone[aCardConnection.connPrefId]++;
						break;
				}
			} else if (myCacheCard.updated) {
				// "UPDATEDONBOTH";
				aRepo.cardbookServerSyncUpdatedCardOnBoth[aCardConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardUpdatedOnBoth", [aCardConnection.connDescription, myCacheCard.fn]);
				var solveConflicts = cardbookBGPreferences.getPref("solveConflicts");
				if (solveConflicts === "Local") {
					var conflictResult = "local";
				} else if (solveConflicts === "Remote") {
					var conflictResult = "remote";
				} else {
					var message = messenger.i18n.getMessage("cardUpdatedOnBoth", [aCardConnection.connDescription, myCacheCard.fn]);
					var conflictResult = await aRepo.askUser("card", aCardConnection.connPrefId, message, aRepo.importConflictChoiceSync2Values);
				}
				
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aCardConnection.connDescription + " : debug mode : conflict resolution : ", conflictResult);
				switch (conflictResult) {
					case "local":
						aRepo.cardbookServerUpdatedCardRequest[aCardConnection.connPrefId]++;
						cardbookBGSynchronizationGoogle2.serverUpdateCard(aRepo, aCardConnection, myCacheCard);
						break;
					case "remote":
						aRepo.cardbookServerMultiGetArray[aCardConnection.connPrefId].push(aId);
						break;
					case "merge":
						aRepo.cardbookServerGetCardForMergeRequest[aCardConnection.connPrefId]++;
						cardbookBGSynchronizationGoogle2.serverGetForMerge(aRepo, aCardConnection, aEtag, myCacheCard);
						break;
					default:
						aRepo.cardbookServerCardSyncDone[aCardConnection.connPrefId]++;
						break;
				}
			} else {
				// "UPDATEDONSERVER";
				aRepo.cardbookServerMultiGetArray[aCardConnection.connPrefId].push(aId);
				aRepo.cardbookServerSyncUpdatedCardOnServer[aCardConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardUpdatedOnServer", [aCardConnection.connDescription, myCacheCard.fn, aEtag, myCacheCard.etag]);
			}
		} else {
			// "NEWONSERVER";
			aRepo.cardbookServerMultiGetArray[aCardConnection.connPrefId].push(aId);
			aRepo.cardbookServerSyncNewCardOnServer[aCardConnection.connPrefId]++;
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardNewOnServer", [aCardConnection.connDescription]);
		}
		aRepo.cardbookServerSyncCompareCardWithCacheDone[aCardConnection.connPrefId]++;
	},

	handleRemainingCardCache: async function (aRepo, aConnection) {
		if (aRepo.cardbookCardsFromCache[aConnection.connPrefId]) {
			for (var i in aRepo.cardbookCardsFromCache[aConnection.connPrefId]) {
				var aCard = aRepo.cardbookCardsFromCache[aConnection.connPrefId][i];
				if (aCard.created) {
					// "NEWONDISK";
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardNewOnDisk", [aConnection.connDescription, aCard.fn]);
					aRepo.cardbookServerCreatedCardRequest[aConnection.connPrefId]++;
					aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncNewCardOnDisk[aConnection.connPrefId]++;
					var aCreateConnection = JSON.parse(JSON.stringify(aConnection));
					cardbookBGSynchronizationGoogle2.serverCreateCard(aRepo, aCreateConnection, aCard);
				} else if (aCard.updated) {
					// "UPDATEDONDISKDELETEDONSERVER";
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardUpdatedOnDiskDeletedOnServer", [aConnection.connDescription, aCard.fn]);
					aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncUpdatedCardOnDiskDeletedCardOnServer[aConnection.connPrefId]++;
					var solveConflicts = cardbookBGPreferences.getPref("solveConflicts");
					if (solveConflicts === "Local") {
						var conflictResult = "keep";
					} else if (solveConflicts === "Remote") {
						var conflictResult = "delete";
					} else {
						var message = messenger.i18n.getMessage("cardUpdatedOnDiskDeletedOnServer", [aConnection.connDescription, aCard.fn]);
						var conflictResult = await aRepo.askUser("card", aConnection.connPrefId, message, aRepo.importConflictChoiceSync1Values);
					}
					
					cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aConnection.connDescription + " : debug mode : conflict resolution : ", conflictResult);
					switch (conflictResult) {
						case "keep":
							aRepo.cardbookServerCreatedCardRequest[aConnection.connPrefId]++;
							var aCreateConnection = JSON.parse(JSON.stringify(aConnection));
							cardbookBGUtils.nullifyEtag(aCard);
							cardbookBGSynchronizationGoogle2.serverCreateCard(aRepo, aCreateConnection, aCard);
							break;
						case "delete":
							await aRepo.removeCardFromRepository(aCard, true);
							aRepo.cardbookServerGetCardRequest[aConnection.connPrefId]++;
							aRepo.cardbookServerGetCardResponse[aConnection.connPrefId]++;
							aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
							break;
						default:
							aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
							break;
					}
				} else if (!aCard.deleted) {
					// "DELETEDONSERVER";
					aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardDeletedOnServer", [aConnection.connDescription, aCard.fn]);
					await aRepo.removeCardFromRepository(aCard, true);
					aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncDeletedCardOnServer[aConnection.connPrefId]++;
				}
				aRepo.cardbookServerSyncHandleRemainingCardDone[aConnection.connPrefId]++;
			}
		}
	},

	serverGetForMerge: function(aRepo, aConnection, aEtag, aCacheCard) {
		let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken};
		let params = {};
		params["personFields"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].GET_PERSON_FIELDS;
		let data = cardbookBGSynchronizationUtils.encodeParams(params);
		aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACT_URL + "/" + aCacheCard.uid + "?" + data;
		let request = fetch(aConnection.connUrl, {
			method: "GET",
			headers: headers,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});

		request.then(async (response) => {
			if (response.ok) {
				let responseJSON = await response.json();
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetOK", [aConnection.connDescription, aCacheCard.fn]);
				let tempCard = await cardbookBGSynchronizationGoogle2.parseGoogleContactToCard(aRepo, responseJSON, aConnection.connType, aConnection.connPrefId);
				await aRepo.mergeCardsFromSync(aCacheCard, tempCard, aConnection, aEtag, "SYNC");
			} else {
				aRepo.cardbookServerGetCardForMergeError[aConnection.connPrefId]++;
				aRepo.cardbookServerGetCardForMergeResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, response.status], "Error");
			}
		}).catch(async (response) => {
			console.log(response);
			aRepo.cardbookServerGetCardForMergeError[aConnection.connPrefId]++;
			aRepo.cardbookServerGetCardForMergeResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, response.status], "Error");
		});
	},

	serverMultiGet: function(aRepo, aConnection) {
		let multiget = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].BATCH_GET_URL_SIZE;
		for (var i = 0; i < aRepo.cardbookServerMultiGetArray[aConnection.connPrefId].length; i = i + +multiget) {
			let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken};
			let params = {};
			params["personFields"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].GET_PERSON_FIELDS;
			let data = cardbookBGSynchronizationUtils.encodeParams(params);
			aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].BATCH_GET_URL + "?" + data;
			let subArray = aRepo.cardbookServerMultiGetArray[aConnection.connPrefId].slice(i, i + +multiget);
			let length = subArray.length;
			let resources = subArray.map(value => "resourceNames=people/" + value).join("&");
			aConnection.connUrl = aConnection.connUrl + "&" + resources;
			aRepo.cardbookServerMultiGetRequest[aConnection.connPrefId]++;

			let request = fetch(aConnection.connUrl, {
				method: "GET",
				headers: headers,
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});

			request.then(async (response) => {
				if (response.ok) {
					let responseJSON = await response.json();
					for (let contact of responseJSON.responses) {
						if (contact.httpStatusCode == "200") {
							let myCard = await cardbookBGSynchronizationGoogle2.parseGoogleContactToCard(aRepo, contact.person, aConnection.connType, aConnection.connPrefId);
							if (aRepo.cardbookCards[myCard.dirPrefId+"::"+myCard.uid]) {
								let myOldCard = aRepo.cardbookCards[myCard.dirPrefId+"::"+myCard.uid];
								await aRepo.removeCardFromRepository(myOldCard, true);
							}
							await cardbookBGUtils.changeMediaFromFileToContent(myCard);
							await aRepo.addCardToRepository(myCard, true);
							cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetOK", [aConnection.connDescription, myCard.fn]);
							aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
						} else {
							aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
							aRepo.cardbookServerGetCardError[aConnection.connPrefId]++;
							cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, response.status], "Error");
						}
					}
				} else {
					aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] = aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] + length;
					aRepo.cardbookServerMultiGetError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerMultiGetResponse[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] = aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] + length;
				aRepo.cardbookServerMultiGetError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerMultiGetResponse[aConnection.connPrefId]++;
			});
		}
	},

	serverDeleteCard: function(aRepo, aConnection, aCard) {
		if (cardbookBGSynchronizationUtils.getModifsPushed(aRepo, aConnection.connPrefId) <= cardbookBGPreferences.getMaxModifsPushed()) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardSendingDeletion", [aConnection.connDescription, aCard.fn]);
			aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACT_URL + "/" + aCard.uid + ":deleteContact";
			let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken};
			let request = fetch(aConnection.connUrl, {
				method: "DELETE",
				headers: headers,
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});
			request.then(async (response) => {
				if (response.ok) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardDeletedFromServer", [aConnection.connDescription, aCard.fn]);
					await aRepo.removeCardFromRepository(aCard, true);
				} else if (response.status == 404) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardNotExistServer", [aConnection.connDescription, aCard.fn]);
					await aRepo.removeCardFromRepository(aCard, true);
				} else {
					aRepo.cardbookServerDeletedCardError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardDeleteFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
				aRepo.cardbookServerDeletedCardResponse[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerDeletedCardError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardDeleteFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
				aRepo.cardbookServerDeletedCardResponse[aConnection.connPrefId]++;
			});
		} else {
			aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			aRepo.cardbookServerDeletedCardResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerNotPushed[aConnection.connPrefId]++;
		}
	},

	serverUpdateCard: function(aRepo, aConnection, aCard) {
		if (cardbookBGSynchronizationUtils.getModifsPushed(aRepo, aConnection.connPrefId) <= cardbookBGPreferences.getMaxModifsPushed()) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardSendingUpdate", [aConnection.connDescription, aCard.fn]);
			let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken};
			let params = {};
			params["updatePersonFields"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].UPDATE_PERSON_FIELDS;
			let data = cardbookBGSynchronizationUtils.encodeParams(params);
			aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACT_URL + "/" + aCard.uid + ":updateContact" + "?" + data;
			let GoogleContact = cardbookBGSynchronizationGoogle2.parseCardToGoogleContact(aRepo, aCard);
			let request = fetch(aConnection.connUrl, {
				method: "PATCH",
				headers: headers,
				body: JSON.stringify(GoogleContact),
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});

			request.then(async (response) => {
				if (response.ok) {
					let responseJSON = await response.json();
					let aModifiedCard = await cardbookBGSynchronizationGoogle2.parseGoogleContactToCard(aRepo, responseJSON, aConnection.connType, aConnection.connPrefId);
					let etag = responseJSON.metadata.sources[0].etag;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardUpdatedOnServerWithEtag", [aConnection.connDescription, aModifiedCard.fn, etag]);
					cardbookBGUtils.addEtag(aModifiedCard, etag);
					aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
					await cardbookBGUtils.changeMediaFromFileToContent(aCard);
					aModifiedCard.photo = JSON.parse(JSON.stringify(aCard.photo));
					if (aModifiedCard.photo.value) {
						cardbookBGUtils.addTagUpdated(aModifiedCard);
						aRepo.cardbookServerUpdatedCardPhotoRequest[aConnection.connPrefId]++;
						cardbookBGSynchronizationGoogle2.serverUpdateCardPhoto(aRepo, aConnection, aModifiedCard);
					} else {
						aRepo.cardbookServerUpdatedCardPhotoRequest[aConnection.connPrefId]++;
						cardbookBGSynchronizationGoogle2.serverDeleteCardPhoto(aRepo, aConnection, aModifiedCard);
						await aRepo.removeCardFromRepository(aCard, true);
						await aRepo.addCardToRepository(aModifiedCard, true);
					}
				} else {
					aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
					aRepo.cardbookServerUpdatedCardError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardUpdateFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerUpdatedCardResponse[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerUpdatedCardResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
				aRepo.cardbookServerUpdatedCardError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardUpdateFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
			});
		} else {
			aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			aRepo.cardbookServerUpdatedCardResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerNotPushed[aConnection.connPrefId]++;
		}
	},

	serverCreateCard: function(aRepo, aConnection, aCard) {
		if (cardbookBGSynchronizationUtils.getModifsPushed(aRepo, aConnection.connPrefId) <= cardbookBGPreferences.getMaxModifsPushed()) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardSendingCreate", [aConnection.connDescription, aCard.fn]);
			let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken};
			let params = {};
			params["personFields"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CREATE_PERSON_FIELDS;
			let data = cardbookBGSynchronizationUtils.encodeParams(params);
			aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACT_URL + ":createContact" + "?" + data;
			let GoogleContact = cardbookBGSynchronizationGoogle2.parseCardToGoogleContact(aRepo, aCard);
			let request = fetch(aConnection.connUrl, {
				method: "POST",
				headers: headers,
				body: JSON.stringify(GoogleContact),
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});

			request.then(async (response) => {
				if (response.ok) {
					let responseJSON = await response.json();
					await cardbookBGUtils.changeMediaFromFileToContent(aCard);
					if (aRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid]) {
						var myOldCard = aRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid];
						await aRepo.removeCardFromRepository(myOldCard, true);
					}
					let newCard = await cardbookBGSynchronizationGoogle2.parseGoogleContactToCard(aRepo, responseJSON, aConnection.connType, aConnection.connPrefId);
					let etag = responseJSON.metadata.sources[0].etag;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardCreatedOnServerWithEtag", [aConnection.connDescription, newCard.fn, etag]);
					cardbookBGUtils.addEtag(newCard, etag);
					// if aCard and aCard have the same cached medias
					newCard.photo = JSON.parse(JSON.stringify(aCard.photo));
					if (aCard.photo.value) {
						cardbookBGUtils.addTagUpdated(newCard);
						aRepo.cardbookServerUpdatedCardPhotoRequest[aConnection.connPrefId]++;
						cardbookBGSynchronizationGoogle2.serverUpdateCardPhoto(aRepo, aConnection, newCard);
					}
					await aRepo.addCardToRepository(newCard, true);
				} else {
					aRepo.cardbookServerCreatedCardError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardCreateFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerCreatedCardResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerCreatedCardError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardCreateFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerCreatedCardResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			});
		} else {
			aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			aRepo.cardbookServerCreatedCardResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerNotPushed[aConnection.connPrefId]++;
		}
	},

	serverUpdateCardPhoto: function(aRepo, aConnection, aCard) {
		if (cardbookBGSynchronizationUtils.getModifsPushed(aRepo, aConnection.connPrefId) <= cardbookBGPreferences.getMaxModifsPushed()) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardSendingUpdate", [aConnection.connDescription, aCard.fn]);
			let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken};
			let params = {};
			params["personFields"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].UPDATEPHOTO_PERSON_FIELDS;
			let data = cardbookBGSynchronizationUtils.encodeParams(params);
			aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACT_URL + "/" + aCard.uid + ":updateContactPhoto" + "?" + data;
			let photo = { "photoBytes": aCard.photo.value };
			let request = fetch(aConnection.connUrl, {
				method: "PATCH",
				headers: headers,
				body: JSON.stringify(photo),
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});

			request.then(async (response) => {
				if (response.ok) {
					let responseJSON = await response.json();
					let etag = responseJSON.person.metadata.sources[0].etag;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardUpdatedOnServerWithEtag", [aConnection.connDescription, aCard.fn, etag]);
					cardbookBGUtils.addEtag(aCard, etag);
					cardbookBGUtils.nullifyTagModification(aCard);
				} else {
					aRepo.cardbookServerUpdatedCardPhotoError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardUpdateFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
				}
				if (aRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid]) {
					await cardbookBGUtils.changeMediaFromFileToContent(aCard);
					var myOldCard = aRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid];
					await aRepo.removeCardFromRepository(myOldCard, true);
				}
				await aRepo.addCardToRepository(aCard, true);
				aRepo.cardbookServerUpdatedCardPhotoResponse[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerUpdatedCardPhotoError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardUpdateFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
				if (aRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid]) {
					await cardbookBGUtils.changeMediaFromFileToContent(aCard);
					var myOldCard = aRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid];
					await aRepo.removeCardFromRepository(myOldCard, true);
				}
				await aRepo.addCardToRepository(aCard, true);
				aRepo.cardbookServerUpdatedCardPhotoResponse[aConnection.connPrefId]++;
			});
		} else {
			aRepo.cardbookServerUpdatedCardPhotoResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerNotPushed[aConnection.connPrefId]++;
		}
	},

	serverDeleteCardPhoto: function(aRepo, aConnection, aCard) {
		if (cardbookBGSynchronizationUtils.getModifsPushed(aRepo, aConnection.connPrefId) <= cardbookBGPreferences.getMaxModifsPushed()) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardSendingUpdate", [aConnection.connDescription, aCard.fn]);
			let headers = { "Content-Type": "application/json", "GData-Version": "3", "Authorization": aConnection.accessToken};
			let params = {};
			params["personFields"] = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].UPDATEPHOTO_PERSON_FIELDS;
			let data = cardbookBGSynchronizationUtils.encodeParams(params);
			aConnection.connUrl = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnection.connType].CONTACT_URL + "/" + aCard.uid + ":deleteContactPhoto" + "?" + data;
			let request = fetch(aConnection.connUrl, {
				method: "DELETE",
				headers: headers,
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});

			request.then(async (response) => {
				if (response.ok) {
					let responseJSON = await response.json();
					let etag = responseJSON.person.metadata.sources[0].etag;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardUpdatedOnServerWithEtag", [aConnection.connDescription, aCard.fn, etag]);
					cardbookBGUtils.addEtag(aCard, etag);
					cardbookBGUtils.nullifyTagModification(aCard);
					if (aRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid]) {
						var myOldCard = aRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid];
						await aRepo.removeCardFromRepository(myOldCard, true);
					}
					await aRepo.addCardToRepository(aCard, true);
				}
				aRepo.cardbookServerUpdatedCardPhotoResponse[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerUpdatedCardPhotoResponse[aConnection.connPrefId]++;
			});
		} else {
			aRepo.cardbookServerUpdatedCardPhotoResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerNotPushed[aConnection.connPrefId]++;
		}
	},

	parseCardToGoogleContact: function (aRepo, aCard) {
		// console.debug(aCard);
		let GoogleContact = {};
		// no need for created contacts
		if (cardbookBGSynchronizationGoogle2.contacts[aCard.dirPrefId][aCard.uid]) {
			GoogleContact.resourceName = "people/" + aCard.uid;
			GoogleContact.etag = cardbookBGSynchronizationGoogle2.contacts[aCard.dirPrefId][aCard.uid].resourceEtag;
			GoogleContact.metadata = { objectType: "PERSON", sources: [ { etag: aCard.etag, id: aCard.uid, type: "CONTACT" } ] };
		}
		GoogleContact.names = [];                                                          
		let name = {};
		name.familyName = aCard.lastname;
		name.givenName = aCard.firstname;
		name.honorificPrefix = aCard.prefixname;
		name.honorificSuffix = aCard.suffixname;
		name.middleName = aCard.othername;
		let isN = aCard.lastname || aCard.firstname || aCard.prefixname || aCard.suffixname || aCard.othername;
		if (isN || aCard.org.length) {
			name.displayName = aCard.fn;
			name.unstructuredName = aCard.fn;
			if (!isN) {
				name.givenName = aCard.fn;
			}
		}
		GoogleContact.names.push(name);

		if (aCard.note) {
			GoogleContact.biographies = [];
			let biographie = {};
			biographie.value = aCard.note;
			biographie.contentType = "TEXT_PLAIN";
			GoogleContact.biographies.push(biographie);
		}

		if (aCard.nickname) {
			GoogleContact.nicknames = [];
			let nickname = {};
			nickname.value = aCard.nickname;
			GoogleContact.nicknames.push(nickname);
		}

		// if (["F", "M"].includes(aCard.gender)) {
		// 	GoogleContact.genders = [];
		// 	let gender = {};
		// 	if (aCard.gender == "F") {
		// 		gender.value = "female";
		// 	} else if (aCard.gender == "M") {
		// 		gender.value = "male";
		// 	}
		// 	GoogleContact.genders.push(gender);
		// }

		let isDate = cardbookHTMLDates.convertDateStringToDateUTC(aCard.bday);
		if (isDate != "WRONGDATE") {
			GoogleContact.birthdays = [];
			let birthday = {};
			let dateSplitted = cardbookHTMLDates.splitUTCDateIntoComponents(isDate);
			let day = parseInt(dateSplitted.day);
			let month = parseInt(dateSplitted.month);
			if (dateSplitted.year == "1604") {
				birthday.date = {month: month, day: day};
				birthday.text = dateSplitted.day + "/" + dateSplitted.month;
			} else {
				birthday.date = {year: dateSplitted.year, month: month, day: day};
				birthday.text = dateSplitted.day + "/" + dateSplitted.month + "/" + dateSplitted.year;
			}
			GoogleContact.birthdays.push(birthday);
		}
		
		if (aCard.org.length || aCard.title) {
			GoogleContact.organizations = [];
			let organization = {};
			if (aCard.org[1]) {
				organization.department = aCard.org[1];
			}
			if (aCard.org[0]) {
				organization.name = aCard.org[0];
			}
			if (aCard.title) {
				organization.title = aCard.title;
			}
			if (aCard.role) {
				organization.jobDescription = aCard.role;
			}
			GoogleContact.organizations.push(organization);
		}

		if (aCard.categories.length) {
			GoogleContact.memberships = [];
			for (let category of aCard.categories) {
				let membership = {};
				for (let i in aRepo.cardbookCategories) {
					let myCategory = aRepo.cardbookCategories[i];
					if (myCategory.name == category && myCategory.dirPrefId == aCard.dirPrefId) {
						membership.contactGroupId = myCategory.uid;
						membership.contactGroupResourceName = "contactGroups/" + myCategory.uid;
						break;
					}          
				}
				GoogleContact.memberships.push({contactGroupMembership: membership});
			}
		}
		if (cardbookBGSynchronizationGoogle2.contacts[aCard.dirPrefId][aCard.uid]) {
			if (cardbookBGSynchronizationGoogle2.contacts[aCard.dirPrefId][aCard.uid].memberships) {
				for (let membership of cardbookBGSynchronizationGoogle2.contacts[aCard.dirPrefId][aCard.uid].memberships) {
					if (!GoogleContact.memberships) {
						GoogleContact.memberships = [];
					}
					if (membership.contactGroupMembership && membership.contactGroupMembership.contactGroupId) {
						if (cardbookBGSynchronizationGoogle2.skippedLabels[aCard.dirPrefId].includes(membership.contactGroupMembership.contactGroupId)) {
							GoogleContact.memberships.push(membership);
						}
					}
				}
			}
		}

		let resultCustoms = [];
		let customFields = cardbookBGPreferences.getAllCustomFields();
		for (let type of ["personal", "org"]) {
			for (let custom of customFields[type]) {
				let customValue = cardbookHTMLUtils.getCardValueByField(aCard, custom[0], false)[0];
				if (customValue) {
					resultCustoms.push([custom[1], customValue]);
				}
			}
		}
		if (resultCustoms.length) {
			GoogleContact.userDefined = [];
			for (let resultCustom of resultCustoms) {
				let custom = {};
				custom.key = resultCustom[0];
				custom.value = resultCustom[1];
				GoogleContact.userDefined.push(custom);
			}
		}
		
		let resultEvents = [];
		let events = cardbookHTMLUtils.getEventsFromCard(aCard.note.split("\n"), aCard.others);
		for (let event of events.result) {
			let isDate = cardbookHTMLDates.convertDateStringToDateUTC(event[0]);
			if (isDate != "WRONGDATE") {
				let dateSplitted = cardbookHTMLDates.splitUTCDateIntoComponents(isDate);
				resultEvents.push([event[1], dateSplitted]);
			}
		}
		if (resultEvents.length) {
			GoogleContact.events = [];
			for (let resultEvent of resultEvents) {
				let event = {};
				event.type = resultEvent[0];
				event.date = resultEvent[1];
				GoogleContact.events.push(event);
			}
		}
		
		function buildElement(aType, aElement, aLine, aLineValue) {
			let types = [];
			types = cardbookHTMLUtils.getOnlyTypesFromTypes(aLine[1]);
			if (aLine[3].length != 0 && aLine[2] != "") {
				let found = false;
				for (let j = 0; j < aLine[3].length; j++) {
					let tmpArray = aLine[3][j].split(":");
					if (tmpArray[0] == "X-ABLABEL") {
						aElement.type = tmpArray[1];
						found = true;
						break;
					}
				}
				if (!found) {
					aElement.type = types[0];
				}
			} else {
				aElement.type = types[0];
			}

			let pref = aLine[1].filter(type => type.toLowerCase() == "pref=1" || type.toLowerCase() == "pref" || type.toLowerCase() == "type=pref");
			if (pref.length) {
				aElement.metadata = {}
				aElement.metadata.primary = true;
			}
			if (aLineValue) {
				aElement.value = aLineValue;
			}
			return aElement;
		}
		if (aCard.email.length) {
			GoogleContact.emailAddresses = [];
			for (let emailLine of aCard.email) {
				let emailAddress = {};
				emailAddress = buildElement("email", emailAddress, emailLine, emailLine[0][0]);
				GoogleContact.emailAddresses.push(emailAddress);
			}
		}
		if (aCard.tel.length) {
			GoogleContact.phoneNumbers = [];
			for (let telLine of aCard.tel) {
				let phoneNumber = {};
				phoneNumber = buildElement("tel", phoneNumber, telLine, telLine[0][0]);
				GoogleContact.phoneNumbers.push(phoneNumber);
			}
		}
		if (aCard.url.length) {
			GoogleContact.urls = [];
			for (let urlLine of aCard.url) {
				let url = {};
				url = buildElement("url", url, urlLine, urlLine[0][0]);
				GoogleContact.urls.push(url);
			}
		}
		if (aCard.adr.length) {
			GoogleContact.addresses = [];
			for (let adrLine of aCard.adr) {
				let address = {};
				address = buildElement("adr", address, adrLine);
				address.poBox = adrLine[0][0];
				address.extendedAddress = adrLine[0][1];
				address.streetAddress = adrLine[0][2];
				address.city = adrLine[0][3];
				address.region = adrLine[0][4];
				address.postalCode = adrLine[0][5];
				let countryCode = cardbookHTMLUtils.getCountryCodeFromCountryName(adrLine[0][6]);
				if (countryCode.length == 2) {
					address.countryCode = countryCode;
				} else if (countryCode) {
					address.country = countryCode;
				}
				GoogleContact.addresses.push(address);
			}
		}
		if (aCard.impp.length) {
			GoogleContact.imClients = [];
			for (let imppLine of aCard.impp) {
				let imClient = {};
				imClient = buildElement("impp", imClient, imppLine);
				let imppArray = imppLine[0][0].split(":");
				imClient.protocol = imppArray[0];
				imClient.username = imppArray[1];
				GoogleContact.imClients.push(imClient);
			}
		}
		// console.debug(GoogleContact);
		return GoogleContact;
	},

	parseGoogleContactToCard: async function (aRepo, aGoogleContact, aConnectionType, aDirPrefId) {
		// console.debug(aGoogleContact);
		let tmpArray = aGoogleContact.resourceName.split("/");
		let uid = tmpArray[tmpArray.length - 1];
		let ABType = cardbookBGPreferences.getType(aDirPrefId);
		let href = cardbookBGSynchronizationGoogle2.googleOAuthData[ABType].CONTACT_URL + "/" + uid;
		let objectType = cardbookBGSynchronizationGoogle2.googleOAuthData[ABType].OBJECT_TYPE;
		let aCard = new cardbookCardParser("", href, aGoogleContact.etag, aDirPrefId);
		aCard.uid = uid;
		aCard.cardurl = href;
		aCard.etag = aGoogleContact.metadata.sources[0].etag;
		aCard.version = cardbookBGSynchronizationGoogle2.googleOAuthData[ABType].VCARD_VERSIONS[0];
		cardbookBGUtils.setCacheURIFromCard(aCard, ABType);
		if (aGoogleContact.names && aGoogleContact.names[0]) {
			let name = aGoogleContact.names[0];
			if (name.displayName) {
				aCard.fn = name.displayName;
			}
			if (name.familyName) {
				aCard.lastname = name.familyName;
			}
			if (name.givenName) {
				aCard.firstname = name.givenName;
			}
			if (name.honorificPrefix) {
				aCard.prefixname = name.honorificPrefix;
			}
			if (name.honorificSuffix) {
				aCard.suffixname = name.honorificSuffix;
			}
			if (name.middleName) {
				aCard.othername = name.middleName;
			}
		}
		if (aGoogleContact.biographies && aGoogleContact.biographies[0]) {
			let biographie = aGoogleContact.biographies[0];
			aCard.note = biographie.value;
		}
		if (aGoogleContact.nicknames && aGoogleContact.nicknames[0]) {
			let nickname = aGoogleContact.nicknames[0];
			aCard.nickname = nickname.value;
		}
		// if (aGoogleContact.genders && aGoogleContact.genders[0]) {
		// 	let gender = aGoogleContact.genders[0];
		// 	if (gender.value == "male") {
		// 		aCard.gender = "M";
		// 	} else if (gender.value == "female") {
		// 		aCard.gender = "F";
		// 	}
		// }
		if (aGoogleContact.birthdays && aGoogleContact.birthdays[0]) {
			let birthday = aGoogleContact.birthdays[0];
			if (birthday.date) {
				let day = cardbookHTMLDates.lPad(birthday.date.day);
				let month = cardbookHTMLDates.lPad(birthday.date.month);
				if (birthday.date.year) {
					aCard.bday = cardbookHTMLDates.getFinalDateString(day, month, birthday.date.year, cardbookBGSynchronizationGoogle2.googleOAuthData[aConnectionType].VCARD_VERSIONS[0]);
				} else {
					aCard.bday = cardbookHTMLDates.getFinalDateString(day, month, "", cardbookBGSynchronizationGoogle2.googleOAuthData[aConnectionType].VCARD_VERSIONS[0]);
				}
			} else if (birthday.text) {
				let dateArray = birthday.text.split("/");
				let day = cardbookHTMLDates.lPad(dateArray[0]);
				let month = cardbookHTMLDates.lPad(dateArray[1]);
				if (dateArray[2]) {
					aCard.bday = cardbookHTMLDates.getFinalDateString(day, month, dateArray[2], cardbookBGSynchronizationGoogle2.googleOAuthData[aConnectionType].VCARD_VERSIONS[0]);
				} else {
					aCard.bday = cardbookHTMLDates.getFinalDateString(day, month, "", cardbookBGSynchronizationGoogle2.googleOAuthData[aConnectionType].VCARD_VERSIONS[0]);
				}
			}
		}
		if (aGoogleContact.organizations && aGoogleContact.organizations[0]) {
			let organization = aGoogleContact.organizations[0];
			if (organization.name) {
				aCard.org.push(organization.name);
			}
			if (organization.title) {
				aCard.title = organization.title;
			}
			if (organization.jobDescription) {
				aCard.role = organization.jobDescription;
			}
			if (organization.department) {
				aCard.org.push(organization.department);
			}
		}
		if (aGoogleContact.memberships) {
			for (let membership of aGoogleContact.memberships) {
				if (membership.contactGroupMembership && membership.contactGroupMembership.contactGroupId) {
					let catId = membership.contactGroupMembership.contactGroupId;
					for (let i in aRepo.cardbookCategories) {
						let myCategory = aRepo.cardbookCategories[i];
						if (myCategory.uid == catId) {
							aCard.categories.push(myCategory.name);
							break;
						}
					}
				}
			}
		}
		if (aGoogleContact.photos) {
			for (let photo of aGoogleContact.photos) {
				let value = photo.url;
				let extension = cardbookHTMLUtils.getFileExtension(value);
				aCard.photo = {types: [], value: "", URI: value, extension: extension, attachmentId: ""};
				break;
			}
		}
		if (aGoogleContact.userDefined) {
			let customFields = cardbookBGPreferences.getAllCustomFields();
			for (let custom of aGoogleContact.userDefined) {
				if (custom.value) {
					let found = false;
					for (let customCB of customFields.personal) {
						if (customCB[1] == custom.key) {
							cardbookHTMLUtils.setCardValueByField(aCard, customCB[0], custom.value);
							found = true;
							break;
						}
					}
					if (!found) {
						for (let customCB of customFields.org) {
							if (customCB[1] == custom.key) {
								cardbookHTMLUtils.setCardValueByField(aCard, customCB[0], custom.value);
								found = true;
								break;
							}
						}
					}
					if (!found) {
						let i = 0;
						let condition = true;
						let rootName = "X-GOOGLE";
						while (condition) {
							let newfound = false;
							for (let customCB of customFields.personal) {
								if (rootName + i == customCB[0]) {
									newfound = true;
									break;
								}
							}
							if (newfound) {
								i++;
							} else {
								condition = false;
							}
						}
						cardbookBGPreferences.setCustomFields("personal", customFields.personal.length, rootName + i + ":" + custom.key);
						cardbookHTMLUtils.setCardValueByField(aCard, rootName + i, custom.value);
					}
				}
			}
		}
		if (aGoogleContact.events) {
			let events = [];
			let sourceDateFormat = cardbookBGSynchronizationGoogle2.googleOAuthData[aConnectionType].VCARD_VERSIONS[0];
			for (let event of aGoogleContact.events) {
				if (event.metadata && event.metadata.source && event.metadata.source.type && event.metadata.source.type == objectType) {
					let eventDate = "";
					if (event.date.year && String(event.date.year).length == 4) {
						eventDate = cardbookHTMLDates.getFinalDateString(String(event.date.day), String(event.date.month), String(event.date.year), sourceDateFormat);
					} else {
						eventDate = cardbookHTMLDates.getFinalDateString(String(event.date.day), String(event.date.month), "", sourceDateFormat);
					}
					events.push([eventDate, event.type, (event.metadata && event.metadata.primary == true)]);
				}
			}
			let dateFormat = cardbookHTMLUtils.getDateFormat(aCard.dirPrefId, aCard.version);
			let myPGNextNumber = cardbookHTMLUtils.rebuildAllPGs(aCard);
			cardbookHTMLUtils.addEventstoCard(aCard, events, myPGNextNumber, sourceDateFormat, dateFormat);
		}
		function buildLine(aElement, aElementName, aValue) {
			let line = [];
			if (aElement.type) {
				let type = "";
				for (let elementType of cardbookHTMLTypes.cardbookCoreTypes.GOOGLE2[aElementName]) {
					if (elementType[1] == aElement.type) {
						type = elementType[1];
						break;
					}
				}
				if (type) {
					line = [ aValue, [ "TYPE=" + type ], "", [], "" ];
				} else {
					let myPGNextNumber = cardbookHTMLUtils.rebuildAllPGs(aCard);
					line = [ aValue, [], "ITEM" + myPGNextNumber, [ "X-ABLABEL:" + aElement.type ], "" ];
				}
			} else {
				line = [ aValue, [], "", [], "" ];
			}
			return line;
		}
		function addPrefToLine(aLine, aElement) {
			if (aElement.metadata && aElement.metadata.primary == true) {
				if (aCard.version == "3.0") {
					aLine[1].push("TYPE=PREF");
				} else {
					aLine[1].push("PREF");
				}
			}
		}
		if (aGoogleContact.emailAddresses) {
			for (let email of aGoogleContact.emailAddresses) {
				if (email.metadata && email.metadata.source && email.metadata.source.type && email.metadata.source.type == objectType) {
					let emailLine = buildLine(email, "email", [ email.value ]);
					addPrefToLine(emailLine, email);
					aCard.email.push(emailLine);
				}
			}
		}
		if (aGoogleContact.phoneNumbers) {
			for (let tel of aGoogleContact.phoneNumbers) {
				if (tel.metadata && tel.metadata.source && tel.metadata.source.type && tel.metadata.source.type == objectType) {
					let telLine = buildLine(tel, "tel", [ tel.value ]);
					addPrefToLine(telLine, tel);
					aCard.tel.push(telLine);
				}
			}
		}
		if (aGoogleContact.urls) {
			for (let url of aGoogleContact.urls) {
				if (url.metadata && url.metadata.source && url.metadata.source.type && url.metadata.source.type == objectType) {
					let urlLine = buildLine(url, "url", [ url.value ]);
					addPrefToLine(urlLine, url);
					aCard.url.push(urlLine);
				}
			}
		}
		if (aGoogleContact.addresses) {
			for (let adr of aGoogleContact.addresses) {
				if (adr.metadata && adr.metadata.source && adr.metadata.source.type && adr.metadata.source.type == objectType) {
					let country = adr.countryCode || adr.country || "";
					let region = adr.region || "";
					let city = adr.city || "";
					let poBox = adr.poBox || "";
					let postalCode = adr.postalCode || "";
					let streetAddress = adr.streetAddress || "";
					let extendedAddress = adr.extendedAddress || "";
					if (poBox || extendedAddress || streetAddress || city || region || postalCode || country) {
						let adrLine = buildLine(adr, "adr", [ poBox, extendedAddress, streetAddress, city, region, postalCode, country ]);
						addPrefToLine(adrLine, adr);
						aCard.adr.push(adrLine);
					}
				}
			}
		}
		if (aGoogleContact.imClients) {
			for (let impp of aGoogleContact.imClients) {
				if (impp.metadata && impp.metadata.source && impp.metadata.source.type && impp.metadata.source.type == objectType) {
					let serviceLine = [];
					if (impp.protocol) {
						serviceLine = cardbookHTMLTypes.getIMPPLineForCode(impp.protocol);
					}
					let protocol = serviceLine[2] || impp.protocol;
					let value = impp.username;
					if (protocol) {
						value = protocol + ":" + value;
					}
					let imppLine = [ [ value ], [], "", [], "" ];
					addPrefToLine(imppLine, impp);
					aCard.impp.push(imppLine);
				}
			}
		}
		if (!aCard.fn) {
			cardbookHTMLFormulas.getDisplayedName(aCard, aCard.dirPrefId,
				[aCard.prefixname, aCard.firstname, aCard.othername, aCard.lastname, aCard.suffixname, aCard.nickname],
				[aCard.org, aCard.title, aCard.role]);
		}
		cardbookBGUtils.setCalculatedFields(aCard);
		// console.debug(aCard);
		return aCard;
	},

};
