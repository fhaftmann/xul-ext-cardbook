import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookBGUtils } from "./cardbookBGUtils.mjs";
import { cardbookCardParser } from "./cardbookCardParser.mjs";
import { cardbookIDBImage } from "../indexedDB/cardbookIDBImage.mjs";
import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";

export var cardbookBGDuplicate = {
	makeSearchStringWithoutNumber: function (aString) {
		return cardbookBGUtils.makeSearchString(aString).replace(/([0123456789]+)/g, "");
	},

    generateCardArray: function (aCard) {
        try {
            let myResultTry = [];
            let myResultSure = {id: "", firstname: "", lastname: "", tel: [], email: []};
            for (let field of [ "firstname" , "lastname" ]) {
                if (aCard[field]) {
                    myResultTry.push(cardbookBGDuplicate.makeSearchStringWithoutNumber(aCard[field]));
                    myResultSure[field] = cardbookBGDuplicate.makeSearchStringWithoutNumber(aCard[field]);
                }
            }
            for (let emailLine of aCard.email) {
                let email = emailLine[0][0];
                var myCleanEmail = email.replace(/([\\\/\:\*\?\"\'\-\<\>\| ]+)/g, "").replace(/([0123456789]+)/g, "").toUpperCase();
                var myEmailArray = myCleanEmail.split("@");
                var myEmailArray1 = myEmailArray[0].replace(/([^\+]*)(.*)/, "$1").split(".");
                myResultTry = myResultTry.concat(myEmailArray1);
                myResultSure.email.push(email.toUpperCase());
            }
            for (let telLine of aCard.tel) {
                let tel = telLine[0][0];
                tel = cardbookHTMLUtils.formatTelForSearching(tel);
                myResultSure.tel.push(tel);
            }
            myResultSure.id = aCard.uid;
            myResultTry = cardbookHTMLUtils.arrayUnique(myResultTry);
            return {resultTry : myResultTry, resultSure : myResultSure};
        }
        catch (e) {
            console.log("cardbookBGDuplicate.generateCardArray error : " + e, "Error");
        }
    },

    compareCardArrayTry: function (aArray1, aArray2) {
        try {
            if (aArray1.length == 1) {
                if (aArray2.length != 1) {
                    return false;
                } else if (aArray1[0] == aArray2[0]) {
                    return true;
                } else {
                    return false;
                }
            } else {
                var count = 0;
                for (var i = 0; i < aArray1.length; i++) {
                    for (var j = 0; j < aArray2.length; j++) {
                        if (aArray1[i] == aArray2[j]) {
                            count++;
                            break;
                        }
                    }
                    if (count == 2) {
                        return true;
                    }
                }
            }
            return false;
        }
        catch (e) {
            console.log("cardbookBGDuplicate.compareCardArrayTry error : " + e, "Error");
        }
    },

    compareCardArraySure: function (aArray1, aArray2) {
        try {
            if (aArray1.lastname && aArray1.firstname && aArray2.lastname && aArray2.firstname) {
                if ((aArray1.lastname == aArray2.lastname && aArray1.firstname == aArray2.firstname) ||
                    (aArray1.lastname == aArray2.firstname && aArray1.firstname == aArray2.lastname)) {
                    return true;
                }
            }
            for (let field of [ "email" , "tel" ]) {
                for (var i = 0; i < aArray1[field].length; i++) {
                    for (var j = 0; j < aArray2[field].length; j++) {
                        if (aArray1[field][i] == aArray2[field][j]) {
                            return true;
                            break;
                        }
                    }
                }
            }
            return false;
        }
        catch (e) {
            console.log("cardbookBGDuplicate.compareCardArraySure error : " + e, "Error");
        }
    },

    mergeOne: async function (aRepo, aRecord, aSourceCat, aTargetCat, aActionId) {
        let outCard = new cardbookCardParser();
        outCard.dirPrefId = aRecord[0].dirPrefId;
        outCard.version = aRecord[0].version;
        
        for (var j of [ 'photo' ]) {
            var dirname = cardbookBGPreferences.getName(outCard.dirPrefId);
            var image = {};
            await cardbookIDBImage.getImage(aRepo.statusInformation, j, dirname, aRecord[0].dirPrefId+"::"+aRecord[0].uid, aRecord[0].fn)
                .then(imageFound => {
                    if (imageFound && imageFound.content && imageFound.extension) {
                        image = imageFound;
                    }})
                .catch( () => {} );
            if (image.content && image.content != "") {
                outCard[j].value = image.content;
                outCard[j].extension = image.extension;
            } else {
                var out = false;
                for (var k = 1; k < aRecord.length; k++) {
                    await cardbookIDBImage.getImage(aRepo.statusInformation, j, dirname, aRecord[k].dirPrefId+"::"+aRecord[k].uid, aRecord[k][j].fn)
                        .then(image => {
                            if (image && image.content && image.extension) {
                                outCard[j].value = image.content;
                                outCard[j].extension = image.extension;
                                out = true;
                            }
                        })
                        .catch( () => { } );
                    if (out == true) {
                        break;
                    }
                }
            }
        }
        var fields = cardbookHTMLUtils.allColumns.display.concat(cardbookHTMLUtils.allColumns.personal);
        fields = fields.concat(cardbookHTMLUtils.allColumns.org);
        for (let j of fields) {
            outCard[j] = aRecord[0][j];
            if (!outCard[j]) {
                for (let k = 1; k < aRecord.length; k++) {
                    if (aRecord[k][j]) {
                        outCard[j] = JSON.parse(JSON.stringify(aRecord[k][j]));
                        break;
                    }
                }
            }
        }
        let customFields = cardbookBGPreferences.getAllCustomFields();
        for (let j in customFields) {
            for (let k = 0; k < customFields[j].length; k++) {
                if (!cardbookHTMLUtils.getCardValueByField(outCard, customFields[j][k][0], false).length) {
                    for (let l = 1; l < aRecord.length; l++) {
                        if (cardbookHTMLUtils.getCardValueByField(aRecord[l], customFields[j][k][0], false).length) {
                            outCard.others.push(customFields[j][k][0] + ":" + cardbookHTMLUtils.getCardValueByField(aRecord[l], customFields[j][k][0], false)[0]);
                            break;
                        }
                    }
                }
            }
        }
        for (let j of cardbookHTMLUtils.allColumns.categories) {
            outCard[j] = JSON.parse(JSON.stringify(aRecord[0][j]));
            for (let k = 1; k < aRecord.length; k++) {
                outCard[j] = outCard[j].concat(aRecord[k][j]);
            }
            outCard[j] = cardbookHTMLUtils.arrayUnique(outCard[j]);
        }
        for (let j of cardbookHTMLUtils.multilineFields) {
            outCard[j] = JSON.parse(JSON.stringify(aRecord[0][j]));
            for (let k = 1; k < aRecord.length; k++) {
                outCard[j] = outCard[j].concat(aRecord[k][j]);
            }
            for (var k=0; k<outCard[j].length; ++k) {
                for (var l=k+1; l<outCard[j].length; ++l) {
                    if (j == "tel" && cardbookHTMLUtils.formatTelForSearching(outCard[j][k][0][0]) == cardbookHTMLUtils.formatTelForSearching(outCard[j][l][0][0])) {
                        outCard[j].splice(l--, 1);
                    } else if (j != "tel" && outCard[j][k][0][0] == outCard[j][l][0][0]) {
                        outCard[j].splice(l--, 1);
                    }
                }
            }
        }
        for (let j of [ 'event' ]) {
            for (let k = 0; k < aRecord.length; k++) {
                let myEvents = cardbookHTMLUtils.getEventsFromCard(aRecord[k].note.split("\n"), aRecord[k].others);
                let dateFormat = cardbookHTMLUtils.getDateFormat(aRecord[k].dirPrefId, aRecord[k].version);
                let myPGNextNumber = cardbookHTMLUtils.rebuildAllPGs(outCard);
                cardbookHTMLUtils.addEventstoCard(outCard, myEvents.result, myPGNextNumber, dateFormat, dateFormat);
            }
            // to do array unique
        }
        for (let j of cardbookHTMLUtils.allColumns.note) {
            outCard[j] = aRecord[0][j];
            if (!outCard[j]) {
                for (let k = 1; k < aRecord.length; k++) {
                    if (aRecord[k][j]) {
                        outCard[j] = JSON.parse(JSON.stringify(aRecord[k][j]));
                        break;
                    }
                }
            }
        }

        for (let card of aRecord) {
            let newCard = new cardbookCardParser();
            await cardbookBGUtils.cloneCard(card, newCard);
            cardbookHTMLUtils.addCategoryToCard(newCard, aSourceCat);
            await aRepo.saveCardFromUpdate(card, newCard, aActionId, true);
        }
        cardbookHTMLUtils.addCategoryToCard(outCard, aTargetCat);
        await aRepo.saveCardFromUpdate({}, outCard, aActionId, true);
    }
};
