import { cardbookListConversion } from "./cardbookListConversion.mjs";
import { cardbookCardParser } from "./cardbookCardParser.mjs";
import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookBGSynchronizationUtils } from "./cardbookBGSynchronizationUtils.mjs";

import { cardbookHTMLDates } from "../../HTML/utils/scripts/cardbookHTMLDates.mjs"
import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";
import { MimeAddressParser } from "../mimeParser/MimeJSComponents.sys.mjs";
import { cardbookIDBImage } from "../indexedDB/cardbookIDBImage.mjs";

export var cardbookBGUtils = {

	addTagCreated: function(aCard) {
		cardbookBGUtils.nullifyTagModification(aCard);
		aCard.created = true;
	},

	addTagUpdated: function(aCard) {
		cardbookBGUtils.nullifyTagModification(aCard);
		aCard.updated = true;
	},

	addTagDeleted: function(aCard) {
		cardbookBGUtils.nullifyTagModification(aCard);
		aCard.deleted = true;
	},

	nullifyTagModification: function(aCard) {
		aCard.created = false;
		aCard.updated = false;
		aCard.deleted = false;
	},

	updateRev: function(aCard) {
		let sysdate = cardbookHTMLDates.getDateUTC();
		if (aCard.version == "4.0") {
			aCard.rev = sysdate.year + sysdate.month + sysdate.day + "T" + sysdate.hour + sysdate.min + sysdate.sec + "Z";
		} else {
			aCard.rev = sysdate.year + "-" + sysdate.month + "-" + sysdate.day + "T" + sysdate.hour + ":" + sysdate.min + ":" + sysdate.sec + "Z";
		}
	},

	addEtag: function(aCard, aEtag) {
		if (aEtag) {
			cardbookBGUtils.nullifyEtag(aCard);
			aCard.etag = aEtag;
		}
	},

	nullifyEtag: function(aCard) {
		aCard.etag = "";
	},

	getAccountId: function(aAccount) {
		var result = aAccount.split("::");
		if (result) {
			return result[0];
		} else {
			return aAccount;
		}
	},

	sumElements: function (aObject) {
		let sum = 0;
		for (let i in aObject) {
			sum = sum + aObject[i];
		}
		return sum;
	},

	isMyAccountRemote: function (aType) {
		switch(aType) {
			case "STANDARD":
			case "DIRECTORY":
			case "FILE":
			case "LOCALDB":
			case "SEARCH":
				return false;
		};
		return true;
	},
	
	isMyAccountLocal: function (aType) {
		return !cardbookBGUtils.isMyAccountRemote(aType);
	},

	getABIconType: function (aType) {
		switch(aType) {
			case "DIRECTORY":
				return "directory";
				break;
			case "FILE":
				return "file";
				break;
			case "LOCALDB":
				return "localdb";
				break;
			case "APPLE":
			case "CARDDAV":
			case "GOOGLE2":
			case "GOOGLE3":
			case "OFFICE365":
			case "YAHOO":
				return "remote";
				break;
			case "SEARCH":
				return "search";
				break;
			case "ALL":
				return [ "directory", "file", "localdb", "remote", "search" ];
				break;
		};
		return aType.toLowerCase();
	},

	setCardUUID: function (aCard) {
		var result = cardbookHTMLUtils.getUUID();
		if (aCard.dirPrefId) {
			if (cardbookBGPreferences.getUrnuuid(aCard.dirPrefId)) {
				aCard.uid = "urn:uuid:" + result;
			} else {
				aCard.uid = result;
			}
		} else {
			aCard.uid = result;
		}
		aCard.cbid = aCard.dirPrefId + "::" + aCard.uid;
	},

	setCacheURIFromValue: function(aCard, aValue) {
		if (aCard.cacheuri != "") {
			return;
		} else if (aValue) {
			aCard.cacheuri = aValue;
		}
	},

	cloneCategory: function(sourceCategory, targetCategory) {
		targetCategory.dirPrefId = sourceCategory.dirPrefId;
		targetCategory.href = sourceCategory.href;
		targetCategory.etag = sourceCategory.etag;
		targetCategory.cbid = sourceCategory.cbid;
		targetCategory.uid = sourceCategory.uid;
		targetCategory.name = sourceCategory.name;
		targetCategory.updated = sourceCategory.updated;
		targetCategory.created = sourceCategory.created;
		targetCategory.deleted = sourceCategory.deleted;
	},

	cloneCard: async function(sourceCard, targetCard) {
		if (Object.keys(sourceCard).length === 0) {
			return;
		}
		targetCard.dirPrefId = sourceCard.dirPrefId;
		targetCard.cardurl = sourceCard.cardurl;
		targetCard.etag = sourceCard.etag;

		targetCard.lastname = sourceCard.lastname;
		targetCard.firstname = sourceCard.firstname;
		targetCard.othername = sourceCard.othername;
		targetCard.prefixname = sourceCard.prefixname;
		targetCard.suffixname = sourceCard.suffixname;
		targetCard.fn = sourceCard.fn;
		targetCard.nickname = sourceCard.nickname;
		targetCard.bday = sourceCard.bday;
		targetCard.gender = sourceCard.gender;
		targetCard.birthplace = sourceCard.birthplace;
		targetCard.anniversary = sourceCard.anniversary;
		targetCard.deathdate = sourceCard.deathdate;
		targetCard.deathplace = sourceCard.deathplace;

		targetCard.adr = JSON.parse(JSON.stringify(sourceCard.adr));
		targetCard.tel = JSON.parse(JSON.stringify(sourceCard.tel));
		targetCard.email = JSON.parse(JSON.stringify(sourceCard.email));
		targetCard.emails = JSON.parse(JSON.stringify(sourceCard.emails));
		targetCard.url = JSON.parse(JSON.stringify(sourceCard.url));
		targetCard.impp = JSON.parse(JSON.stringify(sourceCard.impp));
		targetCard.categories = JSON.parse(JSON.stringify(sourceCard.categories));

		targetCard.mailer = sourceCard.mailer;
		targetCard.tz = JSON.parse(JSON.stringify(sourceCard.tz));
		targetCard.geo = JSON.parse(JSON.stringify(sourceCard.geo));
		targetCard.title = sourceCard.title;
		targetCard.role = sourceCard.role;
		targetCard.agent = sourceCard.agent;
		targetCard.org = JSON.parse(JSON.stringify(sourceCard.org));
		targetCard.note = sourceCard.note;
		targetCard.sortstring = sourceCard.sortstring;
		targetCard.uid = sourceCard.uid;

		targetCard.member = JSON.parse(JSON.stringify(sourceCard.member));
		targetCard.kind = sourceCard.kind;

		for (let media of cardbookHTMLUtils.allColumns.media) {
			targetCard[media] = JSON.parse(JSON.stringify(sourceCard[media]));
		}

		targetCard.version = sourceCard.version;
		targetCard.class1 = sourceCard.class1;
		targetCard.key = sourceCard.key;

		targetCard.updated = sourceCard.updated;
		targetCard.created = sourceCard.created;
		targetCard.deleted = sourceCard.deleted;

		targetCard.others = JSON.parse(JSON.stringify(sourceCard.others));
		
		targetCard.isAList = sourceCard.isAList;
		targetCard.cbid = sourceCard.cbid;
		targetCard.prodid = sourceCard.prodid;
		targetCard.rev = sourceCard.rev;
	},

	splitLine: function (vString) {
		let lLineLength = 75;
		let lResult = "";
		while (vString.length) {
			if (lResult == "") {
				lResult = vString.substr(0, lLineLength);
				vString = vString.substr(lLineLength);
			} else {
				lResult = lResult + "\r\n " + vString.substr(0, lLineLength - 1);
				vString = vString.substr(lLineLength - 1);
			}
		}
		return lResult;
	},

	appendArrayToVcardData: function (aInitialValue, aField, aVersion, aArray) {
		let aResultValue = aInitialValue;
		for (let line of aArray) {
			if (line[2]) {
				let lString = "";
				if (cardbookHTMLUtils.getPrefBooleanFromTypes(line[1])) {
					if (aVersion == "4.0") {
						let prefValue = cardbookBGPreferences.getPrefFromTypes(line[1], aVersion);
						if (prefValue != "") {
							lString = `PREF=${prefValue}:`;
						} else {
							lString = "PREF=1:";
						}
					} else {
						lString = "TYPE=PREF:";
					}
				}
				aResultValue = cardbookBGUtils.appendToVcardData1(aResultValue, line[2] + "." + aField, false, lString + cardbookBGUtils.escapeArrays(line[0]).join(";"));
				for (let j = 0; j < line[3].length; j++) {
					let tmpArray = line[3][j].split(":");
					aResultValue = cardbookBGUtils.appendToVcardData1(aResultValue, line[2] + "." + tmpArray[0], false, tmpArray[1]);
				}
			} else {
				let lString = "";
				if (cardbookHTMLUtils.getPrefBooleanFromTypes(line[1])) {
					if (aVersion == "4.0") {
						lString = "PREF=1;";
					} else {
						lString = "TYPE=PREF;";
					}
				}
				let inputTypes = cardbookHTMLUtils.getOnlyTypesFromTypes(line[1]);
				for (let j = 0; j < inputTypes.length; j++) {
					lString = lString + "TYPE=" + inputTypes[j] + ";";
				}
				if (lString != "") {
					lString = lString.slice(0, -1);
					lString = lString + ":";
				}
				aResultValue = cardbookBGUtils.appendToVcardData1(aResultValue, aField, false, lString + cardbookBGUtils.escapeArrays(line[0]).join(";"));
			}
		}
		return aResultValue;
	},

	appendToVcardData1: function (vString1, vString2, vBool1, vString3) {
		let lResult = "";
		if (vBool1) {
			lResult = vString1 + vString2 + "\r\n";
		} else {
			if (vString3) {
				vString3 = vString3.replace(/\\n/g,",");
				if (vString2) {
					let lString4 = vString3.toUpperCase();
					if (lString4.indexOf("TYPE=") != -1 || lString4.indexOf("PREF") != -1) {
						lResult = vString1 + cardbookBGUtils.splitLine(`${vString2};${vString3}`) + "\r\n";
					} else {
						lResult = vString1 + cardbookBGUtils.splitLine(`${vString2}:${vString3}`) + "\r\n";
					}
				} else {
					lResult = vString1 + cardbookBGUtils.splitLine(vString3) + "\r\n";
				}
			} else {
				lResult = vString1;
			}
		}
		return lResult;
	},

	appendToVcardData2: function (vString1, vString2, vBool1, vString3) {
		let lResult = "";
		if (vBool1) {
			lResult = vString1 + vString2 + "\r\n";
		} else {
			if (vString3) {
				if (vString2) {
					lResult = vString1 + cardbookBGUtils.splitLine(`${vString2}:${vString3}`) + "\r\n";
				} else {
					lResult = vString1 + cardbookBGUtils.splitLine(vString3) + "\r\n";
				}
			} else {
				lResult = vString1;
			}
		}
		return lResult;
	},

	// for media
	appendToVcardData3: function (vString1, vString2, vArray) {
		let lResult = vString1;
		for (let line of vArray) {
			lResult = lResult + cardbookBGUtils.splitLine(`${vString2}${line}`) + "\r\n";
		}
		return lResult;
	},

	// for geo and tz
	// GEO;VALUE=FLOAT:90.000;91.000
	appendToVcardData4: function (vString1, vString2, vArray) {
		if (vArray == "" || (Array.isArray(vArray) && vArray.length == 1 && vArray[0] == "")) {
			return vString1;
		}
		let lResult = vString1;
		for (let arrayLine of vArray) {
			if (Array.isArray(arrayLine)) {
				let line = vString2;
				if (arrayLine[1].length) {
					line = line + ";" + arrayLine[1].join(";");
				}
				line = line + ":" + arrayLine[0].join(";");
				lResult = lResult + cardbookBGUtils.splitLine(line) + "\r\n";
			} else {
				let line = vString2 + ":" + arrayLine;
				lResult = lResult + cardbookBGUtils.splitLine(line) + "\r\n";
			}
		}
		return lResult;
	},

	escapeArrayComma: function (vArray) {
		let result = [];
		for (let line of vArray){
			if (line && line != ""){
				result.push(line.replace(/,/g,"\\,"));
			}
		}
		return result;
	},

	escapeArraySemiColon: function (vArray) {
		let result = [];
		for (let line of vArray){
			if (line && line != ""){
				result.push(line.replace(/;/g,"\\;"));
			}
		}
		return result;
	},

	escapeArrayBackslash: function (vArray) {
		let result = [];
		for (let line of vArray){
			if (line && line != ""){
				result.push(cardbookHTMLUtils.escapeStringBackslash(line));
			}
		}
		return result;
	},

	escapeStrings: function (vString) {
		return vString.replace(/;/g,"\\;").replace(/,/g,"\\,").split("\n").join("\\n");
	},

	escapeArrays: function (vArray) {
		let result = [];
		for (let line of vArray){
			if (line && line != ""){
				let value = cardbookHTMLUtils.escapeStringBackslash(line);
				result.push(cardbookBGUtils.escapeStrings(value));
			} else {
				result.push("");
			}
		}
		return result;
	},

	escapeStringComma: function (vString) {
		return vString.replace(/,/g,"\\,").split("\n").join("\\n");
	},

	getKeyContentForCard: function(aCard) {
		try {
			let result = [];
			for (let keyType of aCard.key) {
				let pref = cardbookHTMLUtils.getPrefBooleanFromTypes(keyType.types);
				if (keyType.URI) {
					if (aCard.version === "4.0") {
						if (pref) {
							result.push(";PREF=1;VALUE=URI:" + keyType.URI);
						} else {
							result.push(";VALUE=URI:" + keyType.URI);
						}
					} else {
						if (pref) {
							result.push(";TYPE=PREF;VALUE=URI:" + keyType.URI);
						} else {
							result.push(";VALUE=URI:" + keyType.URI);
						}
					}
				} else if (keyType.value) {
					if (aCard.version === "4.0") {
						if (pref) {
							result.push(":pref=1;data:application/pgp-keys;base64," + keyType.value);
						} else {
							result.push(":base64," + keyType.value);
						}
					} else if (aCard.version === "3.0") {
						if (pref) {
							result.push(";TYPE=PREF;ENCODING=B:" + keyType.value);
						} else {
							result.push(";ENCODING=B:" + keyType.value);
						}
					}
				}
			}
			return result;
		}
		catch (e) {
			console.log("cardbookBGUtils.getKeyContentForCard error : " + e, "Error");
		}
	},

	cacheGetMediaCard: async function(aLog, aCard, aType, aMediaFromDB = true) {
		return new Promise( async function(resolve, reject) {
			var result = [];
			if (aCard[aType].value) {
				if (aCard.version === "4.0") {
					if (aCard[aType].extension != "") {
						let myExtension = cardbookHTMLUtils.formatExtension(aCard[aType].extension, aCard[aType].version);
						result.push(":data:image/" + myExtension + ";base64," + aCard[aType].value);
					} else {
						result.push(":base64," + aCard[aType].value);
					}
				} else if (aCard.version === "3.0") {
					if (aCard[aType].extension != "") {
						let myExtension = cardbookHTMLUtils.formatExtension(aCard[aType].extension, aCard[aType].version);
						result.push(";ENCODING=B;TYPE=" + myExtension + ":" + aCard[aType].value);
					} else {
						result.push(";ENCODING=B:" + aCard[aType].value);
					}
				}
			} else if (aMediaFromDB) {
				let dirname = cardbookBGPreferences.getName(aCard.dirPrefId);
				let image = await cardbookIDBImage.getImage(aLog, aType, dirname, aCard.cbid, aCard.fn);

				if (image && image.content && image.extension) {
					let extension = image.extension || aCard[aType].extension;
					if (aCard.version === "4.0") {
						if (extension) {
							let extension1 = cardbookHTMLUtils.formatExtension(extension, aCard.version);
							result.push(":data:image/" + extension1 + ";base64," + image.content);
						} else {
							result.push(":base64," + image.content);
						}
					} else if (aCard.version === "3.0") {
						if (extension) {
							let extension1 = cardbookHTMLUtils.formatExtension(extension, aCard.version);
							result.push(";ENCODING=B;TYPE=" + extension1 + ":" + image.content);
						} else {
							result.push(";ENCODING=B:" + image.content);
						}
					}
				}
			}
			resolve(result);
		});
	},

	cardToVcardData: async function (aLog, vCard, aMediaFromDB = true) {
		if (vCard.uid == "") {
			return "";
		}
		let tmpBackslash1 = "";
		let tmpBackslash2 = "";
		let tmpBackslash3 = "";
		let tmpBackslash4 = "";
		let tmpBackslash5 = "";
		let tmpBackslashArray = [];
		let vCardData = "";
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"BEGIN:VCARD",true,"");
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"VERSION",false,vCard.version);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"PRODID",false,vCard.prodid);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"UID",false,vCard.uid);

		tmpBackslashArray = cardbookBGUtils.escapeArrayBackslash(vCard.categories);
		tmpBackslashArray = cardbookBGUtils.escapeArrayComma(tmpBackslashArray);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"CATEGORIES",false,tmpBackslashArray.join(","));
		// FN required bn
		if (vCard.fn == "") {
			vCardData = vCardData + "FN:" + "\r\n";
		} else {
			tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.fn);
			vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"FN",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		}
		// N required in 3.0
		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.lastname);
		tmpBackslash2 = cardbookHTMLUtils.escapeStringBackslash(vCard.firstname);
		tmpBackslash3 = cardbookHTMLUtils.escapeStringBackslash(vCard.othername);
		tmpBackslash4 = cardbookHTMLUtils.escapeStringBackslash(vCard.prefixname);
		tmpBackslash5 = cardbookHTMLUtils.escapeStringBackslash(vCard.suffixname);
		if (vCard.version == "3.0") {
			vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"N",false,cardbookBGUtils.escapeStrings(tmpBackslash1) + ";" + cardbookBGUtils.escapeStrings(tmpBackslash2) + ";" +
													cardbookBGUtils.escapeStrings(tmpBackslash3) + ";" + cardbookBGUtils.escapeStrings(tmpBackslash4) + ";" + cardbookBGUtils.escapeStrings(tmpBackslash5));
		} else if (!(vCard.lastname == "" && vCard.firstname == "" && vCard.othername == "" && vCard.prefixname == "" && vCard.suffixname == "")) {
			vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"N",false,cardbookBGUtils.escapeStrings(tmpBackslash1) + ";" + cardbookBGUtils.escapeStrings(tmpBackslash2) + ";" +
													cardbookBGUtils.escapeStrings(tmpBackslash3) + ";" + cardbookBGUtils.escapeStrings(tmpBackslash4) + ";" + cardbookBGUtils.escapeStrings(tmpBackslash5));
		}
		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.nickname);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"NICKNAME",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.sortstring);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"SORT-STRING",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"BDAY",false,cardbookHTMLDates.getDateForvCard(vCard.bday,vCard.version));
		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.gender);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"GENDER",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.birthplace);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"BIRTHPLACE",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"ANNIVERSARY",false,cardbookHTMLDates.getDateForvCard(vCard.anniversary,vCard.version));
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"DEATHDATE",false,cardbookHTMLDates.getDateForvCard(vCard.deathdate,vCard.version));
		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.deathplace);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"DEATHPLACE",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.title);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"TITLE",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.role);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"ROLE",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));

		tmpBackslashArray = cardbookBGUtils.escapeArrayBackslash(vCard.org);
		tmpBackslashArray = cardbookBGUtils.escapeArrayComma(tmpBackslashArray);
		tmpBackslashArray = cardbookBGUtils.escapeArraySemiColon(tmpBackslashArray);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"ORG",false,tmpBackslashArray.join(";"));

		vCardData = cardbookBGUtils.appendArrayToVcardData(vCardData, "EMAIL", vCard.version, vCard.email);
		vCardData = cardbookBGUtils.appendArrayToVcardData(vCardData, "TEL", vCard.version, vCard.tel);
		vCardData = cardbookBGUtils.appendArrayToVcardData(vCardData, "ADR", vCard.version, vCard.adr);
		vCardData = cardbookBGUtils.appendArrayToVcardData(vCardData, "IMPP", vCard.version, vCard.impp);
		vCardData = cardbookBGUtils.appendArrayToVcardData(vCardData, "URL", vCard.version, vCard.url);

		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.note);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"NOTE",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		// old values
		if (Array.isArray(vCard.geo)) {
			vCardData = cardbookBGUtils.appendToVcardData4(vCardData, "GEO", vCard.geo);
		} else {
			vCardData = cardbookBGUtils.appendToVcardData4(vCardData, "GEO", [vCard.geo]);
		}
		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.mailer);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"MAILER",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		
		if (vCard.version == "4.0") {
			tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.kind);
			vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"KIND",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
			for (let i = 0; i < vCard.member.length; i++) {
				vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"MEMBER",false,vCard.member[i]);
			}
		}

		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.class1);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"CLASS",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.rev);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"REV",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCard.agent);
		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"AGENT",false,cardbookBGUtils.escapeStringComma(tmpBackslash1));
		// old values
		if (Array.isArray(vCard.tz)) {
			vCardData = cardbookBGUtils.appendToVcardData4(vCardData, "TZ", vCard.tz);
		} else {
			vCardData = cardbookBGUtils.appendToVcardData4(vCardData, "TZ", [vCard.tz]);
		}
		vCardData = cardbookBGUtils.appendToVcardData3(vCardData,"KEY",cardbookBGUtils.getKeyContentForCard(vCard));

		for (let media of cardbookHTMLUtils.allColumns.media) {
			// always convert
			let content = await cardbookBGUtils.cacheGetMediaCard(aLog, vCard, media, aMediaFromDB);
			if (content.length) {
				vCardData = cardbookBGUtils.appendToVcardData3(vCardData, media.toUpperCase(), content);
			}
		}

		for (let other of vCard.others) {
			let localDelim1 = other.indexOf(":",0);
			// Splitting data
			if (localDelim1 >= 0) {
				let vCardDataArrayHeader = other.substr(0,localDelim1).trim();
				let vCardDataArrayTrailer = other.substr(localDelim1+1,other.length).trim();
				tmpBackslash1 = cardbookHTMLUtils.escapeStringBackslash(vCardDataArrayTrailer);
				vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"",false,vCardDataArrayHeader + ":" + cardbookBGUtils.escapeStringComma(tmpBackslash1));
			}
		}

		vCardData = cardbookBGUtils.appendToVcardData2(vCardData,"END:VCARD",true,"");
		return vCardData;
	},

	getvCardForEmail: async function(aLog, aCard) {
		let myTempCard = new cardbookCardParser();
		await cardbookBGUtils.cloneCard(aCard, myTempCard);
		myTempCard.rev = "";
		let cardContent = await cardbookBGUtils.cardToVcardData(aLog, myTempCard);
		myTempCard = null;
		return cardContent;
	},

	getvCardForServer: async function(aLog, aCard) {
		return await cardbookBGUtils.cardToVcardData(aLog, aCard);
	},

	getDataForUpdatingFile: async function(aLog, aListOfCards, aActionId) {
		let dataForExport = "";
		let count = 0;
		for (let card of aListOfCards) {
			let data = await cardbookBGUtils.cardToVcardData(aLog, card);
			if (count === 0) {
				dataForExport = data;
			} else {
				dataForExport = `${dataForExport}\r\n${data}`;
			}
			count++;
			if (count % 100 == 0) {
				if (aActionId) {
					await messenger.runtime.sendMessage({query: "cardbook.updateCurrentAction", actionId: aActionId, doneCards: count});
				}
			}
		}
		if (aActionId) {
			await messenger.runtime.sendMessage({query: "cardbook.updateCurrentAction", actionId: aActionId, doneCards: count});
		}
		return dataForExport;
	},

	changeMediaFromFileToContent: async function (aCard) {
		try {
			for (let media of cardbookHTMLUtils.allColumns.media) {
				if (aCard[media].value == "") {
					if (aCard[media].URI && aCard[media].URI != "") {
						let user = cardbookBGPreferences.getUser(aCard.dirPrefId);
						let [ base64, extension ] = await cardbookBGSynchronizationUtils.getImageFromURI([], aCard, media, user, aCard.dirPrefId);
						if (base64) {
							aCard[media].value = base64;
							aCard[media].extension = extension;
						}
					} else {
						let dirname = cardbookBGPreferences.getName(aCard.dirPrefId);
						await cardbookIDBImage.getImage(null, media, dirname, aCard.cbid, aCard.fn)
							.then( image => {
								if (image && image.content && image.extension) {
									aCard[media].value = image.content;
								}
							})
							.catch( () => {} );
					}
				}
			}
		}
		catch (e) {
			throw new Error("cardbookBGUtils.changeMediaFromFileToContent error : " + e);
		}
	},

	getFormattedFileName: function(aName) {
		return aName.replace(/([\\\/\:\*\?\"\<\>\|]+)/g, "-");
	},

	getFileNameFromUrl: function(aUrl) {
		let cleanUrl = cardbookBGSynchronizationUtils.getSlashedUrl(aUrl).slice(0, -1);
		let keyArray = cleanUrl.split("/");
		let key = decodeURIComponent(keyArray[keyArray.length - 1]).replace(/^urn:uuid:/i, "");
		return cardbookBGUtils.getFormattedFileName(key);
	},

	getFileNameForCard: function(aDirName, aName, aExtension) {
		let file = cardbookBGUtils.getFormattedFileName(aName);
		let filename = `${file}.${aExtension}`;
		let tmpArray = aDirName.split("/");
		let sep = (tmpArray.length > 1) ? "/" : String.raw`\a`.replace("a", "");
		if (aDirName[aDirName.length - 1] != sep) {
			aDirName += sep;
		}
		return `${aDirName}${filename}`;
	},

	getFileNameForCard2: function(aCard, aListOfNames, aExtension) {
		let i = 1;
		let name = cardbookBGUtils.getFormattedFileName(aCard.fn) + aExtension;
		while (aListOfNames.includes(name) && i < 100) {
			name = cardbookBGUtils.getFormattedFileName(aCard.fn) + i + aExtension;
			i++;
		}
		return name;
	},

	setCacheURIFromCard: function(aCard, aPrefIdType) {
		if (aCard.cacheuri != "") {
			return;
		} else if (aPrefIdType === "DIRECTORY") {
			let myDirPrefIdUrl = cardbookBGPreferences.getUrl(aCard.dirPrefId);
			aCard.cacheuri = cardbookBGUtils.getFileNameForCard(myDirPrefIdUrl, aCard.fn, ".vcf");
		} else {
			if (aCard.cardurl) {
				if (aPrefIdType == "OFFICE365") {
					aCard.cacheuri = aCard.uid;
				} else {
					aCard.cacheuri = cardbookBGUtils.getFileNameFromUrl(aCard.cardurl);
				}
			} else {
				if (aPrefIdType == "GOOGLE2" || aPrefIdType == "GOOGLE3") {
					aCard.cacheuri = cardbookBGUtils.getFileNameFromUrl(aCard.uid);
				} else if (aPrefIdType == "OFFICE365") {
					aCard.cacheuri = aCard.uid;
				} else {
					aCard.cacheuri = cardbookBGUtils.getFileNameFromUrl(aCard.uid) + ".vcf";
				}
			}
		}
	},

	isMyCardAList: function (aCard) {
		if (aCard.version == "4.0") {
			return (aCard.kind.toLowerCase() == "group");
		} else if (aCard.version == "3.0") {
			let kindCustom = cardbookBGPreferences.getPref("kindCustom");
			for (let othersLine of aCard.others) {
				let localDelim1 = othersLine.indexOf(":", 0);
				if (localDelim1 >= 0) {
					let header = othersLine.substr(0, localDelim1);
					if (header == kindCustom) {
						let trailer = othersLine.substr(localDelim1+1, othersLine.length);
						return (trailer.toLowerCase() == "group");
					}
				}
			}
		}
		return false;
	},

	getMembersFromCard: function (aRepo, aCard) {
		let result = { mails: [], uids: [], kind: "" };
		if (aCard.version == "4.0") {
			result.kind = aCard.kind;
			for (let member of aCard.member) {
				if (member.startsWith("mailto:")) {
					result.mails.push(member.replace("mailto:", ""));
				} else {
					let uid = member.replace("urn:uuid:", "");
					if (aRepo.cardbookCards[aCard.dirPrefId+"::"+uid]) {
						result.uids.push(aRepo.cardbookCards[aCard.dirPrefId+"::"+uid]);
					}
				}
			}
		} else if (aCard.version == "3.0") {
			let kindCustom = cardbookBGPreferences.getPref("kindCustom");
			let memberCustom = cardbookBGPreferences.getPref("memberCustom");
			result.kind = aCard.kind;
			for (let other of aCard.others) {
				let localDelim1 = other.indexOf(":",0);
				if (localDelim1 >= 0) {
					let header = other.substr(0,localDelim1).toUpperCase();
					let trailer = other.substr(localDelim1+1,other.length);
					if (header == kindCustom) {
						result.kind = trailer;
					} else if (header == memberCustom) {
						if (trailer.startsWith("mailto:")) {
							result.mails.push(trailer.replace("mailto:", ""));
						} else {
							let uid = trailer.replace("urn:uuid:", "");
							if (aRepo.cardbookCards[aCard.dirPrefId+"::"+uid]) {
								result.uids.push(aRepo.cardbookCards[aCard.dirPrefId+"::"+uid]);
							}
						}
					}
				}
			}
		}
		return result;
	},

	setCalculatedFieldsWithoutRev: function(aCard) {
		aCard.isAList = cardbookBGUtils.isMyCardAList(aCard);
		if (!aCard.isAList) {
			aCard.emails = cardbookHTMLUtils.getPrefAddressFromCard(aCard, "email", cardbookBGPreferences.getPref("preferEmailPref"));
		}
		if (aCard.dirPrefId != "" && aCard.uid != "") {
			aCard.cbid = aCard.dirPrefId + "::" + aCard.uid;
		}
		if (aCard.prodid == "") {
			aCard.prodid = cardbookBGPreferences.getPref("prodid");
		}
	},

	setCalculatedFields: function(aCard) {
		cardbookBGUtils.setCalculatedFieldsWithoutRev(aCard);
		cardbookBGUtils.updateRev(aCard);
	},

	getMimeEmailsFromCards: function (aListOfCards, aOnlyEmail) {
		let useOnlyEmail = aOnlyEmail ? aOnlyEmail : cardbookBGPreferences.getPref("useOnlyEmail");
		let result = [];
		for (let card of aListOfCards) {
			for (let email of card.emails) {
				if (useOnlyEmail == true) {
					result.push(email);
				} else {
					result.push(MimeAddressParser.prototype.makeMimeAddress(card.fn, email));
				}
			}
		}
		return result;
	},

	getMimeEmailsFromCardsAndLists: function (aRepo, aListOfCards, aIdentity, aOnlyEmail) {
		let useOnlyEmail = aOnlyEmail ? aOnlyEmail : cardbookBGPreferences.getPref("useOnlyEmail");
		let result = {};
		result.emptyResults = [];
		result.notEmptyResults = [];
		for (let card of aListOfCards) {
			if (card.isAList) {
				var myConversion = new cardbookListConversion(aRepo, card.fn + " <" + card.fn + ">", aIdentity, aOnlyEmail);
				myConversion.emailResult = cardbookHTMLUtils.arrayUnique(myConversion.emailResult).join(", ");
				result.notEmptyResults = result.notEmptyResults.concat(myConversion.emailResult);
			} else {
				if (card.emails.length == 0) {
					result.emptyResults.push(card.fn);
				} else {
					for (let email of card.emails) {
						if (useOnlyEmail) {
							result.notEmptyResults.push(email);
						} else {
							result.notEmptyResults.push(MimeAddressParser.prototype.makeMimeAddress(card.fn, email));
						}
					}
				}
			}
		}
		return result;
	},

	getEmailsFromCard: function (aCard, aEmailPref) {
		let listOfEmail = [];
		if (aCard) {
			let notfoundOnePrefEmail = true;
			let listOfPrefEmail = [];
			let myPrefValue;
			let myOldPrefValue = 0;
			for (var j = 0; j < aCard.email.length; j++) {
				let emailText = aCard.email[j][0][0];
				if (aEmailPref) {
					for (let k = 0; k < aCard.email[j][1].length; k++) {
						if (aCard.email[j][1][k].toUpperCase().indexOf("PREF") >= 0) {
							if (aCard.email[j][1][k].toUpperCase().indexOf("PREF=") >= 0) {
								myPrefValue = aCard.email[j][1][k].toUpperCase().replace("PREF=","");
							} else {
								myPrefValue = 1;
							}
							if (myPrefValue == myOldPrefValue || myOldPrefValue === 0) {
								listOfPrefEmail.push(emailText);
								myOldPrefValue = myPrefValue;
							} else if (myPrefValue < myOldPrefValue) {
								listOfPrefEmail = [];
								listOfPrefEmail.push(emailText);
								myOldPrefValue = myPrefValue;
							}
							notfoundOnePrefEmail = false;
						}
					}
				} else {
					listOfEmail.push(emailText);
					notfoundOnePrefEmail = false;
				}
			}
			if (notfoundOnePrefEmail) {
				for (let j = 0; j < aCard.email.length; j++) {
					let email = aCard.email[j][0][0];
					listOfEmail.push(email);
				}
			} else {
				for (let j = 0; j < listOfPrefEmail.length; j++) {
					listOfEmail.push(listOfPrefEmail[j]);
				}
			}
		}
		return listOfEmail;
	},

	getUidsFromList: function (aRepo, aList) {
		let uidResult = [];
		let recursiveList = [];
		
		function _verifyRecursivity(aList1) {
			for (let list of recursiveList) {
				if (list == aList1.cbid) {
					cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "errorInfiniteLoopRecursion", [recursiveList], "Warning");
					return false;
				}
			}
			recursiveList.push(aList1.cbid);
			return true;
		};
				
		function _getEmails(aCard) {
			if (aCard.isAList) {
				if (_verifyRecursivity(aCard)) {
					_convert(aCard);
				}
			} else {
				uidResult.push(aCard.cbid);
			}
		};
				
		function _convert(aList) {
			recursiveList.push(aList.cbid);
			let myMembers = cardbookBGUtils.getMembersFromCard(aRepo, aList);
			for (let card of myMembers.uids) {
				_getEmails(card);
			}
		};
		_convert(aList);
		return uidResult;
	},

	getParentOrg: function (aId) {
		let idArray = aId.split("::");
		idArray.pop();
		return idArray.join("::");
	},

	normalizeString: function (aString) {
		if (aString) {
			return aString.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
		} else {
			return "";
		}
	},

	makeSearchString: function (aString) {
		return cardbookBGUtils.normalizeString(aString.replace(/([\\\/\:\*\?\"\'\-\<\>\| ]+)/g, "").toLowerCase());
	},

	getLongSearchString: function(aCard) {
		let lResult = "";
		let sep = "|";
		lResult = lResult + aCard.lastname + sep;
		lResult = lResult + aCard.firstname + sep;
		lResult = lResult + aCard.othername + sep;
		lResult = lResult + aCard.prefixname + sep;
		lResult = lResult + aCard.suffixname + sep;
		lResult = lResult + aCard.fn + sep;
		lResult = lResult + aCard.nickname + sep;
		lResult = lResult + aCard.bday + sep;
		lResult = lResult + aCard.categories.join(sep) + sep;
		for (let i = 0; i < aCard.adr.length; i++) {
			lResult = lResult + aCard.adr[i][0].join() + sep;
		}
		for (let i = 0; i < aCard.tel.length; i++) {
			lResult = lResult + aCard.tel[i][0].join() + sep;
		}
		for (let i = 0; i < aCard.email.length; i++) {
			lResult = lResult + aCard.email[i][0].join() + sep;
		}
		lResult = lResult + aCard.title + sep;
		lResult = lResult + aCard.role + sep;
		lResult = lResult + aCard.org + sep;
		lResult = lResult + aCard.note + sep;
		for (let i = 0; i < aCard.url.length; i++) {
			lResult = lResult + aCard.url[i][0].join() + sep;
		}
		for (let i = 0; i < aCard.impp.length; i++) {
			lResult = lResult + aCard.impp[i][0].join() + sep;
		}
		let customFields = cardbookBGPreferences.getAllCustomFields();
		for (let type of ["personal", "org"]) {
			for (let custom of customFields[type]) {
				let customValue = cardbookHTMLUtils.getCardValueByField(aCard, custom[0], false)[0];
				if (customValue) {
					lResult = lResult + customValue + sep;
				}
			}
		}
		lResult = lResult.slice(0, -1);
		return cardbookBGUtils.makeSearchString(lResult);
	},

	isMyCardFoundInRules: function (aCard, aRules, aMatchAll) {
		let myRegexp;
		let inverse;
		let fields = [];
		let result;
	
		function buildRegExp(aCard, aCase, aField, aTerm, aValue, aDiacritic) {
			fields = cardbookHTMLUtils.getCardValueByField(aCard, aField, false);
			if (aDiacritic && fields.length != 0) {
				fields = fields.map(x => cardbookBGUtils.normalizeString(x));
			}
			if (aTerm == "Contains") {
				myRegexp = new RegExp("(.*)" + aValue + "(.*)", aCase);
				inverse = false;
			} else if (aTerm == "DoesntContain") {
				myRegexp = new RegExp("(.*)" + aValue + "(.*)", aCase);
				inverse = true;
			} else if (aTerm == "Is") {
				myRegexp = new RegExp("^" + aValue + "$", aCase);
				inverse = false;
			} else if (aTerm == "Isnt") {
				myRegexp = new RegExp("^" + aValue + "$", aCase);
				inverse = true;
			} else if (aTerm == "BeginsWith") {
				myRegexp = new RegExp("^" + aValue + "(.*)", aCase);
				inverse = false;
			} else if (aTerm == "EndsWith") {
				myRegexp = new RegExp("(.*)" + aValue + "$", aCase);
				inverse = false;
			} else if (aTerm == "IsEmpty") {
				myRegexp = new RegExp("^$", aCase);
				inverse = false;
			} else if (aTerm == "IsntEmpty") {
				myRegexp = new RegExp("^$", aCase);
				inverse = true;
			}
		};
		for (let rule of aRules) {
			var myCaseOperator = rule.case;
			if (myCaseOperator.startsWith("d")) {
				var myDiacritic = true;
				var myCaseOperator = rule.case.substr(1);
				var myValue = cardbookBGUtils.normalizeString(rule.value);
			} else {
				var myDiacritic = false;
				var myCaseOperator = rule.case;
				var myValue = rule.value;
			}
		
			buildRegExp(aCard, myCaseOperator, rule.field, rule.term, myValue, myDiacritic);
			function searchArray(element) {
				return element.search(myRegexp) != -1;
			};
			if (fields.length == 0) {
				if (rule.term == "IsEmpty") {
					var found = true;
				} else if (rule.term == "IsntEmpty") {
					var found = true;
				} else {
					var found = false;
				}
			} else if (fields.find(searchArray) == undefined) {
				var found = false;
			} else {
				var found = true;
			}
			
			if (aMatchAll) {
				result = true;
				if ((!found && !inverse) || (found && inverse)) {
					result = false;
					break;
				}
			} else {
				result = false;
				if ((found && !inverse) || (!found && inverse)) {
					result = true;
					break;
				}
			}
		}
		return result;
	},

};



