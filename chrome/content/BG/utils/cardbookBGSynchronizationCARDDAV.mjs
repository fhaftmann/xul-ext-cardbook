import { cardbookBGLog } from "./cardbookBGLog.mjs";
import { cardbookBGUtils } from "./cardbookBGUtils.mjs";
import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookBGSynchronizationUtils } from "./cardbookBGSynchronizationUtils.mjs";
import { cardbookCardParser } from "./cardbookCardParser.mjs";
import { XMLTEXTToJSONParser } from "./XMLTEXTToJSONParser.mjs";
import { cardbookFetchParser } from "./cardbookFetchParser.mjs";

import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs"

export var cardbookBGSynchronizationCARDDAV = {

	getCardsNumber: function (aRepo, aPrefId) {
		aRepo.cardbookCardsFromCache[aPrefId] = {};
		if (!aRepo.cardbookFileCacheCards[aPrefId]) {
			aRepo.cardbookFileCacheCards[aPrefId] = {}
		}
		if (aRepo.cardbookFileCacheCards[aPrefId]) {
			aRepo.cardbookCardsFromCache[aPrefId] = JSON.parse(JSON.stringify(aRepo.cardbookFileCacheCards[aPrefId]));
		}
		var length = 0;
		if (aRepo.cardbookFileCacheCards[aPrefId]) {
			for (var i in aRepo.cardbookFileCacheCards[aPrefId]) {
				length++;
			}
		}
		return length;
	},

	handleWrongPassword: async function(aRepo, aConnection, aStatus) {
		if (aStatus == 401) {
			let type = cardbookBGPreferences.getType(aConnection.connPrefId);
			if (type == "GOOGLE" || type == "GOOGLE2" || type == "GOOGLE3" ) {
				return false;
			}
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationUnauthorized", [aConnection.connDescription], "Warning");
			// first register the problem
			let myPwdGetId = aConnection.connUser + "::" + cardbookBGSynchronizationUtils.getRootUrl(aConnection.connUrl);
			if (!aRepo.cardbookServerChangedPwd[myPwdGetId]) {
				aRepo.cardbookServerChangedPwd[myPwdGetId] = {dirPrefIdList: [aConnection.connPrefId], openWindow: false, pwdChanged: false};
			} else {
				aRepo.cardbookServerChangedPwd[myPwdGetId].dirPrefIdList.push(aConnection.connPrefId);
			}
			// then ask for a new password
			// if never asked, ask
			// else finish ok : the sync would be done again if the password is changed
			if (!aRepo.cardbookServerChangedPwd[myPwdGetId].openWindow) {
				aRepo.cardbookServerChangedPwd[myPwdGetId].openWindow = true;
				let newPwd = await aRepo.askPassword(aConnection.connUser, aConnection.connUrl);
				if (newPwd != "") {
					aRepo.cardbookServerChangedPwd[myPwdGetId].pwdChanged = true;
				}
			}
			return true;
		}
		return false;
	},

	discoverPhase1: async function(aRepo, aConnection, aOperationType, aParams) {
		aRepo.cardbookServerDiscoveryRequest[aConnection.connPrefId]++;
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationRequestDiscovery1", [aConnection.connDescription, aConnection.connUrl]);
		let aRootUrl = cardbookBGSynchronizationUtils.getRootUrl(aConnection.connUrl);
		let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : aRootUrl});
		let req = new cardbookFetchParser(aConnection.connUser, password);
        let headers = new Headers();
        headers.append("Depth", "0");
        headers.append("Content-Type", "application/xml; charset=utf-8");
        let data = cardbookBGSynchronizationUtils.buildPropfindRequest(["D:current-user-principal"]);
		let request = req.fetch(aConnection.connUrl, {
			method: "PROPFIND",
			headers: headers,
			body: data,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});
		request.then(async (response) => {
			let responseTEXT = await response.text();
			let responseJSON = new XMLTEXTToJSONParser(responseTEXT);
			if (responseJSON && responseJSON["parsererror"] && responseJSON["parsererror"][0]["sourcetext"] && responseJSON["parsererror"][0]["sourcetext"][0]) {
                cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "unableToParseResponse", [aConnection.connDescription, responseJSON["parsererror"][0]["sourcetext"][0]], "Error");
                aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
                aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
                aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
            } else if (responseJSON && responseJSON["multistatus"] && response.ok) {
                try {
                    let rootUrl = cardbookBGSynchronizationUtils.getRootUrl(aConnection.connUrl);
                    let jsonResponses = responseJSON["multistatus"][0]["response"];
                    for (var prop in jsonResponses) {
                        var jsonResponse = jsonResponses[prop];
                        let propstats = jsonResponse["propstat"];
                        for (var prop1 in propstats) {
                            var propstat = propstats[prop1];
                            if (cardbookBGSynchronizationUtils.isStatusCorrect(propstat["status"][0])) {
                                if (propstat["prop"] != null && propstat["prop"] !== undefined && propstat["prop"] != "") {
                                    let prop2 = propstat["prop"][0];
                                    let rsrcType = prop2["current-user-principal"][0];
                                    let href = decodeURIComponent(rsrcType["href"][0]);
                                    if (href[href.length - 1] != '/') {
                                        href += '/';
                                    }
                                    cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : current-user-principal found : " + href);
                                    aConnection.connUrl = rootUrl + href;
                                    await cardbookBGSynchronizationCARDDAV.discoverPhase2(aRepo, aConnection, rootUrl, aOperationType, aParams);
                                }
                            }
                        }
                    }
                }
                catch(e) {
                    cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, aConnection.connDescription + " : cardbookBGSynchronizationCARDDAV.discoverPhase1 error : " + e + " : " + responseJSON, "Error");
                    aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
                    aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
                }
                aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
            } else {
                // only if it is not an initial setup
                if (aOperationType == "GETDISPLAYNAME" || ! await cardbookBGSynchronizationCARDDAV.handleWrongPassword(aRepo, aConnection, response.status)) {
                    cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "discoverPhase1", aConnection.connUrl, response.status], "Error");
                }
                aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
                aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
                aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
            }
		}).catch(async (response) => {
			console.log(response);
			if (aOperationType == "GETDISPLAYNAME") {
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "discoverPhase1", aConnection.connUrl, response.status], "Error");
			}
			aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
			aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
		});
	},

	discoverPhase2: async function(aRepo, aConnection, aRootUrl, aOperationType, aParams) {
		aRepo.cardbookServerDiscoveryRequest[aConnection.connPrefId]++;
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationRequestDiscovery2", [aConnection.connDescription, aConnection.connUrl]);
		aConnection.connUrl = cardbookBGSynchronizationUtils.getSlashedUrl(aConnection.connUrl);
		let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : aRootUrl});
		let req = new cardbookFetchParser(aConnection.connUser, password);
        let headers = new Headers();
        headers.append("Depth", "0");
        headers.append("Content-Type", "application/xml; charset=utf-8");
        let data = cardbookBGSynchronizationUtils.buildPropfindRequest(["C:addressbook-home-set"]);
		let request = req.fetch(aConnection.connUrl, {
			method: "PROPFIND",
			headers: headers,
			body: data,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});
		request.then(async (response) => {
			let responseTEXT = await response.text();
			let responseJSON = new XMLTEXTToJSONParser(responseTEXT);
			if (responseJSON && responseJSON["parsererror"] && responseJSON["parsererror"][0]["sourcetext"] && responseJSON["parsererror"][0]["sourcetext"][0]) {
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "unableToParseResponse", [aConnection.connDescription, responseJSON["parsererror"][0]["sourcetext"][0]], "Error");
				aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
				aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			} else if (responseJSON && responseJSON["multistatus"] && (response.status > 199 && response.status < 400)) {
				try {
					let jsonResponses = responseJSON["multistatus"][0]["response"];
					for (let prop in jsonResponses) {
						let jsonResponse = jsonResponses[prop];
						let propstats = jsonResponse["propstat"];
						for (let prop1 in propstats) {
							let propstat = propstats[prop1];
							if (cardbookBGSynchronizationUtils.isStatusCorrect(propstat["status"][0])) {
								if (propstat["prop"] != null && propstat["prop"] !== undefined && propstat["prop"] != "") {
									let prop2 = propstat["prop"][0];
									let rsrcType = prop2["addressbook-home-set"][0];
									for (let href of rsrcType["href"]) {
										href = decodeURIComponent(href);
										if (href[href.length - 1] != '/') {
											href += '/';
										}
										cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : addressbook-home-set found : " + href);
										if (href.startsWith("http")) {
											aConnection.connUrl = href;
										} else {
											aConnection.connUrl = aRootUrl + href;
										}
										await cardbookBGSynchronizationCARDDAV.discoverPhase3(aRepo, aConnection, aRootUrl, aOperationType, aParams);
									}
								}
							}
						}
					}
				}
				catch(e) {
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, aConnection.connDescription + " : cardbookBGSynchronizationCARDDAV.discoverPhase2 error : " + e + " : " + responseJSON, "Error");
					aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				}
				aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
			} else {
				// only if it is not an initial setup
				if (aOperationType == "GETDISPLAYNAME" || ! await cardbookBGSynchronizationCARDDAV.handleWrongPassword(aRepo, aConnection, response.status, responseJSON)) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "discoverPhase2", aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
				aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			}
		}).catch(async (response) => {
			console.log(response);
			if (aOperationType == "GETDISPLAYNAME") {
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "discoverPhase2", aConnection.connUrl, response.status], "Error");
			}
			aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
			aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
		});
	},

	discoverPhase3: async function(aRepo, aConnection, aRootUrl, aOperationType, aParams) {
		aRepo.cardbookServerDiscoveryRequest[aConnection.connPrefId]++;
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationRequestDiscovery3", [aConnection.connDescription, aConnection.connUrl]);
		aConnection.connUrl = cardbookBGSynchronizationUtils.getSlashedUrl(aConnection.connUrl);
		let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : aRootUrl});
		let req = new cardbookFetchParser(aConnection.connUser, password);
        let headers = new Headers();
        headers.append("Depth", "1");
        headers.append("Content-Type", "application/xml; charset=utf-8");
        let data = cardbookBGSynchronizationUtils.buildPropfindRequest(["D:current-user-privilege-set", "D:resourcetype", "D:displayname"]);
		let request = req.fetch(aConnection.connUrl, {
			method: "PROPFIND",
			headers: headers,
			body: data,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});
		request.then(async (response) => {
			let responseTEXT = await response.text();
			let responseJSON = new XMLTEXTToJSONParser(responseTEXT);
			if (responseJSON && responseJSON["parsererror"] && responseJSON["parsererror"][0]["sourcetext"] && responseJSON["parsererror"][0]["sourcetext"][0]) {
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "unableToParseResponse", [aConnection.connDescription, responseJSON["parsererror"][0]["sourcetext"][0]], "Error");
				aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
				aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			} else if (responseJSON && responseJSON["multistatus"] && (response.status > 199 && response.status < 400)) {
				try {
					let jsonResponses = responseJSON["multistatus"][0]["response"];
					for (let prop in jsonResponses) {
						let jsonResponse = jsonResponses[prop];
						let href = decodeURIComponent(jsonResponse["href"][0]);
						if (href[href.length - 1] != '/') {
							href += '/';
						}
						let propstats = jsonResponse["propstat"];
						for (let prop1 in propstats) {
							let propstat = propstats[prop1];
							if (cardbookBGSynchronizationUtils.isStatusCorrect(propstat["status"][0])) {
								if (propstat["prop"] != null && propstat["prop"] !== undefined && propstat["prop"] != "") {
									let prop2 = propstat["prop"][0];
									if (prop2["resourcetype"] != null && prop2["resourcetype"] !== undefined && prop2["resourcetype"] != "") {
										let rsrcType = prop2["resourcetype"][0];
										cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : rsrcType found : " + JSON.stringify(rsrcType));
										if (rsrcType["vcard-collection"] || rsrcType["addressbook"]) {
											if (href.startsWith("http")) {
												aConnection.connUrl = href;
											} else {
												aConnection.connUrl = aRootUrl + href;
											}

											if (aOperationType == "GETDISPLAYNAME") {
												let displayName = cardbookBGSynchronizationUtils.getDisplayname(prop2);
												let readOnly = cardbookBGSynchronizationUtils.getReadOnly(prop2);
												cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : href found : " + href);
												cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : displayName found : " + displayName);
												cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : readonly found : " + readOnly);
												aRepo.cardbookServerValidation[aConnection.connPrefId]['length']++;
												aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl] = {}
												aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl].displayName = displayName;
												aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl].forget = false;
												aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl].readOnly = readOnly;
												let aABConnection = {connPrefId: aConnection.connPrefId, connUser: aConnection.connUser, connUrl: aConnection.connUrl, connDescription: aConnection.connDescription};
												await cardbookBGSynchronizationCARDDAV.discoverPhase4(aRepo, aABConnection, aOperationType, aParams);
											} else if (aOperationType == "SYNCSERVER") {
												await cardbookBGSynchronizationCARDDAV.serverSyncCards(aRepo, aConnection, aParams.aPrefIdType, aParams.aValue);
											}
										}
									}
								}
							}
						}
					}
				}
				catch(e) {
					console.log(e)
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, aConnection.connDescription + " : cardbookBGSynchronizationCARDDAV.discoverPhase3 error : " + e + " : " + JSON.stringify(responseJSON), "Error");
					aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				}
				aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
			} else {
				// only if it is not an initial setup
				if (aOperationType == "GETDISPLAYNAME" || ! await cardbookBGSynchronizationCARDDAV.handleWrongPassword(aRepo, aConnection, response.status, responseJSON)) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "discoverPhase3", aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
				aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			}
		}).catch(async (response) => {
			console.log(response);
			if (aOperationType == "GETDISPLAYNAME") {
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "discoverPhase3", aConnection.connUrl, response.status], "Error");
			}
			aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
			aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
		});
	},

	discoverPhase4: async function(aRepo, aConnection, aOperationType, aParams) {
		aConnection.connUrl = cardbookBGSynchronizationUtils.getSlashedUrl(aConnection.connUrl);
		aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl].version = [];
		if (aParams.aPrefIdType == "APPLE" || aParams.aPrefIdType == "YAHOO") {
			return;
		}
		aRepo.cardbookServerDiscoveryRequest[aConnection.connPrefId]++;
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationRequestDiscovery4", [aConnection.connDescription, aConnection.connUrl]);
		let aRootUrl = cardbookBGSynchronizationUtils.getRootUrl(aConnection.connUrl);
		let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : aRootUrl});
		let req = new cardbookFetchParser(aConnection.connUser, password);
        let headers = new Headers();
        headers.append("Depth", "1");
        headers.append("Content-Type", "application/xml; charset=utf-8");
        let data = cardbookBGSynchronizationUtils.buildPropfindRequest(["C:supported-address-data"]);
		let request = req.fetch(aConnection.connUrl, {
			method: "PROPFIND",
			headers: headers,
			body: data,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});
		request.then(async (response) => {
			let responseTEXT = await response.text();
			let parser = new DOMParser();
			let responseXML = parser.parseFromString(responseTEXT, "application/xml");
			if (responseXML && (response.status > 199 && response.status < 400)) {
				let ns = "urn:ietf:params:xml:ns:carddav";
				if (responseXML.getElementsByTagNameNS(ns, "address-data-type")) {
					let versions = responseXML.getElementsByTagNameNS(ns, "address-data-type");
					for (let j = 0; j < versions.length; j++) {
						if (versions[j].getAttribute("Content-Type") == "text/vcard") {
							if (versions[j].getAttribute("version")) {
								let myVersion = versions[j].getAttribute("version");
								cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : version found : " + myVersion + " (" + aConnection.connUrl + ")");
								aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl].version.push(myVersion);
							}
						}
					}
				}
				aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl].version = cardbookHTMLUtils.arrayUnique(aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl].version);
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
			} else {
				// only if it is not an initial setup
				if (aOperationType == "GETDISPLAYNAME" || ! await cardbookBGSynchronizationCARDDAV.handleWrongPassword(aRepo, aConnection, response.status, response)) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "discoverPhase4", aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			}
		}).catch(async (response) => {
			console.log(response);
			if (aOperationType == "GETDISPLAYNAME") {
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "discoverPhase4", aConnection.connUrl, response.status], "Error");
			}
			aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
		});
	},

	// only called at setup
	validateWithoutDiscovery: async function(aRepo, aConnection, aOperationType, aParams) {
		aRepo.cardbookServerDiscoveryRequest[aConnection.connPrefId]++;
		let aRootUrl = cardbookBGSynchronizationUtils.getRootUrl(aConnection.connUrl);
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationRequestDiscovery", [aConnection.connDescription, aConnection.connUrl]);
		let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : aRootUrl});
		let req = new cardbookFetchParser(aConnection.connUser, password);
        let headers = new Headers();
        headers.append("Depth", "1");
        headers.append("Content-Type", "application/xml; charset=utf-8");
        let data = cardbookBGSynchronizationUtils.buildPropfindRequest(["D:current-user-privilege-set", "D:resourcetype", "D:displayname"]);
		let request = req.fetch(aConnection.connUrl, {
			method: "PROPFIND",
			headers: headers,
			body: data,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});
		request.then(async (response) => {
			if (response.status == 401) {
				if (! await cardbookBGSynchronizationCARDDAV.handleWrongPassword(aRepo, aConnection, response.status)) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "serverSyncCards", aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			} else {
				let responseTEXT = await response.text();
				let responseJSON = new XMLTEXTToJSONParser(responseTEXT);
				if (responseJSON && responseJSON["parsererror"] && responseJSON["parsererror"][0]["sourcetext"] && responseJSON["parsererror"][0]["sourcetext"][0]) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "unableToParseResponse", [aConnection.connDescription, responseJSON["parsererror"][0]["sourcetext"][0]], "Error");
					aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
					aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				} else if (responseJSON && responseJSON["multistatus"] && (response.status > 199 && response.status < 400)) {
					try {
						let jsonResponses = responseJSON["multistatus"][0]["response"];
						for (let prop in jsonResponses) {
							let jsonResponse = jsonResponses[prop];
							let href = decodeURIComponent(jsonResponse["href"][0]);
							if (href[href.length - 1] != '/') {
								href += '/';
							}
							let propstats = jsonResponse["propstat"];
							for (let prop1 in propstats) {
								let propstat = propstats[prop1];
								if (cardbookBGSynchronizationUtils.isStatusCorrect(propstat["status"][0])) {
									if (propstat["prop"] != null && propstat["prop"] !== undefined && propstat["prop"] != "") {
										let prop2 = propstat["prop"][0];
										if (prop2["resourcetype"] != null && prop2["resourcetype"] !== undefined && prop2["resourcetype"] != "") {
											let rsrcType = prop2["resourcetype"][0];
											cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : rsrcType found : " + rsrcType);
											if (rsrcType["vcard-collection"] || rsrcType["addressbook"]) {
												let displayName = cardbookBGSynchronizationUtils.getDisplayname(prop2);
												let readOnly = cardbookBGSynchronizationUtils.getReadOnly(prop2);
												cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : href found : " + href);
												cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : displayName found : " + displayName);
												cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : readonly found : " + readOnly);

												if (href.indexOf(aRootUrl) >= 0 ) {
													aConnection.connUrl = href;
												} else {
													aConnection.connUrl = aRootUrl + href;
												}
												aRepo.cardbookServerValidation[aConnection.connPrefId]['length']++;
												aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl] = {}
												aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl].displayName = displayName;
												aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl].forget = false;
												aRepo.cardbookServerValidation[aConnection.connPrefId][aConnection.connUrl].readOnly = readOnly;
												let aABConnection = {connPrefId: aConnection.connPrefId, connUser: aConnection.connUser, connUrl: aConnection.connUrl, connDescription: aConnection.connDescription};
												await cardbookBGSynchronizationCARDDAV.discoverPhase4(aRepo, aABConnection, aRootUrl, aOperationType, aParams);
											}
										}
									}
								}
							}
						}
					}
					catch(e) {
						cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, aConnection.connDescription + " : cardbookBGSynchronizationCARDDAV.validateWithoutDiscovery error : " + e + " : " + responseJSON, "Error");
						aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
					}
					aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				} else {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "validateWithoutDiscovery", aConnection.connUrl, status], "Error");
					aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
					aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				}
			}
		}).catch(async (response) => {
			console.log(response);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "validateWithoutDiscovery", aConnection.connUrl, status], "Error");
			aRepo.cardbookServerDiscoveryError[aConnection.connPrefId]++;
			aRepo.cardbookServerDiscoveryResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
		});
	},

	serverSearchRemote: async function(aRepo, aConnection, aValue) {
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationSearchingCards", [aConnection.connDescription]);
		let pwdurl = cardbookBGPreferences.getUrl(aConnection.connPrefId);
		let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : pwdurl});
		let req = new cardbookFetchParser(aConnection.connUser, password);
        let headers = new Headers();
        headers.append("Depth", "1");
        headers.append("Content-Type", "application/xml; charset=utf-8");
        let data = cardbookBGSynchronizationUtils.buildQueryRequest(["D:getetag", "C:address-data Content-Type='text/vcard'"], aValue);
		let request = req.fetch(aConnection.connUrl, {
			method: "REPORT",
			headers: headers,
			body: data,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});
		request.then(async (response) => {
			if (response.status == 401) {
				if (! await cardbookBGSynchronizationCARDDAV.handleWrongPassword(aRepo, aConnection, response.status)) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "serverSyncCards", aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			} else {
				let responseTEXT = await response.text();
				let responseJSON = new XMLTEXTToJSONParser(responseTEXT);
				if (response.status == 401) {
					if (! await cardbookBGSynchronizationCARDDAV.handleWrongPassword(aRepo, aConnection, status, responseJSON)) {
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "serverSearchRemote", aConnection.connUrl, status], "Error");
					}
					aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				} else if (responseJSON && responseJSON["parsererror"] && responseJSON["parsererror"][0]["sourcetext"] && responseJSON["parsererror"][0]["sourcetext"][0]) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "unableToParseResponse", [aConnection.connDescription, responseJSON["parsererror"][0]["sourcetext"][0]], "Error");
					aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				} else if (responseJSON && responseJSON["multistatus"] && (response.status > 199 && response.status < 400)) {
					try {
						if (responseJSON["multistatus"][0] && responseJSON["multistatus"][0]["response"]) {
							let jsonResponses = responseJSON["multistatus"][0]["response"];
							for (let prop in jsonResponses) {
								let jsonResponse = jsonResponses[prop];
								try {
									let href = decodeURIComponent(jsonResponse["href"][0]);
									let propstats = jsonResponse["propstat"];
									for (let prop1 in propstats) {
										let propstat = propstats[prop1];
										aRepo.cardbookServerGetCardRequest[aConnection.connPrefId]++;
										if (cardbookBGSynchronizationUtils.isStatusCorrect(propstat["status"][0])) {
											if (propstat["prop"] != null && propstat["prop"] !== undefined && propstat["prop"] != "") {
												let prop2 = propstat["prop"][0];
												let etag = "";
												if (prop2["getetag"]) {
													etag = prop2["getetag"][0];
												}
												try {
													var myContent = decodeURIComponent(prop2["address-data"][0]);
												}
												catch (e) {
													var myContent = prop2["address-data"][0];
												}
												try {
													var aRootUrl = cardbookBGSynchronizationUtils.getRootUrl(aConnection.connUrl);
													var myCard = new cardbookCardParser(myContent, aRootUrl + href, etag, aConnection.connPrefId);
												}
												catch (e) {
													aRepo.cardbookServerGetCardResponse[aConnection.connPrefId]++;
													aRepo.cardbookServerGetCardError[aConnection.connPrefId]++;
													if (e.message == "") {
														cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "parsingCardError", [aConnection.connDescription, messenger.i18n.getMessage(e.code), myContent], "Error");
													} else {
														cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "parsingCardError", [aConnection.connDescription, e.message, myContent], "Error");
													}
													continue;
												}
												cardbookBGUtils.setCacheURIFromValue(myCard, cardbookBGUtils.getFileNameFromUrl(aConnection.connUrl + href))
												await aRepo.addCardToRepository(myCard, true);
												cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetOK", [aConnection.connDescription, myCard.fn]);
											} else {
												aRepo.cardbookServerGetCardError[aConnection.connPrefId]++;
												cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, status], "Error");
											}
										} else {
											aRepo.cardbookServerGetCardError[aConnection.connPrefId]++;
											cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, status], "Error");
										}
										aRepo.cardbookServerGetCardResponse[aConnection.connPrefId]++;
									}
								}
								catch(e) {
									aRepo.cardbookServerGetCardResponse[aConnection.connPrefId]++;
									aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
									cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, aConnection.connDescription + " : cardbookBGSynchronizationCARDDAV.serverSearchRemote error : " + e, "Error");
								}
							}
						}
					}
					catch(e) {
						cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, aConnection.connDescription + " : cardbookBGSynchronizationCARDDAV.serverSearchRemote error : " + e, "Error");
						aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
					}
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				} else {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "serverSearchRemote", aConnection.connUrl, status], "Error");
					aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				}
			}
		}).catch(async (response) => {
			console.log(response);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "serverSearchRemote", aConnection.connUrl, status], "Error");
			aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
		});
	},
	
	serverSyncCards: async function(aRepo, aConnection, aValue) {
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationSearchingCards", [aConnection.connDescription]);
		let pwdurl = cardbookBGPreferences.getUrl(aConnection.connPrefId);
		let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : pwdurl});
		let req = new cardbookFetchParser(aConnection.connUser, password);
        let headers = new Headers();
        headers.append("Depth", "1");
        headers.append("Content-Type", "application/xml; charset=utf-8");
        let data = aValue ? cardbookBGSynchronizationUtils.buildQueryRequest(["D:getcontenttype", "D:getetag"], aValue) :
                            cardbookBGSynchronizationUtils.buildPropfindRequest(["D:getcontenttype", "D:getetag"]);
		let method = aValue ? "REPORT" : "PROPFIND";
		let request = req.fetch(aConnection.connUrl, {
			method: method,
			headers: headers,
			body: data,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});
		request.then(async (response) => {
			if (response.status == 401) {
				if (! await cardbookBGSynchronizationCARDDAV.handleWrongPassword(aRepo, aConnection, response.status)) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "serverSyncCards", aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
				aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
			} else {
				let responseTEXT = await response.text();
				let responseJSON = new XMLTEXTToJSONParser(responseTEXT);
				if (responseJSON && responseJSON["parsererror"] && responseJSON["parsererror"][0]["sourcetext"] && responseJSON["parsererror"][0]["sourcetext"][0]) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "unableToParseResponse", [aConnection.connDescription, responseJSON["parsererror"][0]["sourcetext"][0]], "Error");
					aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				} else if (responseJSON && responseJSON["multistatus"] && (response.status > 199 && response.status < 400)) {
					try {
						let length = cardbookBGSynchronizationCARDDAV.getCardsNumber(aRepo, aConnection.connPrefId);
						aRepo.cardbookServerSyncHandleRemainingCardTotal[aConnection.connPrefId] = length;
						if (responseJSON["multistatus"][0] && responseJSON["multistatus"][0]["response"]) {
							let baseUrl = cardbookBGSynchronizationUtils.getSlashedUrl(aConnection.connUrl);
							let jsonResponses = responseJSON["multistatus"][0]["response"];
							for (let prop in jsonResponses) {
								let jsonResponse = jsonResponses[prop];
								let href = decodeURIComponent(jsonResponse["href"][0]);
								let propstats = jsonResponse["propstat"];
								// 2015.04.27 14:03:55 : href : /remote.php/carddav/addressbooks/11111/contacts/
								// 2015.04.27 14:03:55 : propstats : [{prop:[{getcontenttype:[null], getetag:[null]}], status:["HTTP/1.1 404 Not Found"]}]
								// 2015.04.27 14:03:55 : href : /remote.php/carddav/addressbooks/11111/contacts/C68894CF-D340-0001-78C3-1E301B4011F5.vcf
								// 2015.04.27 14:03:55 : propstats : [{prop:[{getcontenttype:["text/x-vcard"], getetag:["\"6163e30117192647e1967de751fb5467\""]}], status:["HTTP/1.1 200 OK"]}]
								for (let prop1 in propstats) {
									let propstat = propstats[prop1];
									if (cardbookBGSynchronizationUtils.isStatusCorrect(propstat["status"][0])) {
										if (propstat["prop"] != null && propstat["prop"] !== undefined && propstat["prop"] != "") {
											let prop2 = propstat["prop"][0];
											if (href != aConnection.connUrl) {
												let contType = "";
												if (prop2["getcontenttype"]) {
													contType = prop2["getcontenttype"][0];
												}
												if (typeof(prop2["getetag"]) == "undefined") {
													continue;
												}
												if (href.indexOf("/", href.length -1) !== -1) {
													continue;
												}
												let etag = prop2["getetag"][0];
												let keyArray = href.split("/");
												let key = decodeURIComponent(keyArray[keyArray.length - 1]);
												let myUrl = baseUrl + key;
												let myFileName = cardbookBGUtils.getFileNameFromUrl(myUrl);
												if (cardbookBGSynchronizationUtils.isSupportedContentType(contType, myFileName)) {
													aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId]++;
													aRepo.cardbookServerSyncCompareCardWithCacheTotal[aConnection.connPrefId]++;
													let aCardConnection = {connPrefId: aConnection.connPrefId, connUrl: myUrl, connDescription: aConnection.connDescription, connUser: aConnection.connUser};
													await cardbookBGSynchronizationCARDDAV.compareServerCardWithCache(aRepo, aCardConnection, myUrl, etag, myFileName);
													if (aRepo.cardbookCardsFromCache[aConnection.connPrefId][myFileName]) {
														delete aRepo.cardbookCardsFromCache[aConnection.connPrefId][myFileName];
														aRepo.cardbookServerSyncHandleRemainingCardTotal[aConnection.connPrefId]--;
													}
												}
											}
										}
									}
								}
							}
						}
						await cardbookBGSynchronizationCARDDAV.handleRemainingCardCache(aRepo, aConnection);
					}
					catch(e) {
						console.log(e)
						cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aConnection.connDescription + " : cardbookBGSynchronizationCARDDAV.serverSyncCards error : " + e, "Error");
						aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
						aRepo.cardbookServerSyncHandleRemainingCardTotal[aConnection.connPrefId] = aRepo.cardbookServerSyncHandleRemainingCardDone[aConnection.connPrefId];
					}
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				} else {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "serverSyncCards", aConnection.connUrl, status], "Error");
					aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
				}
			}
		}).catch(async (response) => {
			console.log(response);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "serverSyncCards", aConnection.connUrl, status], "Error");
			aRepo.cardbookServerCardSyncError[aConnection.connPrefId]++;
			aRepo.cardbookServerSyncResponse[aConnection.connPrefId]++;
		});
    },

	compareServerCardWithCache: async function (aRepo, aCardConnection, aUrl, aEtag, aFileName) {
		if (aRepo.cardbookFileCacheCards[aCardConnection.connPrefId] && aRepo.cardbookFileCacheCards[aCardConnection.connPrefId][aFileName]) {
			var myCacheCard = aRepo.cardbookFileCacheCards[aCardConnection.connPrefId][aFileName];
			var myServerCard = new cardbookCardParser();
			await cardbookBGUtils.cloneCard(myCacheCard, myServerCard);
			cardbookBGUtils.addEtag(myServerCard, aEtag);
			if (myCacheCard.etag == aEtag) {
				if (myCacheCard.deleted) {
					// "DELETEDONDISK";
					aRepo.cardbookServerDeletedCardRequest[aCardConnection.connPrefId]++;
					aRepo.cardbookServerSyncDeletedCardOnDisk[aCardConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardDeletedOnDisk", [aCardConnection.connDescription, myCacheCard.fn]);
					await cardbookBGSynchronizationCARDDAV.serverDeleteCard(aRepo, aCardConnection, myCacheCard);
				} else if (myCacheCard.updated) {
					// "UPDATEDONDISK";
					aRepo.cardbookServerUpdatedCardRequest[aCardConnection.connPrefId]++;
					aRepo.cardbookServerSyncUpdatedCardOnDisk[aCardConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardUpdatedOnDisk", [aCardConnection.connDescription, myCacheCard.fn]);
					await cardbookBGSynchronizationCARDDAV.serverUpdateCard(aRepo, aCardConnection, myCacheCard, myServerCard);
				} else {
					// "NOTUPDATED";
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardAlreadyGetFromCache", [aCardConnection.connDescription, myCacheCard.fn]);
					aRepo.cardbookServerCardSyncDone[aCardConnection.connPrefId]++;
					aRepo.cardbookServerSyncNotUpdatedCard[aCardConnection.connPrefId]++;
				}
			} else if (myCacheCard.deleted) {
				// "DELETEDONDISKUPDATEDONSERVER";
				aRepo.cardbookServerSyncDeletedCardOnDiskUpdatedCardOnServer[aCardConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardDeletedOnDiskUpdatedOnServer", [aCardConnection.connDescription, myCacheCard.fn]);
				var solveConflicts = cardbookBGPreferences.getPref("solveConflicts");
				if (solveConflicts === "Local") {
					var conflictResult = "delete";
				} else if (solveConflicts === "Remote") {
					var conflictResult = "keep";
				} else {
					var message = messenger.i18n.getMessage("cardDeletedOnDiskUpdatedOnServer", [aCardConnection.connDescription, myCacheCard.fn]);
					var conflictResult = await aRepo.askUser("card", aCardConnection.connPrefId, message, aRepo.importConflictChoiceSync1Values);
				}
				
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aCardConnection.connDescription + " : debug mode : conflict resolution : ", conflictResult);
				switch (conflictResult) {
					case "keep":
						await aRepo.removeCardFromRepository(myCacheCard, true);
						aRepo.cardbookServerMultiGetArray[aCardConnection.connPrefId].push(aUrl);
						break;
					case "delete":
						aRepo.cardbookServerDeletedCardRequest[aCardConnection.connPrefId]++;
						await cardbookBGSynchronizationCARDDAV.serverDeleteCard(aRepo, aCardConnection, myCacheCard);
						break;
					default:
						aRepo.cardbookServerCardSyncDone[aCardConnection.connPrefId]++;
						break;
				}
			} else if (myCacheCard.updated) {
				// "UPDATEDONBOTH";
				aRepo.cardbookServerSyncUpdatedCardOnBoth[aCardConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardUpdatedOnBoth", [aCardConnection.connDescription, myCacheCard.fn]);
				var solveConflicts = cardbookBGPreferences.getPref("solveConflicts");
				if (solveConflicts === "Local") {
					var conflictResult = "local";
				} else if (solveConflicts === "Remote") {
					var conflictResult = "remote";
				} else {
					var message = messenger.i18n.getMessage("cardUpdatedOnBoth", [aCardConnection.connDescription, myCacheCard.fn]);
					var conflictResult = await aRepo.askUser("card", aCardConnection.connPrefId, message, aRepo.importConflictChoiceSync2Values);
				}
				
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aCardConnection.connDescription + " : debug mode : conflict resolution : ", conflictResult);
				switch (conflictResult) {
					case "local":
						aRepo.cardbookServerUpdatedCardRequest[aCardConnection.connPrefId]++;
						await cardbookBGSynchronizationCARDDAV.serverUpdateCard(aRepo, aCardConnection, myCacheCard, myServerCard);
						break;
					case "remote":
						aRepo.cardbookServerMultiGetArray[aCardConnection.connPrefId].push(aUrl);
						break;
					case "merge":
						aRepo.cardbookServerGetCardForMergeRequest[aCardConnection.connPrefId]++;
						await cardbookBGSynchronizationCARDDAV.serverGetForMerge(aRepo, aCardConnection, aEtag, myCacheCard);
						break;
					default:
						aRepo.cardbookServerCardSyncDone[aCardConnection.connPrefId]++;
						break;
				}
			} else {
				// "UPDATEDONSERVER";
				aRepo.cardbookServerMultiGetArray[aCardConnection.connPrefId].push(aUrl);
				aRepo.cardbookServerSyncUpdatedCardOnServer[aCardConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardUpdatedOnServer", [aCardConnection.connDescription, myCacheCard.fn, aEtag, myCacheCard.etag]);
			}
		} else {
			// "NEWONSERVER";
			aRepo.cardbookServerMultiGetArray[aCardConnection.connPrefId].push(aUrl);
			aRepo.cardbookServerSyncNewCardOnServer[aCardConnection.connPrefId]++;
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardNewOnServer", [aCardConnection.connDescription]);
		}
		aRepo.cardbookServerSyncCompareCardWithCacheDone[aCardConnection.connPrefId]++;
	},

	handleRemainingCardCache: async function (aRepo, aConnection) {
		if (aRepo.cardbookCardsFromCache[aConnection.connPrefId]) {
			for (var i in aRepo.cardbookCardsFromCache[aConnection.connPrefId]) {
				var aCard = aRepo.cardbookCardsFromCache[aConnection.connPrefId][i];
				if (aCard.created) {
					// "NEWONDISK";
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardNewOnDisk", [aConnection.connDescription, aCard.fn]);
					aRepo.cardbookServerCreatedCardRequest[aConnection.connPrefId]++;
					aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncNewCardOnDisk[aConnection.connPrefId]++;
					var aCreateConnection = JSON.parse(JSON.stringify(aConnection));
					await cardbookBGSynchronizationCARDDAV.serverCreateCard(aRepo, aCreateConnection, aCard);
				} else if (aCard.updated) {
					// "UPDATEDONDISKDELETEDONSERVER";
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardUpdatedOnDiskDeletedOnServer", [aConnection.connDescription, aCard.fn]);
					aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncUpdatedCardOnDiskDeletedCardOnServer[aConnection.connPrefId]++;
					var solveConflicts = cardbookBGPreferences.getPref("solveConflicts");
					if (solveConflicts === "Local") {
						var conflictResult = "keep";
					} else if (solveConflicts === "Remote") {
						var conflictResult = "delete";
					} else {
						var message = messenger.i18n.getMessage("cardUpdatedOnDiskDeletedOnServer", [aConnection.connDescription, aCard.fn]);
						var conflictResult = await aRepo.askUser("card", aConnection.connPrefId, message, aRepo.importConflictChoiceSync1Values);
					}
					
					cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aConnection.connDescription + " : debug mode : conflict resolution : ", conflictResult);
					switch (conflictResult) {
						case "keep":
							aRepo.cardbookServerCreatedCardRequest[aConnection.connPrefId]++;
							var aCreateConnection = JSON.parse(JSON.stringify(aConnection));
							cardbookBGUtils.nullifyEtag(aCard);
							await cardbookBGSynchronizationCARDDAV.serverCreateCard(aRepo, aCreateConnection, aCard);
							break;
						case "delete":
							await aRepo.removeCardFromRepository(aCard, true);
							aRepo.cardbookServerGetCardRequest[aConnection.connPrefId]++;
							aRepo.cardbookServerGetCardResponse[aConnection.connPrefId]++;
							aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
							break;
						default:
							aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
							break;
					}
				} else if (!aCard.deleted) {
					// "DELETEDONSERVER";
					aRepo.cardbookServerCardSyncTotal[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardDeletedOnServer", [aConnection.connDescription, aCard.fn]);
					await aRepo.removeCardFromRepository(aCard, true);
					aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
					aRepo.cardbookServerSyncDeletedCardOnServer[aConnection.connPrefId]++;
				}
				aRepo.cardbookServerSyncHandleRemainingCardDone[aConnection.connPrefId]++;
			}
		}
	},

	serverGetForMerge: async function(aRepo, aConnection, aEtag, aCacheCard) {
		let pwdurl = cardbookBGPreferences.getUrl(aConnection.connPrefId);
		let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : pwdurl});
		let req = new cardbookFetchParser(aConnection.connUser, password);
        let headers = new Headers();
        headers.append("accept", "text/vcard");
        headers.append("Content-Type", "application/xml; charset=utf-8");
		let request = req.fetch(aConnection.connUrl, {
			method: "GET",
			headers: headers,
			signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
		});
		request.then(async (response) => {
			let responseTEXT = await response.text();
			if (response.status == 401) {
				if (! await cardbookBGSynchronizationCARDDAV.handleWrongPassword(aRepo, aConnection, response.status)) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [aConnection.connDescription, "serverGetForMerge", aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerGetCardForMergeError[aConnection.connPrefId]++;
				aRepo.cardbookServerGetCardForMergeResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			} else if (response.status > 199 && response.status < 400) {
				let tempCard = new cardbookCardParser(responseTEXT, aConnection.connUrl, aEtag, aConnection.connPrefId);
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetOK", [aConnection.connDescription, tempCard.fn]);
				await aRepo.mergeCardsFromSync(aCacheCard, tempCard, aConnection, aEtag, "SYNC");
			} else {
				aRepo.cardbookServerGetCardForMergeError[aConnection.connPrefId]++;
				aRepo.cardbookServerGetCardForMergeResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, status], "Error");
			}
		}).catch(async (response) => {
			console.log(response);
			aRepo.cardbookServerGetCardForMergeError[aConnection.connPrefId]++;
			aRepo.cardbookServerGetCardForMergeResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, status], "Error");
		});
	},

	serverMultiGet: async function(aRepo, aConnection) {
		let multiget = cardbookBGPreferences.getPref("multiget");
		for (let i = 0; i < aRepo.cardbookServerMultiGetArray[aConnection.connPrefId].length; i = i + +multiget) {
			aRepo.cardbookServerMultiGetRequest[aConnection.connPrefId]++;
			let subArray = aRepo.cardbookServerMultiGetArray[aConnection.connPrefId].slice(i, i + +multiget);
			let length = subArray.length;
			let pwdurl = cardbookBGPreferences.getUrl(aConnection.connPrefId);
			let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : pwdurl});
			let req = new cardbookFetchParser(aConnection.connUser, password);
			let headers = new Headers();
			headers.append("Depth", "1");
			headers.append("Content-Type", "application/xml; charset=utf-8");
			let version = cardbookBGPreferences.getVCardVersion(aConnection.connPrefId);
			let data = cardbookBGSynchronizationUtils.buildMultigetRequest(subArray, version);
			let request = req.fetch(aConnection.connUrl, {
				method: "REPORT",
				headers: headers,
				body: data,
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});
			request.then(async (response) => {
				let responseTEXT = await response.text();
				let responseJSON = new XMLTEXTToJSONParser(responseTEXT);
				if (responseJSON && responseJSON["parsererror"] && responseJSON["parsererror"][0]["sourcetext"] && responseJSON["parsererror"][0]["sourcetext"][0]) {
					aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] = aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] + length;
					aRepo.cardbookServerMultiGetError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "unableToParseResponse", [aConnection.connDescription, responseJSON["parsererror"][0]["sourcetext"][0]], "Error");
				} else if (responseJSON && responseJSON["multistatus"] && (response.status > 199 && response.status < 400)) {
					try {
						let jsonResponses = responseJSON["multistatus"][0]["response"];
						for (var prop in jsonResponses) {
							var jsonResponse = jsonResponses[prop];
							try {
								let href = decodeURIComponent(jsonResponse["href"][0]);
								let propstats = jsonResponse["propstat"];
								// 2015.04.27 14:03:55 : href : /remote.php/carddav/addressbooks/11111/contacts/
								// 2015.04.27 14:03:55 : propstats : [{prop:[{getcontenttype:[null], getetag:[null]}], status:["HTTP/1.1 404 Not Found"]}]
								// 2015.04.27 14:03:55 : href : /remote.php/carddav/addressbooks/11111/contacts/C68894CF-D340-0001-78C3-1E301B4011F5.vcf
								// 2015.04.27 14:03:55 : propstats : [{prop:[{getcontenttype:["text/x-vcard"], getetag:["\"6163e30117192647e1967de751fb5467\""]}], status:["HTTP/1.1 200 OK"]}]
								for (var prop1 in propstats) {
									var propstat = propstats[prop1];
									aRepo.cardbookServerGetCardRequest[aConnection.connPrefId]++;
									if (cardbookBGSynchronizationUtils.isStatusCorrect(propstat["status"][0])) {
										if (propstat["prop"] != null && propstat["prop"] !== undefined && propstat["prop"] != "") {
											let prop2 = propstat["prop"][0];
											if (typeof(prop2["getetag"]) == "undefined") {
												var etag = "";
											} else {
												var etag = prop2["getetag"][0];
											}
											try {
												var myContent = decodeURIComponent(prop2["address-data"][0]);
											}
											catch (e) {
												var myContent = prop2["address-data"][0];
											}
											try {
												var aRootUrl = cardbookBGSynchronizationUtils.getRootUrl(aConnection.connUrl);
												var myCard = new cardbookCardParser(myContent, aRootUrl + href, etag, aConnection.connPrefId);
											}
											catch (e) {
												aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
												aRepo.cardbookServerGetCardResponse[aConnection.connPrefId]++;
												aRepo.cardbookServerGetCardError[aConnection.connPrefId]++;
												if (e.message == "") {
													cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "parsingCardError", [aConnection.connDescription, messenger.i18n.getMessage(e.code), myContent], "Error");
												} else {
													cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "parsingCardError", [aConnection.connDescription, e.message, myContent], "Error");
												}
												continue;
											}
											if (aRepo.cardbookCards[myCard.dirPrefId+"::"+myCard.uid]) {
												let myOldCard = aRepo.cardbookCards[myCard.dirPrefId+"::"+myCard.uid];
												await aRepo.removeCardFromRepository(myOldCard, true);
											}
											cardbookBGUtils.setCacheURIFromValue(myCard, cardbookBGUtils.getFileNameFromUrl(aConnection.connUrl + href))
											await cardbookBGUtils.changeMediaFromFileToContent(myCard);
											await aRepo.addCardToRepository(myCard, true);
											cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetOK", [aConnection.connDescription, myCard.fn]);
											aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
											aRepo.cardbookServerGetCardResponse[aConnection.connPrefId]++;
										} else {
											aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
											aRepo.cardbookServerGetCardResponse[aConnection.connPrefId]++;
											aRepo.cardbookServerGetCardError[aConnection.connPrefId]++;
											cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, status], "Error");
										}
									} else {
										aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
										aRepo.cardbookServerMultiGetError[aConnection.connPrefId]++;
										cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, status], "Error");
									}
								}
							} catch(e) {
								aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] = aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] + length;
								aRepo.cardbookServerMultiGetError[aConnection.connPrefId]++;
								cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aConnection.connDescription + " : cardbookBGSynchronizationCARDDAV.serverMultiGet error : " + e, "Error");
							}
						}
					} catch(e) {
						aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] = aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] + length;
						aRepo.cardbookServerMultiGetError[aConnection.connPrefId]++;
						cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aConnection.connDescription + " : cardbookBGSynchronizationCARDDAV.serverMultiGet error : " + e, "Error");
					}
				} else {
					aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] = aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] + length;
					aRepo.cardbookServerMultiGetError[aConnection.connPrefId]++;
				}
				aRepo.cardbookServerMultiGetResponse[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] = aRepo.cardbookServerCardSyncDone[aConnection.connPrefId] + length;
				aRepo.cardbookServerMultiGetError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardGetFailed", [aConnection.connDescription, aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerMultiGetResponse[aConnection.connPrefId]++;
			});
		}
	},

	serverDeleteCard: async function(aRepo, aConnection, aCard) {
		if (cardbookBGSynchronizationUtils.getModifsPushed(aRepo, aConnection.connPrefId) <= cardbookBGPreferences.getMaxModifsPushed()) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardSendingDeletion", [aConnection.connDescription, aCard.fn]);
			let pwdurl = cardbookBGPreferences.getUrl(aConnection.connPrefId);
			let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : pwdurl});
			let req = new cardbookFetchParser(aConnection.connUser, password);
			let headers = new Headers();
			headers.append("If-Match", aCard.etag);
			headers.append("Content-Type", "application/xml; charset=utf-8");
			let request = req.fetch(aConnection.connUrl, {
				method: "DELETE",
				headers: headers,
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});
			request.then(async (response) => {
				if (response.status > 199 && response.status < 400) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardDeletedFromServer", [aConnection.connDescription, aCard.fn]);
					await aRepo.removeCardFromRepository(aCard);
				} else if (response.status == 404) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardNotExistServer", [aConnection.connDescription, aCard.fn]);
					await aRepo.removeCardFromRepository(aCard);
				} else {
					aRepo.cardbookServerDeletedCardError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardDeleteFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
				aRepo.cardbookServerDeletedCardResponse[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerDeletedCardError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardDeleteFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
				aRepo.cardbookServerDeletedCardResponse[aConnection.connPrefId]++;
			});
		} else {
			aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			aRepo.cardbookServerDeletedCardResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerNotPushed[aConnection.connPrefId]++;
		}
	},

	serverUpdateCard: async function(aRepo, aConnection, aCard, aModifiedCard) {
		if (cardbookBGSynchronizationUtils.getModifsPushed(aRepo, aConnection.connPrefId) <= cardbookBGPreferences.getMaxModifsPushed()) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardSendingUpdate", [aConnection.connDescription, aModifiedCard.fn]);
			cardbookBGUtils.nullifyTagModification(aModifiedCard);
			let pwdurl = cardbookBGPreferences.getUrl(aConnection.connPrefId);
			let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : pwdurl});
			let req = new cardbookFetchParser(aConnection.connUser, password);
			let headers = new Headers();
			if (aModifiedCard.etag && aModifiedCard.etag != "0") {
				headers["If-Match"] = aModifiedCard.etag;
			} else {
				headers["If-None-Match"] = "*";
			}
			headers.append("Content-Type", "text/vcard; charset=utf-8");
			let data = await cardbookBGUtils.getvCardForServer(aRepo.statusInformation, aModifiedCard);
			let request = req.fetch(aConnection.connUrl, {
				method: "PUT",
				headers: headers,
				body: data,
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});
			request.then(async (response) => {
				if (response.status > 199 && response.status < 400) {
					let responseEtag = response.headers.get("etag");
					if (responseEtag) {
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardUpdatedOnServerWithEtag", [aConnection.connDescription, aModifiedCard.fn, responseEtag]);
						cardbookBGUtils.addEtag(aModifiedCard, responseEtag);
					} else {
						aRepo.cardbookServerSyncAgain[aConnection.connPrefId] = true;
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardUpdatedOnServerWithoutEtag", [aConnection.connDescription, aModifiedCard.fn]);
					}
					aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
					await cardbookBGUtils.changeMediaFromFileToContent(aModifiedCard);
					await aRepo.removeCardFromRepository(aCard, true);
					cardbookBGUtils.setCacheURIFromValue(aModifiedCard, cardbookBGUtils.getFileNameFromUrl(aConnection.connUrl))
					await aRepo.addCardToRepository(aModifiedCard, true);
				} else {
					cardbookBGUtils.addTagUpdated(aModifiedCard);
					aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
					aRepo.cardbookServerUpdatedCardError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardUpdateFailed", [aConnection.connDescription, aModifiedCard.fn, aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerUpdatedCardResponse[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerUpdatedCardError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardUpdateFailed", [aConnection.connDescription, aModifiedCard.fn, aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
				aRepo.cardbookServerUpdatedCardResponse[aConnection.connPrefId]++;
			});
		} else {
			aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			aRepo.cardbookServerUpdatedCardResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerNotPushed[aConnection.connPrefId]++;
		}
	},

	serverCreateCard: async function(aRepo, aConnection, aCard) {
		if (cardbookBGSynchronizationUtils.getModifsPushed(aRepo, aConnection.connPrefId) <= cardbookBGPreferences.getMaxModifsPushed()) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardSendingCreate", [aConnection.connDescription, aCard.fn]);
			cardbookBGSynchronizationUtils.prepareCardForCreation(aCard, aConnection.connUrl);
			aConnection.connUrl = aCard.cardurl;
			let pwdurl = cardbookBGPreferences.getUrl(aConnection.connPrefId);
			let password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aConnection.connUser, url : pwdurl});
			let req = new cardbookFetchParser(aConnection.connUser, password);
			let headers = new Headers();
			if (aCard.etag && aCard.etag != "0") {
				headers["If-Match"] = aCard.etag;
			} else {
				headers["If-None-Match"] = "*";
			}
			headers.append("Content-Type", "text/vcard; charset=utf-8");
			let data = await cardbookBGUtils.getvCardForServer(aRepo.statusInformation, aCard);
			let request = req.fetch(aConnection.connUrl, {
				method: "PUT",
				headers: headers,
				body: data,
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});
			request.then(async (response) => {
				if (response.status > 199 && response.status < 400) {
					if (aRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid]) {
						await cardbookBGUtils.changeMediaFromFileToContent(aCard);
						var myOldCard = aRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid];
						await aRepo.removeCardFromRepository(myOldCard, true);
					}
					let responseEtag = response.headers.get("etag");
					if (responseEtag) {
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardCreatedOnServerWithEtag", [aConnection.connDescription, aCard.fn, responseEtag]);
						cardbookBGUtils.addEtag(aCard, responseEtag);
					} else {
						aRepo.cardbookServerSyncAgain[aConnection.connPrefId] = true;
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardCreatedOnServerWithoutEtag", [aConnection.connDescription, aCard.fn]);
					}
					cardbookBGUtils.nullifyTagModification(aCard);
					cardbookBGUtils.setCacheURIFromValue(aCard, cardbookBGUtils.getFileNameFromUrl(aConnection.connUrl))
					await aRepo.addCardToRepository(aCard, true);
				} else {
					aRepo.cardbookServerCreatedCardError[aConnection.connPrefId]++;
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardCreateFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
				}
				aRepo.cardbookServerCreatedCardResponse[aConnection.connPrefId]++;
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			}).catch(async (response) => {
				console.log(response);
				aRepo.cardbookServerCreatedCardError[aConnection.connPrefId]++;
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "serverCardCreateFailed", [aConnection.connDescription, aCard.fn, aConnection.connUrl, response.status], "Error");
				aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
				aRepo.cardbookServerCreatedCardResponse[aConnection.connPrefId]++;
			});
		} else {
			aRepo.cardbookServerCardSyncDone[aConnection.connPrefId]++;
			aRepo.cardbookServerCreatedCardResponse[aConnection.connPrefId]++;
			aRepo.cardbookServerNotPushed[aConnection.connPrefId]++;
		}
	},
};
