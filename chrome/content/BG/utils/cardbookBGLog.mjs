import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";

export var cardbookBGLog = {
	getTime: function() {
		let objToday = new Date();
		let year = objToday.getFullYear();
		let month = ("0" + (objToday.getMonth() + 1)).slice(-2);
		let day = ("0" + objToday.getDate()).slice(-2);
		let hour = ("0" + objToday.getHours()).slice(-2);
		let min = ("0" + objToday.getMinutes()).slice(-2);
		let sec = ("0" + objToday.getSeconds()).slice(-2);
		let msec = ("00" + objToday.getMilliseconds()).slice(-3);
		return year + "." + month + "." + day + " " + hour + ":" + min + ":" + sec + ":" + msec;
	},

	formatStringForOutput: function(aLog, aStringCode, aValuesArray, aErrorCode) {
		if (aLog) {
			if (aValuesArray) {
				if (aErrorCode) {
					cardbookBGLog.updateStatusProgressInformation(aLog, messenger.i18n.getMessage(aStringCode, aValuesArray), aErrorCode);
				} else {
					cardbookBGLog.updateStatusProgressInformation(aLog, messenger.i18n.getMessage(aStringCode, aValuesArray));
				}
			} else {
				if (aErrorCode) {
					cardbookBGLog.updateStatusProgressInformation(aLog, messenger.i18n.getMessage(aStringCode), aErrorCode);
				} else {
					cardbookBGLog.updateStatusProgressInformation(aLog, messenger.i18n.getMessage(aStringCode));
				}
			}
		}
	},

	updateStatusProgressInformation: function(aLog, aLogLine, aErrorType) {
		if (aLog.length >= cardbookBGPreferences.getPref("statusInformationLineNumber")) {
			aLog.shift();
		}
		if (aErrorType) {
			aLog.push([cardbookBGLog.getTime() + " : " + aLogLine, aErrorType]);
		} else {
			aLog.push([cardbookBGLog.getTime() + " : " + aLogLine, "Normal"]);
		}
	},

	updateStatusProgressInformationWithDebug1: function(aLog, aLogLine, aResponse) {
		if (aResponse) {
			if (cardbookBGPreferences.getPref("debugMode")) {
				cardbookBGLog.updateStatusProgressInformation(aLog, aLogLine + aResponse);
			}
		}
	},

	updateStatusProgressInformationWithDebug2: function(aLog, aLogLine) {
		if (cardbookBGPreferences.getPref("debugMode")) {
			cardbookBGLog.updateStatusProgressInformation(aLog, aLogLine);
		}
	}
};
