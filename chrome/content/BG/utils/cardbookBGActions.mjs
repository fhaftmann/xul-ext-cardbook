import { cardbookHTMLPluralRules } from "../../HTML/utils/scripts/cardbookHTMLPluralRules.mjs";
import { cardbookBGLog } from "./cardbookBGLog.mjs";
import { cardbookIDBUndo } from "../indexedDB/cardbookIDBUndo.mjs";

export var cardbookBGActions = {

    startAction: function (aRepo, aActionCode, aArray, aRefreshAccount, aTotalEstimated, aTotal) {
        aRepo.currentActionId++;
        if (!aRepo.currentAction[aRepo.currentActionId]) {
            aRepo.currentAction[aRepo.currentActionId] = {actionCode : aActionCode, message : "", oldCards: [], newCards: [], oldCats: [], newCats: [], addedCards: 0, totalCards: 0, doneCards: 0, totalCats: 0, doneCats: 0, files: [], refresh: ""};
        }
        if (aRefreshAccount) {
            aRepo.currentAction[aRepo.currentActionId].refresh = aRefreshAccount;
        }
        if (aTotalEstimated) {
            aRepo.currentAction[aRepo.currentActionId].totalEstimatedCards = aTotalEstimated;
        }
        if (aTotal) {
            aRepo.currentAction[aRepo.currentActionId].totalCards = aTotal;
        }
        if (aRepo.currentAction[aRepo.currentActionId]) {
            let actionCode = aRepo.currentAction[aRepo.currentActionId].actionCode;
            switch(actionCode) {
                case "cardsConverted":
                case "outgoingEmailCollected":
                case "emailCollectedByFilter":
                case "emailDeletedByFilter":
                case "cardsMerged":
                case "cardsDuplicated":
                case "cardsPasted":
                case "cardsDragged":
                case "displayNameGenerated":
                case "linePasted":
                    aRepo.currentAction[aRepo.currentActionId].message = messenger.i18n.getMessage(actionCode + 'Undo');
                    break;
                case "cardsImportedFromFile":
                case "cardsImportedFromDir":
                case "undoActionDone":
                case "redoActionDone":
                case "cardCreated":
                case "cardModified":
                case "categoryCreated":
                case "categoryRenamed":
                case "categorySelected":
                case "categoryUnselected":
                case "categoryDeleted":
                case "categoryConvertedToList":
                case "listConvertedToCategory":
                case "nodeRenamed":
                case "nodeDeleted":
                case "listCreatedFromNode":
                case "cardsFormatted":
                        aRepo.currentAction[aRepo.currentActionId].message = messenger.i18n.getMessage(actionCode + 'Undo', aArray);
                    break;
                case "cardsExportedToCsv":
                    aRepo.currentAction[aRepo.currentActionId].message = messenger.i18n.getMessage("exportCardToCSVFileLabel");
                    break;
                case "cardsExportedToVcf":
                    aRepo.currentAction[aRepo.currentActionId].message = messenger.i18n.getMessage("exportCardToVCFFileLabel");
                    break;
                case "cardsExportedToZip":
                    aRepo.currentAction[aRepo.currentActionId].message = messenger.i18n.getMessage("exportCardToZIPFileLabel");
                    break;
                case "cardsImagesExported":
                    aRepo.currentAction[aRepo.currentActionId].message = messenger.i18n.getMessage("exportCardImagesLabel");
                    break;
                case "cardsDeleted":
                    if (aArray && aArray.length == 1) {
                        aRepo.currentAction[aRepo.currentActionId].message = messenger.i18n.getMessage(actionCode + '1' + 'Undo', [aArray[0].fn]);
                    } else {
                        aRepo.currentAction[aRepo.currentActionId].message = messenger.i18n.getMessage(actionCode + '2' + 'Undo');
                    }
                    break;
            }
        }
        return aRepo.currentActionId;
    },

    alertAction: async function (aRepo, aActionId, aFinishParams) {
        if (aRepo.currentAction[aActionId]) {
            let actionCode = aRepo.currentAction[aActionId].actionCode;
            let source = aFinishParams?.source || "HTML";
            let alertFunction = (source == "HTML") ? messenger.runtime.sendMessage : messenger.NotifyTools.notifyExperiment;
            let alertTitle = "";
            let alertMessage = "";
            if (actionCode == "cardsImportedFromFile") {
                alertTitle = messenger.i18n.getMessage("importCardFromFileLabel");
                alertMessage = cardbookHTMLPluralRules.getPluralMessage("importCardFromFileMessagePF", aFinishParams.length, [aFinishParams.length, aFinishParams.name]);
                cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "importFinishedResult", [aFinishParams.targetName]);
                cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "importCardsOK", [aFinishParams.targetName, aFinishParams.length]);
            } else if (actionCode == "cardsImportedFromDir") {
                alertTitle = messenger.i18n.getMessage("importCardFromDirLabel");
                alertMessage = cardbookHTMLPluralRules.getPluralMessage("importCardFromDirMessagePF", aFinishParams.length, [aFinishParams.length, aFinishParams.name]);
                cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "importFinishedResult", [aFinishParams.targetName]);
                cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "importCardsOK", [aFinishParams.targetName, aFinishParams.length]);
            } else {
                return
            }
            await alertFunction({query: "cardbook.finishImport", title: alertTitle, message: alertMessage});
        }
    },

    addUndoCardsAction: async function (aRepo, aActionCode, aActionMessage, aOldCards, aNewCards, aOldCats, aNewCats) {
        let myNextUndoId = aRepo.currentUndoId + 1;
        await cardbookIDBUndo.addUndoItem(aRepo, myNextUndoId, aActionCode, aActionMessage, aOldCards, aNewCards, aOldCats, aNewCats, false);
    },

    endAction: async function (aRepo, aActionId, aFinishParams) {
        if (aRepo.currentAction[aActionId]) {
            let action = aRepo.currentAction[aActionId];
            if (action.files.length > 0) {
                await messenger.NotifyTools.notifyExperiment({query: "cardbook.addActivityFromUndo", actionCode: action.actionCode, actionName: action.message});
                if (action.actionCode != "undoActionDone" && action.actionCode != "redoActionDone"  && action.actionCode != "syncMyPhoneExplorer"
                    && action.actionCode != "cardsExportedToZip" && action.actionCode != "cardsImagesExported"
                    && action.actionCode != "cardsExportedToCsv"  && action.actionCode != "cardsExportedToVcf") {
                        await cardbookBGActions.addUndoCardsAction(aRepo, action.actionCode, action.message, action.oldCards, action.newCards, action.oldCats, action.newCats);
                }
                await aRepo.reWriteFiles(action.files);
            }
            await cardbookBGActions.alertAction(aRepo, aActionId, aFinishParams);
            aRepo.currentAction[aActionId] = null;
        }
    },
 };
