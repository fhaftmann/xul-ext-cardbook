/* Inside WindowListener */
// NotifyTools
//  https://github.com/thundernest/addon-developer-support/wiki/Tutorial:-Convert-add-on-parts-individually-by-using-a-messaging-system
//  https://github.com/thundernest/addon-developer-support/tree/master/scripts/notifyTools
//  https://github.com/thundernest/addon-developer-support/tree/master/auxiliary-apis/NotifyTools

//call with
//    await messenger.runtime.sendMessage('cardbook@vigneau.philippe', options);
//  options:
//    {query: 'version'}                return version info
//      {version: api-version, exclusive: true|false}
//    {query: 'addressbooks'}                return version info
//      {name: ..., id: ..., type: ..., readonly: ..., ... } });}
//    {query: 'lists'}                  return array of all lists
//      [{name: fn, id: id, bcolor: backgroundcolor, fcolor: foregroundcolor}, ...]
//    {query: 'lists', id: id}          return array of emails for list with id
//      [{fn: fn, email: email}, ...]  (fn only if exists)
//    {query: 'contacts', search: searchterm}   return array of contacts which matches searchterm
//      [complete card from cardbook, ...]    (should probably reduced to essential data)
//    {query: 'openBook'}               open cardbook window, returns nothing

import { cardbookBGPreferences } from "../../BG/utils/cardbookBGPreferences.mjs";
import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookBGUtils } from "../utils/cardbookBGUtils.mjs";

export var simpleMailRedirection = {
		
	API_VERSION: 2,

	version: function () {
		let exclusive = cardbookBGPreferences.getPref("exclusive");
		return { version: simpleMailRedirection.API_VERSION, exclusive: exclusive };
	},

	addressbooks: function (aRepo) {
		let addressbooks = aRepo.cardbookAccounts;
		addressbooks = addressbooks.filter((e) => e[2] && e[3] != "SEARCH" ); //exclude disabled books and Searches
		addressbooks = addressbooks.map((e) => {
            let bcolor = cardbookBGPreferences.getColor(e[1]);
            let fcolor = cardbookHTMLUtils.getTextColorFromBackgroundColor(bcolor);
            return { name: e[0], id: e[1], type: e[3], readonly: e[4], bcolor: bcolor, fcolor: fcolor }
        });
		return addressbooks;
	},

	lists: function (aRepo, id) {
		if (id) {
			let emails = [];
			let list = aRepo.cardbookCards[id];
			if (!list) return emails;
			let members = cardbookBGUtils.getMembersFromCard(aRepo, list);
			for (let email of members.mails) {	//pure emails
				emails.push({ email: email });
			}
			for (let card of members.uids) {
				if (card.fn)
					emails.push({ fn: card.fn, email: card.emails[0] });
				else
					emails.push({ email: card.emails[0] });
			}
			return emails;
		} else {
			let lists = [];
			for (let j in aRepo.cardbookCards) {
				let card = aRepo.cardbookCards[j];
				if (card.isAList) {
					let ab = aRepo.cardbookAccounts.find((ab)=>ab[1]==card.dirPrefId);
                    if (!ab[2] || ab[3] == "SEARCH") continue;  //exclude lists from disables books or searches
					let bcolor = cardbookBGPreferences.getColor(card.dirPrefId);
					let nodeColors = cardbookBGPreferences.getNodeColors();
					for (let category of card.categories) {
						let color = nodeColors[category];
						if (!color) {
							continue;
						} else {
							bcolor = color;
							break;
						}
					}
					let fcolor = cardbookHTMLUtils.getTextColorFromBackgroundColor(bcolor);
					lists.push({ name: card.fn, id: card.uid, abid: card.dirPrefId,
													abname: ab[0], bcolor: bcolor, fcolor: fcolor });
				}
			}
			return lists;
		}
	},

	contacts: function (aRepo, search, books) {
		let contacts = new Array();
		let searchString = cardbookBGUtils.makeSearchString(search);
		let searchArray = cardbookBGPreferences.getPref("autocompleteRestrictSearch")
			? aRepo.cardbookCardShortSearch
			: aRepo.cardbookCardLongSearch;
		if (Object.keys(searchArray).length == 0) return contacts;
		let nodeColors = cardbookBGPreferences.getNodeColors();
		for (let account of aRepo.cardbookAccounts) {
			let dirPrefId = account[1];
			if (books && !books.includes(dirPrefId)) continue;
			if (account[2] && account[3] != "SEARCH") { //exclude disabled books and searches
				for (let j in searchArray[dirPrefId]) {
					if (j.indexOf(searchString) >= 0 || searchString == "") {
						for (let card of searchArray[dirPrefId][j]) {
							let bcolor = cardbookBGPreferences.getColor(card.dirPrefId);
							for (let category of card.categories) {
								let color = nodeColors[category];
								if (!color) {
									continue;
								} else {
									bcolor = color;
									break;
								}
							}
							let fcolor = cardbookHTMLUtils.getTextColorFromBackgroundColor(bcolor);
							card.bcolor = bcolor;
							card.fcolor = fcolor;
							contacts.push(card);
						}
					}
				}
			}
		}
		return contacts;
	}
}