import { cardbookIDBEncryptor } from "./cardbookIDBEncryptor.mjs";
import { cardbookBGLog } from "../utils/cardbookBGLog.mjs";

export var cardbookIDBMailPop = {
	cardbookMailPopDatabaseVersion: "1",
	cardbookMailPopDatabaseName: "CardBookMailPop",
    db: {},

	// generic output when errors on DB
	onerror: function(e) {
		console.log(e);
	},

	// first step for getting the mail popularities
	openMailPopDB: function() {
        let openMailpopDB = new Promise( async function(resolve, reject) {
            var request = indexedDB.open(cardbookIDBMailPop.cardbookMailPopDatabaseName, cardbookIDBMailPop.cardbookMailPopDatabaseVersion);
            // when version changes
            // It's an assumed choice not to have the email as the key of the indexedDB DB
            // in order not to provide unencrypted emails in an encrypted DB
            request.onupgradeneeded = function(e) {
                var db = e.target.result;
                e.target.transaction.onerror = cardbookIDBMailPop.onerror;
                if (e.oldVersion < 1) {
                    if (db.objectStoreNames.contains("mailPops")) {
                        db.deleteObjectStore("mailPops");
                    }
                    let store = db.createObjectStore("mailPops", {keyPath: "mailPopId", autoIncrement: true});
                }
            };
            // when success, call the observer for starting the load cache and maybe the sync
            request.onsuccess = function(e) {
				cardbookIDBMailPop.db = e.target.result;
				resolve();
            };
            
            // when error, call the observer for starting the load cache and maybe the sync
            request.onerror = function(e) {
				cardbookIDBMailPop.onerror(e);
                reject();
            };
        });
        return openMailpopDB;
	},

	// check if the mail popularity is in a wrong encryption state
	// then decrypt the mail popularity if possible
	checkMailPop: async function(aRepo, aMailPop) {
		try {
			var stateMismatched = cardbookIDBEncryptor.encryptionEnabled != 'encrypted' in aMailPop;
			var versionMismatched = aMailPop.encryptionVersion && aMailPop.encryptionVersion != cardbookIDBEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ('encrypted' in aMailPop) {
					aMailPop = await cardbookIDBEncryptor.decryptMailPop(aMailPop);
				}
				cardbookIDBMailPop.addMailPop(aRepo, aMailPop);
			} else {
				if ('encrypted' in aMailPop) {
					aMailPop = await cardbookIDBEncryptor.decryptMailPop(aMailPop);
				}
			}
			return aMailPop;
		}
		catch(e) {
			cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Decryption failed e : " + e, "Error");
			throw new Error("failed to decrypt the mail popularity : " + e);
		}
	},

	clearData: async function(aData) {
		let removeData = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBMailPop.db.transaction(["mailPops"], "readwrite");
			var store = transaction.objectStore("mailPops");
			var cursorRequest = store.clear();
		
			cursorRequest.onsuccess = async function(e) {
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				console.log(e)
				reject();
			};
		});
		await removeData;
	},

	loadData: async function(aData) {
		let loadData = new Promise( async function(resolve, reject) {
			let total = aData.length;
            if (total == 0) {
                resolve("OK");
                return
            }
			let current = 0;
			for (let data of aData) {
				var storedData = cardbookIDBEncryptor.encryptionEnabled ? (await cardbookIDBEncryptor.encryptMailPop(data)) : data;
				var transaction = cardbookIDBMailPop.db.transaction(["mailPops"], "readwrite");
				var store = transaction.objectStore("mailPops");
				var cursorRequest = store.put(storedData);
				cursorRequest.onsuccess = async function(e) {
					current++;
					if (current == total) {
						resolve("OK");
					}
				};
				cursorRequest.onerror = async function(e) {
					console.log(e);
					reject("KO");
				};
			}
        });
        return loadData;
	},

	updateMailPop: async function(aRepo, aEmail, aCount) {
		aEmail = aEmail.toLowerCase();

		if (aCount && aRepo.cardbookMailPopularityIndex[aEmail] && aCount == aRepo.cardbookMailPopularityIndex[aEmail].count) {
			return;
		}
		let mailPop;
		if (aCount && aCount != "0") {
			aCount = parseInt(aCount);
			if (aRepo.cardbookMailPopularityIndex[aEmail]) {
				mailPop = {email: aEmail, count: aCount, mailPopId: aRepo.cardbookMailPopularityIndex[aEmail].mailPopId};
			} else {
				mailPop = {email: aEmail, count: aCount};
			}
			cardbookIDBMailPop.addMailPop(aRepo, mailPop);
		} else if (aCount && aCount == "0") {
			cardbookIDBMailPop.removeMailPop(aRepo, aEmail);
		} else {
			if (aRepo.cardbookMailPopularityIndex[aEmail]) {
				mailPop = {email: aEmail, count: aRepo.cardbookMailPopularityIndex[aEmail].count + 1, mailPopId: aRepo.cardbookMailPopularityIndex[aEmail].mailPopId};
			} else {
				mailPop = {email: aEmail, count: 1};
			}
			cardbookIDBMailPop.addMailPop(aRepo, mailPop);
		}
	},

	// add or override the mail popularity to the cache
	addMailPop: async function(aRepo, aMailPop, aMode) {
		try {
			if (aMailPop.count == "0") {
				return
			}
			aMailPop.email = aMailPop.email.toLowerCase();
			aMailPop.count = parseInt(aMailPop.count);
			if (!aMailPop.mailPopId) {
				aMailPop.mailPopId = cardbookIDBMailPop.getMailPopId(aRepo);
			}
			var storedMailPop = cardbookIDBEncryptor.encryptionEnabled ? (await cardbookIDBEncryptor.encryptMailPop(aMailPop)) : aMailPop;
			var transaction = cardbookIDBMailPop.db.transaction(["mailPops"], "readwrite");
			var store = transaction.objectStore("mailPops");
			var cursorRequest = store.put(storedMailPop);

			cursorRequest.onsuccess = async function(e) {
				cardbookIDBMailPop.addMailPopToIndex(aRepo, aMailPop);
				if (cardbookIDBEncryptor.encryptionEnabled) {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : Mail popularity " + aMailPop.email + " written to encrypted MailPopDB");
				} else {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : Mail popularity " + aMailPop.email + " written to MailPopDB");
				}
				if (aMode) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: aMode});
				}
			};

			cursorRequest.onerror = cardbookIDBMailPop.onerror;
		} catch(e) {
			cardbookIDBMailPop.onerror(e);
		}
	},

	// delete the mail popularity
	removeMailPop: function(aRepo, aEmail) {
		aEmail = aEmail.toLowerCase();
		if (aRepo.cardbookMailPopularityIndex[aEmail]) {
			var mailPopId = aRepo.cardbookMailPopularityIndex[aEmail].mailPopId;
		} else {
			return;
		}
		var transaction = cardbookIDBMailPop.db.transaction(["mailPops"], "readwrite");
		var store = transaction.objectStore("mailPops");
		var cursorDelete = store.delete(mailPopId);
		
		cursorDelete.onsuccess = async function(e) {
			if (aRepo.cardbookMailPopularityIndex[aEmail]) {
				delete aRepo.cardbookMailPopularityIndex[aEmail];
			}
			if (cardbookIDBEncryptor.encryptionEnabled) {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : Mail popularity " + aEmail + " deleted from encrypted MailPopDB");
			} else {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : Mail popularity " + aEmail + " deleted from MailPopDB");
			}
		};
		cursorDelete.onerror = cardbookIDBMailPop.onerror;
	},

	// once the DB is open, this is the second step 
	loadMailPop: function(aRepo) {
		let loadMailPop = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBMailPop.db.transaction(["mailPops"], "readonly");
			var store = transaction.objectStore("mailPops");
			var cursorRequest = store.getAll();
			const handleMailPop = async mailPop => {
				try {
					mailPop = await cardbookIDBMailPop.checkMailPop(aRepo, mailPop);
				} catch(e) {}
				if (mailPop.email && mailPop.count) {
					cardbookIDBMailPop.addMailPopToIndex(aRepo, mailPop);
				}
			};
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var mailPop of result) {
						handleMailPop(mailPop);
					}
				}
				resolve();
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBMailPop.onerror(e);
				reject();
			}
		});
		return loadMailPop;
	},

	getMailPopId: function(aRepo) {
		aRepo.cardbookMailPopularityLastIndex++;
		return aRepo.cardbookMailPopularityLastIndex;
	},
	
	addMailPopToIndex: function(aRepo, aMailPop) {
		aRepo.cardbookMailPopularityIndex[aMailPop.email.toLowerCase()] = {count: aMailPop.count, mailPopId: aMailPop.mailPopId};
		if (aMailPop.mailPopId > aRepo.cardbookMailPopularityLastIndex) {
			aRepo.cardbookMailPopularityLastIndex = aMailPop.mailPopId;
		}
	},
	
	encryptMailPops: async function(aCallBack, aRepo) {
		var mailPopsTransaction = cardbookIDBMailPop.db.transaction(["mailPops"], "readonly");
		return aCallBack(
			cardbookIDBMailPop.db,
			mailPopsTransaction.objectStore("mailPops"),
			async mailPop => {
				try {
					cardbookIDBMailPop.addMailPop(aRepo, mailPop, "encryption");
				}
				catch(e) {
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Encryption failed e : " + e, "Error");
				}
			},
			mailPop => !("encrypted" in mailPop),
			"encryption"
		);
	},

	decryptMailPops: async function(aCallBack, aRepo) {
		var mailPopsTransaction = cardbookIDBMailPop.db.transaction(["mailPops"], "readonly");
		return aCallBack(
			cardbookIDBMailPop.db,
			mailPopsTransaction.objectStore("mailPops"),
			async mailPop => {
				try {
					mailPop = await cardbookIDBEncryptor.decryptMailPop(mailPop);
					cardbookIDBMailPop.addMailPop(aRepo, mailPop, "decryption");
				}
				catch(e) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "decryption"});
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Decryption failed e : " + e, "Error");
				}
			},
			mailPop => ("encrypted" in mailPop),
			"decryption"
		);
	},

	upgradeMailPops: async function(aCallBack, aRepo) {
		var mailPopsTransaction = cardbookIDBMailPop.db.transaction(["mailPops"], "readonly");
		return aCallBack(
			cardbookIDBMailPop.db,
			mailPopsTransaction.objectStore("mailPops"),
			async mailPop => {
				try {
					mailPop = await cardbookIDBEncryptor.decryptMailPop(mailPop);
					cardbookIDBMailPop.addMailPop(aRepo, mailPop, "encryption");
				}
				catch(e) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "encryption"});
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Upgrade failed e : " + e, "Error");
				}
			},
			mailPop => ("encrypted" in mailPop && mailPop.encryptionVersion != cardbookIDBEncryptor.VERSION),
			"encryption"
		);
	}
};
