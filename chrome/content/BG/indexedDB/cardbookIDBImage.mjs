import { cardbookIDBEncryptor } from "./cardbookIDBEncryptor.mjs";
import { cardbookBGLog } from "../utils/cardbookBGLog.mjs";
import { cardbookBGPreferences } from "../utils/cardbookBGPreferences.mjs";
import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";

export var cardbookIDBImage = {
	cardbookImageDatabaseVersion: "1",
	cardbookImageDatabaseName: "CardBookImage",
	db: {},

    // generic output when errors on DB
    onerror: function(e) {
		console.log(e);
    },

	// first step for getting the images
	openImageDB: function() {
        let openImageDB = new Promise( async function(resolve, reject) {
            var request = indexedDB.open(cardbookIDBImage.cardbookImageDatabaseName, cardbookIDBImage.cardbookImageDatabaseVersion);
            // when version changes
            // for the moment delete all and recreate one new empty
            request.onupgradeneeded = function(e) {
                var db = e.target.result;
                e.target.transaction.onerror = cardbookIDBImage.onerror;
                if (e.oldVersion < 1) {
                    for (let media of cardbookHTMLUtils.allColumns.media) {
                        if (db.objectStoreNames.contains(media)) {
                            db.deleteObjectStore(media);
                        }
                        let store = db.createObjectStore(media, {keyPath: "cbid", autoIncrement: false});
                        store.createIndex("dirPrefIdIndex", "dirPrefId", { unique: false });
                    }
                }
            };
            // when success, call the observer for starting the load cache and maybe the sync
            request.onsuccess = function(e) {
				cardbookIDBImage.db = e.target.result;
				resolve();
            };
            // when error, call the observer for starting the load cache and maybe the sync
            request.onerror = function(e) {
				cardbookIDBImage.onerror(e);
                reject();
            };
        });
        return openImageDB;
	},

	// check if the image is in a wrong encryption state
	// then decrypt the image if possible
	checkImage: async function(aLog, aDB, aDirPrefName, aImage) {
		try {
			var stateMismatched = cardbookIDBEncryptor.encryptionEnabled != 'encrypted' in aImage;
			var versionMismatched = aImage.encryptionVersion && aImage.encryptionVersion != cardbookIDBEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ('encrypted' in aImage) {
					aImage = await cardbookIDBEncryptor.decryptImage(aImage);
				}
				await cardbookIDBImage.addImage(aLog, aDB, aDirPrefName, aImage);
			} else {
				if ('encrypted' in aImage) {
					aImage = await cardbookIDBEncryptor.decryptImage(aImage);
				}
			}
			return aImage;
		}
		catch(e) {
			cardbookBGLog.updateStatusProgressInformation(aLog, "debug mode : Decryption failed e : " + e, "Error");
			throw new Error("failed to decrypt the image : " + e);
		}
	},

	clearData: async function(aData) {
		let checkResult = {};
		let removeData = new Promise( async function(resolve, reject) {
			for (let media of cardbookHTMLUtils.allColumns.media) {
				var transaction = cardbookIDBImage.db.transaction([media], "readwrite");
				var store = transaction.objectStore(media);
				var cursorRequest = store.clear();
			
				cursorRequest.onsuccess = async function(e) {
					checkResult[media] = "OK";
					let check = "";
					for (let media1 of cardbookHTMLUtils.allColumns.media) {
						if (checkResult[media1]) {
							check = check + checkResult[media1];
						}
					}
					if (check == "OKOKOK") {
						resolve();
					}
				};
				cursorRequest.onerror = async function(e) {
					console.log(e)
					reject();
				};
			}
		});
		await removeData;
	},

	loadData: async function(aData) {
		let checkResult = {};
		let total = {};
		let current = {};
		let loadData = new Promise( async function(resolve, reject) {
			let checkTotal = 0;
			for (let media of cardbookHTMLUtils.allColumns.media) {
				checkTotal = checkTotal + aData[media].length;
			}
            if (checkTotal == 0) {
                resolve("OK");
                return
            }
			for (let media of cardbookHTMLUtils.allColumns.media) {
				total[media] = aData[media].length;
				if (total[media] == 0) {
					checkResult[media] = "OK";
				}
				current[media] = 0;
				for (let data of aData[media]) {
					var storedData = cardbookIDBEncryptor.encryptionEnabled ? (await cardbookIDBEncryptor.encryptImage(data)) : data;
					var transaction = cardbookIDBImage.db.transaction([media], "readwrite");
					var store = transaction.objectStore(media);
					var cursorRequest = store.put(storedData);
					cursorRequest.onsuccess = async function(e) {
						current[media]++;
						if (current[media] == total[media]) {
							checkResult[media] = "OK";
							let check = "";
							for (let media1 of cardbookHTMLUtils.allColumns.media) {
								if (checkResult[media1]) {
									check = check + checkResult[media1];
								}
							}
							if (check == "OKOKOK") {
								resolve("OK");
							}
						}
					};
					cursorRequest.onerror = async function(e) {
						console.log(e);
						reject("KO");
					};
				}
			}
        });
        return loadData;
	},

	// add or override the image to the cache
	addImage: async function(aLog, aDB, aDirPrefName, aImage, aCardName, aMode) {
		try {
			var storedImage = cardbookIDBEncryptor.encryptionEnabled ? (await cardbookIDBEncryptor.encryptImage(aImage)) : aImage;
			let result = new Promise( function(resolve, reject) {
				try {
					var transaction = cardbookIDBImage.db.transaction([aDB], "readwrite");
					var store = transaction.objectStore(aDB);
					var cursorRequest = store.put(storedImage);
					cursorRequest.onsuccess = async function(e) {
						if (aLog) {
							if (cardbookIDBEncryptor.encryptionEnabled) {
								cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Image for " + aCardName + " written to encrypted ImageDB (" + aDB + ")");
							} else {
								cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Image for " + aCardName + " written to ImageDB (" + aDB + ")");
							}
						}
						if (aMode) {
							await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: aMode});
						}
						resolve();
					};
					
					cursorRequest.onerror = async function(e) {
						if (aMode) {
							await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: aMode});
						}
						cardbookIDBImage.onerror(e);
						reject();
					};
				} catch(e) {
					cardbookIDBImage.onerror(e);
					reject();
				}
			});
			let dummy = await result;
		} catch(e) {}
	},

    removeImagesFromAccount: async function(aLog, aDirPrefId, aDirPrefName) {
		let checkResult = {};
		let removeImagesFromAccount = new Promise( function(resolve, reject) {
			for (let media of cardbookHTMLUtils.allColumns.media) {
				var transaction = cardbookIDBImage.db.transaction([media], "readwrite");
				var store = transaction.objectStore(media);
				var keyRange = IDBKeyRange.bound(aDirPrefId, aDirPrefId + '\uffff');
				var cursorRequest = store.delete(keyRange);
				cursorRequest.onsuccess = async function(e) {
					if (cardbookIDBEncryptor.encryptionEnabled) {
						cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : deleted from encrypted ImageDB (" + media + ")");
					} else {
						cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : deleted from ImageDB (" + media + ")");
					}
					checkResult[media] = "OK";
					let check = "";
					for (let media1 of cardbookHTMLUtils.allColumns.media) {
						if (checkResult[media1]) {
							check = check + checkResult[media1];
						}
					}
					if (check == "OKOKOK") {
						resolve();
					}
				};
				cursorRequest.onerror = async function(e) {
					cardbookIDBImage.onerror(e);
					checkResult[media] = "OK";
					let check = "";
					for (let media1 of cardbookHTMLUtils.allColumns.media) {
						if (checkResult[media1]) {
							check = check + checkResult[media1];
						}
					}
					if (check == "OKOKOK") {
						resolve();
					}
				};
			}
		});
		await removeImagesFromAccount;
	},

	// delete the image
	removeImage: async function(aLog, aDB, aDirPrefName, aImage, aCardName) {
		return new Promise( function(resolve, reject) {
			var transaction = cardbookIDBImage.db.transaction([aDB], "readwrite");
			var store = transaction.objectStore(aDB);
			var cursorRequest = store.delete(aImage.cbid);
			cursorRequest.onsuccess = function(e) {
				if (cardbookIDBEncryptor.encryptionEnabled) {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Image " + aCardName + " deleted from encrypted ImageDB (" + aDB + ")");
				} else {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Image " + aCardName + " deleted from ImageDB (" + aDB + ")");
				}
				resolve();
			};
			cursorRequest.onerror = function(e) {
				cardbookIDBImage.onerror(e);
				reject();
			};
		});
	},

	getImage: function(aLog, aDB, aDirPrefName, aImageId, aCardName) {
		return new Promise( function(resolve, reject) {
			var transaction = cardbookIDBImage.db.transaction([aDB], "readonly");
			var store = transaction.objectStore(aDB);
			var cursorRequest = store.get(aImageId);
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					let image = await cardbookIDBImage.checkImage(aLog, aDB, aDirPrefName, result, aCardName);
					resolve(image);
				} else {
					resolve();
				}
			};
			cursorRequest.onerror = function(e) {
				cardbookIDBImage.onerror(e);
				resolve();
			};
		});
	},
	
	encryptImages: async function(aCallBack, aRepo) {
		for (let media of cardbookHTMLUtils.allColumns.media) {
			var imagesTransaction = cardbookIDBImage.db.transaction([media], "readonly");
			return aCallBack(
				cardbookIDBImage.db,
				imagesTransaction.objectStore(media),
				async image => {
					try {
						await cardbookIDBImage.addImage(aRepo.statusInformation, media, cardbookBGPreferences.getName(image.dirPrefId), image, "unknown", "encryption");
					}
					catch(e) {
                        cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Encryption failed e : " + e, "Error");
					}
				},
				image => !("encrypted" in image),
				"encryption"
			);
		}
	},

	decryptImages: async function(aCallBack, aRepo) {
		for (let media of cardbookHTMLUtils.allColumns.media) {
			var imagesTransaction = cardbookIDBImage.db.transaction([media], "readonly");
			return aCallBack(
				cardbookIDBImage.db,
				imagesTransaction.objectStore(media),
				async image => {
					try {
						image = await cardbookIDBEncryptor.decryptImage(image);
						await cardbookIDBImage.addImage(aRepo.statusInformation, media, cardbookBGPreferences.getName(image.dirPrefId), image, "unknown", "decryption");
					}
					catch(e) {
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "decryption"});
                        cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Decryption failed e : " + e, "Error");
					}
				},
				image => ("encrypted" in image),
				"decryption"
			);
		}
	},

	upgradeImages: async function(aCallBack, aRepo) {
		for (let media of cardbookHTMLUtils.allColumns.media) {
			var imagesTransaction = cardbookIDBImage.db.transaction([media], "readonly");
			return aCallBack(
				cardbookIDBImage.db,
				imagesTransaction.objectStore(media),
				async image => {
					try {
						image = await cardbookIDBEncryptor.decryptCard(image);
						await cardbookIDBImage.addImage(aRepo.statusInformation, media, cardbookBGPreferences.getName(image.dirPrefId), image, "unknown", "encryption");
					}
					catch(e) {
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "encryption"});
						cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Upgrade failed e : " + e, "Error");
					}
				},
				image => ("encrypted" in image && image.encryptionVersion != cardbookIDBEncryptor.VERSION),
				"encryption"
			);
		}
	}
};
