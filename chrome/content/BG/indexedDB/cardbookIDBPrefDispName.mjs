import { cardbookIDBEncryptor } from "./cardbookIDBEncryptor.mjs";
import { cardbookBGLog } from "../utils/cardbookBGLog.mjs";

export var cardbookIDBPrefDispName = {
	cardbookPrefDispNameDatabaseVersion: "1",
	cardbookPrefDispNameDatabaseName: "CardBookPrefDispName",
    db: {},

    // generic output when errors on DB
    onerror: function(e) {
		console.log(e);
    },

	// first step for getting the emails
	openPrefDispNameDB: function() {
        let openPrefDispNameDB = new Promise( function(resolve, reject) {
            var request = indexedDB.open(cardbookIDBPrefDispName.cardbookPrefDispNameDatabaseName, cardbookIDBPrefDispName.cardbookPrefDispNameDatabaseVersion);
            // when version changes
            // It's an assumed choice not to have the email as the key of the indexedDB DB
            // in order not to provide unencrypted emails in an encrypted DB
            request.onupgradeneeded = function(e) {
                var db = e.target.result;
                e.target.transaction.onerror = cardbookIDBPrefDispName.onerror;
                if (e.oldVersion < 1) {
                    if (db.objectStoreNames.contains("prefDispNames")) {
                        db.deleteObjectStore("prefDispNames");
                    }
                    let store = db.createObjectStore("prefDispNames", {keyPath: "prefDispNameId", autoIncrement: true});
                }
            };
            // when success, call the observer for starting the load cache and maybe the sync
            request.onsuccess = function(e) {
				cardbookIDBPrefDispName.db = e.target.result;
				resolve();
            };
            // when error, call the observer for starting the load cache and maybe the sync
            request.onerror = function(e) {
				cardbookIDBPrefDispName.onerror(e);
                reject();
            };
        });
        return openPrefDispNameDB;
	},

	// check if the prefer display name is in a wrong encryption state
	// then decrypt the prefer display name if possible
	checkPrefDispName: async function(aRepo, aPrefDispName) {
		try {
			var stateMismatched = cardbookIDBEncryptor.encryptionEnabled != 'encrypted' in aPrefDispName;
			var versionMismatched = aPrefDispName.encryptionVersion && aPrefDispName.encryptionVersion != cardbookIDBEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ('encrypted' in aPrefDispName) {
					aPrefDispName = await cardbookIDBEncryptor.decryptPrefDispName(aPrefDispName);
				}
				cardbookIDBPrefDispName.addPrefDispName(aRepo, aPrefDispName);
			} else {
				if ('encrypted' in aPrefDispName) {
					aPrefDispName = await cardbookIDBEncryptor.decryptPrefDispName(aPrefDispName);
				}
			}
			return aPrefDispName;
		}
		catch(e) {
			cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Decryption failed e : " + e, "Error");
			throw new Error("failed to decrypt the prefer display name : " + e);
		}
	},

	clearData: async function(aData) {
		let removeData = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBPrefDispName.db.transaction(["prefDispNames"], "readwrite");
			var store = transaction.objectStore("prefDispNames");
			var cursorRequest = store.clear();
		
			cursorRequest.onsuccess = async function(e) {
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				console.log(e)
				reject();
			};
		});
		await removeData;
	},

	loadData: async function(aData) {
		let loadData = new Promise( async function(resolve, reject) {
			let total = aData.length;
            if (total == 0) {
                resolve("OK");
                return
            }
			let current = 0;
			for (let data of aData) {
				var storedData = data;
				var transaction = cardbookIDBPrefDispName.db.transaction(["prefDispNames"], "readwrite");
				var store = transaction.objectStore("prefDispNames");
				var cursorRequest = store.put(storedData);
				cursorRequest.onsuccess = async function(e) {
					current++;
					if (current == total) {
						resolve("OK");
					}
				};
				cursorRequest.onerror = async function(e) {
					console.log(e);
					reject("KO");
				};
			}
        });
        return loadData;
	},

	// add or override the prefer display name to the cache
	addPrefDispName: async function(aRepo, aPrefDispName, aMode) {
		try {
			aPrefDispName.email = aPrefDispName.email.toLowerCase();
			if (!aPrefDispName.prefDispNameId) {
				aPrefDispName.prefDispNameId = cardbookIDBPrefDispName.getPrefDispNameId(aRepo);
			}
			var storedPrefDispName = cardbookIDBEncryptor.encryptionEnabled ? (await cardbookIDBEncryptor.encryptPrefDispName(aPrefDispName)) : aPrefDispName;
			var transaction = cardbookIDBPrefDispName.db.transaction(["prefDispNames"], "readwrite");
			var store = transaction.objectStore("prefDispNames");
			var cursorRequest = store.put(storedPrefDispName);

			cursorRequest.onsuccess = async function(e) {
				cardbookIDBPrefDispName.addPrefDispNameToIndex(aRepo, aPrefDispName);
				if (cardbookIDBEncryptor.encryptionEnabled) {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : Prefer display name " + aPrefDispName + " written to encrypted PrefDispNameDB");
				} else {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : Prefer display name " + aPrefDispName + " written to PrefDispNameDB");
				}
				if (aMode) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: aMode});
				}
			};

			cursorRequest.onerror = cardbookIDBPrefDispName.onerror;
		} catch(e) {
			cardbookIDBPrefDispName.onerror(e);
		}
	},

	// delete the prefer display name
	removePrefDispName: function(aRepo, aEmail) {
		aEmail = aEmail.toLowerCase();
		if (aRepo.cardbookPreferDisplayNameIndex[aEmail]) {
			let prefDispNameId = aRepo.cardbookPreferDisplayNameIndex[aEmail].prefDispNameId;
			cardbookIDBPrefDispName.removePrefDispNameWithId(aRepo, prefDispNameId, aEmail);
		} else {
			return;
		}
	},

	removePrefDispNameWithId: function(aRepo, aPrefDispNameId, aEmail) {
		var transaction = cardbookIDBPrefDispName.db.transaction(["prefDispNames"], "readwrite");
		var store = transaction.objectStore("prefDispNames");
		var cursorDelete = store.delete(aPrefDispNameId);
		
		cursorDelete.onsuccess = async function(e) {
			if (aRepo.cardbookPreferDisplayNameIndex[aEmail]) {
				delete aRepo.cardbookPreferDisplayNameIndex[aEmail];
			}
			if (cardbookIDBEncryptor.encryptionEnabled) {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : Prefer display name " + aEmail + " deleted from encrypted PrefDispNameDB");
			} else {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : Prefer display name " + aEmail + " deleted from PrefDispNameDB");
			}
		};
		cursorDelete.onerror = cardbookIDBPrefDispName.onerror;
	},

	// once the DB is open, this is the second step 
	loadPrefDispName: function(aRepo) {
		let loadPrefDispName = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBPrefDispName.db.transaction(["prefDispNames"], "readonly");
			var store = transaction.objectStore("prefDispNames");
			var cursorRequest = store.getAll();
			const handlePrefDispName = async prefDispName => {
				try {
					prefDispName = await cardbookIDBPrefDispName.checkPrefDispName(aRepo, prefDispName);
				} catch(e) {}
				if (prefDispName) {
					if (aRepo.cardbookPreferDisplayNameIndex[prefDispName.email]) {
						cardbookIDBPrefDispName.removePrefDispNameWithId(aRepo, prefDispName.prefDispNameId, prefDispName.email);
					} else {
						cardbookIDBPrefDispName.addPrefDispNameToIndex(aRepo, prefDispName);
					}
				}
			};
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var prefDispName of result) {
						handlePrefDispName(prefDispName);
					}
				}
				resolve();
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBPrefDispName.onerror(e);
				reject();
			}
		});
		return loadPrefDispName;
	},

	getPrefDispNameId: function(aRepo) {
		aRepo.cardbookPreferDisplayNameLastIndex++;
		return aRepo.cardbookPreferDisplayNameLastIndex;
	},
	
	addPrefDispNameToIndex: function(aRepo, aPrefDispName) {
		aRepo.cardbookPreferDisplayNameIndex[aPrefDispName.email] = {prefDispNameId: aPrefDispName.prefDispNameId};
		if (aPrefDispName.prefDispNameId > aRepo.cardbookPreferDisplayNameLastIndex) {
			aRepo.cardbookPreferDisplayNameLastIndex = aPrefDispName.prefDispNameId;
		}
	},
	
	encryptPrefDispNames: async function(aCallBack, aRepo) {
		var prefDispNamesTransaction = cardbookIDBPrefDispName.db.transaction(["prefDispNames"], "readonly");
		return aCallBack(
			cardbookIDBPrefDispName.db,
			prefDispNamesTransaction.objectStore("prefDispNames"),
			async prefDispName => {
				try {
					cardbookIDBPrefDispName.addPrefDispName(aRepo, prefDispName, "encryption");
				}
				catch(e) {
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Encryption failed e : " + e, "Error");
				}
			},
			prefDispName => !("encrypted" in prefDispName),
			"encryption"
		);
	},

	decryptPrefDispNames: async function(aCallBack, aRepo) {
		var prefDispNamesTransaction = cardbookIDBPrefDispName.db.transaction(["prefDispNames"], "readonly");
		return aCallBack(
			cardbookIDBPrefDispName.db,
			prefDispNamesTransaction.objectStore("prefDispNames"),
			async prefDispName => {
				try {
					prefDispName = await cardbookIDBEncryptor.decryptPrefDispName(prefDispName);
					cardbookIDBPrefDispName.addPrefDispName(aRepo, prefDispName, "decryption");
				}
				catch(e) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "decryption"});
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Decryption failed e : " + e, "Error");
				}
			},
			prefDispName => ("encrypted" in prefDispName),
			"decryption"
		);
	},

	upgradePrefDispNames: async function(aCallBack, aRepo) {
		var prefDispNamesTransaction = cardbookIDBPrefDispName.db.transaction(["prefDispNames"], "readonly");
		return aCallBack(
			cardbookIDBPrefDispName.db,
			prefDispNamesTransaction.objectStore("prefDispNames"),
			async prefDispName => {
				try {
					prefDispName = await cardbookIDBEncryptor.decryptPrefDispName(prefDispName);
					cardbookIDBPrefDispName.addPrefDispName(aRepo, prefDispName, "encryption");
				}
				catch(e) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "encryption"});
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Upgrade failed e : " + e, "Error");
				}
			},
			prefDispName => ("encrypted" in prefDispName && prefDispName.encryptionVersion != cardbookIDBEncryptor.VERSION),
			"encryption"
		);
	}
};
