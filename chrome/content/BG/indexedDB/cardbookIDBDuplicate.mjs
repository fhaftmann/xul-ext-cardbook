import { cardbookBGLog } from "../utils/cardbookBGLog.mjs";

export var cardbookIDBDuplicate = {
	cardbookDuplicateDatabaseVersion: "9",
	cardbookDuplicateDatabaseName: "cardbookDuplicate",
    db: {},

    // generic output when errors on DB
    onerror: function(e) {
		console.log(e);
    },

    // first step for getting the mail popularities
	openDuplicateDB: async function() {
        let openDuplicateDB = new Promise( function(resolve, reject) {
            var request = indexedDB.open(cardbookIDBDuplicate.cardbookDuplicateDatabaseName, cardbookIDBDuplicate.cardbookDuplicateDatabaseVersion);
            request.onupgradeneeded = function(e) {
                var db = e.target.result;
                e.target.transaction.onerror = cardbookIDBDuplicate.onerror;
                if (e.oldVersion < 9) {
                    if (db.objectStoreNames.contains("duplicates")) {
                        db.deleteObjectStore("duplicates");
                    }
                    let store = db.createObjectStore("duplicates", {keyPath: "duplicateId", autoIncrement: false});
                }
            };
            // when success, call the observer for starting the load cache and maybe the sync
            request.onsuccess = async function(e) {
				cardbookIDBDuplicate.db = e.target.result;
				resolve();
            };
            // when error, call the observer for starting the load cache and maybe the sync
            request.onerror = function(e) {
				cardbookIDBDuplicate.onerror(e);
                reject();
            };
        });
        return openDuplicateDB;
	},

	clearData: async function(aData) {
		let removeData = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBDuplicate.db.transaction(["duplicates"], "readwrite");
			var store = transaction.objectStore("duplicates");
			var cursorRequest = store.clear();
		
			cursorRequest.onsuccess = async function(e) {
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				console.log(e)
				reject();
			};
		});
		await removeData;
	},

	loadData: async function(aData) {
		let loadData = new Promise( async function(resolve, reject) {
			let total = aData.length;
            if (total == 0) {
                resolve("OK");
                return
            }
			let current = 0;
			for (let data of aData) {
				var storedData = data;
				var transaction = cardbookIDBDuplicate.db.transaction(["duplicates"], "readwrite");
				var store = transaction.objectStore("duplicates");
				var cursorRequest = store.put(storedData);
				cursorRequest.onsuccess = async function(e) {
					current++;
					if (current == total) {
						resolve("OK");
					}
				};
				cursorRequest.onerror = async function(e) {
					console.log(e);
					reject("KO");
				};
			}
        });
        return loadData;
	},

    // add or override the duplicate to the cache
	addDuplicate: async function(aLog, aId1, aId2) {
        let load = new Promise( function(resolve, reject) {
            let duplicate = {"duplicateId": aId1+"::"+aId2, "cbid1": aId1, "cbid2": aId2};
            var transaction = cardbookIDBDuplicate.db.transaction(["duplicates"], "readwrite");
            var store = transaction.objectStore("duplicates");
            var cursorRequest = store.put(duplicate);

            cursorRequest.onsuccess = function(e) {
                cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, "debug mode : Duplicate " + duplicate.cbid1 + "::" + duplicate.cbid2 + " written to duplicateDB");
                resolve();
            };

			cursorRequest.onerror = function(e) {
				cardbookIDBDuplicate.onerror(e);
				reject();
			};
        });
        let duplicate = await load;
        return duplicate;
    },

	// delete the duplicate
	removeDuplicate: async function(aLog, aId1) {
        let deleteDup = new Promise( function(resolve, reject) {
            var transaction = cardbookIDBDuplicate.db.transaction(["duplicates"], "readwrite");
            var store = transaction.objectStore("duplicates");
            var cursorRequest = store.delete(aId1);
        
            cursorRequest.onsuccess = async function(e) {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, "debug mode : Duplicate " + aId1 + " deleted from duplicateDB");
                resolve();
            };
			cursorRequest.onerror = function(e) {
				cardbookIDBDuplicate.onerror(e);
				reject();
			};
        });
        await deleteDup;
	},

	loadDuplicate: async function() {
        let load = new Promise( function(resolve, reject) {
            var transaction = cardbookIDBDuplicate.db.transaction(["duplicates"], "readonly");
            var store = transaction.objectStore("duplicates");
            var cursorRequest = store.getAll();
        
            cursorRequest.onsuccess = async function(e) {
                let results = {};
                var result = e.target.result;
                if (result) {
                    for (let duplicate of result) {
                        let cbid1 = duplicate.cbid1;
                        let cbid2 = duplicate.cbid2;
                        if (cbid1 > cbid2) {
                            if (!results[cbid1]) {
                                results[cbid1] = [];
                            }
                            results[cbid1].push(cbid2);
                        } else {
                            if (!results[cbid2]) {
                                results[cbid2] = [];
                            }
                            results[cbid2].push(cbid1);
                        }
                    }
                }
                resolve(results);
            };
			cursorRequest.onerror = function(e) {
				cardbookIDBDuplicate.onerror(e);
				reject();
			};
        });
        let duplicate = await load;
        return duplicate;
	}
};
