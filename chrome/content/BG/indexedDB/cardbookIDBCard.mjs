import { cardbookIDBEncryptor } from "./cardbookIDBEncryptor.mjs";
import { cardbookIDBSearch } from "./cardbookIDBSearch.mjs";

import { cardbookBGUtils } from "../utils/cardbookBGUtils.mjs";
import { cardbookBGLog } from "../utils/cardbookBGLog.mjs";
import { cardbookBGPreferences } from "../utils/cardbookBGPreferences.mjs";

import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookCardParser } from "../utils/cardbookCardParser.mjs";

export var cardbookIDBCard = {
	cardbookCardDatabaseVersion: "9",
	cardbookCardDatabaseName: "CardBookCard",
	db: {},

	// generic output when errors on DB
	onerror: function(e) {
		console.log(e);
	},

	// first step in the initial load data
	openCardDB: function() {
        let openCardDB = new Promise( async function(resolve, reject) {
			// var request = indexedDB.deleteDatabase(cardbookIDBCard.cardbookCardDatabaseName);
			var request = indexedDB.open(cardbookIDBCard.cardbookCardDatabaseName, cardbookIDBCard.cardbookCardDatabaseVersion);
			// when version changes
			// for the moment delete all and recreate one new empty
			request.onupgradeneeded = function(e) {
				var db = e.target.result;
				e.target.transaction.onerror = cardbookIDBCard.onerror;

				if (e.oldVersion <= 1) {
					let store = db.createObjectStore("cards", {keyPath: "cbid", autoIncrement: false});
					store.createIndex("cacheuriIndex", "cacheuri", { unique: false });
				}
				if (e.oldVersion < 7) {
					let store = request.transaction.objectStore("cards");
					store.createIndex("dirPrefIdIndex", "dirPrefId", { unique: false });
				}
				if (e.oldVersion < 8) {
					let store = request.transaction.objectStore("cards");
					store.createIndex("catsIndex", "categories", { unique: false, multiEntry: true});
					store.deleteIndex("cacheuriIndex");
					let viewStore = db.createObjectStore("views", {keyPath: "id", autoIncrement: true});
					viewStore.createIndex("displayIdIndex", "displayId", { unique: false});
					viewStore.createIndex("displayIdCbidIndex", [ "displayId", "cbid" ], { unique: true});
				}
				if (e.oldVersion < 9) {
					let store = request.transaction.objectStore("cards");
					store.deleteIndex("catsIndex");
				}
			};
			// when success, call the observer for starting the load cache and maybe the sync
			request.onsuccess = function(e) {
				cardbookIDBCard.db = e.target.result;
				resolve();
			};
			// when error, call the observer for starting the load cache and maybe the sync
			request.onerror = function(e) {
				cardbookIDBCard.onerror(e);
				reject();
			};
		});
		return openCardDB;
	},

	// check if the card is in a wrong encryption state
	// then decrypt the card if possible
	checkCard: async function(aLog, aDirPrefName, aCard) {
		try {
			var stateMismatched = cardbookIDBEncryptor.encryptionEnabled != "encrypted" in aCard;
			var versionMismatched = aCard.encryptionVersion && aCard.encryptionVersion != cardbookIDBEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ("encrypted" in aCard) {
					aCard = await cardbookIDBEncryptor.decryptCard(aCard);
				}
				await cardbookIDBCard.addCardToCards(aLog, aDirPrefName, aCard);
			} else {
				if ("encrypted" in aCard) {
					aCard = await cardbookIDBEncryptor.decryptCard(aCard);
				}
			}
			return aCard;
		}
		catch(e) {
			cardbookBGLog.updateStatusProgressInformation(aLog, "debug mode : Decryption failed e : " + e, "Error");
			throw new Error("failed to decrypt the card : " + e);
		}
	},

	clearData: async function() {
		let removeData = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBCard.db.transaction(["cards"], "readwrite");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.clear();
		
			cursorRequest.onsuccess = async function(e) {
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				console.log(e)
				reject();
			};
		});
		await removeData;
	},

	loadData: async function(aData) {
		let loadData = new Promise( async function(resolve, reject) {
 			let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
			let current = 0;
			for (let data of aData) {
				var storedData = cardbookIDBEncryptor.encryptionEnabled ? (await cardbookIDBEncryptor.encryptCard(data)) : data;
				var transaction = cardbookIDBCard.db.transaction(["cards"], "readwrite");
				var store = transaction.objectStore("cards");
				var cursorRequest = store.put(storedData);
				cursorRequest.onsuccess = async function(e) {
					current++;
				};
				cursorRequest.onerror = async function(e) {
					console.log(e);
				};
			}

			let allDirPrefIds = cardbookBGPreferences.getAllPrefIds();
			for (let dirPrefId of allDirPrefIds) {
				if (cardbookBGPreferences.getType(dirPrefId) == "SEARCH") {
					await cardbookIDBSearch.openSearchDB();
					let search = await cardbookIDBSearch.getSearch(dirPrefId);
					if (search) {
						for (let card of aData) {
							if (cardbookBGUtils.isMyCardFoundInRules(card, search.rules, search.matchAll)) {
								await cardbookIDBCard.addCardToViews(dirPrefId, card);
								if (card.categories.length != 0) {
									for (let category of card.categories) {
										await cardbookIDBCard.addCardToViews(dirPrefId+"::categories::"+category, card);
									}
								} else {
									await cardbookIDBCard.addCardToViews(dirPrefId+"::categories::"+uncat, card);
								}
							}
						}
					}
				} else if (cardbookBGPreferences.getNode(dirPrefId) == "org") {
					for (let card of aData) {
						let parent = dirPrefId + "::org";
						if (card.org) {
							let orgArray = cardbookHTMLUtils.unescapeArray(cardbookHTMLUtils.escapeString(card.org).split(";"));
							for (let org of orgArray) {
								let id = parent + "::" + org;
								await cardbookIDBCard.addCardToViews(id, card);
								parent = id;
							}
						} else {
							let id = parent + "::" + uncat;
							await cardbookIDBCard.addCardToViews(id, card);
						}
					}
				}
			}
			resolve("OK")
        });
        return loadData;
	},

	cardsMigrated2: async function() {
		let cardsMigrated2 = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.getAll();
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
					for (let card of result) {
						await cardbookIDBCard.removeCardFromViews(card.dirPrefId, card);
						await cardbookIDBCard.addCardToViews(card.dirPrefId, card);
						if (card.categories.length != 0) {
							for (let category of card.categories) {
								await cardbookIDBCard.addCardToViews(card.dirPrefId+"::categories::"+category, card);
							}
						} else {
							await cardbookIDBCard.addCardToViews(card.dirPrefId+"::categories::"+uncat, card);
						}
					}
				}
				resolve("OK");
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
				resolve("OK");
			}
		});
		return cardsMigrated2;
	},

	cardsMigrated3Function: async function(aRepo, aCard) {
		if (!Array.isArray(aCard.org)) {
			let newCard = new cardbookCardParser(); 
			await cardbookBGUtils.cloneCard(aCard, newCard);
			let newOrg = cardbookHTMLUtils.unescapeArray(cardbookHTMLUtils.escapeString(aCard.org).split(";"));
			newCard.org = JSON.parse(JSON.stringify(newOrg));
			await cardbookIDBCard.addCardToCards(aRepo.statusInformation, cardbookBGPreferences.getName(aCard.dirPrefId), newCard);
		}
	},

	cardsMigrated3: async function(aRepo) {
		let cardsMigrated3 = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.getAll();
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (let card of result) {
						cardbookIDBCard.cardsMigrated3Function(aRepo, card);
					}
				}
				resolve("OK");
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
				resolve("OK");
			}
		});
		return cardsMigrated3;
	},

	// add or override the contact to the cache
	addCardToCards: async function(aLog, aDirPrefName, aCard, aMode) {
        let addCardToCards = new Promise( async function(resolve, reject) {
			var storedCard = cardbookIDBEncryptor.encryptionEnabled ? (await cardbookIDBEncryptor.encryptCard(aCard)) : aCard;
			var transaction = cardbookIDBCard.db.transaction(["cards"], "readwrite");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.put(storedCard);
			cursorRequest.onsuccess = async function(e) {
				if (cardbookIDBEncryptor.encryptionEnabled) {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Contact " + aCard.fn + " written to encrypted card DB");
				} else {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Contact " + aCard.fn + " written to card DB");
				}
				if (aMode) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: aMode});
				}
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				if (aMode) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: aMode});
				}
				cardbookIDBCard.onerror(e);
				reject();
			};
        });
        await addCardToCards;
	},

	// add or override the contact to the cache
	addCardToViews: async function(aDisplayId, aCard) {
        let addCardToViews = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBCard.db.transaction(["views"], "readwrite");
			var store = transaction.objectStore("views");
			var storedView = { displayId: aDisplayId, cbid: aCard.cbid };
			var cursorRequest = store.put(storedView);
			cursorRequest.onsuccess = async function(e) {
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				//cardbookIDBCard.onerror(e);
				resolve();
			};
        });
        await addCardToViews;
	},

	removeCardsFromAccount: async function(aLog, aDirPrefId, aDirPrefName) {
        let removeCardsFromAccount = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBCard.db.transaction(["cards"], "readwrite");
			var store = transaction.objectStore("cards");
			var keyRange = IDBKeyRange.bound(aDirPrefId, aDirPrefId + "\uffff");
			var cursorRequest = store.delete(keyRange);
		
			cursorRequest.onsuccess = async function(e) {
				if (cardbookIDBEncryptor.encryptionEnabled) {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : deleted from encrypted cards DB");
				} else {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : deleted from cards DB");
				}
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				cardbookIDBCard.onerror(e);
				resolve();
			};
        });
        await removeCardsFromAccount;
	},

	removeViewsFromAccount: async function(aDirPrefId) {
        let removeViewsFromAccount = new Promise( async function(resolve, reject) {
			var keyRange = IDBKeyRange.bound(aDirPrefId, aDirPrefId + "\uffff");
			var transaction = cardbookIDBCard.db.transaction(["views"], "readwrite");
			var store = transaction.objectStore("views");
			var cursorRequest = store.index("displayIdIndex").openCursor(keyRange);

			cursorRequest.onsuccess = function(e) {
				var result = e.target.result;
				if (result) {
					store.delete(result.primaryKey);
					result.continue();
				}
				resolve();
			};

			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
			}
			transaction.oncomplete = function () {
				resolve();
			};
        });
        await removeViewsFromAccount;
	},

	// delete the contact
	removeCardFromCards: function(aLog, aDirPrefName, aCard) {
		var transaction = cardbookIDBCard.db.transaction(["cards"], "readwrite");
		var store = transaction.objectStore("cards");
		var cursorRequest = store.delete(aCard.cbid);
		cursorRequest.onsuccess = function(e) {
			if (cardbookIDBEncryptor.encryptionEnabled) {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Contact " + aCard.fn + " deleted from encrypted cards DB");
			} else {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Contact " + aCard.fn + " deleted from cards DB");
			}
		};
		
		cursorRequest.onerror = cardbookIDBCard.onerror;
	},

	// delete the contact
	removeCardFromViews: async function(aDisplayId, aCard) {
		let removeCardFromViews = new Promise( function(resolve, reject) {
			var transaction = cardbookIDBCard.db.transaction(["views"], "readwrite");
			var store = transaction.objectStore("views");
			var keyRange = IDBKeyRange.only([ aDisplayId, aCard.cbid ]);
			var cursorRequest = store.index("displayIdCbidIndex").openCursor(keyRange);
			cursorRequest.onsuccess = function(e) {
				var result = e.target.result;
				if (result) {
					store.delete(result.primaryKey);
					result.continue();
				}
				resolve();
			};
			
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
				reject();
			};
		});
		await removeCardFromViews;
	},
	
	removeDisplayFromViews: async function(aDisplayId) {
		let removeDisplayFromViews = new Promise( function(resolve, reject) {
			var transaction = cardbookIDBCard.db.transaction(["views"], "readwrite");
			var store = transaction.objectStore("views");
			var keyRange = IDBKeyRange.only(aDisplayId);
			var cursorRequest = store.index("displayIdIndex").openCursor(keyRange);
			cursorRequest.onsuccess = function(e) {
				var result = e.target.result;
				if (result) {
					store.delete(result.primaryKey);
					result.continue();
				}
				resolve();
			};
			
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
				reject();
			};
		});
		await removeDisplayFromViews;
	},

	// Check if a card is present in the database
	getCard: function(aLog, aCardId) {
		return new Promise( function(resolve, reject) {
			var transaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.get(aCardId);
		
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				let card = {};
				if (result) {
					card = await cardbookIDBCard.checkCard(aLog, "", result);
				}
				resolve(card);
			};
			
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
				reject();
			};
		});
	},
	
	// Check if a card is present in the database
	checkCardForUndoAction: function(aRepo, aMessage, aCard, aActionId) {
		return new Promise( function(resolve, reject) {
			var transaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.get(aCard.cbid);
		
			cursorRequest.onsuccess = async function(e) {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aMessage);
				cardbookBGUtils.addTagCreated(aCard);
				var card = e.target.result;
				if (card) {
					aCard.etag = card.etag;
				}
				await aRepo.saveCardFromMove({}, aCard, aActionId, false);
				let action = aRepo.currentAction[aActionId].actionCode;
				await messenger.runtime.sendMessage({query: "cardbook.notifyObserver", value: `cardbook.${action}`});
				resolve();
			};
			
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
				reject();
			};
		});
	},
	
	// once the DB is open, this is the second step for the AB
	// which use the DB caching
	loadCards: function(aRepo, aDirPrefId, aDirPrefName) {
        let loadCards = new Promise( async function(resolve, reject) {
			var keyRange = IDBKeyRange.bound(aDirPrefId, aDirPrefId + "\uffff");
			var transaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.index("dirPrefIdIndex").getAll(keyRange);
			const handleCard = async card => {
				try {
					card = await cardbookIDBCard.checkCard(aRepo.statusInformation, aDirPrefName, card);
				}
				catch(e) {
					return;
				}
				if (!card.deleted) {
					await aRepo.addCardToRepository(card, false, card.cacheuri);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardLoadedFromCacheDB", [aDirPrefName, card.fn]);
				} else {
					if (aRepo.cardbookFileCacheCards[aDirPrefId]) {
						aRepo.cardbookFileCacheCards[aDirPrefId][card.cacheuri] = card;
					} else {
						aRepo.cardbookFileCacheCards[aDirPrefId] = {};
						aRepo.cardbookFileCacheCards[aDirPrefId][card.cacheuri] = card;
					}
				}
			};
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var card of result) {
						await handleCard(card);
					}
				}
				resolve();
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
				reject();
			}
		});
		return loadCards;
	},

	getLists: async function(aDirPrefId) {
		let results = [];
        let getLists = new Promise( async function(resolve, reject) {
			var keyRange = IDBKeyRange.only(aDirPrefId);
			var transaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.index("dirPrefIdIndex").getAll(keyRange);
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (let card of result) {
						card = await cardbookIDBCard.checkCard({}, "", card);
						if (card.isAList) {
							results.push(card);
						}
					}
				}
				resolve(results);
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
				resolve(results);
			}
		});
		await getLists;
		return results;
	},

	verifySearch: function(aSearch, aCard) {
		if (!aSearch) {
			return true;
		} else if (aSearch instanceof Object) {
			if ((aSearch.searchAB == aCard.dirPrefId) || (aSearch.searchAB === "allAddressBooks")) {
				return cardbookBGUtils.isMyCardFoundInRules(aCard, aSearch.rules, aSearch.matchAll);
			} else {
				return false;
			}
		} else if (typeof aSearch === "string" || aSearch instanceof String) {
			return cardbookBGUtils.getLongSearchString(aCard).indexOf(aSearch) != -1;
		} else {
			return true;
		}
	},

	getCards: async function(aDirPrefId, aDisplayId, aSearch, aSearchAll, aSearchId) {
		let tmpArray = aDisplayId.split("::");
		let category = "";
		let orgView = false;
		if (tmpArray[1] && tmpArray[1] == "categories") {
			category = tmpArray[tmpArray.length-1];
		} else if (tmpArray[1] && tmpArray[1] == "org") {
			orgView = true;
		}

		let search = aSearch;
		if (aSearch instanceof String) {
			search = aSearch.toLowerCase();
		}
		let windowSearch = false;
		if (aSearch instanceof Object) {
			windowSearch = true;
		}
		let results = [];
		if (!aDirPrefId && !aDisplayId) {
			return results;
		} else if (windowSearch) {
			results = await cardbookIDBCard.getAllCardsFromCards(search);
		} else if (aSearchAll == "allAB") {
			results = await cardbookIDBCard.getAllCardsFromCards(search);
		} else if (cardbookBGPreferences.getType(aDirPrefId) == "SEARCH") {
			await cardbookIDBSearch.openSearchDB();
			let searchRules = await cardbookIDBSearch.getSearch(aDirPrefId);
			if (aDisplayId && aDirPrefId != aDisplayId) {
				results = await cardbookIDBCard.getCardsFromViews(aDirPrefId, aDisplayId, search);
			} else if (searchRules.rules.length == 1 && searchRules.rules[0].field == "version" && searchRules.rules[0].term == "IsntEmpty") {
				results = await cardbookIDBCard.getAllCardsFromCards(search);
			} else {
				results = await cardbookIDBCard.getCardsFromViews(aDirPrefId, aDisplayId, search);
			}
		} else if (orgView) {
			results = await cardbookIDBCard.getCardsFromViews(aDirPrefId, aDisplayId, search);
		} else {
			if (category) {
				results = await cardbookIDBCard.getCardsFromViews(aDirPrefId, aDisplayId, search);
			} else {
				results = await cardbookIDBCard.getCardsFromCards(aDirPrefId, search);
			}
		}
		return [results, aSearchId];
	},

	getCardsFromCards: async function(aDirPrefId, aSearchString) {
		let results = [];
        let getCardsFromCards = new Promise( async function(resolve, reject) {
			var keyRange = IDBKeyRange.only(aDirPrefId);
			var transaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.index("dirPrefIdIndex").getAll(keyRange);
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (let card of result) {
						card = await cardbookIDBCard.checkCard({}, "", card);
						if (aDirPrefId == card.dirPrefId && !card.deleted) {
							let insert = cardbookIDBCard.verifySearch(aSearchString, card);
							if (insert) {
								results.push(card);
							}
						}
					}
				}
				resolve(results);
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
				resolve(results);
			}
		});
		await getCardsFromCards;
		return results;
	},

	getAllCardsFromCards: async function(aSearch) {
		let results = [];
		let allDirPrefIds = cardbookBGPreferences.getAllPrefIds();
		for (let dirPrefId of allDirPrefIds) {
			if (cardbookBGPreferences.getEnabled(dirPrefId) && cardbookBGPreferences.getType(dirPrefId) != "SEARCH") {
				let getCards = new Promise( async function(resolve, reject) {
					var keyRange = IDBKeyRange.only(dirPrefId);
					var transaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
					var store = transaction.objectStore("cards");
					var cursorRequest = store.index("dirPrefIdIndex").getAll(keyRange);
					cursorRequest.onsuccess = async function(e) {
						var result = e.target.result;
						if (result) {
							for (let card of result) {
								card = await cardbookIDBCard.checkCard({}, "", card);
								if (!card.deleted) {
									let insert = cardbookIDBCard.verifySearch(aSearch, card);
									if (insert) {
										results.push(card);
									}
								}
							}
						}
						resolve(results);
					};
					cursorRequest.onerror = (e) => {
						cardbookIDBCard.onerror(e);
						resolve(results);
					}
				});
				await getCards;
			}
		}
		return results;
	},

	getCardsFromViews: async function(aDirPrefId, aDisplayId, aSearchString) {
		let cbidResults = [];
		let results = [];
		if (!cardbookBGPreferences.getEnabled(aDirPrefId)) {
			return results;
		} else {
			let dirPrefId = cardbookBGUtils.getAccountId(aDisplayId);
			if (!cardbookBGPreferences.getEnabled(dirPrefId)) {
				return results;
			}
		}

        let getCardsFromViews = new Promise( async function(resolve, reject) {
			var keyRange = aDisplayId ? IDBKeyRange.only(aDisplayId) : IDBKeyRange.only(aDirPrefId);
			var transaction = cardbookIDBCard.db.transaction(["views"], "readonly");
			var store = transaction.objectStore("views");
			var cursorRequest = store.index("displayIdIndex").openCursor(keyRange);
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					cbidResults.push(result.value.cbid);
					result.continue();
				}
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
			}
			transaction.oncomplete = function () {
				resolve();
			};
		});
		await getCardsFromViews;

		for (let cbid of cbidResults) {
			let getCards = new Promise( async function(resolve, reject) {
				var keyRange = IDBKeyRange.only(cbid);
				var transaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
				var store = transaction.objectStore("cards");
				var cursorRequest = store.get(keyRange);
				cursorRequest.onsuccess = async function(e) {
					var result = e.target.result;
					if (result) {
						let card = await cardbookIDBCard.checkCard({}, "", result);
						if (!card.deleted) {
							if (cardbookBGPreferences.getEnabled(card.dirPrefId)) {
								let insert = cardbookIDBCard.verifySearch(aSearchString, card);
								if (insert) {
									results.push(card);
								}
							}
						}
					}
					resolve();
				};
				cursorRequest.onerror = (e) => {
					cardbookIDBCard.onerror(e);
					resolve();
				}
			});
			await getCards;
		}
		return results;
	},

	getNodesFromViews: async function(aDirPrefId) {
		let results = {};
        let getNodesFromViews = new Promise( async function(resolve, reject) {
			var keyRange = aDirPrefId ? IDBKeyRange.bound(aDirPrefId, aDirPrefId + "\uffff") : IDBKeyRange.lowerBound("0");
			var transaction = cardbookIDBCard.db.transaction(["views"], "readonly");
			var store = transaction.objectStore("views");
			var cursorRequest = store.index("displayIdIndex").openCursor(keyRange, "nextunique");
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					if (result.key.includes("::org::")) {
						let tmpArray = result.key.split("::");
						let dirPrefId = tmpArray[0];
						if (cardbookBGPreferences.getEnabled(dirPrefId)) {
							let name = tmpArray[tmpArray.length-1];
							let id = result.key;
							if (!results[dirPrefId]) {
								results[dirPrefId] = [];
							}
							results[dirPrefId].push({name, id});
						}
					}
					result.continue();
				}
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
			}
			transaction.oncomplete = function () {
				resolve();
			};
		});
		await getNodesFromViews;
		return results;
	},

	getUncatFromViews: async function(aDirPrefId) {
		let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
		let results = {};
        let getNodesFromViews = new Promise( async function(resolve, reject) {
			var keyRange = aDirPrefId ? IDBKeyRange.bound(aDirPrefId, aDirPrefId + "\uffff") : IDBKeyRange.lowerBound("0");
			var transaction = cardbookIDBCard.db.transaction(["views"], "readonly");
			var store = transaction.objectStore("views");
			var cursorRequest = store.index("displayIdIndex").openCursor(keyRange, "nextunique");
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					if (result.key.endsWith(`::categories::${uncat}`)) {
						let tmpArray = result.key.split("::");
						let dirPrefId = tmpArray[0];
						if (cardbookBGPreferences.getEnabled(dirPrefId)) {
							let id = result.key;
							if (!results[dirPrefId]) {
								results[dirPrefId] = [];
							}
							results[dirPrefId].push({name: uncat, id});
						}
					}
					result.continue();
				}
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBCard.onerror(e);
			}
			transaction.oncomplete = function () {
				resolve();
			};
		});
		await getNodesFromViews;
		return results;
	},

	encryptCards: async function(aCallBack, aRepo) {
		var cardsTransaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
		return aCallBack(
			cardbookIDBCard.db,
			cardsTransaction.objectStore("cards"),
			async card => {
				try {
					await cardbookIDBCard.addCardToCards(aRepo.statusInformation, cardbookBGPreferences.getName(card.dirPrefId), card, "encryption");
				}
				catch(e) {
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Encryption failed e : " + e, "Error");
				}
			},
			card => !("encrypted" in card),
			"encryption"
		);
	},

	decryptCards: async function(aCallBack, aRepo) {
		var cardsTransaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
		return aCallBack(
			cardbookIDBCard.db,
			cardsTransaction.objectStore("cards"),
			async card => {
				try {
					card = await cardbookIDBEncryptor.decryptCard(card);
					await cardbookIDBCard.addCardToCards(aRepo.statusInformation, cardbookBGPreferences.getName(card.dirPrefId), card, "decryption");
				}
				catch(e) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "decryption"});
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Decryption failed e : " + e, "Error");
				}
			},
			card => ("encrypted" in card),
			"decryption"
		);
	},

	upgradeCards: async function(aCallBack, aRepo) {
		var cardsTransaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
		return aCallBack(
			cardbookIDBCard.db,
			cardsTransaction.objectStore("cards"),
			async card => {
				try {
					card = await cardbookIDBEncryptor.decryptCard(card);
					await cardbookIDBCard.addCardToCards(aRepo.statusInformation, cardbookBGPreferences.getName(card.dirPrefId), card, "encryption");
				}
				catch(e) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "encryption"});
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Upgrade failed e : " + e, "Error");
				}
			},
			card => ("encrypted" in card && card.encryptionVersion != cardbookIDBEncryptor.VERSION),
			"encryption"
		);
	}
};
