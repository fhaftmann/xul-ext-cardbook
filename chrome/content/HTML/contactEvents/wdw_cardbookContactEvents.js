import { cardbookHTMLUtils } from "../utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookHTMLRichContext } from "../utils/scripts/cardbookHTMLRichContext.mjs";
import { cardbookHTMLTools } from "../utils/scripts/cardbookHTMLTools.mjs";

if ("undefined" == typeof(wdw_cardbookContactEvents)) {
	var wdw_cardbookContactEvents = {
		allEvents: [],
		emailArray: [],
		displayName: "",

		getIndexFromName: function (aName) {
			let tmpArray = aName.split("_");
			return tmpArray[tmpArray.length - 1];
		},

		getTableCurrentIndex: function (aTableName) {
			let selectedList = document.getElementById(aTableName).querySelectorAll("tr[rowselected='true']");
			if (selectedList.length) {
				return wdw_cardbookContactEvents.getIndexFromName(selectedList[0].id);
			}
		},

		clickTree: function (aEvent) {
            if (aEvent.target.tagName.toLowerCase() == "td") {
				let row = aEvent.target.closest("tr");
				let tbody = aEvent.target.closest("tbody");
				for (let child of tbody.childNodes) {
					child.removeAttribute("rowselected");
				}
				row.setAttribute("rowselected", "true");
				wdw_cardbookContactEvents.buttonShowing();
			}
		},

		clickToSort: async function (aEvent) {
            if (aEvent.target.tagName.toLowerCase() == "th" || aEvent.target.tagName.toLowerCase() == "img") {
				let column = aEvent.target.closest("th");
				let columnName = column.getAttribute("data-value");
				let table = column.closest("table");
				if (table.getAttribute("data-sort-column") == columnName) {
					if (table.getAttribute("data-sort-order") == "ascending") {
						table.setAttribute("data-sort-order", "descending");
					} else {
						table.setAttribute("data-sort-order", "ascending");
					}
				} else {
					table.setAttribute("data-sort-column", columnName);
					table.setAttribute("data-sort-order", "ascending");
				}
                await wdw_cardbookContactEvents.displayEvents();
			}
			aEvent.stopImmediatePropagation();
		},
	
		getTableMapColumn: function (aColumnName) {
			if (aColumnName == "eventsTableTitle") {
				return "title";
			} else if (aColumnName == "eventsTableStartdate") {
				return "startDate";
			} else if (aColumnName == "eventsTableEnddate") {
				return "endDate";
			} else if (aColumnName == "eventsTableCategories") {
				return "categories";
			} else if (aColumnName == "eventsTableLocation") {
				return "location";
			} else if (aColumnName == "eventsTableCalendarname") {
				return "calendarName";
			}
		},

		doubleClickTree: async function (aEvent) {
            if (aEvent.target.tagName.toLowerCase() == "th") {
				return;
            } else if (aEvent.target.tagName.toLowerCase() == "td") {
				await wdw_cardbookContactEvents.editEvent();
			} else {
				await wdw_cardbookContactEvents.createEvent();
			}
		},

		displayEvents: async function () {
			let table = document.getElementById("eventsTable");
			let order = table.getAttribute("data-sort-order") == "ascending" ? 1 : -1;
			let columnName = table.getAttribute("data-sort-column");
			let columnSort = wdw_cardbookContactEvents.getTableMapColumn(columnName);

			let headers = [ "eventsTableTitle", "eventsTableStartdate", "eventsTableEnddate", "eventsTableCategories", "eventsTableLocation", "eventsTableCalendarname" ];
			if (wdw_cardbookContactEvents.emailArray[0] != "") {
            	wdw_cardbookContactEvents.allEvents = await messenger.runtime.sendMessage({query: "cardbook.getEvents", emails: wdw_cardbookContactEvents.emailArray, column: columnSort, order: order});
			}
            let data = wdw_cardbookContactEvents.allEvents.map(x => [ x[0], x[1], x[2], x[3], x[4], x[5] ])
			let dataParameters = [];
			let rowParameters = {};
            let tableParameters = { "events": [ [ "click", wdw_cardbookContactEvents.clickTree ],
                                                [ "dblclick", wdw_cardbookContactEvents.doubleClickTree ],
                                                [ "keydown", wdw_cardbookContactEvents.chooseActionForKey ] ] };
            let sortFunction = wdw_cardbookContactEvents.clickToSort;
			cardbookHTMLTools.addTreeTable("eventsTable", headers, data, dataParameters, rowParameters, tableParameters, sortFunction);
			wdw_cardbookContactEvents.buttonShowing();
		},

		buttonShowing: function () {
			var btnEdit = document.getElementById("editEventLabel");
			let currentIndex = wdw_cardbookContactEvents.getTableCurrentIndex("eventsTable");
			if (currentIndex) {
				btnEdit.disabled = false;
			} else {
				btnEdit.disabled = true;
			}
		},

		editEvent: async function() {
			let currentIndex = wdw_cardbookContactEvents.getTableCurrentIndex("eventsTable");
			if (currentIndex) {
				let eventId = wdw_cardbookContactEvents.allEvents[currentIndex][6];
				let calendarId = wdw_cardbookContactEvents.allEvents[currentIndex][7];
                await messenger.runtime.sendMessage({query: "cardbook.editEvent", eventId: eventId, calendarId: calendarId});
			}
		},

		// code taken from createEventWithDialog
		createEvent: async function() {
			let contacts = [ wdw_cardbookContactEvents.emailArray, wdw_cardbookContactEvents.displayName ];
			await messenger.runtime.sendMessage({query: "cardbook.createEvent", contacts: contacts});
		},

		chooseActionForKey: function (aEvent) {
			if (aEvent.key == "Enter") {
				wdw_cardbookContactEvents.editEvent();
				aEvent.stopPropagation();
			}
		},
		
		load: async function () {
            let urlParams = new URLSearchParams(window.location.search);
            wdw_cardbookContactEvents.displayName = urlParams.get("displayName");
            wdw_cardbookContactEvents.emailArray = urlParams.get("listOfEmail").split(",");
        
			i18n.updateDocument();
			cardbookHTMLRichContext.loadRichContext();
			document.title = messenger.i18n.getMessage("eventContactsWindowLabel", [wdw_cardbookContactEvents.displayName]);

           	// button
            document.getElementById("createEventLabel").addEventListener("click", event => wdw_cardbookContactEvents.createEvent());
            document.getElementById("editEventLabel").addEventListener("click", event => wdw_cardbookContactEvents.editEvent());
            document.getElementById("closeEditionLabel").addEventListener("click", event => wdw_cardbookContactEvents.do_close());

            await wdw_cardbookContactEvents.displayEvents();
		},
	
		do_close: async function () {
			let win = await cardbookHTMLRichContext.getWindow();
			await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
			cardbookHTMLRichContext.closeWindow();
		}
	};
};

window.addEventListener("beforeunload", async function() {
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
});

messenger.runtime.onMessage.addListener( (info) => {
	switch (info.query) {
		case "cardbook.displayEvents":
			wdw_cardbookContactEvents.displayEvents();
			break;
		}
});

await wdw_cardbookContactEvents.load()