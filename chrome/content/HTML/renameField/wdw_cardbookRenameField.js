import { cardbookHTMLRichContext } from "../utils/scripts/cardbookHTMLRichContext.mjs";
import { cardbookHTMLNotification } from "../utils/scripts/cardbookHTMLNotification.mjs";
import { cardbookHTMLUtils } from "../utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookHTMLValidations } from "../utils/scripts/cardbookHTMLValidations.mjs";

import { cardbookIDBCard } from "../../BG/indexedDB/cardbookIDBCard.mjs";
import { cardbookIDBCat } from "../../BG/indexedDB/cardbookIDBCat.mjs";

import { cardbookBGUtils } from "../../BG/utils/cardbookBGUtils.mjs";
import { cardbookBGPreferences } from "../../BG/utils/cardbookBGPreferences.mjs";

var context = "";
var id = "";
var name = "";
var type = "";
var color = "";
var showColor = "false";
var mailpop = "";
var validationList = [];
var dirPrefId = "";

async function getNodes(aType, aDirPrefId, aId, aName) {
	let result = [];
	if (aType == "categories") {
		let nodes = await cardbookIDBCat.getCategories(aDirPrefId);
		if (nodes[aDirPrefId]) {
			result = nodes[aDirPrefId].map(child => child.name).filter(child => child != aName);
			result.push(cardbookBGPreferences.getPref("uncategorizedCards"));
		}
	} else {
		let nodes = await cardbookIDBCard.getNodesFromViews(aDirPrefId);
		if (nodes[aDirPrefId]) {
			let parentList = nodes[aDirPrefId].filter(child => cardbookBGUtils.getParentOrg(child.id) == cardbookBGUtils.getParentOrg(aId));
			result = parentList.map(child => child.name).filter(child => child != aName);
		}
	}
	return result;
};

function validate () {
	let btnSave = document.getElementById("validateButton");
	let notificationMessage = document.getElementById("notificationMessage");
    let value = document.getElementById("nameTextBox").value;
	if (!value) {
		btnSave.disabled = true;
		return false;
	} else {
		let myValidationList = validationList.filter(x => x.toUpperCase() == value.toUpperCase());
		if (myValidationList.length) {
            cardbookHTMLNotification.setNotification(notificationMessage, "warning", "valueAlreadyExists", [value]);
			btnSave.disabled = true;
			return false;
		}
		if (context == "CreateCat" || context == "EditCat") {
			if (!cardbookHTMLValidations.validateNodesNumber(notificationMessage)) {
				btnSave.disabled = true;
				return false;
			}
		}
		cardbookHTMLNotification.setNotification(notificationMessage, "OK");
		btnSave.disabled = false;
		return true;
	}
};

async function onLoadDialog () {
	let urlParams = new URLSearchParams(window.location.search);
	context = urlParams.get("context");
	id = urlParams.get("id");
	type = urlParams.get("type");
	name = urlParams.get("name");
	color = urlParams.get("color");
	showColor = urlParams.get("showColor");
	mailpop = urlParams.get("mailpop") || 0;
	dirPrefId = urlParams.get("dirPrefId");

    i18n.updateDocument();
    cardbookHTMLRichContext.loadRichContext();

	// input
	document.getElementById("mailPopTextBox").addEventListener("input", event => validate());
	document.getElementById("nameTextBox").addEventListener("input", event => validate());
	// button
	document.getElementById("cancelButton").addEventListener("click", onCancelDialog);
	document.getElementById("validateButton").addEventListener("click", onAcceptDialog);

	await cardbookIDBCard.openCardDB({});
	await cardbookIDBCat.openCatDB({});
	document.title = messenger.i18n.getMessage(`wdw_cardbookRenameField${context}Title`);
	if (context == "EditNode") {
        let orgStructure = cardbookBGPreferences.getPref("orgStructure");
        if (orgStructure.length) {
			let idArray = id.split("::");
			document.getElementById("nameLabel").textContent = messenger.i18n.getMessage(`wdw_cardbookRenameField${context}Label`, [orgStructure[idArray.length - 3]]);
		} else {
			document.getElementById("nameLabel").textContent = messenger.i18n.getMessage("orgNodeLabel");
		}
	} else {
		document.getElementById("nameLabel").textContent = messenger.i18n.getMessage(`wdw_cardbookRenameField${context}Label`);
	}

	if (showColor == "true") {
		document.getElementById("useColorCheck").checked = color ? true : false;
		document.getElementById("colorInput").value = color;
	} else {
		document.getElementById("useColorCheck").checked = false;
		document.getElementById("colorRow").classList.add("hidden");
	}
	if (context == "CreateCat" || context == "EditCat") {
        document.getElementById("mailPopTextBox").value = mailpop;
	} else {
		document.getElementById("mailPopRow").classList.add("hidden");
	}
	validationList = await getNodes(type, dirPrefId, id, name);
	document.getElementById("nameTextBox").value = name;
	document.getElementById("nameTextBox").focus();
};

async function onAcceptDialog (aEvent) {
	if (validate()) {
		let urlParams = {};
		urlParams.dirPrefId = dirPrefId;
		urlParams.type = type;
		urlParams.id = id;
		urlParams.oldName = name;
		urlParams.name = document.getElementById("nameTextBox").value.trim();
		if (!document.getElementById("colorRow").classList.contains("hidden")) {
			urlParams.oldColor = color;
			urlParams.color = document.getElementById("useColorCheck").checked ? document.getElementById("colorInput").value : "";
		}
		if (!document.getElementById("mailPopRow").classList.contains("hidden")) {
			urlParams.oldMailpop = mailpop;
			urlParams.mailpop = document.getElementById("mailPopTextBox").value.trim() || 0;
		}
		if (context.toLowerCase().startsWith("create")) {
			await messenger.runtime.sendMessage({query: "cardbook.createCategory", category: urlParams});
		} else if (context.toLowerCase().startsWith("edit")) {
			await messenger.runtime.sendMessage({query: "cardbook.modifyNode", node: urlParams});
		}
		onCancelDialog();
	}
	document.getElementById("validateButton").disabled = false;
};

async function onCancelDialog () {
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
	cardbookHTMLRichContext.closeWindow();
};

window.addEventListener("beforeunload", async function() {
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
});

await onLoadDialog();