import { cardbookHTMLDates } from "./cardbookHTMLDates.mjs";
import { cardbookHTMLUtils } from "./cardbookHTMLUtils.mjs";
import { cardbookHTMLTools } from "./cardbookHTMLTools.mjs";
import { cardbookHTMLTypes } from "./cardbookHTMLTypes.mjs";

import { cardbookBGUtils } from "../../../BG/utils/cardbookBGUtils.mjs";

import { cardbookIDBCard } from "../../../BG/indexedDB/cardbookIDBCard.mjs";

import { cardbookBGPreferences } from "../../../BG/utils/cardbookBGPreferences.mjs";
import { PhoneNumber } from "../phonenumberutils/PhoneNumber.mjs";

export var cardbookHTMLWindowUtils = {
	lastUrlValueBox: {},
	lastKeyValueBox: {},
	fields: [ "fn", "lastname", "firstname", "othername", "prefixname", "suffixname", "nickname",
					"birthplace", "deathplace", "mailer", "sortstring",
					"bday", "anniversary", "deathdate", "version", "rev",
					"class1", "agent", "prodid", "uid", "dirPrefId", "cardurl", "etag" ],
	adrNullValue: [ "", "", "", "", "", "", "" ],
	typeNullValue: [ "" ],

	clearCard: function () {
		for (let field of cardbookHTMLWindowUtils.fields) {
			if (document.getElementById(`${field}InputText`)) {
				document.getElementById(`${field}InputText`).value = "";
			}
			if (document.getElementById(`${field}InputLabel`)) {
				document.getElementById(`${field}InputLabel`).textContent = "";
			}
			cardbookHTMLWindowUtils.hideRow(field);
		}
		cardbookHTMLWindowUtils.hideRow("gender");

		if (document.getElementById("noteInputText")) {
			document.getElementById("noteInputText").value = "";
		}
		if (document.getElementById("noteInputLabel")) {
			document.getElementById("noteInputLabel").textContent = "";
		}
		if (document.getElementById("vCardInputLabel")) {
			document.getElementById("vCardInputLabel").textContent = "";
		}

		for (let field of cardbookHTMLDates.dateFields) {
			document.getElementById(`${field}InputDate`).clearValue();
			document.getElementById(`${field}InputLabel`).textContent = "";
			cardbookHTMLWindowUtils.hideRow(field);
		}

		cardbookHTMLTools.deleteTableRows("orgTableBody");

		for (let type of ["personal", "org", "misc", "tech"]) {
			if (document.getElementById(`${type}Groupbox`)) {
				document.getElementById(`${type}Groupbox`).classList.add("hidden");
			}
		}

		// need to remove the Custom from Pers
		// for the Org, everything is cleared out
		let table = document.getElementById("personalTableBody");
		for (let i = table.rows.length -1; i >= 0; i--) {
			let row = table.rows[i];
			if (row.id.startsWith("customField")) {
				table.removeChild(row);
			}
		}

		document.getElementById("defaultCardImage").src = "";
		document.getElementById("imageForSizing").src = "";
		document.getElementById("imageBox").classList.add("hidden");
		for (let type of cardbookHTMLUtils.multilineFields) {
			cardbookHTMLTools.deleteRows(`${type}Groupbox`);
		}
		cardbookHTMLWindowUtils.clearCategories();
		cardbookHTMLTools.deleteRows("emailpropertyGroupbox");
		cardbookHTMLTools.deleteRows("keyGroupbox");
		cardbookHTMLTools.deleteRows("eventGroupbox");
		cardbookHTMLTools.deleteRows("tzGroupbox");
		cardbookHTMLTools.deleteRows("addedCardsTable");

		document.getElementById("listGroupbox").classList.add("hidden");
	},

	hideRow: async function (field) {
		if (document.getElementById(`${field}Row`)) {
			document.getElementById(`${field}Row`).classList.add("hidden");
		}
	},

	noHideRow: async function (field) {
		if (document.getElementById(`${field}Row`)) {
			document.getElementById(`${field}Row`).classList.remove("hidden");
		}
	},

	hideInputText: async function (field) {
		document.getElementById(`${field}InputText`).classList.add("hidden");
	},

	noHideInputText: async function (field) {
		document.getElementById(`${field}InputText`).classList.remove("hidden");
	},

	hideInputDate: async function (field) {
		document.getElementById(`${field}InputDate`).classList.add("hidden");
	},

	noHideInputDate: async function (field) {
		document.getElementById(`${field}InputDate`).classList.remove("hidden");
	},

	hideInputLabel: async function (field) {
		document.getElementById(`${field}InputLabel`).classList.add("hidden");
	},

	noHideInputLabel: async function (field) {
		document.getElementById(`${field}InputLabel`).classList.remove("hidden");
	},

	displayCard: async function (aCard, aReadOnly, aInputFunction, aDisplaynameFunction) {
		for (let type of ["personal", "org", "misc", "tech"]) {
			if (document.getElementById(`${type}Groupbox`)) {
				document.getElementById(`${type}Groupbox`).classList.remove("hidden");
			}
		}

		for (let field of cardbookHTMLWindowUtils.fields) {
			if (document.getElementById(`${field}InputText`)) {
				let inputText = document.getElementById(`${field}InputText`);
				let value = aCard[field];
				if (value) {
					inputText.value = value;
				}
				if (cardbookHTMLUtils.newFields.includes(field) && aCard.version == "3.0") {
					cardbookHTMLWindowUtils.hideRow(field);
				} else if (aReadOnly && value) {
					cardbookHTMLWindowUtils.noHideRow(field);
					cardbookHTMLWindowUtils.hideInputText(field);
				} else if (aReadOnly && !value) {
					cardbookHTMLWindowUtils.hideRow(field);
				} else {
					cardbookHTMLWindowUtils.noHideRow(field);
					cardbookHTMLWindowUtils.noHideInputText(field);
				}
			}
			if (document.getElementById(`${field}InputDate`)) {
				let inputDate = document.getElementById(`${field}InputDate`);
				let value = aCard[field];
				if (value) {
					let dateUTC = cardbookHTMLDates.convertDateStringToDateUTC(aCard[field]);
					if (dateUTC != "WRONGDATE") {
						let splittedDate = cardbookHTMLDates.splitUTCDateIntoComponents(dateUTC);
						inputDate.setValue(splittedDate.day, splittedDate.month, splittedDate.year);
					}
				}
				if (cardbookHTMLUtils.newFields.includes(field) && aCard.version == "3.0") {
					cardbookHTMLWindowUtils.hideRow(field);
				} else if (aReadOnly && value) {
					cardbookHTMLWindowUtils.noHideRow(field);
					cardbookHTMLWindowUtils.hideInputDate(field);
				} else if (aReadOnly && !value) {
					cardbookHTMLWindowUtils.hideRow(field);
				} else {
					cardbookHTMLWindowUtils.noHideRow(field);
					cardbookHTMLWindowUtils.noHideInputDate(field);
				}
			}
			if (document.getElementById(`${field}InputLabel`) && document.getElementById(`${field}InputText`)) {
				let inputLabel = document.getElementById(`${field}InputLabel`);
				let value = "";
				if (cardbookHTMLDates.dateFields.includes(field)) {
					value = cardbookHTMLDates.getFormattedDateForCard(aCard, field);
				} else if (aCard[field]) {
					value = aCard[field];
				}
				inputLabel.textContent = value;
				if (cardbookHTMLUtils.newFields.includes(field) && aCard.version == "3.0") {
					cardbookHTMLWindowUtils.hideInputLabel(field);
				} else if (!aReadOnly) {
					cardbookHTMLWindowUtils.hideInputLabel(field);
				} else {
					cardbookHTMLWindowUtils.noHideInputLabel(field);
				}
			} else if (document.getElementById(`${field}InputLabel`)) {
				let inputLabel = document.getElementById(`${field}InputLabel`);
				let value = "";
				if (cardbookHTMLDates.dateFields.includes(field)) {
					value = cardbookHTMLDates.getFormattedDateForCard(aCard, field);
				} else if (aCard[field]) {
					value = aCard[field];
				}
				inputLabel.textContent = value;
				if (cardbookHTMLUtils.newFields.includes(field) && aCard.version == "3.0") {
					cardbookHTMLWindowUtils.hideRow(field);
				} else if (aReadOnly && value) {
					cardbookHTMLWindowUtils.noHideRow(field);
				} else if (aReadOnly && !value) {
					cardbookHTMLWindowUtils.hideRow(field);
				} else {
					cardbookHTMLWindowUtils.noHideRow(field);
					cardbookHTMLWindowUtils.hideInputLabel(field);
				}
			}
		}

		if (document.getElementById("genderMenulist")) {
			let inputMenulist = document.getElementById("genderMenulist");
			let value = aCard.gender;
			inputMenulist.value = value;
			let field = "gender";
			if (aCard.version == "3.0") {
				cardbookHTMLWindowUtils.hideRow(field);
			} else if (aReadOnly && value) {
				cardbookHTMLWindowUtils.noHideRow(field);
				inputMenulist.classList.add("hidden");
			} else if (aReadOnly && !value) {
				cardbookHTMLWindowUtils.hideRow(field);
			} else {
				cardbookHTMLWindowUtils.noHideRow(field);
				inputMenulist.classList.remove("hidden");
			}
		}
		if (document.getElementById("genderInputLabel")) {
			let inputLabel = document.getElementById("genderInputLabel");
			let gender = aCard.gender.toLowerCase();
			let value = messenger.i18n.getMessage(`types.gender.${gender}`)
			inputLabel.textContent = value;
			if (aCard.version == "3.0") {
				inputLabel.classList.add("hidden");
			} else if (!aReadOnly) {
				inputLabel.classList.add("hidden");
			} else {
				inputLabel.classList.remove("hidden");
			}
		}

		if (document.getElementById("listGroupbox")) {
			let listbox = document.getElementById("listGroupbox");
			if (aCard.isAList) {
				listbox.classList.remove("hidden");
			} else {
				listbox.classList.add("hidden");
			}
		}

		await cardbookHTMLWindowUtils.loadCategories(aCard.categories, aReadOnly, aCard.dirPrefId);
		cardbookHTMLWindowUtils.loadDefaultVersion(aCard.version);

		let myRemainingOthers = [];
		myRemainingOthers = cardbookHTMLWindowUtils.constructCustom(aReadOnly, "personal", aCard.others, aInputFunction);
		
		cardbookHTMLWindowUtils.constructOrg(aReadOnly, aCard.org, aCard.title, aCard.role, aInputFunction, aDisplaynameFunction);
		myRemainingOthers = cardbookHTMLWindowUtils.constructCustom(aReadOnly, "org", myRemainingOthers, aInputFunction);

		var myNoteArray = aCard.note.split("\n");
		var myEvents = cardbookHTMLUtils.getEventsFromCard(myNoteArray, myRemainingOthers);
		for (let field of cardbookHTMLUtils.multilineFields) {
			if (aReadOnly) {
				if (aCard[field].length > 0) {
					cardbookHTMLWindowUtils.constructStaticTypesRows(field, aCard[field]);
				}
			} else {
				await cardbookHTMLWindowUtils.constructDynamicTypesRows(field, aCard[field]);
			}
		}
		
		if (aReadOnly) {
			cardbookHTMLWindowUtils.constructStaticEventsRows(myEvents.result);
		} else {
			cardbookHTMLWindowUtils.constructDynamicEventsRows("event", myEvents.result);
		}
		let field = [];
		if (Array.isArray(aCard.tz)) {
			field = aCard.tz;
		} else {
			field = [[aCard.tz]];
		}
		if (aReadOnly) {
			cardbookHTMLWindowUtils.constructStaticTzRows(field);
		} else {
			await cardbookHTMLWindowUtils.constructDynamicTzRows("tz", field);
		}

		if (document.getElementById("othersInputText")) {
			document.getElementById("othersInputText").value = myEvents.remainingOthers.join("\n");
		}

		let noteDateButton = document.getElementById("noteDateButton");
		let noteInputText = document.getElementById("noteInputText");
		let noteInputLabel = document.getElementById("noteInputLabel");
		if (noteInputText) {
			noteInputText.value = myEvents.remainingNote.join("\n");
			if (aReadOnly) {
				noteDateButton.classList.add("hidden");
				noteInputText.classList.add("hidden");
			} else {
				noteDateButton.classList.remove("hidden");
				noteInputText.classList.remove("hidden");
			}
		}
		if (noteInputLabel) {
			noteInputLabel.textContent = myEvents.remainingNote.join("\n");
			if (aReadOnly) {
				noteInputLabel.classList.remove("hidden");
			} else {
				noteInputLabel.classList.add("hidden");
			}
		}

		await cardbookHTMLWindowUtils.constructEmailProperties(aCard, aReadOnly);
		
		if (aReadOnly) {
			cardbookHTMLWindowUtils.constructStaticKeysRows(aCard.key);
		} else {
			cardbookHTMLWindowUtils.constructDynamicKeysRows("key", aCard.key);
		}

		if (aReadOnly) {
			for (let type of ["personal", "org", "misc", "tech"]) {
				if (document.getElementById(`${type}Groupbox`)) {
					let rows = document.getElementById(`${type}Table`).querySelectorAll("tr:not(.hidden)");
					if (!rows.length) {
						document.getElementById(`${type}Groupbox`).classList.add("hidden");
					}
				}
			}
		}
	},

	clearCategories: function () {
		document.getElementById("categoriesInputDropDown").clearOptions();
		document.getElementById("categoriesRow").classList.add("hidden");
	},

	loadCategories: async function (aCategories, aReadOnly, aDirPrefId) {
		let allCategories = await messenger.runtime.sendMessage({ query: "cardbook.getCategories", defaultPrefId: aDirPrefId, noUncat: true });
		let options = [];
		for (let [label, value] of allCategories) {
			let checked = aCategories.includes(label) ? true : false;
			options.push([label, value, checked]);
		}
		document.getElementById("categoriesInputDropDown").loadOptions(`${aDirPrefId}::categories::`, options);

		document.getElementById("categoriesRow").classList.remove("hidden");
		if (aReadOnly) {
			if (aCategories.length) {
				document.getElementById("categoriesInputDropDown").setReadOnly();
			} else {
				document.getElementById("categoriesRow").classList.add("hidden");
			}
		} else {
			let message = messenger.i18n.getMessage("categoriesTextboxLabel");
			document.getElementById("categoriesInputDropDown").setPlaceholder(message);
		}
	},

	loadDefaultVersion: function (aVersion) {
		if (aVersion == "") {
			let dirPrefId = document.getElementById("addressbookMenulist").value;
			document.getElementById("versionInputText").value = cardbookBGPreferences.getVCardVersion(dirPrefId);
		} else {
			document.getElementById("versionInputText").value = aVersion;
		}
	},

	constructCustom: function (aReadOnly, aType, aOtherValue, aInputFunction) {
		let othersTemp = JSON.parse(JSON.stringify(aOtherValue));
		let customFields = cardbookBGPreferences.getAllCustomFields();
		let result = customFields[aType];
		for (let i = 0; i < result.length; i++) {
			let myCode = result[i][0];
			let myLabel = result[i][1];
			let myField = `customField${i}${aType}`;
			let myValue = "";
			let j;
			for (j = 0; j < othersTemp.length; j++) {
				let delim = othersTemp[j].indexOf(":", 0);
				let header = othersTemp[j].substr(0, delim);
				let value = othersTemp[j].substr(delim+1, othersTemp[j].length);
				let headerTmp = header.split(";");
				if (myCode == headerTmp[0]) {
					myValue = value;
					break;
				}
			}
			let dummy = othersTemp.splice(j,1);
			j--;
			let tablebody = document.getElementById(aType + "TableBody");
			if (aReadOnly) {
				if (myValue != "") {
					let currentRow = cardbookHTMLTools.addHTMLTR(tablebody, `${myField}Row`, {class: "indentRow", "data-field-name": myCode, "data-field-label": myLabel});
					let labelData = cardbookHTMLTools.addHTMLTD(currentRow, `${myField}Label.1`);
					cardbookHTMLTools.addHTMLLABEL(labelData, `${myField}Label`, myLabel, {class: "boldFont"});
					let textboxData = cardbookHTMLTools.addHTMLTD(currentRow, `${myField}InputLabel.2`);
					let myTextbox = cardbookHTMLTools.addHTMLLABEL(textboxData, `${myField}InputLabel`, myValue);
				}
			} else {
				let currentRow = cardbookHTMLTools.addHTMLTR(tablebody, `${myField}Row`, {class: "indentRow", "data-field-name": myCode});
				let labelData = cardbookHTMLTools.addHTMLTD(currentRow, `${myField}Label.1`);
				cardbookHTMLTools.addHTMLLABEL(labelData, `${myField}Label`, myLabel, {class: "boldFont"});
				let textboxData = cardbookHTMLTools.addHTMLTD(currentRow, `${myField}InputText.2`);
				let textbox = cardbookHTMLTools.addHTMLINPUT(textboxData, `${myField}InputText`, myValue);
				textbox.addEventListener("input", aInputFunction, false);
				cardbookHTMLTools.addProcessButton(textboxData, `${myCode}ProcessButton`, cardbookHTMLWindowUtils.setConvertFunction);
			}
		}
		return othersTemp;
	},

	constructOrg: function (aReadOnly, aOrgValue, aTitleValue, aRoleValue, aInputFunction, aDisplaynameFunction) {
		let aOrigBox = document.getElementById("orgTableBody");
		let orgStructure = cardbookBGPreferences.getPref("orgStructure");
		if (orgStructure.length) {
			for (let i = 0; i < orgStructure.length; i++) {
				let myValue = "";
				if (aOrgValue[i]) {
					myValue = aOrgValue[i] || "";
				}
				if (aReadOnly) {
					if (myValue != "") {
						let currentRow = cardbookHTMLTools.addHTMLTR(aOrigBox, `orgRow_${i}`, {class: "indentRow", "data-field-name": "org_" + orgStructure[i], "data-field-label": orgStructure[i]});
						let labelData = cardbookHTMLTools.addHTMLTD(currentRow, `orgInputLabel_${i}.1`);
						cardbookHTMLTools.addHTMLLABEL(labelData, `orgLabel_${i}`, orgStructure[i], {class: "boldFont"});
						let textboxData = cardbookHTMLTools.addHTMLTD(currentRow, `orgInputLabel_${i}.2`);
						let myTextbox = cardbookHTMLTools.addHTMLLABEL(textboxData, `orgInputLabel_${i}`, myValue);
					}
				} else {
					let currentRow = cardbookHTMLTools.addHTMLTR(aOrigBox, `orgRow_${i}`, {class: "indentRow", "data-field-name": "org_" + orgStructure[i]});
					let labelData = cardbookHTMLTools.addHTMLTD(currentRow, `orgInputText_${i}.1`);
					cardbookHTMLTools.addHTMLLABEL(labelData, `orgLabel_${i}`, orgStructure[i], {class: "boldFont"});
					let textboxData = cardbookHTMLTools.addHTMLTD(currentRow, `orgInputText_${i}.2`);
					let myInputText = cardbookHTMLTools.addHTMLINPUT(textboxData, `orgInputText_${i}`, myValue);
					myInputText.addEventListener("input", event => {
						aInputFunction(event);
						aDisplaynameFunction();
					}, false);
					cardbookHTMLTools.addProcessButton(textboxData, `org_${orgStructure[i]}ProcessButton`, cardbookHTMLWindowUtils.setConvertFunction);
				}
			}
		} else {
			if (aReadOnly) {
				if (aOrgValue[0]) {
					let currentRow = cardbookHTMLTools.addHTMLTR(aOrigBox, "orgRow_0", {class: "indentRow", "data-field-name": "org"});
					let myLabel = messenger.i18n.getMessage("orgLabel");
					let labelData = cardbookHTMLTools.addHTMLTD(currentRow, "orgInputLabel_0.1");
					cardbookHTMLTools.addHTMLLABEL(labelData, "orgLabel", myLabel, {class: "boldFont"});
					let textboxData = cardbookHTMLTools.addHTMLTD(currentRow, "orgInputLabel_0.2");
					let myTextbox = cardbookHTMLTools.addHTMLLABEL(textboxData, "orgInputLabel_0", aOrgValue.join(";"));
				}
			} else {
				let currentRow = cardbookHTMLTools.addHTMLTR(aOrigBox, "orgRow_0", {class: "indentRow", "data-field-name": "org"});
				let labelData = cardbookHTMLTools.addHTMLTD(currentRow, "orgLabel.1");
				cardbookHTMLTools.addHTMLLABEL(labelData, "orgLabel", messenger.i18n.getMessage("orgLabel"), {class: "boldFont"});
				let textboxData = cardbookHTMLTools.addHTMLTD(currentRow, "orgInputText_0.2");
				let myInputText = cardbookHTMLTools.addHTMLINPUT(textboxData, "orgInputText_0", aOrgValue.join(";"));
				myInputText.addEventListener("input", event => {
					aInputFunction(event);
					aDisplaynameFunction();
				}, false);
				cardbookHTMLTools.addProcessButton(textboxData, "orgProcessButton", cardbookHTMLWindowUtils.setConvertFunction);
			}
		}
		if (aReadOnly) {
			if (aTitleValue != "") {
				let currentRow = cardbookHTMLTools.addHTMLTR(aOrigBox, "titleRow", {class: "indentRow", "data-field-name": "title"});
				let myLabel = messenger.i18n.getMessage("titleLabel");
				let labelData = cardbookHTMLTools.addHTMLTD(currentRow, "titleLabel" + ".1");
				cardbookHTMLTools.addHTMLLABEL(labelData, "titleLabel", myLabel, {class: "boldFont"});
				let textboxData = cardbookHTMLTools.addHTMLTD(currentRow, "titleInputLabel" + ".2");
				let myTextbox = cardbookHTMLTools.addHTMLLABEL(textboxData, "titleInputLabel", aTitleValue);
			}
			if (aRoleValue != "") {
				let currentRow = cardbookHTMLTools.addHTMLTR(aOrigBox, "roleRow", {class: "indentRow", "data-field-name": "role"});
				let myLabel = messenger.i18n.getMessage("roleLabel");
				let labelData = cardbookHTMLTools.addHTMLTD(currentRow, "roleLabel" + ".1");
				cardbookHTMLTools.addHTMLLABEL(labelData, "roleLabel", myLabel, {class: "boldFont"});
				let textboxData = cardbookHTMLTools.addHTMLTD(currentRow, "roleInputLabel" + ".2");
				let myTextbox = cardbookHTMLTools.addHTMLLABEL(textboxData, "roleInputLabel", aRoleValue);
			}
		} else {
			let titleRow = cardbookHTMLTools.addHTMLTR(aOrigBox, "titleRow", {class: "indentRow", "data-field-name": "title"});
			let titleLabelData = cardbookHTMLTools.addHTMLTD(titleRow, "titleLabel" + ".1");
			cardbookHTMLTools.addHTMLLABEL(titleLabelData, "titleLabel", messenger.i18n.getMessage("titleLabel"), {class: "boldFont"});
			let titleTextboxData = cardbookHTMLTools.addHTMLTD(titleRow, "titleInputText" + ".2");
			let titleInputText = cardbookHTMLTools.addHTMLINPUT(titleTextboxData, "titleInputText", aTitleValue);
			titleInputText.addEventListener("input", event => {
				aInputFunction(event);
				aDisplaynameFunction();
			}, false);
			cardbookHTMLTools.addProcessButton(titleTextboxData, "titleProcessButton", cardbookHTMLWindowUtils.setConvertFunction);
			let roleRow = cardbookHTMLTools.addHTMLTR(aOrigBox, "roleRow", {class: "indentRow", "data-field-name": "role"});
			let roleLabelData = cardbookHTMLTools.addHTMLTD(roleRow, "roleLabel" + ".1");
			cardbookHTMLTools.addHTMLLABEL(roleLabelData, "roleLabel", messenger.i18n.getMessage("roleLabel"), {class: "boldFont"});
			let roleTextboxData = cardbookHTMLTools.addHTMLTD(roleRow, "roleInputText" + ".2");
			let roleInputText = cardbookHTMLTools.addHTMLINPUT(roleTextboxData, "roleInputText", aRoleValue);
			roleInputText.addEventListener("input", event => {
				aInputFunction(event);
				aDisplaynameFunction();
			}, false);
			cardbookHTMLTools.addProcessButton(roleTextboxData, "roleProcessButton", cardbookHTMLWindowUtils.setConvertFunction);
		}
	},

	setConvertFunction: function (aEvent) {
		let button = aEvent.target;
		if (!button.hasAttribute("autoConvertField")) {
			button.setAttribute("autoConvertField", "true");
			button.setAttribute("title", messenger.i18n.getMessage("dontAutoConvertField"));
		} else {
			button.removeAttribute("autoConvertField");
			button.setAttribute("title", messenger.i18n.getMessage("autoConvertField"));
		}
	},

	openAdrPanel: function (aType, aIndex) {
		if (document.getElementById(`${aType}_${aIndex}_adrPanel`)) {
			document.getElementById(`${aType}_${aIndex}_adrPanel`).remove();
			return;
		}

		// add template
		let myTempResult = cardbookHTMLWindowUtils.getTypeForLine(aType, aIndex);
		let adrLine = ["", "", "", "", "", "", ""];
		if (myTempResult.length != 0) {
			adrLine = myTempResult[0];
		}
		let template = document.getElementById("adrTemplate");
		let clonedTemplate = template.content.cloneNode(true);
		let node = clonedTemplate.querySelector(".hbox");
		node.id = `${aType}_${aIndex}_adrPanel`;
		document.getElementById(`${aType}_${aIndex}_hbox`).after(clonedTemplate);
		i18n.updateDocument();

		// set template values
		let postOffice = node.querySelector("#postOfficeInputText");
		postOffice.value = adrLine[0];
		let extendedAddr = node.querySelector("#extendedAddrInputText");
		extendedAddr.value = adrLine[1];
		let street = node.querySelector("#streetInputText");
		street.value = adrLine[2];
		street.focus();
		let locality = node.querySelector("#localityInputText");
		locality.value = adrLine[3];
		let region = node.querySelector("#regionInputText");
		region.value = adrLine[4];
		let postalCode = node.querySelector("#postalCodeInputText");
		postalCode.value = adrLine[5];
		let country = node.querySelector("#countryMenulist");
		let countryValue = adrLine[6];
		if (countryValue == "") {
			if (document.getElementById("cardDefaultRegion").value != "") {
				let cardRegion = document.getElementById("cardDefaultRegion").value.toLowerCase();
				countryValue = messenger.i18n.getMessage(`region-name-${cardRegion}`);
			}
		}
		let countryList = cardbookHTMLUtils.getCountries(true);
		cardbookHTMLTools.loadCountries(countryList, country, countryValue);

		// fields edition
		function checkEditionFields(aField) {
			if (editionFields[0][0] == "allFields") {
				return true;
			}
			for (let field of editionFields) {
				if (field[2] == aField) {
					return field[0];
				}
			}
			return false;
		}
		let editionFields = cardbookHTMLUtils.getEditionFields();
		for (let field of cardbookHTMLUtils.adrElements) {
			let partialIndex = cardbookHTMLUtils.adrElements.indexOf(field);
			if (checkEditionFields(field) || document.getElementById(`${aType}_${aIndex}_valueBox_${partialIndex}`).value != "") {
				document.getElementById(`${field}Row`).classList.remove("hidden");
			} else {
				document.getElementById(`${field}Row`).classList.add("hidden");
			}
		}

		// convertion button
		let forceHide = false;
		switch (document.getElementById("loadEditionMode").value) {
			case "ViewResult":
			case "ViewResultHideCreate":
			case "ViewContact":
			case "ViewList":
				forceHide = true;
		}
		let itemsList = node.querySelectorAll("img.cardbookProcessClass");
		for (let item of itemsList) {
			if (forceHide) {
				item.classList.add("hidden");
			} else {
				let code = item.id.replace("ProcessButton", "");
				for (let field of editionFields) {
					if (field[0] == "allFields") {
						item.classList.add("hidden");
						break;
					} else if (code == field[2]) {
						if (field[3] != "") {
							item.classList.remove("hidden");
						} else {
							item.classList.add("hidden");
						}
						break;
					}
				}
			}
		}
		async function onInputField(aEvent) {
			let textbox = aEvent.target;
			let row = textbox.closest("tr");
			let field = row.getAttribute("data-field-name");
			let button = document.getElementById(`${field}ProcessButton`);
			if (!button.classList.contains("hidden") && button.hasAttribute("autoConvertField")) {
				let tmpArray = editionFields.filter(x => x[2] == field);
				let convertionFunction = tmpArray[0][4];
				let value = cardbookHTMLUtils.convertField(convertionFunction, textbox.value);
				textbox.value = value;
			}
		}
		postOffice.addEventListener("input", onInputField, false);
		extendedAddr.addEventListener("input", onInputField, false);
		street.addEventListener("input", onInputField, false);
		locality.addEventListener("input", onInputField, false);
		region.addEventListener("input", onInputField, false);
		postalCode.addEventListener("input", onInputField, false);
		let convertButtons = node.querySelectorAll("img.cardbookProcessClass");
		for (let button of convertButtons) {
			button.addEventListener("click", cardbookHTMLWindowUtils.setConvertFunction);
		}
	
		// buttons
		function validateAdrOnClick(aEvent) {
			if (aEvent.button == 0) {
				document.getElementById(`${aType}_${aIndex}_valueBox_0`).value = document.getElementById("postOfficeInputText").value.trim();
				document.getElementById(`${aType}_${aIndex}_valueBox_1`).value = document.getElementById("extendedAddrInputText").value.trim();
				document.getElementById(`${aType}_${aIndex}_valueBox_2`).value = document.getElementById("streetInputText").value.replace(/\n/g, "\\n").trim();
				document.getElementById(`${aType}_${aIndex}_valueBox_3`).value = document.getElementById("localityInputText").value.trim();
				document.getElementById(`${aType}_${aIndex}_valueBox_4`).value = document.getElementById("regionInputText").value.trim();
				document.getElementById(`${aType}_${aIndex}_valueBox_5`).value = document.getElementById("postalCodeInputText").value.trim();
				let countryCode = document.getElementById("countryMenulist").value.trim().toLowerCase();
				let countryValue = messenger.i18n.getMessage(`region-name-${countryCode}`);
				document.getElementById(`${aType}_${aIndex}_valueBox_6`).value = countryValue;
				let tmpArray = [];
				for (var i = 0; i < 7; i++) {
					tmpArray.push(document.getElementById(`${aType}_${aIndex}_valueBox_${i}`).value.replace(/\\n/g, " ").trim());
				}
				document.getElementById(`${aType}_${aIndex}_valueBox`).value = cardbookHTMLUtils.formatAddressForInput(tmpArray);
				document.getElementById(`${aType}_${aIndex}_valueBox`).dispatchEvent(new Event("input"));
				cancelAdrOnClick();
			}
		};
		let validateButton = node.querySelector("#validateAdrPanelButton");
		validateButton.addEventListener("click", validateAdrOnClick, false);
		function cancelAdrOnClick(aEvent) {
			if (document.getElementById(`${aType}_${aIndex}_adrPanel`)) {
				document.getElementById(`${aType}_${aIndex}_adrPanel`).remove();
			}
		};
		let cancelButton = node.querySelector("#cancelAdrPanelButton");
		cancelButton.addEventListener("click", cancelAdrOnClick, false);
	},

	getPrefForLine: function (aType, aIndex) {
		let result = [];
		let prefButton = document.getElementById(`${aType}_${aIndex}_PrefImage`);
		let version = document.getElementById("versionInputText").value;
		if (prefButton.hasAttribute("haspref")) {
			if (version == "4.0") {
				result.push("PREF=1");
			} else {
				result.push("TYPE=PREF");
			}
		}
		return result;
	},

	getTypeForLine: function (aType, aIndex) {
		let dirPrefId = document.getElementById("dirPrefIdInputText").value;
		let myLineTypeResult = cardbookHTMLWindowUtils.getPrefForLine(aType, aIndex);

		if (document.getElementById(`${aType}_${aIndex}_othersTypesBox`)) {
			let value = document.getElementById(`${aType}_${aIndex}_othersTypesBox`).value;
			myLineTypeResult = myLineTypeResult.concat(value.split(","));
		}
		
		let myLineTypeType = [];

		let myTypes = [];
		if (document.getElementById(`${aType}_${aIndex}_menuType`)) {
			let value = document.getElementById(`${aType}_${aIndex}_menuType`).value;
			if (value) {
				myTypes = [value, "PG"];
				var ABType = cardbookBGPreferences.getType(dirPrefId);
				var ABTypeFormat = cardbookHTMLUtils.getABTypeFormat(ABType);
				for (var i = 0; i < cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType].length; i++) {
					if (value.toUpperCase() == cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType][i][0].toUpperCase()) {
						var prefPossibility = cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType][i][1].split(";")[0];
						if (cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType][i][2] && cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType][i][2] == "PG") {
							myTypes = [prefPossibility, "PG"];
						} else {
							myTypes = [prefPossibility, "NOPG"];
						}
					}
				}
			} else {
				myTypes = [];
			}
		}
		var myOutputPg = [];
		var myPgName = "";
		if (myTypes.length != 0) {
			if (myTypes[1] == "PG") {
				myOutputPg = [ `X-ABLABEL:${myTypes[0]}` ];
				myPgName = "ITEM1";
			} else {
				myLineTypeType.push(`TYPE=${myTypes[0]}`);
			}
		}

		if (myLineTypeType.length > 0) {
			myLineTypeResult = myLineTypeResult.concat(myLineTypeType);
			myLineTypeResult = cardbookHTMLUtils.unescapeArray(cardbookHTMLTypes.formatTypes(cardbookHTMLUtils.escapeArray(myLineTypeResult)));
		}
		
		if (aType == "adr") {
			var j = 0;
			var myLineTypeValue = [];
			while (true) {
				if (document.getElementById(`${aType}_${aIndex}_valueBox_${j}`)) {
					var myTypeValue = document.getElementById(`${aType}_${aIndex}_valueBox_${j}`).value.replace(/\\n/g, "\n").trim();
					myLineTypeValue.push(myTypeValue);
					j++;
				} else {
					break;
				}
			}
		} else if (aType == "impp") {
			var myLineTypeValue = [];
			myLineTypeValue.push(document.getElementById(`${aType}_${aIndex}_valueBox`).value);
			if (myLineTypeValue.join("") != "") {
				function removeServiceType(element) {
					return (element == element.replace(/^X-SERVICE-TYPE=/i, ""));
				}
				myLineTypeResult = myLineTypeResult.filter(removeServiceType);
				var code = "";
				if (document.getElementById(`${aType}_${aIndex}_menuIMPPType`).value) {
					code = document.getElementById(`${aType}_${aIndex}_menuIMPPType`).value
				}
				var serviceLine = cardbookHTMLTypes.getIMPPLineForCode(code);

				if (serviceLine[0]) {
					var myRegexp = new RegExp("^" + serviceLine[2] + ":");
					myLineTypeValue[0] = myLineTypeValue[0].replace(myRegexp, "");
					myLineTypeValue[0] = serviceLine[2] + ":" + myLineTypeValue[0];
				}
			}
		} else {
			var myLineTypeValue = [ "" ];
			if (document.getElementById(`${aType}_${aIndex}_valueBox`).value) {
				myLineTypeValue = [document.getElementById(`${aType}_${aIndex}_valueBox`).value.trim()];
			}
		}
		
		return [myLineTypeValue, myLineTypeResult, myPgName, myOutputPg];
	},

	getAllTypes: function (aType, aRemoveNull) {
		let i = 0;
		let result = [];
		while (true) {
			if (document.getElementById(`${aType}_${i}_hbox`)) {
				let lineResult = cardbookHTMLWindowUtils.getTypeForLine(aType, i);
				if (lineResult[0].join("") != "" || !aRemoveNull) {
					result.push([lineResult[0], lineResult[1], lineResult[2], lineResult[3]]);
				}
				i++;
			} else {
				break;
			}
		}
		return result;
	},

	getAllEvents: function (aRemoveNull) {
		let type = "event";
		let i = 0;
		let result = [];
		while (true) {
			if (document.getElementById(`${type}_${i}_hbox`)) {
				let prefButton = document.getElementById(`${type}_${i}_PrefImage`);
				let dateResult = document.getElementById(`${type}_${i}_InputDate`).getValue();
				let nameResult = document.getElementById(`${type}_${i}_valueBox`).value;
				let pref = "";
				if (prefButton.hasAttribute("haspref")) {
					pref = ";TYPE=PREF";
				}
				if ((nameResult != "" && dateResult != "") || !aRemoveNull) {
					result.push([dateResult, nameResult, pref]);
				}
				i++;
			} else {
				break;
			}
		}
		return result;
	},

	getAllTz: function (aRemoveNull) {
		let type = "tz";
		let i = 0;
		let result = [];
		while (true) {
			if (document.getElementById(`${type}_${i}_hbox`)) {
				let nameResult = document.getElementById(`${type}_${i}_menulistTz`).value;
				if ((nameResult != "") || !aRemoveNull) {
					result.push([[nameResult], []]);
				}
				i++;
			} else {
				break;
			}
		}
		return result;
	},

	getAllKeys: function (aRemoveNull) {
		let type = "key";
		let i = 0;
		let result = [];
		while (true) {
			if (document.getElementById(`${type}_${i}_hbox`)) {
				let keyTypes = cardbookHTMLWindowUtils.getPrefForLine(type, i);
				let keyResult = document.getElementById(`${type}_${i}_valueBox`).value;
				if (keyResult != "" || !aRemoveNull) {
					if ((keyResult.search(/^http/i) >= 0) || (keyResult.search(/^file/i) >= 0)) {
						result.push({types: keyTypes, value: "", URI: keyResult, extension: ""});
					} else {
						result.push({types: keyTypes, value: keyResult, URI: "", extension: ""});
					}
				}
				i++;
			} else {
				break;
			}
		}
		return result;
	},

	findNextLine: function (aType) {
		let i = 0;
		while (true) {
			if (document.getElementById(`${aType}_${i}_hbox`) || document.getElementById(`${aType}_${i}_row`)) {
				i++;
			} else {
				return i;
			}
		}
	},

	constructDynamicTypesRows: async function (aType, aArray) {
		let start = cardbookHTMLWindowUtils.findNextLine(aType);
		for (let i = 0; i < aArray.length; i++) {
			await cardbookHTMLWindowUtils.constructDynamicTypesRow(aType, i+start, aArray[i][1], aArray[i][2], aArray[i][3], aArray[i][0]);
		}
		if (aArray.length == 0) {
			let value = (aType == "adr") ? cardbookHTMLWindowUtils.adrNullValue : cardbookHTMLWindowUtils.typeNullValue;
			await cardbookHTMLWindowUtils.constructDynamicTypesRow(aType, start, [], "", [], value);
		}
	},

	constructDynamicEventsRows: function (aType, aEventType) {
		var start = cardbookHTMLWindowUtils.findNextLine(aType);
		for (var i = 0; i < aEventType.length; i++) {
			cardbookHTMLWindowUtils.constructDynamicEventsRow(aType, i+start, aEventType[i]);
		}
		if (aEventType.length == 0) {
			cardbookHTMLWindowUtils.constructDynamicEventsRow(aType, start, ["", "", ""]);
		}
	},

	constructDynamicTzRows: async function (aType, aTzType) {
		var start = cardbookHTMLWindowUtils.findNextLine(aType);
		for (var i = 0; i < aTzType.length; i++) {
			await cardbookHTMLWindowUtils.constructDynamicTzRow(aType, i+start, aTzType[i],);
		}
		if (aTzType.length == 0) {
			await cardbookHTMLWindowUtils.constructDynamicTzRow(aType, start, [[], []]);
		}
	},

	constructDynamicKeysRows: function (aType, aKeyType) {
		var start = cardbookHTMLWindowUtils.findNextLine(aType);
		for (var i = 0; i < aKeyType.length; i++) {
			cardbookHTMLWindowUtils.constructDynamicKeysRow(aType, i+start, aKeyType[i]);
		}
		if (aKeyType.length == 0) {
			cardbookHTMLWindowUtils.constructDynamicKeysRow(aType, start, {types: [], value: "", URI: "", extension: ""});
		}
	},

	constructEmailProperties: async function (aCard, aReadOnly) {
		let emails = [];
		if (aCard.isAList && aCard.fn) {
			emails.push(aCard.fn.toLowerCase());
		} else {
			for (let email of aCard.email) {
				emails.push(email[0][0].toLowerCase());
			}
		}

		let listsTabLabel = messenger.i18n.getMessage("listsTabLabel");
		document.getElementById("listTab").textContent = `${listsTabLabel} (0)`;
		let aOrigBox = document.getElementById("emailpropertyGroupbox");
		let table = cardbookHTMLTools.addHTMLTABLE(aOrigBox, "emailpropertyTable");
		let i = 0;

		if (!aCard.isAList) {
			let preferMailFormatHeaderRow = cardbookHTMLTools.addHTMLTR(table, `emailproperty_${i}_preferMailFormatHeaderRow`);
			let preferMailFormatHeaderData = cardbookHTMLTools.addHTMLTD(preferMailFormatHeaderRow, `emailproperty_${i}_preferMailFormatHeaderRowTD`);
			let preferMailFormatHeaderLabel = messenger.i18n.getMessage("PreferMailFormatGroupbox.label");
			cardbookHTMLTools.addHTMLLABEL(preferMailFormatHeaderData, `emailproperty_${i}_preferMailFormatHeaderRowValue`, preferMailFormatHeaderLabel, {class: "boldFont"});
			i++;

			let preferMailFormatRow = cardbookHTMLTools.addHTMLTR(table, `emailproperty_${i}_preferMailFormatRow`, {class: "indentRow"});
			let preferMailFormatLabel = messenger.i18n.getMessage("PreferMailFormat.label");
			let list = [ [ "0", messenger.i18n.getMessage("Unknown.label") ],
						[ "1", messenger.i18n.getMessage("PlainText.label") ],
						[ "2", messenger.i18n.getMessage("HTML.label") ] ];
			let mailFormat = cardbookHTMLUtils.getMailFormatFromCard(aCard);
			if (aReadOnly) {
				let labelData = cardbookHTMLTools.addHTMLTD(preferMailFormatRow, `label_${i}_preferMailFormatTD`);
				cardbookHTMLTools.addHTMLLABEL(labelData, `label_${i}_preferMailFormat`, preferMailFormatLabel);
				let valueData = cardbookHTMLTools.addHTMLTD(preferMailFormatRow, `value_${i}_preferMailFormatTD`);
				let message = "";
				if (mailFormat == "1") {
					message = messenger.i18n.getMessage("PlainText.label");
				} else if (mailFormat == "2") {
					message = messenger.i18n.getMessage("HTML.label");
				} else {
					message = messenger.i18n.getMessage("Unknown.label")
				}
				cardbookHTMLTools.addHTMLLABEL(valueData, `value_${i}_preferMailFormat`, message);
			} else {
				let labelData = cardbookHTMLTools.addHTMLTD(preferMailFormatRow, `label_${i}_preferMailFormatTD`);
				cardbookHTMLTools.addHTMLLABEL(labelData, `label_${i}_preferMailFormat`, preferMailFormatLabel);
				let valueData = cardbookHTMLTools.addHTMLTD(preferMailFormatRow, `value_${i}_preferMailFormatTD`);
				let select = cardbookHTMLTools.addHTMLSELECT(valueData, `select_${i}_preferMailFormat`, "");
				cardbookHTMLTools.loadPreferMailFormat(select, mailFormat);
			}
			i++;
		}

		let popLabel = messenger.i18n.getMessage("popularityLabel");
		for (let email of emails) {
			let mailPopularityValue = 0;
			let mailPop = await messenger.runtime.sendMessage({query: "cardbook.getPopularity", email: email.toLowerCase()});
			if (mailPop) {
				mailPopularityValue = mailPop.count;
			}
			let emailRow = cardbookHTMLTools.addHTMLTR(table, `emailproperty_${i}_emailRow`, {class: "indent"});
			let emailData = cardbookHTMLTools.addHTMLTD(emailRow, `emailRow_${i}_inputTextTD`);
			cardbookHTMLTools.addHTMLLABEL(emailData, `emailheader_${i}_inputText`, email, {class: "boldFont"});

			let row = cardbookHTMLTools.addHTMLTR(table, `emailproperty_${i}_Row`, {class: "indentRow"});
			if (aReadOnly) {
				let labelData = cardbookHTMLTools.addHTMLTD(row, `pop_${i}_inputLabelTD`);
				cardbookHTMLTools.addHTMLLABEL(labelData, `pop_${i}_inputLabel`, popLabel);
				let valueData = cardbookHTMLTools.addHTMLTD(row, `popularity_${i}_inputLabelTD`);
				cardbookHTMLTools.addHTMLLABEL(valueData, `popularity_${i}_inputLabel`, mailPopularityValue);
			} else {
				let labelData = cardbookHTMLTools.addHTMLTD(row, `pop_${i}_inputLabelTD`);
				cardbookHTMLTools.addHTMLLABEL(labelData, `pop_${i}_inputLabel`, popLabel, {"for": `popularity_${i}_inputText`});
				let valueData = cardbookHTMLTools.addHTMLTD(row, `popularity_${i}_inputTextTD`);
				cardbookHTMLTools.addHTMLINPUT(valueData, `popularity_${i}_inputText`, mailPopularityValue, {type:"number", min:"0", max:"100000", class:"size5"});
			}
			let labelData = cardbookHTMLTools.addHTMLTD(row, `email_${i}_inputTextTD`, {class: "hidden"});
			cardbookHTMLTools.addHTMLLABEL(labelData, `email_${i}_inputText`, email);

			let memberOfLabel = messenger.i18n.getMessage("memberOfLabel");
			let lists = await cardbookIDBCard.getLists(aCard.dirPrefId);
			let j = 0;
			for (let list of lists) {
				let listConversion = await messenger.runtime.sendMessage({query: "cardbook.getListConversion", email: `${list.fn} <${list.fn}>`});
				if ((listConversion.emailResult.includes(email) && !aCard.isAList) ||
					(listConversion.recursiveList.includes(email) && aCard.isAList && email != list.fn)) {
					let row = cardbookHTMLTools.addHTMLTR(table, `memberOf_${i}_${j}_TableRow`, {class: "indentRow"});
					let labelData = cardbookHTMLTools.addHTMLTD(row, `memberOf_${i}_${j}_inputText`);
					cardbookHTMLTools.addHTMLLABEL(labelData, `memberOf_${i}_${j}_inputText`, memberOfLabel);
					let valueData = cardbookHTMLTools.addHTMLTD(row, `memberOf_${i}_${j}_valueBox`);
					let name = cardbookHTMLUtils.getName(list);
					let listTextbox = cardbookHTMLTools.addHTMLLABEL(valueData, `memberOf_${i}_${j}_valueBox`, name, {"data-id": list.cbid, class: "text-link cardbookMemberOf"});
					listTextbox.addEventListener("click", cardbookHTMLUtils.editCardFromList, false);
					j++;
				}
			}
			let itemList = table.querySelectorAll(".cardbookMemberOf");
			let items = Array.from(itemList, item => item.getAttribute("data-id"));
			items = cardbookHTMLUtils.arrayUnique(items);
			document.getElementById("listTab").textContent = `${listsTabLabel} (${items.length})`;
			i++;
		}
	},

	constructDynamicTypesRow: async function (aType, aIndex, aInputTypes, aPgName, aPgType, aCardValue) {
		let dirPrefId = document.getElementById("dirPrefIdInputText").value;
		let aOrigBox = document.getElementById(`${aType}Groupbox`);
		
		if (aIndex == 0) {
			let value = messenger.i18n.getMessage(`${aType}GroupboxLabel`);
			cardbookHTMLTools.addHTMLLABEL(aOrigBox, `${aType}_caption`, value, {class: "boldFont"});
		}

		let aHBox = cardbookHTMLTools.addHTMLDIV(aOrigBox, `${aType}_${aIndex}_hbox`, {class: "hbox hcentered"});

		let myInputTypes = cardbookHTMLUtils.getOnlyTypesFromTypes(aInputTypes);
		let myOthersTypes = cardbookHTMLUtils.getNotTypesFromTypes(aInputTypes);

		let aPrefButton = cardbookHTMLTools.addPrefStar(aHBox, aType, aIndex, cardbookHTMLUtils.getPrefBooleanFromTypes(aInputTypes), false);
		
		cardbookHTMLTools.addHTMLINPUT(aHBox, `${aType}_${aIndex}_othersTypesBox`, myOthersTypes, {class: "hidden"});

		if (aType != "impp") {
			let myCheckedArrayTypes = [];
			if (aPgType.length != 0 && aPgName != "") {
				let found = false;
				for (let j = 0; j < aPgType.length; j++) {
					let tmpArray = aPgType[j].split(":");
					if (tmpArray[0] == "X-ABLABEL") {
						myCheckedArrayTypes.push(tmpArray[1]);
						found = true;
						break;
					}
				}
				if (!found) {
					for (let j = 0; j < myInputTypes.length; j++) {
						myCheckedArrayTypes.push(myInputTypes[j]);
					}
				}
			} else {
				for (let j = 0; j < myInputTypes.length; j++) {
					myCheckedArrayTypes.push(myInputTypes[j]);
				}
			}

			let sourceList = cardbookHTMLTypes.getTypesFromDirPrefId(aType, dirPrefId);
			cardbookHTMLUtils.sortMultipleArrayByString(sourceList, 0, 1);
			let checkedCode = cardbookHTMLTypes.whichCodeTypeShouldBeChecked(aType, dirPrefId, myCheckedArrayTypes, sourceList);
			if (checkedCode.isAPg && !checkedCode.isAlreadyThere) {
				sourceList.push([checkedCode.result, checkedCode.result]);
			}
			let result = [];
			for (let line of sourceList) {
				result.push([line[0], line[1], line[1] == checkedCode.result]);

			}
			cardbookHTMLTools.addMenuType(aHBox, aType, aIndex, result);
		} else {
			let serviceCode = cardbookHTMLTypes.getIMPPCode(aInputTypes);
			let serviceProtocol = cardbookHTMLTypes.getIMPPProtocol(aCardValue);
			let allIMPPs = cardbookBGPreferences.getAllIMPPs();
			await cardbookHTMLTools.addMenuIMPPType(aHBox, aType, aIndex, allIMPPs, serviceCode, serviceProtocol);
		}

		let keyInput;
		if (aType == "impp") {
			let serviceCode = cardbookHTMLTypes.getIMPPCode(aInputTypes);
			let serviceProtocol = cardbookHTMLTypes.getIMPPProtocol(aCardValue);
			let myValue = aCardValue.join(" ");
			if (serviceCode != "") {
				let serviceLine = [];
				serviceLine = cardbookHTMLTypes.getIMPPLineForCode(serviceCode)
				if (serviceLine[0]) {
					let myRegexp = new RegExp("^" + serviceLine[2] + ":");
					myValue = myValue.replace(myRegexp, "");
				}
			} else if (serviceProtocol != "") {
				let serviceLine = [];
				serviceLine = cardbookHTMLTypes.getIMPPLineForProtocol(serviceProtocol)
				if (serviceLine[0]) {
					let myRegexp = new RegExp("^" + serviceLine[2] + ":");
					myValue = myValue.replace(myRegexp, "");
				}
			}
			keyInput = cardbookHTMLTools.addKeyInput(aHBox, `${aType}_${aIndex}_valueBox`, myValue, {type: "text", class: "fullWidth"});
		} else if (aType == "adr") {
			let myTmpArray = [];
			for (var i = 0; i < aCardValue.length; i++) {
				myTmpArray.push(aCardValue[i].replace(/\n/g, " "));
			}
			keyInput = cardbookHTMLTools.addKeyInput(aHBox, `${aType}_${aIndex}_valueBox`, cardbookHTMLUtils.formatAddressForInput(myTmpArray), {type: "text", class: "fullWidth"});
		} else {
			keyInput = cardbookHTMLTools.addKeyInput(aHBox, `${aType}_${aIndex}_valueBox`, cardbookHTMLUtils.cleanArray(aCardValue).join(" "), {type: "text", class: "fullWidth"});
		}

		if (aType == "adr") {
			let i = 0;
			while ( i < 7 ) {
				if (aCardValue[i]) {
					cardbookHTMLTools.addHTMLINPUT(aHBox, `${aType}_${aIndex}_valueBox_${i}`, aCardValue[i].replace(/\n/g, "\\n"), {class: "hidden"});
				} else {
					cardbookHTMLTools.addHTMLINPUT(aHBox, `${aType}_${aIndex}_valueBox_${i}`, "", {class: "hidden"});
				}
				i++;
			}
			function fireEditAdrOnClick(aEvent) {
				if (aEvent.button == 0) {
					cardbookHTMLWindowUtils.openAdrPanel(aType, aIndex);
				}
			};
			document.getElementById(`${aType}_${aIndex}_valueBox`).addEventListener("click", fireEditAdrOnClick, false);
		} else if (aType == "tel") {
			function fireInputTel(event) {
				let validationButton = document.getElementById(`${aType}_${aIndex}_validateButton`);
				let validationButtonImg = document.getElementById(`${aType}_${aIndex}_validateButtonImg`);
				let cardDefaultRegion = document.getElementById("cardDefaultRegion").value;
				let tel = PhoneNumber.Parse(this.value, cardDefaultRegion);
				if (tel && tel.internationalFormat && this.value == tel.internationalFormat) {
					validationButtonImg.setAttribute("class", "cardbookValidated");
					validationButton.setAttribute("title", messenger.i18n.getMessage("validatedEntryTooltip"));
				} else {
					validationButtonImg.setAttribute("class", "cardbookNotValidated");
					validationButton.setAttribute("title", messenger.i18n.getMessage("notValidatedEntryTooltip"));
				}
			};
			document.getElementById(`${aType}_${aIndex}_valueBox`).addEventListener("input", fireInputTel, false);
		} else if (aType == "email") {
			function fireInputEmail(event) {
				let validationButton = document.getElementById(`${aType}_${aIndex}_validateButton`);
				let validationButtonImg = document.getElementById(`${aType}_${aIndex}_validateButtonImg`);
				let emailLower = this.value.toLowerCase();
				let atPos = this.value.lastIndexOf("@");
				if (this.value == emailLower && atPos > 0 && atPos + 1 < this.value.length) {
					validationButtonImg.setAttribute("class", "cardbookValidated");
					validationButton.setAttribute("title", messenger.i18n.getMessage("validatedEntryTooltip"));
				} else {
					validationButtonImg.setAttribute("class", "cardbookNotValidated");
					validationButton.setAttribute("title", messenger.i18n.getMessage("notValidatedEntryTooltip"));
				}
			};
			document.getElementById(`${aType}_${aIndex}_valueBox`).addEventListener("input", fireInputEmail, false);
		}
	
		if (aType == "tel") {
			function fireValidateTelButton(event) {
				if (document.getElementById(this.id).disabled) {
					return;
				}
				let telInputText = document.getElementById(`${aType}_${aIndex}_valueBox`);
				let buttonImg = document.getElementById(`${this.id}Img`);
				let cardDefaultRegion = document.getElementById("cardDefaultRegion").value;
				let tel = PhoneNumber.Parse(telInputText.value, cardDefaultRegion);
				if (telInputText.value == "") {
					buttonImg.setAttribute("class", "cardbookNotValidated");
					this.setAttribute("title", messenger.i18n.getMessage("notValidatedEntryTooltip"));
				} else if (tel && tel.internationalFormat) {
					telInputText.value = tel.internationalFormat;
					buttonImg.setAttribute("class", "cardbookValidated");
					this.setAttribute("title", messenger.i18n.getMessage("validatedEntryTooltip"));
				} else {
					buttonImg.setAttribute("class", "cardbookNotValidated");
					this.setAttribute("title", messenger.i18n.getMessage("notValidatedEntryTooltip"));
				}
			};
			cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "validated", "validate", fireValidateTelButton);
		} else if (aType == "email") {
			function fireValidateEmailButton(event) {
				if (document.getElementById(this.id).disabled) {
					return;
				}
				let emailInputText = document.getElementById(`${aType}_${aIndex}_valueBox`);
				let buttonImg = document.getElementById(`${this.id}Img`);
				let emailLower = emailInputText.value.toLowerCase();
				emailLower = emailLower.replace("'", "").replace('"', "");
				let atPos = emailInputText.value.lastIndexOf("@");
				if (emailInputText.value == "") {
					buttonImg.setAttribute("class", "cardbookNotValidated");
					this.setAttribute("title", messenger.i18n.getMessage("notValidatedEntryTooltip"));
				} else if (atPos > 0 && atPos + 1 < emailInputText.value.length) {
					emailInputText.value = emailLower;
					buttonImg.setAttribute("class", "cardbookValidated");
					this.setAttribute("title", messenger.i18n.getMessage("validatedEntryTooltip"));
				} else {
					buttonImg.setAttribute("class", "cardbookNotValidated");
					this.setAttribute("title", messenger.i18n.getMessage("notValidatedEntryTooltip"));
				}
			};
			cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "validated", "validate", fireValidateEmailButton);
		} else if (aType == "url") {
			async function fireValidateUrlButton(event) {
				if (document.getElementById(this.id).disabled) {
					return;
				}
				cardbookHTMLWindowUtils.lastUrlValueBox = document.getElementById(`${aType}_${aIndex}_valueBox`);
				let result = await messenger.runtime.sendMessage({ query: "cardbook.callFilePicker", result: "path", title: "fileSelectionTitle", mode: "OPEN", defaultDir: cardbookHTMLWindowUtils.lastUrlValueBox.value});
				cardbookHTMLWindowUtils.lastUrlValueBox.value = "file://" + result.file;
				cardbookHTMLWindowUtils.lastUrlValueBox.dispatchEvent(new Event("input"));
			};
			cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "link", "link", fireValidateUrlButton);
		}
		
		async function fireUpButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllTypes(aType, false);
			if (myAllValuesArray.length <= 1) {
				return;
			}
			var temp = myAllValuesArray[aIndex*1-1];
			myAllValuesArray[aIndex*1-1] = myAllValuesArray[aIndex];
			myAllValuesArray[aIndex] = temp;
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			await cardbookHTMLWindowUtils.constructDynamicTypesRows(aType, myAllValuesArray);
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "up", "up", fireUpButton);
		
		async function fireDownButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllTypes(aType, false);
			if (myAllValuesArray.length <= 1) {
				return;
			}
			var temp = myAllValuesArray[aIndex*1+1];
			myAllValuesArray[aIndex*1+1] = myAllValuesArray[aIndex];
			myAllValuesArray[aIndex] = temp;
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			await cardbookHTMLWindowUtils.constructDynamicTypesRows(aType, myAllValuesArray);
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "down", "down", fireDownButton);

		async function fireRemoveButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllTypes(aType, false);
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			if (myAllValuesArray.length == 0) {
				await cardbookHTMLWindowUtils.constructDynamicTypesRows(aType, myAllValuesArray);
			} else {
				var removed = myAllValuesArray.splice(aIndex, 1);
				await cardbookHTMLWindowUtils.constructDynamicTypesRows(aType, myAllValuesArray);
			}
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "remove", "remove", fireRemoveButton);
		
		async function fireAddButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myValue = document.getElementById(`${aType}_${aIndex}_valueBox`).value;
			if (myValue == "") {                                                                                       
				return;
			}
			var myNextIndex = 1+ 1*aIndex;
			let value = (aType == "adr") ? cardbookHTMLWindowUtils.adrNullValue : cardbookHTMLWindowUtils.typeNullValue;
			await cardbookHTMLWindowUtils.constructDynamicTypesRow(aType, myNextIndex, [], "", [], value);
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "add", "add", fireAddButton);

		keyInput.dispatchEvent(new Event("input"));
	},

	constructDynamicEventsRow: function (aType, aIndex, aEventType) {
		let dirPrefId = document.getElementById("dirPrefIdInputText").value;
		let version = document.getElementById("versionInputText").value;
		let aOrigBox = document.getElementById(`${aType}Groupbox`);
		
		if (aIndex == 0) {
			let value = messenger.i18n.getMessage(`${aType}GroupboxLabel`);
			cardbookHTMLTools.addHTMLLABEL(aOrigBox, `${aType}_caption`, value, {class: "boldFont"});
		}

		let aHBox = cardbookHTMLTools.addHTMLDIV(aOrigBox, `${aType}_${aIndex}_hbox`, {class: "hbox hcentered"});
		
		let pref = (aEventType[2] === true);
		let aPrefButton = cardbookHTMLTools.addPrefStar(aHBox, aType, aIndex, pref, false);

		let dateUTC = cardbookHTMLDates.convertDateStringToDateUTC(aEventType[0]);
		let splittedDate = {};
		if (dateUTC != "WRONGDATE") {
			splittedDate = cardbookHTMLDates.splitUTCDateIntoComponents(dateUTC);
		}
		let inputDate = cardbookHTMLTools.addCUSTOMINPUTDATE(aHBox, `${aType}_${aIndex}_InputDate`);
		inputDate.setValue(splittedDate?.day, splittedDate?.month, splittedDate?.year);

		let keyInput = cardbookHTMLTools.addKeyInput(aHBox, `${aType}_${aIndex}_valueBox`, aEventType[1], {type: "text", class: "thirdWidth"});

		function fireUpButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllEvents(false);
			if (myAllValuesArray.length <= 1) {
				return;
			}
			var temp = myAllValuesArray[aIndex*1-1];
			myAllValuesArray[aIndex*1-1] = myAllValuesArray[aIndex];
			myAllValuesArray[aIndex] = temp;
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			cardbookHTMLWindowUtils.constructDynamicEventsRows(aType, myAllValuesArray);
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "up", "up", fireUpButton);
		
		function fireDownButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllEvents(false);
			if (myAllValuesArray.length <= 1) {
				return;
			}
			var temp = myAllValuesArray[aIndex*1+1];
			myAllValuesArray[aIndex*1+1] = myAllValuesArray[aIndex];
			myAllValuesArray[aIndex] = temp;
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			cardbookHTMLWindowUtils.constructDynamicEventsRows(aType, myAllValuesArray);
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "down", "down", fireDownButton);

		function fireRemoveButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllEvents(false);
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			if (myAllValuesArray.length == 0) {
				cardbookHTMLWindowUtils.constructDynamicEventsRows(aType, myAllValuesArray);
			} else {
				var removed = myAllValuesArray.splice(aIndex, 1);
				cardbookHTMLWindowUtils.constructDynamicEventsRows(aType, myAllValuesArray);
			}
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "remove", "remove", fireRemoveButton);
		
		function fireAddButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myValue = document.getElementById(`${aType}_${aIndex}_valueBox`).value;
			if (myValue == "") {                                                                                       
				return;
			}
			var myNextIndex = 1+ 1*aIndex;
			cardbookHTMLWindowUtils.constructDynamicEventsRow(aType, myNextIndex, ["", "", ""]);
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "add", "add", fireAddButton);

		keyInput.dispatchEvent(new Event("input"));
	},

	constructDynamicTzRow: async function (aType, aIndex, aTzType) {
		let aOrigBox = document.getElementById(`${aType}Groupbox`);
		
		if (aIndex == 0) {
			let value = messenger.i18n.getMessage(`${aType}GroupboxLabel`);
			cardbookHTMLTools.addHTMLLABEL(aOrigBox, `${aType}_caption`, value, {class: "boldFont"});
		}

		let aHBox = cardbookHTMLTools.addHTMLDIV(aOrigBox, `${aType}_${aIndex}_hbox`, {class: "hbox hcentered"});

		let select = await cardbookHTMLTools.addMenuTzlist(aHBox, aType, aIndex, aTzType[0]);
        select.addEventListener("change", cardbookHTMLTools.checkEditButton, false);

		async function fireUpButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllTz(false);
			if (myAllValuesArray.length <= 1) {
				return;
			}
			var temp = myAllValuesArray[aIndex*1-1];
			myAllValuesArray[aIndex*1-1] = myAllValuesArray[aIndex];
			myAllValuesArray[aIndex] = temp;
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			await cardbookHTMLWindowUtils.constructDynamicTzRows(aType, myAllValuesArray);
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "up", "up", fireUpButton);
		
		async function fireDownButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllTz(false);
			if (myAllValuesArray.length <= 1) {
				return;
			}
			var temp = myAllValuesArray[aIndex*1+1];
			myAllValuesArray[aIndex*1+1] = myAllValuesArray[aIndex];
			myAllValuesArray[aIndex] = temp;
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			await cardbookHTMLWindowUtils.constructDynamicTzRows(aType, myAllValuesArray);
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "down", "down", fireDownButton);

		async function fireRemoveButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllTz(false);
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			if (myAllValuesArray.length == 0) {
				await cardbookHTMLWindowUtils.constructDynamicTzRows(aType, myAllValuesArray);
			} else {
				var removed = myAllValuesArray.splice(aIndex, 1);
				await cardbookHTMLWindowUtils.constructDynamicTzRows(aType, myAllValuesArray);
			}
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "remove", "remove", fireRemoveButton);
		
		async function fireAddButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myValue = document.getElementById(`${aType}_${aIndex}_menulistTz`).value;
			if (myValue == "") {                                                                                       
				return;
			}
			var myNextIndex = 1+ 1*aIndex;
			await cardbookHTMLWindowUtils.constructDynamicTzRow(aType, myNextIndex, [[], []]);
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "add", "add", fireAddButton);

		select.dispatchEvent(new Event("change"));
	},

	constructDynamicKeysRow: function (aType, aIndex, aKeyType) {
		let aOrigBox = document.getElementById(`${aType}Groupbox`);

		let aHBox = cardbookHTMLTools.addHTMLDIV(aOrigBox, `${aType}_${aIndex}_hbox`, {class: "hbox hcentered"});

		let aPrefButton = cardbookHTMLTools.addPrefStar(aHBox, aType, aIndex, cardbookHTMLUtils.getPrefBooleanFromTypes(aKeyType.types), false);

		let value;
		if (aKeyType.URI != "") {
			value = aKeyType.URI;
		} else {
			value = aKeyType.value.replaceAll("\\n", "\n").replaceAll("\\r", "\r");
		}
		let keyTextarea = cardbookHTMLTools.addKeyTextarea(aHBox, `${aType}_${aIndex}_valueBox`, value, {cols: 50, rows: 10});

		async function fireLinkKeyButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			cardbookHTMLWindowUtils.lastKeyValueBox = document.getElementById(`${aType}_${aIndex}_valueBox`);
			let result = await messenger.runtime.sendMessage({ query: "cardbook.callFilePicker", result: "path", title: "fileSelectionTitle", mode: "OPEN", defaultDir: cardbookHTMLWindowUtils.lastKeyValueBox.value});
			cardbookHTMLWindowUtils.lastKeyValueBox.value = "file://" + result.file;
			cardbookHTMLWindowUtils.lastKeyValueBox.dispatchEvent(new Event("input"));
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "link", "link", fireLinkKeyButton);

		function fireUpButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllKeys(false);
			if (myAllValuesArray.length <= 1) {
				return;
			}
			var temp = myAllValuesArray[aIndex*1-1];
			myAllValuesArray[aIndex*1-1] = myAllValuesArray[aIndex];
			myAllValuesArray[aIndex] = temp;
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			cardbookHTMLWindowUtils.constructDynamicKeysRows(aType, myAllValuesArray);
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "up", "up", fireUpButton);
		
		function fireDownButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllKeys(false);
			if (myAllValuesArray.length <= 1) {
				return;
			}
			var temp = myAllValuesArray[aIndex*1+1];
			myAllValuesArray[aIndex*1+1] = myAllValuesArray[aIndex];
			myAllValuesArray[aIndex] = temp;
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			cardbookHTMLWindowUtils.constructDynamicKeysRows(aType, myAllValuesArray);
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "down", "down", fireDownButton);

		function fireRemoveButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myAllValuesArray = cardbookHTMLWindowUtils.getAllKeys(false);
			cardbookHTMLTools.deleteRows(`${aType}Groupbox`);
			if (myAllValuesArray.length == 0) {
				cardbookHTMLWindowUtils.constructDynamicKeysRows(aType, myAllValuesArray);
			} else {
				var removed = myAllValuesArray.splice(aIndex, 1);
				cardbookHTMLWindowUtils.constructDynamicKeysRows(aType, myAllValuesArray);
			}
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "remove", "remove", fireRemoveButton);
		
		function fireAddButton(event) {
			if (document.getElementById(this.id).disabled) {
				return;
			}
			var myValue = document.getElementById(`${aType}_${aIndex}_valueBox`).value;
			if (myValue == "") {                                                                                       
				return;
			}
			var myNextIndex = 1+ 1*aIndex;
			cardbookHTMLWindowUtils.constructDynamicKeysRow(aType, myNextIndex, {types: [], value: "", URI: "", extension: ""});
		};
		cardbookHTMLTools.addEditButton(aHBox, aType, aIndex, "add", "add", fireAddButton);

		let keysTabLabel = messenger.i18n.getMessage("keyTabLabel");
		let length = aIndex+ +1;
		let message = (aIndex || aKeyType.value) ? `${keysTabLabel} (${length})` : keysTabLabel;
		document.getElementById("keyTab").textContent = message;
		keyTextarea.dispatchEvent(new Event("input"));
	},

	constructStaticList: async function (addedCards) {
		let addedLabel = messenger.i18n.getMessage("addedCardsGroupboxLabel");
		let label = `${addedLabel} (${addedCards.length})`;
		document.getElementById("addedCardsLabel").textContent = label;

		let table = document.getElementById("addedCardsTable");
		for (let i = 0; i < addedCards.length; i++) {
			let card = addedCards[i];

			let row = cardbookHTMLTools.addHTMLTR(table, `addedCards_${i}_row`, {class: "indentRow"});

			let labelImage = cardbookHTMLTools.addHTMLTD(row, `email_${i}_image.1`);
			let imageCard = cardbookHTMLTools.addHTMLIMAGE(labelImage, `email_${i}_image`);
			let imageClass = card.isEmail === true ? "Mail" : card.isAList === true ? "MailList" : "Contact";
			imageCard.classList.add(imageClass);

			let labelData = cardbookHTMLTools.addHTMLTD(row, `email_${i}_valueBox.1`);
			let valueData = cardbookHTMLTools.addHTMLLABEL(labelData, `email_${i}_valueBox`, card.fn, {"data-id": card.cbid, class: "text-link"});

			if (imageClass == "Contact") {
				let emailsData = cardbookHTMLTools.addHTMLTD(row, `email_${i}_emails.1`);
				cardbookHTMLTools.addHTMLLABEL(emailsData, `email_${i}_emails`, card.emails.join(" "));
			}
			valueData.addEventListener("click", cardbookHTMLUtils.editCardFromList, false);
		}
	},

	constructStaticTypesRows: function (aType, aArray) {
		for (let i = 0; i < aArray.length; i++) {
			cardbookHTMLWindowUtils.constructStaticTypesRow(aType, i, aArray[i][1], aArray[i][2], aArray[i][3], aArray[i][0]);
		}
	},

	constructStaticEventsRows: function (aEventType) {
		for (var i = 0; i < aEventType.length; i++) {
			cardbookHTMLWindowUtils.constructStaticEventsRow("event", i, aEventType[i]);
		}
	},

	constructStaticTzRows: function (aTzType) {
		for (var i = 0; i < aTzType.length; i++) {
			cardbookHTMLWindowUtils.constructStaticTzRow("tz", i, aTzType[i]);
		}
	},

	constructStaticKeysRows: function (aKey) {
		let keysTabLabel = messenger.i18n.getMessage("keyTabLabel");
		document.getElementById("keyTab").textContent = keysTabLabel;
		for (var i = 0; i < aKey.length; i++) {
			cardbookHTMLWindowUtils.constructStaticKeysRow("key", i, aKey[i]);
		}
	},

	constructStaticTypesRow: function (aType, aIndex, aInputTypes, aPgName, aPgType, aCardValue) {
		if (aCardValue.join(" ") == "") {
			return;
		}
		let dirPrefId = document.getElementById("dirPrefIdInputText").value;
		let aOrigBox = document.getElementById(`${aType}Groupbox`);
		let label = messenger.i18n.getMessage(`${aType}GroupboxLabel`);
		
		let table;
		if (aIndex == 0) {
			cardbookHTMLTools.addHTMLLABEL(aOrigBox, `${aType}_caption`, label, {class: "boldFont"});
			table = cardbookHTMLTools.addHTMLTABLE(aOrigBox, `${aType}_Table`);
		} else {
			table = document.getElementById(`${aType}_Table`);
		}

		let row = cardbookHTMLTools.addHTMLTR(table, `${aType}_${aIndex}_TableRow`, {"data-field-name": aType, "data-field-label": label, "data-field-index": aIndex, class: "indentRow"});

		let myInputTypes = [];
		myInputTypes = cardbookHTMLUtils.getOnlyTypesFromTypes(aInputTypes);
		let myDisplayedTypes = [];
		if (aPgType.length != 0 && aPgName != "") {
			let label = "";
			for (let j = 0; j < aPgType.length; j++) {
				let tmpArray = aPgType[j].split(":");
				if (tmpArray[0] == "X-ABLABEL") {
					label = tmpArray[1];
					break;
				}
			}
			if (label) {
				myDisplayedTypes.push(cardbookHTMLTypes.whichLabelTypeShouldBeChecked(aType, dirPrefId, [ label ]));
			} else {
				myDisplayedTypes.push(cardbookHTMLTypes.whichLabelTypeShouldBeChecked(aType, dirPrefId, myInputTypes));
			}
		} else {
			myDisplayedTypes.push(cardbookHTMLTypes.whichLabelTypeShouldBeChecked(aType, dirPrefId, myInputTypes));
		}

		let imageData = cardbookHTMLTools.addHTMLTD(row, `${aType}_${aIndex}_PrefImage.1`);
		cardbookHTMLTools.addPrefStar(imageData, aType, aIndex, cardbookHTMLUtils.getPrefBooleanFromTypes(aInputTypes), true);

		let typeData = cardbookHTMLTools.addHTMLTD(row, `${aType}_${aIndex}_typeBox.1`);
		let valueData = cardbookHTMLTools.addHTMLTD(row, `${aType}_${aIndex}_valueBox.1`);
		let valueBox;
		if (aType == "impp") {
			let serviceCode = cardbookHTMLTypes.getIMPPCode(aInputTypes);
			let serviceProtocol = cardbookHTMLTypes.getIMPPProtocol(aCardValue);
			let myValue = aCardValue.join(" ");
			if (serviceCode != "") {
				let serviceLine = [];
				serviceLine = cardbookHTMLTypes.getIMPPLineForCode(serviceCode)
				if (serviceLine[0]) {
					myDisplayedTypes = myDisplayedTypes.concat(serviceLine[1]);
					cardbookHTMLTools.addHTMLLABEL(typeData,`${aType}_${aIndex}_typeBox`, cardbookHTMLUtils.formatTypesForDisplay(myDisplayedTypes), {});
					let myRegexp = new RegExp("^" + serviceLine[2] + ":");
					myValue = myValue.replace(myRegexp, "");
					valueBox = cardbookHTMLTools.addHTMLLABEL(valueData,`${aType}_${aIndex}_valueBox`, myValue, {class: "text-link"});
				} else {
					myDisplayedTypes = myDisplayedTypes.concat(serviceCode);
					cardbookHTMLTools.addHTMLLABEL(typeData,`${aType}_${aIndex}_typeBox`, cardbookHTMLUtils.formatTypesForDisplay(myDisplayedTypes), {});
					valueBox = cardbookHTMLTools.addHTMLLABEL(valueData,`${aType}_${aIndex}_valueBox`, myValue);
				}
			} else if (serviceProtocol != "") {
				var serviceLine = [];
				serviceLine = cardbookHTMLTypes.getIMPPLineForProtocol(serviceProtocol)
				if (serviceLine[0]) {
					myDisplayedTypes = myDisplayedTypes.concat(serviceLine[1]);
					cardbookHTMLTools.addHTMLLABEL(typeData,`${aType}_${aIndex}_typeBox`, cardbookHTMLUtils.formatTypesForDisplay(myDisplayedTypes), {});
					let myRegexp = new RegExp("^" + serviceLine[2] + ":");
					myValue = myValue.replace(myRegexp, "");
					valueBox = cardbookHTMLTools.addHTMLLABEL(valueData,`${aType}_${aIndex}_valueBox`, myValue, {class: "text-link"});
				} else {
					myDisplayedTypes = myDisplayedTypes.concat(serviceCode);
					cardbookHTMLTools.addHTMLLABEL(typeData,`${aType}_${aIndex}_typeBox`, cardbookHTMLUtils.formatTypesForDisplay(myDisplayedTypes), {});
					valueBox = cardbookHTMLTools.addHTMLLABEL(valueData,`${aType}_${aIndex}_valueBox`, myValue);
				}
			} else {
				cardbookHTMLTools.addHTMLLABEL(typeData,`${aType}_${aIndex}_typeBox`, cardbookHTMLUtils.formatTypesForDisplay(myDisplayedTypes), {});
				valueBox = cardbookHTMLTools.addHTMLLABEL(valueData,`${aType}_${aIndex}_valueBox`, myValue);
			}
		} else {
			cardbookHTMLTools.addHTMLLABEL(typeData,`${aType}_${aIndex}_typeBox`, cardbookHTMLUtils.formatTypesForDisplay(myDisplayedTypes), {});

			if (aType == "adr") {
				let re = /[\n\u0085\u2028\u2029]|\r\n?/;
				let myAdrResult = cardbookHTMLUtils.formatAddress(aCardValue);
				let myAdrResultArray = myAdrResult.split(re);
				let properties = {cols: 50, rows: myAdrResultArray.length, class: "text-link multilineLabel"};
				valueBox = cardbookHTMLTools.addHTMLLABEL(valueData, `${aType}_${aIndex}_valueBox`, myAdrResult, properties);
			} else {
				valueBox = cardbookHTMLTools.addHTMLLABEL(valueData,`${aType}_${aIndex}_valueBox`, cardbookHTMLUtils.cleanArray(aCardValue).join(" "), {class: "text-link"});
			}
		}

		async function fireKeyCheckBox(event) {
			if (aType == "email") {
				await messenger.runtime.sendMessage({query: "cardbook.emailCards", compFields: [{field: "to", value: valueBox.textContent}]});
			} else if (aType == "url") {
				if (valueBox.textContent.startsWith("http")) {
					await cardbookHTMLUtils.openURL(valueBox.textContent);
				}
			} else if (aType == "adr") {
				let dirPrefId = document.getElementById("dirPrefIdInputText").value;
				let uid = document.getElementById("uidInputText").value;
				let cbid = `${dirPrefId}::${uid}`;
				let row = valueBox.closest("tr");
				let index = row.getAttribute("data-field-index");
				let adr = [];
				for (let field of cardbookHTMLUtils.adrElements) {
					let [ clipValue, dummy ] = await cardbookHTMLUtils.getFieldValue(cbid, field, index);
					adr.push(clipValue);
				}
				await cardbookHTMLUtils.localizeCards(null, [adr]);
			} else if (aType == "impp") {
				let row = valueBox.closest("tr");
				let index = row.getAttribute("data-field-index");
				let dirPrefId = document.getElementById("dirPrefIdInputText").value;
				let uid = document.getElementById("uidInputText").value;
				let cbid = `${dirPrefId}::${uid}`;
				await messenger.runtime.sendMessage({query: "cardbook.connectIMPP", cbid: cbid, index: index});
			} else if (aType == "tel") {
				await messenger.runtime.sendMessage({query: "cardbook.connectTel", tel: valueBox.textContent});
			}
		};
		valueBox.addEventListener("click", fireKeyCheckBox, false);
	},

	constructStaticEventsRow: function (aType, aIndex, aEventType) {
		if (aEventType.join(" ") == "") {
			return;
		}
		let dirPrefId = document.getElementById("dirPrefIdInputText").value;
		let version = document.getElementById("versionInputText").value;
		let aOrigBox = document.getElementById(`${aType}Groupbox`);
		let label = messenger.i18n.getMessage(`${aType}GroupboxLabel`);
		
		let table;
		if (aIndex == 0) {
			cardbookHTMLTools.addHTMLLABEL(aOrigBox, `${aType}_caption`, label, {class: "boldFont"});
			table = cardbookHTMLTools.addHTMLTABLE(aOrigBox, `${aType}_Table`);
		} else {
			table = document.getElementById(`${aType}_Table`);
		}

		let row = cardbookHTMLTools.addHTMLTR(table, `${aType}_${aIndex}_TableRow`, {"data-field-name": aType, "data-field-label": label, "data-field-index": aIndex, class: "indentRow"});

		let imageData = cardbookHTMLTools.addHTMLTD(row, `${aType}_${aIndex}_PrefImage.1`);
		let pref = (aEventType[2] === true);
		cardbookHTMLTools.addPrefStar(imageData, aType, aIndex, pref, true);

		let dateDisplayedFormat = cardbookBGPreferences.getPref("dateDisplayedFormat");
		let myFormattedDate = cardbookHTMLDates.getFormattedDateForDateString(aEventType[0], dateDisplayedFormat);

		let typeData = cardbookHTMLTools.addHTMLTD(row, `${aType}_${aIndex}_typeBox.1`);
		cardbookHTMLTools.addHTMLLABEL(typeData,`${aType}_${aIndex}_typeBox`, myFormattedDate);
		let valueData = cardbookHTMLTools.addHTMLTD(row, `${aType}_${aIndex}_valueBox.1`);
		cardbookHTMLTools.addHTMLLABEL(valueData,`${aType}_${aIndex}_valueBox`, aEventType[1]);
	},

	constructStaticTzRow: function (aType, aIndex, aTzType) {
		if (aTzType.length == 1 && aTzType[0] == "") {
			return;
		}

		let aOrigBox = document.getElementById(`${aType}Groupbox`);
		let label = messenger.i18n.getMessage(`${aType}GroupboxLabel`);

		let table;
		if (aIndex == 0) {
			cardbookHTMLTools.addHTMLLABEL(aOrigBox, `${aType}_caption`, label, {class: "boldFont"});
			table = cardbookHTMLTools.addHTMLTABLE(aOrigBox, `${aType}_Table`);
		} else {
			table = document.getElementById(`${aType}_Table`);
		}

		let row = cardbookHTMLTools.addHTMLTR(table, `${aType}_${aIndex}_TableRow`, {"data-field-name": aType, "data-field-label": label, "data-field-index": aIndex, class: "indentRow"});

		let imageData = cardbookHTMLTools.addHTMLTD(row, `${aType}_${aIndex}_PrefImage.1`);
		cardbookHTMLTools.addPrefStar(imageData, aType, aIndex, false, true);

		let valueData = cardbookHTMLTools.addHTMLTD(row, `${aType}_${aIndex}_valueBox.1`);
		cardbookHTMLTools.addHTMLLABEL(valueData,`${aType}_${aIndex}_valueBox`, aTzType[0]);
	},

	constructStaticKeysRow: function (aType, aIndex, aKeyType) {
		if (aKeyType == "") {
			return;
		}

		let aOrigBox = document.getElementById(`${aType}Groupbox`);
		let dirPrefId = document.getElementById("dirPrefIdInputText").value;
		let fn = document.getElementById("fnInputLabel").textContent;

		let pref = cardbookHTMLUtils.getPrefBooleanFromTypes(aKeyType.types);
		let properties = {class: "cardbookKeyClass"};
		if (pref) {
			properties.haspref = "true";
		}
		aKeyType.value ? properties.keyValue = aKeyType.value : properties.keyValue = aKeyType.URI;

		let keyButton = cardbookHTMLTools.addHTMLIMAGE(aOrigBox, `${aType}_${aIndex}_keyButton`, properties);

		let values = {keyValue: properties.keyValue, dirPrefId: dirPrefId, fn: fn};

		async function fireKeyCheckBox(event) {
			await messenger.runtime.sendMessage({query: "cardbook.notifyObserver", value: "cardbook.importKeyFromValue", params: JSON.stringify(values)});
		};
		keyButton.addEventListener("click", fireKeyCheckBox, false);
		let length = aIndex+ +1;
		let keysTabLabel = messenger.i18n.getMessage("keyTabLabel");
		document.getElementById("keyTab").textContent = `${keysTabLabel} (${length})`;
	},

	showPane: function (paneID) {
		if (!paneID) {
			return;
		}
		let pane = document.getElementById(paneID);
		if (!pane) {
			return;
		}
		let tabnodes = document.querySelectorAll(".cardbookTab");
		for (let node of tabnodes) {
			if (node.id != paneID) {
				node.classList.add("hidden");
				document.getElementById(node.id.replace("Panel", "")).removeAttribute("visuallyselected");
			} else {
				document.getElementById(node.id.replace("Panel", "")).setAttribute("visuallyselected", "true");
				node.classList.remove("hidden");
			}
		}
	},

	sortPrefFieldsFirstForGoogle: function (aCard) {
		let type = cardbookBGPreferences.getType(aCard.dirPrefId);
		if (type.startsWith("GOOGLE")) {
			let fields = cardbookHTMLUtils.multilineFields.concat("key")
			for (let field of fields) {
				let result = [];
				let resultPref = [];
				for (let line of aCard[field]){
					let pref = cardbookHTMLUtils.getPrefBooleanFromTypes(line[1]);
					if (pref) {
						resultPref.push(line)
					} else {
						result.push(line)
					}
				}
				aCard[field] = resultPref.concat(result);
			}
		}
		return aCard;
	},
	
};
