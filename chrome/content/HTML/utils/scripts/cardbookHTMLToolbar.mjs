import { cardbookBGPreferences } from "../../../BG/utils/cardbookBGPreferences.mjs";
import { cardbookHTMLTools } from "./cardbookHTMLTools.mjs";

export var cardbookHTMLToolbar = {
    systemItems: [
                    { id: "flex", label: messenger.i18n.getMessage("toolbar.flexspace") },
                    { id: "space", label: messenger.i18n.getMessage("toolbar.space") },
                    { id: "toolsep", label: messenger.i18n.getMessage("toolbar.separator") },
                ],

    appItems: [
                { id: "addcontact", label: messenger.i18n.getMessage("cardbookToolbarAddContactButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarAddContactButtonTooltip") },
                { id: "addlist", label: messenger.i18n.getMessage("cardbookToolbarAddListButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarAddListButtonTooltip") },
                { id: "edit", label: messenger.i18n.getMessage("cardbookToolbarEditButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarEditButtonTooltip") },
                { id: "delete", label: messenger.i18n.getMessage("cardbookToolbarRemoveButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarRemoveButtonTooltip") },
                { id: "sync", label: messenger.i18n.getMessage("cardbookToolbarSyncButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarSyncButtonTooltip") },
                { id: "email", label: messenger.i18n.getMessage("cardbookToolbarWriteButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarWriteButtonTooltip") },
                { id: "prefs", label: messenger.i18n.getMessage("cardbookToolbarConfigurationButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarConfigurationButtonTooltip") },
                { id: "newAB", label: messenger.i18n.getMessage("cardbookToolbarAddServerButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarAddServerButtonTooltip") },
                { id: "chat", label: messenger.i18n.getMessage("cardbookToolbarChatButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarChatButtonTooltip") },
                { id: "template", label: messenger.i18n.getMessage("cardbookToolbarAddTemplateButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarAddTemplateButtonTooltip") },
                { id: "print", label: messenger.i18n.getMessage("cardbookToolbarPrintButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarPrintButtonTooltip") },
                { id: "undo", label: messenger.i18n.getMessage("cardbookToolbarBackButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarBackButtonTooltip") },
                { id: "redo", label: messenger.i18n.getMessage("cardbookToolbarForwardButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarForwardButtonTooltip") },
                { id: "layout", label: messenger.i18n.getMessage("cardbookToolbarLayoutButtonLabel"), title: messenger.i18n.getMessage("cardbookToolbarLayoutButtonTooltip") },
                { id: "search", label: messenger.i18n.getMessage("cardbookToolbarComplexSearchLabel"), title: messenger.i18n.getMessage("cardbookToolbarComplexSearchTooltip") },
                { id: "newevent", label: messenger.i18n.getMessage("cardbookToolbarNewEventLabel"), title: messenger.i18n.getMessage("cardbookToolbarNewEventTooltip") },
                { id: "newtask", label: messenger.i18n.getMessage("cardbookToolbarNewTodoLabel"), title: messenger.i18n.getMessage("cardbookToolbarNewTodoTooltip") },
            ],

    getItemsInfo: function () {
        let allItems = {};
        for (let item of cardbookHTMLToolbar.systemItems.concat(cardbookHTMLToolbar.appItems)) {
            allItems[item.id] = {};
            allItems[item.id].label = item.label;
            allItems[item.id].title = item.title;
        }
        return allItems;
    },

    getDefaultItemsForPrefs: function () {
        let allItems = {};
        allItems = cardbookHTMLToolbar.getItemsInfo();
        let result = [];
        let items = cardbookBGPreferences.getPref("defaultToolbarItems").split(",");
        for (let item of items) {
            if (allItems[item]) {
                result.push([allItems[item].label, allItems[item].title, item]);
            }
        }
        return result;
    },

    getCurrentItemsForPrefs: function () {
        let allItems = {};
        allItems = cardbookHTMLToolbar.getItemsInfo();
        let result = [];
        let items = cardbookBGPreferences.getToolbarItems().split(",");
        for (let item of items) {
            if (allItems[item]) {
                result.push([allItems[item].label, allItems[item].title, item]);
            }
        }
        return result;
    },

    getAllItemsForPrefs: function () {
        let result = [];
        for (let item of cardbookHTMLToolbar.systemItems.concat(cardbookHTMLToolbar.appItems)) {
            result.push([item.label, item.title, item.id]);
        }
        return result;
    },

    constructToolbar: function (toolbar, items, mode) {
        cardbookHTMLTools.deleteRows(toolbar.id);

		switch (mode) {
			case "text":
            case "icons":
                toolbar.setAttribute("mode", mode);
                break;
            case "iconsBesideText":
                toolbar.setAttribute("mode", "full");
                toolbar.setAttribute("labelalign", "end");
                break;
            case "iconsAndText":
                toolbar.setAttribute("mode", "full");
                toolbar.setAttribute("labelalign", "bottom");
                break;
        }

        let i = 0;
        for (let item of items) {
			if (item[2] == "flex") {
				let flex = document.createElement("div");
				flex.classList.add("fullWidth");
                flex.id = `${item[2]}_${i}`;
				toolbar.appendChild(flex);
			} else if (item[2] == "toolsep") {
				let separator = document.createElement("div");
				separator.classList.add("toolsep");
                separator.id = `${item[2]}_${i}`;
				toolbar.appendChild(separator);
			} else if (item[2] == "space") {
				let space = document.createElement("div");
				space.classList.add("space");
                space.id = `${item[2]}_${i}`;
				toolbar.appendChild(space);
			} else if (item[2] == "email" || item[2] == "layout" || item[2] == "sync" || item[2] == "chat") {
                let dropdown = document.createElement("div", {is: "cb-button-dropdown"});
                dropdown.setButtonLabel(item[0]);
                dropdown.setClass([ "down", "arrow-down" ]);
                if (mode != "text") {
                    dropdown.setClass([ item[2] ]);
                }
                dropdown.setMode(true);
                dropdown.id = `${item[2]}_${i}`;
                let button = dropdown.getButton();
				button.classList.add("CBToolbarButton");
				toolbar.appendChild(dropdown);
			} else {
				let button = document.createElement("button");
                button.textContent = item[0];
				button.title = item[1];
				button.classList.add(item[2]);
				button.classList.add("CBToolbarButton");
                button.id = `${item[2]}_${i}`;
				toolbar.appendChild(button);
            }
            i++;
		}
    },
}
