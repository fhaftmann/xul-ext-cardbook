import { cardbookHTMLDates } from "./cardbookHTMLDates.mjs";
import { cardbookHTMLUtils } from "./cardbookHTMLUtils.mjs";

export function ABView(cards, treeId, sortColumn, sortDirection) {
	this._tree = null;
	this.treeId = treeId;
	this._rowMap = [];
	this._persistOpenMap = [];

	this._searchesInProgress = 1;
	for (let card of cards) {
		this._rowMap.push(new abViewCard(card, treeId));
	}
	this._searchesInProgress = 0;
	this.sortBy(sortColumn, sortDirection);
}
ABView.NOT_SEARCHING = 0;
ABView.SEARCHING = 1;
ABView.SEARCH_COMPLETE = 2;
ABView.prototype = {
	get rowCount() {
		return this._rowMap.length;
	},

	/**
	 * CSS files will cue off of these.  Note that we reach into the rowMap's
	 * items so that custom data-displays can define their own properties
	 */
	getCellProperties(aRow, aCol) {
		return this._rowMap[aRow].getProperties(aCol);
	},

	/**
	 * The actual text to display in the tree
	 */
	getCellText(aRow, aCol) {
		return this._rowMap[aRow].getText(aCol.id);
	},

	getCellValue(aRow, aCol) {
		return this._rowMap[aRow].getValue(aCol.id);
	},

	/**
	 * The jstv items take care of assigning this when building children lists
	 */
	getLevel(aIndex) {
		return this._rowMap[aIndex].level;
	},

	/**
	 * This is easy since the jstv items assigned the _parent property when making
	 * the child lists
	 */
	getParentIndex(aIndex) {
		return this._rowMap.indexOf(this._rowMap[aIndex]._parent);
	},

	/**
	 * This is duplicative for our normal jstv views, but custom data-displays may
	 * want to do something special here
	 */
	getRowProperties(aRow) {
		return this._rowMap[aRow].getProperties();
	},

	/**
	 * If an item in our list has the same level and parent as us, it's a sibling
	 */
	hasNextSibling(aIndex, aNextIndex) {
		const targetLevel = this._rowMap[aIndex].level;
		for (let i = aNextIndex + 1; i < this._rowMap.length; i++) {
			if (this._rowMap[i].level == targetLevel) {
				return true;
			}
			if (this._rowMap[i].level < targetLevel) {
				return false;
			}
		}
		return false;
	},

	/**
	 * If we have a child-list with at least one element, we are a container.
	 */
	isContainer(aIndex) {
		return this._rowMap[aIndex].children.length > 0;
	},

	isContainerEmpty(aIndex) {
		// If the container has no children, the container is empty.
		return !this._rowMap[aIndex].children.length;
	},

	/**
	 * Just look at the jstv item here
	 */
	isContainerOpen(aIndex) {
		return this._rowMap[aIndex].open;
	},

	isEditable(aRow, aCol) {
		// We don't support editing rows in the tree yet.
		return false;
	},

	isSeparator(aIndex) {
		// There are no separators in our trees
		return false;
	},

	isSorted() {
		// We do our own customized sorting
		return false;
	},

	setTree(aTree) {
		this._tree = aTree;
	},

	recursivelyAddToMap(aChild, aNewIndex) {
		// When we add sub-children, we're going to need to increase our index
		// for the next add item at our own level.
		const currentCount = this._rowMap.length;
		if (aChild.children.length && aChild.open) {
			for (const [i, child] of this._rowMap[aNewIndex].children.entries()) {
				const index = aNewIndex + i + 1;
				this._rowMap.splice(index, 0, child);
				aNewIndex += this.recursivelyAddToMap(child, index);
			}
		}
		return this._rowMap.length - currentCount;
	},

	/**
	 * Opens or closes a container with children.  The logic here is a bit hairy, so
	 * be very careful about changing anything.
	 */
	toggleOpenState(aIndex) {
		// Ok, this is a bit tricky.
		this._rowMap[aIndex]._open = !this._rowMap[aIndex].open;

		if (!this._rowMap[aIndex].open) {
			// We're closing the current container.  Remove the children

			// Note that we can't simply splice out children.length, because some of
			// them might have children too.  Find out how many items we're actually
			// going to splice
			const level = this._rowMap[aIndex].level;
			let row = aIndex + 1;
			while (row < this._rowMap.length && this._rowMap[row].level > level) {
				row++;
			}
			const count = row - aIndex - 1;
			this._rowMap.splice(aIndex + 1, count);

			// Remove us from the persist map
			const index = this._persistOpenMap.indexOf(this._rowMap[aIndex].id);
			if (index != -1) {
				this._persistOpenMap.splice(index, 1);
			}

			// Notify the tree of changes
			if (this._tree) {
				this._tree.rowCountChanged(aIndex + 1, -count);
			}
		} else {
			// We're opening the container.  Add the children to our map

			// Note that these children may have been open when we were last closed,
			// and if they are, we also have to add those grandchildren to the map
			const oldCount = this._rowMap.length;
			this.recursivelyAddToMap(this._rowMap[aIndex], aIndex);

			// Add this container to the persist map
			const id = this._rowMap[aIndex].id;
			if (!this._persistOpenMap.includes(id)) {
				this._persistOpenMap.push(id);
			}

			// Notify the tree of changes
			if (this._tree) {
				this._tree.rowCountChanged(aIndex + 1, this._rowMap.length - oldCount);
			}
		}

		// Invalidate the toggled row, so that the open/closed marker changes
		if (this._tree) {
			this._tree.invalidateRow(aIndex);
		}
	},

	// We don't implement any of these at the moment
	canDrop(aIndex, aOrientation) {},
	drop(aRow, aOrientation) {},
	selectionChanged() {},
	setCellText(aRow, aCol, aValue) {},
	setCellValue(aRow, aCol, aValue) {},
	getColumnProperties(aCol) {
		return "";
	},
	getImageSrc(aRow, aCol) {},
	getProgressMode(aRow, aCol) {},
	cycleCell(aRow, aCol) {},
	cycleHeader(aCol) {},

	_tree: null,

	/**
	 * An array of jstv items, where each item corresponds to a row in the tree
	 */
	_rowMap: null,

	/**
	 * This is a javascript map of which containers we had open, so that we can
	 * persist their state over-time.  It is designed to be used as a JSON object.
	 */
	_persistOpenMap: null,

	_restoreOpenStates() {
		// Note that as we iterate through here, .length may grow
		for (let i = 0; i < this._rowMap.length; i++) {
			if (this._persistOpenMap.includes(this._rowMap[i].id)) {
				this.toggleOpenState(i);
			}
		}
	},

	sortColumn: "",
	sortDirection: "",

	getCardFromRow(row) {
		return this._rowMap[row] ? this._rowMap[row].card : null;
	},

	getIndexForUID(uid) {
		return this._rowMap.findIndex(row => row.id == uid);
	},

	sortBy(sortColumn, sortDirection, resort) {
		let selectionExists = false;
		if (this._tree) {
			let { selectedIndices, currentIndex } = this._tree;
			selectionExists = selectedIndices.length;
			// Remember what was selected.
			for (let i = 0; i < this._rowMap.length; i++) {
				this._rowMap[i].wasSelected = selectedIndices.includes(i);
				this._rowMap[i].wasCurrent = currentIndex == i;
			}
		}

		// Do the sort.
		if (sortColumn == this.sortColumn && !resort) {
			if (sortDirection == this.sortDirection) {
				return;
			}
			this._rowMap.reverse();
		} else {
			this._rowMap.sort((a, b) => {
				let regexp = new RegExp(`^${this.treeId}`);
				let sortColumn1 = sortColumn.replace(regexp, "");
				if (sortColumn1 == "name") {
					let aText = cardbookHTMLUtils.getName(a.card);
					let bText = cardbookHTMLUtils.getName(b.card);
					if (sortDirection == "descending") {
						return aText.localeCompare(bText);
					} else {
						return bText.localeCompare(aText);
					}
				} else if (sortColumn1 == "cardIcon") {
					let aText = a.card.isEmail ? 2 : a.card.isAList ? 1 : 0;
					let bText = b.card.isEmail ? 2 : b.card.isAList ? 1 : 0;
					if (sortDirection == "descending") {
						return (aText - bText);
					} else {
						return (bText - aText);
					}
				} else if (sortColumn1 == "gender") {
					let genderLookup = { "F": messenger.i18n.getMessage("types.gender.f"),
											"M": messenger.i18n.getMessage("types.gender.m"),
											"N": messenger.i18n.getMessage("types.gender.n"),
											"O": messenger.i18n.getMessage("types.gender.o"),
											"U": messenger.i18n.getMessage("types.gender.u") };
					let aText = genderLookup[a.card.gender];
					let bText = genderLookup[b.card.gender];
					if (sortDirection == "descending") {
						return aText.localeCompare(bText);
					} else {
						return bText.localeCompare(aText);
					}
				} else if (sortColumn1 == "bday" || sortColumn1 == "anniversary" || sortColumn1 == "deathdate" || sortColumn1 == "rev") {
					let aText = cardbookHTMLDates.getDateForCompare(a.card, sortColumn1);
					let bText = cardbookHTMLDates.getDateForCompare(b.card, sortColumn1);
					if (sortDirection == "descending") {
						return aText > bText;
					} else {
						return bText > aText;
					}
				} else {
					let aTextArray = cardbookHTMLUtils.getCardValueByField(a.card, sortColumn1, false);
					let aText = aTextArray.join();
					let bTextArray = cardbookHTMLUtils.getCardValueByField(b.card, sortColumn1, false);
					let bText = bTextArray.join();
					if (sortDirection == "descending") {
						return aText.localeCompare(bText);
					} else {
						return bText.localeCompare(aText);
					}
				}
			});
		}

		// Restore what was selected.
		if (this._tree) {
			this._tree.invalidate();
			if (selectionExists) {
				for (let i = 0; i < this._rowMap.length; i++) {
					this._tree.toggleSelectionAtIndex(i, this._rowMap[i].wasSelected, true);
				}
				// Can't do this until updating the selection is finished.
				for (let i = 0; i < this._rowMap.length; i++) {
					if (this._rowMap[i].wasCurrent) {
						this._tree.currentIndex = i;
						break;
					}
				}
				this.selectionChanged();
			}
		}
		this.sortColumn = sortColumn;
		this.sortDirection = sortDirection;
	},

	get searchState() {
		if (this._searchesInProgress === undefined) {
			return ABView.NOT_SEARCHING;
		}
		return this._searchesInProgress ? ABView.SEARCHING : ABView.SEARCH_COMPLETE;
	},

	// nsITreeView
	selectionChanged() {},
	setTree(tree) {
		this._tree = tree;
	},
};

export function abViewCard(card, treeId) {
	this.card = card;
	this.treeId = treeId;
	this._getTextCache = {};
}
abViewCard.prototype = {
	_getText(columnID) {
		try {
			if (columnID == "cardIcon") return "";
			else if (columnID == "name") return cardbookHTMLUtils.getName(this.card);
			else if (columnID == "gender") {
				let genderLookup = { "F": messenger.i18n.getMessage("types.gender.f"),
				"M": messenger.i18n.getMessage("types.gender.m"),
				"N": messenger.i18n.getMessage("types.gender.n"),
				"O": messenger.i18n.getMessage("types.gender.o"),
				"U": messenger.i18n.getMessage("types.gender.u") };
				return genderLookup[this.card.gender];
			} else if (columnID == "bday") return cardbookHTMLDates.getFormattedDateForCard(this.card, columnID);
			else if (columnID == "anniversary") return cardbookHTMLDates.getFormattedDateForCard(this.card, columnID);
			else if (columnID == "deathdate") return cardbookHTMLDates.getFormattedDateForCard(this.card, columnID);
			else if (columnID == "rev") return cardbookHTMLDates.getFormattedDateForCard(this.card, columnID);
			else {
				let textArray = cardbookHTMLUtils.getCardValueByField(this.card, columnID, false);
				return textArray.join();
			}
		} catch (ex) {
			return "";
		}
	},

	getText(columnID) {
		let regexp = new RegExp(`^${this.treeId}`);
		let colName = columnID.replace(regexp, "");
		if (!(colName in this._getTextCache)) {
			this._getTextCache[colName] = this._getText(colName)?.trim() ?? "";
		}
		return this._getTextCache[colName];
	},

	get id() {
		return this.card.uid;
	},

	get open() {
		return false;
	},

	get level() {
		return 0;
	},

	get children() {
		return [];
	},

	getProperties() {
		return "";
	},
};
