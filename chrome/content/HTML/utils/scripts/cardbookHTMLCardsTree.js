import { ABView }  from "./cardbookHTMLCardsView.js";
import { cardbookHTMLUtils } from "./cardbookHTMLUtils.mjs";

import { cardbookBGPreferences } from "../../../BG/utils/cardbookBGPreferences.mjs";

export function CBTree(dirPrefId, tablename, availableColumns, events, sortResource, sortDirection, treeColumns, setCSS, applyColumns) {
	this.columns = [];
	this.sortResource = "";
	this.sortDirection = "";
	this.availableColumns = [];

	this.copyKey = events.copyKey || null;
	this.pasteKey = events.pasteKey || null;
	this.cutKey = events.cutKey || null;
	this.deleteKey = events.deleteKey || null;
	this.enterKey = events.enterKey || null;
	this.dblclick = events.dblclick || null;
	this.dragstart = events.dragstart || null;
	this.dragover = events.dragover || null;
	this.drop = events.drop || null;
	this.select = events.select || null;
	this.applyTo = events.applyTo || null;

	this.getSortResource = sortResource?.get || this._getDefaultSortResource;
	this.getSortDirection = sortDirection?.get || this._getDefaultSortDirection;
	this.setSortResource = sortResource?.set || this._setDefaultSortResource;
	this.setSortDirection = sortDirection?.set || this._setDefaultSortDirection;

	this.getTreeColumns = treeColumns?.get || this._getTreeColumns;
	this.setTreeColumns = treeColumns?.set || this._setTreeColumns;

	this.applyColumns = applyColumns || false;
	this.setCSSRow = setCSS?.row || null;

	this.dirPrefId = dirPrefId;

	this.cardsList = document.getElementById(tablename);
	if (availableColumns) {
		this.availableColumns = availableColumns;
	}

	this.init();
}
CBTree.prototype = {
	init() {
		this.table = this.cardsList.table;
		this.table.editable = true;
		this.table.setBodyID(`${this.cardsList.id}Body`);
		this.cardsList.setAttribute("rows", "cb-table-card-row");
		this.table.applyColumns = this.applyColumns;

		this.table.setCSSRow = this.setCSSRow;

		this.table.addEventListener("columns-changed", this);
		this.table.addEventListener("sort-changed", this);
		this.table.addEventListener("column-resized", this);
		this.table.addEventListener("reorder-columns", this);
		this.table.addEventListener("restore-columns", this);
		this.table.addEventListener("applyTo-columns", this);
		this.cardsList.addEventListener("keydown", this);
		this.cardsList.addEventListener("dblclick", this);
		this.cardsList.addEventListener("select", this);
		this.cardsList.addEventListener("dragstart", this);
		this.cardsList.addEventListener("dragover", this);
		this.cardsList.addEventListener("drop", this);

		window.addEventListener("uidensitychange", () => this.densityChange());
		customElements.whenDefined("cb-table-card-row").then(() => this.densityChange());
	},

	handleEvent(event) {
		switch (event.type) {
			case "keydown":
				this._onKeyDown(event);
				break;
			case "dblclick":
				if (this.dblclick) {
					this.dblclick(event);
				}
				break;
			case "select":
				if (this.select) {
					this.select(event);
				}
				break;
			case "dragstart":
				if (this.dragstart) {
					this.dragstart(event);
				}
				break;
			case "dragover":
				if (this.dragover) {
					this.dragover(event);
				}
				break;
			case "drop":
				if (this.drop) {
					this.drop(event);
				}
				break;
			case "columns-changed":
				this._onColumnsChanged(event.detail);
				break;
			case "sort-changed":
				this._onSortChanged(event);
				break;
			case "column-resized":
				this._onColumnResized(event);
				break;
			case "reorder-columns":
				this._onReorderColumns(event.detail);
				break;
			case "restore-columns":
				this._onRestoreDefaultColumns();
				break;
			case "applyTo-columns":
				this.applyTo(event);
				break;
		}
	},

	_onKeyDown(event) {
		if (event.altKey || event.shiftKey) {
			return;
		}
		let modifier = event.ctrlKey;
		let antiModifier = event.metaKey;
		if (navigator.userAgent.indexOf("Mac") !== -1) {
			[modifier, antiModifier] = [antiModifier, modifier];
		}
		if (antiModifier) {
			return;
		}
		switch (event.key) {
			case "a":
				if (modifier) {
					this.cardsList.view.selection.selectAll();
					this.cardsList.dispatchEvent(new CustomEvent("select"));
					event.preventDefault();
				}
				break;
			case "c":
				if (modifier) {
					if (this.copyKey) {
						this.copyKey(event);
					}
				}
				break;
			case "v":
				if (modifier) {
					if (this.pasteKey) {
						this.pasteKey(event);
					}
				}
				break;
			case "x":
				if (modifier) {
					if (this.cutKey) {
						this.cutKey(event);
					}
				}
				break;
			case "Delete":
				if (!modifier) {
					if (this.deleteKey) {
						this.deleteKey(event);
					}
					event.preventDefault();
				}
				break;
			case "Enter":
				if (!modifier) {
					if (this.enterKey) {
						this.enterKey(event);
					}
					event.preventDefault();
				}
				break;
		}
	},

	async _onRestoreDefaultColumns() {
		this.setSortResource = this._setDefaultSortResource;
		this.setSortDirection =this._setDefaultSortDirection;
		let defaultColumns = cardbookBGPreferences.getPref(`${this.cardsList.id}DefaultColumns`).split(",");
		defaultColumns = defaultColumns.map(x => this.cardsList.id + x.split(":")[0]);

		function arraymove(arr, from, to) {
			var element = arr[from];
			arr.splice(from, 1);
			arr.splice(to, 0, element);
		}
		let index = 0;
		for (let column of defaultColumns) {
			let from = this.columns.map(element => element.id).indexOf(column);
			arraymove(this.columns, from, index);
			index++;
		}
		let ordinal = 0
		for (let column of this.columns) {
			column.hidden = !defaultColumns.includes(column.id);
			column.ordinal = ordinal++;
		}

		this.table.setColumns(this.columns);
		this.table.querySelector(".sorting")?.classList.remove("sorting", "ascending", "descending");
		this.table.querySelector(`#${this.cardsList.id}${this.sortResource} button`)?.classList.add("sorting", this.sortDirection);
		this.cardsList.invalidate();
		await this.setColumnsWidths(this.cardsList.id, this.table.getColumnsWidths());
	},

	async _onColumnResized(event) {
		await this.setColumnsWidths(this.cardsList.id, this.table.getColumnsWidths());
	},

	async _onSortChanged(event) {
		if (this.cardsList.view) {
			const { sortColumn, sortDirection } = this.cardsList.view;
			const column = event.detail.column;
			await this.sortRows(column, sortColumn == column && sortDirection == "ascending" ? "descending" : "ascending");
		}
	},

	async _onColumnsChanged(data) {
		let column = data.value;
		let checked = data.target.classList.contains("active");
		let visibleOrdinal = this.columns.filter(x => x.hidden === false).map(x => x.ordinal);
		let valuesVisibleOrdinal = Object.values(visibleOrdinal);
		let maxVisibleOrdinal = Math.max(...valuesVisibleOrdinal);

		for (let columnDef of this.columns) {
			if (columnDef.id == column) {
				columnDef.hidden = !checked;
				columnDef.ordinal = maxVisibleOrdinal++;
				break;
			}
		}
		this.table.updateColumns(this.columns);
		this.cardsList.invalidate();
		await this.setColumnsWidths(this.cardsList.id, this.table.getColumnsWidths());
	},

	async _onReorderColumns(data) {
		this.columns = data.columns;
		this.table.updateColumns(this.columns);
		this.cardsList.invalidate();
		await this.setColumnsWidths(this.cardsList.id, this.table.getColumnsWidths());
	},

	_getTreeColumns() {
		return cardbookBGPreferences.getPref(`${this.cardsList.id}DefaultColumns`);
	},

	async _setTreeColumns(aValue) {
		await cardbookBGPreferences.setPref(`${this.cardsList.id}DefaultColumns`, aValue);
	},

	async setColumnsWidths() {
		let columns = this.table.getColumnsWidths()
		let regexp = new RegExp(`^${this.cardsList.id}`);
		columns = columns.map(x => x.replace(regexp, ""));
		await this.setTreeColumns(columns.join(","), this.cardsList.id, this.dirPrefId);
	},

	_getDefaultSortResource() {
		let sortResource = cardbookBGPreferences.getPref(`${this.cardsList.id}SortResource`);
		return sortResource;
	},

	async _setDefaultSortResource(aValue) {
		await cardbookBGPreferences.setPref(`${this.cardsList.id}SortResource`, aValue);
	},

	_getDefaultSortDirection() {
		return cardbookBGPreferences.getPref(`${this.cardsList.id}SortDirection`);
	},

	async _setDefaultSortDirection(aValue) {
		await cardbookBGPreferences.setPref(`${this.cardsList.id}SortDirection`, aValue);
	},

	setHeader() {
		let columns = this.getTreeColumns(this.cardsList.id, this.dirPrefId);
		this.sortResource = this.getSortResource(this.cardsList.id, this.dirPrefId);
		this.sortDirection = this.getSortDirection(this.cardsList.id, this.dirPrefId);
		this.columns = [];
		let savedColumns = {};
		let ordinal = 0;
		for (let col of columns.split(",")) {
			let colName = col.split(":")[0];
			let colId = `${this.cardsList.id}${colName}`;
			let colWidth = typeof col.split(":")[1] !== 'undefined' ? col.split(":")[1] : 0;
			savedColumns[colId] = {};
			savedColumns[colId].width = colWidth;
			savedColumns[colId].ordinal = ordinal++;
		}

		let allColumns = [];
		if (this.availableColumns.length) {
			allColumns = JSON.parse(JSON.stringify(this.availableColumns));
		} else {
			allColumns = cardbookHTMLUtils.getAllAvailableColumns("cardstree");
		}
		for (let column of allColumns) {
			let col = {};
			col.id = `${this.cardsList.id}${column[0]}`;
			col.hidden = typeof savedColumns[col.id] !== 'undefined' ? false : true;
			col.width = typeof savedColumns[col.id] !== 'undefined' ? savedColumns[col.id].width : 0;
			col.ordinal = typeof savedColumns[col.id] !== 'undefined' ? savedColumns[col.id].ordinal : allColumns.length + 1;
			col.label = column[1];
			this.columns.push(col);
		}
		this.columns.sort(function(a, b) {
			return a.ordinal-b.ordinal
		})
		this.table.setColumns(this.columns);
		this.table.restoreColumnsWidths(savedColumns);
	},

	async displayBook(cards) {
		this.setHeader();
		this.cardsList.view = new ABView(cards, this.cardsList.id, this.sortResource, this.sortDirection);
		await this.sortRows(`${this.cardsList.id}${this.sortResource}`, this.sortDirection);
	},

	async sortRows(column, direction) {
		// Unmark the header of previously sorted column, then mark the header of
		// the column to be sorted.
		this.table.querySelector(".sorting")?.classList.remove("sorting", "ascending", "descending");
		this.table.querySelector(`#${column} button`)?.classList.add("sorting", direction);
		if (this.cardsList.view.sortColumn == column && this.cardsList.view.sortDirection == direction) {
			return;
		}

		this.cardsList.view.sortBy(column, direction);

		let regexp1 = new RegExp(`^${this.cardsList.id}`);
		let regexp2 = new RegExp(`Button$`);
		let sortResource = column.replace(regexp1, "").replace(regexp2, "");
		await this.setSortResource(sortResource, this.cardsList.id, this.dirPrefId);
		await this.setSortDirection(direction, this.cardsList.id, this.dirPrefId);
	},

	updatePlaceholder(idsToShow) {
		this.cardsList.updatePlaceholders(idsToShow);
	},

	async densityChange() {
		let tableRowClass = customElements.get("cb-table-card-row");
		let density = await messenger.runtime.sendMessage({ query: "cardbook.getDensity"});
		switch (density) {
			case 0: // MODE_COMPACT
				tableRowClass.ROW_HEIGHT = 18;
				break;
			case 2: // MODE_TOUCH
				tableRowClass.ROW_HEIGHT = 32;
				break;
			default: // MODE_NORMAL
				tableRowClass.ROW_HEIGHT = 22;
				break;
		}
		this.cardsList.invalidate();
	},
};
