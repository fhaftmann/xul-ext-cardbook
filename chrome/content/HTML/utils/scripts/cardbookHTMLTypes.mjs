import { cardbookBGPreferences } from "../../../BG/utils/cardbookBGPreferences.mjs";
import { cardbookHTMLUtils } from "./cardbookHTMLUtils.mjs";

export var cardbookHTMLTypes = {
	
	cardbookCoreTypes: { "GOOGLE2": { "adr" : [ ["hometype", "home"], ["worktype", "work"], ["othertype", "other"] ],
										"email" : [ ["hometype", "home"], ["worktype", "work"], ["othertype", "other"] ],
										"tel" : [ ["hometype", "home"], ["worktype", "work"], ["celltype", "mobile"], ["pagertype", "pager"], ["workfaxtype", "workFax"], ["homefaxtype", "homeFax"], ["othertype", "other"], ["maintype", "main"] ],
 										"url" : [ ["hometype", "home"], ["worktype", "work"], ["othertype", "other"], ["blogtype", "blog"], ["homepagetype", "homePage"], ["profiletype", "profile"] ],
										"impp" : [ ["hometype", "home"], ["worktype", "work"], ["othertype", "other"] ],
										"addnew" : true },
						"GOOGLE3": { "adr" : [ ["hometype", "home"], ["worktype", "work"], ["othertype", "other"] ],
										"email" : [ ["hometype", "home"], ["worktype", "work"], ["othertype", "other"] ],
										"tel" : [ ["hometype", "home"], ["worktype", "work"], ["celltype", "mobile"], ["pagertype", "pager"], ["workfaxtype", "workFax"], ["homefaxtype", "homeFax"], ["othertype", "other"], ["maintype", "main"] ],
 										"url" : [ ["hometype", "home"], ["worktype", "work"], ["othertype", "other"], ["blogtype", "blog"], ["homepagetype", "homePage"], ["profiletype", "profile"] ],
										"impp" : [ ["hometype", "home"], ["worktype", "work"], ["othertype", "other"] ],
										"addnew" : true },
						"APPLE": { "adr" : [ ["hometype", "HOME"], ["worktype", "WORK"] ],
									"email" : [ ["hometype", "HOME;HOME,INTERNET"], ["worktype", "WORK;WORK,INTERNET"], ["othertype", "OTHER;OTHER,INTERNET"] ],
									"tel" : [ ["hometype", "HOME;HOME,VOICE"], ["worktype", "WORK;WORK,VOICE"], ["celltype", "CELL;CELL,VOICE"], ["faxtype", "FAX;FAX,VOICE"], ["pagertype", "PAGER"],
												["workfaxtype", "FAX,WORK;FAX,WORK,VOICE"], ["homefaxtype", "FAX,HOME;FAX,HOME,VOICE"],
												["othertype", "OTHER;OTHER,VOICE"], ["maintype", "MAIN"], ["iphonetype", "CELL,IPHONE;CELL,IPHONE,VOICE"] ],
									"url" : [ ["hometype", "HOME"], ["worktype", "WORK"], ["othertype", "OTHER"] ],
									"impp" : [ ["hometype", "HOME"], ["worktype", "WORK"] ],
									"addnew" : true },
						"YAHOO": { "adr" : [ ["hometype", "HOME;HOME,POSTAL,PARCEL,WORK"], ["worktype", "WORK;WORK,POSTAL,PARCEL"] ],
									"email" : [ ["hometype", "HOME;HOME,INTERNET"], ["worktype", "WORK;WORK,INTERNET"] ],
									"tel" : [ ["hometype", "HOME"], ["worktype", "WORK"], ["faxtype", "FAX"], ["celltype", "CELL"] ],
									"url" : [ ["hometype", "HOME"], ["worktype", "WORK"] ],
									"impp" : [ ["hometype", "HOME"], ["worktype", "WORK"] ],
									"addnew" : false },
						"OFFICE365": { "adr" : [ ["hometype", "HOME"], ["worktype", "WORK"], ["othertype", "OTHER"] ],
									"email" : [ ],
									"tel" : [ ["hometype", "HOME"], ["worktype", "WORK"], ["faxtype", "FAX"], ["celltype", "CELL"], ["workfaxtype", "FAX,WORK"],
												["homefaxtype", "FAX,HOME"], ["otherfaxtype", "FAX,OTHER"], ["othertype", "OTHER"], ["assistanttype", "ASSISTANT"],
												["callbacktype", "CALLBACK"], ["carphonetype", "CARPHONE"], ["pagertype", "PAGER"], ["radiotype", "RADIO"],
												["telextype", "TELEX"], ["ttytype", "TTY"], ],
									"url" : [ ],
									"impp" : [ ],
									"addnew" : false },
						"CARDDAV": { "adr" : [ ["hometype", "HOME"], ["worktype", "WORK"] ],
									"email" : [ ["hometype", "HOME"], ["worktype", "WORK"], ["othertype", "OTHER"] ],
									"tel" : [ ["hometype", "HOME;HOME,VOICE"], ["worktype", "WORK;WORK,VOICE"], ["celltype", "CELL;CELL,IPHONE;CELL,VOICE"], ["faxtype", "FAX;FAX,VOICE"], ["pagertype", "PAGER"], ["workfaxtype", "FAX,WORK;FAX,WORK,VOICE"], ["homefaxtype", "FAX,HOME;FAX,HOME,VOICE"],
												["othertype", "OTHER;OTHER,VOICE"], ["maintype", "MAIN"] ],
									"url" : [ ["hometype", "HOME"], ["worktype", "WORK"], ["othertype", "OTHER"] ],
									"impp" : [ ["hometype", "HOME"], ["worktype", "WORK"] ],
									"addnew" : true } },
	
	rebuildAllPGs: function (aCard) {
		let myPgNumber = 1;
		for (let field of cardbookHTMLUtils.multilineFields) {
			for (var j = 0; j < aCard[field].length; j++) {
				let myTempString = aCard[field][j][2];
				if (myTempString.startsWith("ITEM")) {
					aCard[field][j][2] = "ITEM" + myPgNumber;
					myPgNumber++;
				}
			}
		}
		let myNewOthers = [];
		let myPGMap = {};
		for (var j = 0; j < aCard.others.length; j++) {
			let myTempString = aCard.others[j];
			var relative = []
			relative = myTempString.match(/^ITEM([0-9]*)\.(.*)/i);
			if (relative && relative[1] && relative[2]) {
				if (myPGMap[relative[1]]) {
					myNewOthers.push("ITEM" + myPGMap[relative[1]] + "." + relative[2]);
				} else {
					myNewOthers.push("ITEM" + myPgNumber + "." + relative[2]);
					myPGMap[relative[1]] = myPgNumber;
					myPgNumber++;
				}
			} else {
				myNewOthers.push(aCard.others[j]);
			}
		}
		aCard.others = JSON.parse(JSON.stringify(myNewOthers));
		return myPgNumber;
	},

	whichCodeTypeShouldBeChecked: function (aType, aDirPrefId, aSourceArray, aSourceList) {
		if (aSourceArray.length == 0) {
			return {result: "", isAPg: false, isAlreadyThere: false};
		} else {
			var ABType = cardbookBGPreferences.getType(aDirPrefId);
			var ABTypeFormat = cardbookHTMLUtils.getABTypeFormat(ABType);
			var match = false;
			for (var i = 0; i < cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType].length && !match; i++) {
				var code = cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType][i][0];
				var types = cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType][i][1];
				var possibilities = types.split(";").map(value => value.toUpperCase());
				for (var j = 0; j < possibilities.length && !match; j++) {
					var possibility = possibilities[j].split(",");
					for (var k = 0; k < aSourceArray.length; k++) {
						if (possibility.indexOf(aSourceArray[k].toUpperCase()) == -1) {
							break;
						} else if (possibility.indexOf(aSourceArray[k].toUpperCase()) != -1 && k == aSourceArray.length - 1 ) {
							// here we are sure that aSourceArray in included in possibility
							if (aSourceArray.length == possibility.length) {
								match = true;
								if (cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType][i][2] && cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType][i][2] == "PG") {
									return {result: code, isAPg: true, isAlreadyThere: true};
								} else {
									return {result: code, isAPg: false};
								}
							}
						}
					}
				}
			}
			// the strange string may already be in the basic translated strings
			for (var i = 0; i < aSourceList.length; i++) {
				for (var j = 0; j < aSourceArray.length; j++) {
					if (aSourceArray[j].toUpperCase() == aSourceList[i][0].toUpperCase()) {
						return {result: aSourceList[i][1], isAPg: false};
					}
				}
			}
			return {result: aSourceArray[0], isAPg: true, isAlreadyThere: false};
		}
	},

	whichLabelTypeShouldBeChecked: function (aType, aDirPrefId, aSourceArray) {
		if (aSourceArray.length == 0) {
			return "";
		} else {
			var ABType = cardbookBGPreferences.getType(aDirPrefId);
			var ABTypeFormat = cardbookHTMLUtils.getABTypeFormat(ABType);
			var match = false;
			for (var i = 0; i < cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType].length && !match; i++) {
				var code = cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType][i][0];
				var types = cardbookHTMLTypes.cardbookCoreTypes[ABTypeFormat][aType][i][1];
				var possibilities = types.split(";").map(value => value.toUpperCase());
				for (var j = 0; j < possibilities.length && !match; j++) {
					var possibility = possibilities[j].split(",");
					for (var k = 0; k < aSourceArray.length; k++) {
						if (possibility.indexOf(aSourceArray[k].toUpperCase()) == -1) {
							break;
						} else if (possibility.indexOf(aSourceArray[k].toUpperCase()) != -1 && k == aSourceArray.length - 1 ) {
							// here we are sure that aSourceArray in included in possibility
							if (aSourceArray.length == possibility.length) {
								match = true;
								return cardbookHTMLTypes.getTypeLabelFromTypeCode(ABTypeFormat, aType, code);
							}
						}
					}
				}
			}
			return aSourceArray[0];
		}
	},

	isMyCodePresent: function (aType, aCode, aABTypeFormat, aSourceArray) {
		var match = false;
		for (var i = 0; i < cardbookHTMLTypes.cardbookCoreTypes[aABTypeFormat][aType].length && !match; i++) {
			var code = cardbookHTMLTypes.cardbookCoreTypes[aABTypeFormat][aType][i][0];
			if (code.toUpperCase() != aCode.toUpperCase()) {
				continue;
			}
			var types = cardbookHTMLTypes.cardbookCoreTypes[aABTypeFormat][aType][i][1];
			var possibilities = types.split(";").map(value => value.toUpperCase());
			for (var j = 0; j < possibilities.length && !match; j++) {
				var possibility = possibilities[j].split(",");
				for (var k = 0; k < aSourceArray.length; k++) {
					if (possibility.indexOf(aSourceArray[k].toUpperCase()) == -1) {
						break;
					} else if (possibility.indexOf(aSourceArray[k].toUpperCase()) != -1 && k == aSourceArray.length - 1 ) {
						// here we are sure that aSourceArray in included in possibility
						if (aSourceArray.length == possibility.length) {
							return true;
						}
					}
				}
			}
		}
		return false;
	},

	getTypeLabelFromTypeCode: function (aABType, aType, aTypeCode) {
		let pref = `${cardbookBGPreferences.prefCardBookCustomTypes}${aABType}.${aType}.${aTypeCode}.value`;
		let prefResult = cardbookBGPreferences.getPref(pref);
		if (prefResult) {
			return prefResult;
		} else {
			return messenger.i18n.getMessage(aTypeCode);
		}
	},

	getTypeDisabledFromTypeCode: function (aABType, aType, aTypeCode) {
		let pref = `${cardbookBGPreferences.prefCardBookCustomTypes}${aABType}.${aType}.${aTypeCode}.disabled`;
		return cardbookBGPreferences.getPref(pref) ?? false;
	},

	getTypes: function (aABType, aType, aResetToCore) {
		var result = [];
		for (let k = 0; k < cardbookHTMLTypes.cardbookCoreTypes[aABType][aType].length; k++) {
			var myCoreCodeType = cardbookHTMLTypes.cardbookCoreTypes[aABType][aType][k][0];
			var myDisabled = cardbookHTMLTypes.getTypeDisabledFromTypeCode(aABType, aType, myCoreCodeType);
			if (!myDisabled || aResetToCore) {
				var myLabel = cardbookHTMLTypes.getTypeLabelFromTypeCode(aABType, aType, myCoreCodeType);
				result.push([myLabel, myCoreCodeType]);
			}
		}
		if (!aResetToCore) {
			var tmpArray = [];
			for (const [key, value] of cardbookBGPreferences.getBranch(`customTypes.${aABType}.${aType}`)) {
				if (key.endsWith(".value")) {
					tmpArray.push(key.replace("customTypes." + aABType + "." + aType + ".", "").replace(".value", ""));
				}
			}
			for (let k = 0; k < tmpArray.length; k++) {
				var myCustomType = tmpArray[k];
				var isItACore = false;
				for (let l = 0; l < cardbookHTMLTypes.cardbookCoreTypes[aABType][aType].length; l++) {
					var myCoreCodeType = cardbookHTMLTypes.cardbookCoreTypes[aABType][aType][l][0];
					if (myCustomType == myCoreCodeType) {
						isItACore = true;
						break;
					}
				}
				if (!isItACore) {
					result.push([myCustomType, myCustomType]);
				}
			}
		}
		return result;
	},

	getTypesFromDirPrefId: function (aType, aDirPrefId) {
		var result = [];
		if (aDirPrefId) {
            var myABType = cardbookBGPreferences.getType(aDirPrefId);
			var myABTypeFormat = cardbookHTMLUtils.getABTypeFormat(myABType);
		} else {
			var myABTypeFormat = "CARDDAV";
		}
		result = cardbookHTMLTypes.getTypes(myABTypeFormat, aType, false);
		return result;
	},

	getCodeType: function (aType, aDirPrefId, aLine) {
		let myInputTypes = [];
		myInputTypes = cardbookHTMLTypes.getOnlyTypesFromTypes(aLine);
		let sourceList = cardbookHTMLTypes.getTypesFromDirPrefId(aType, aDirPrefId);
		cardbookHTMLUtils.sortMultipleArrayByString(sourceList, 0, 1);
		return cardbookHTMLTypes.whichCodeTypeShouldBeChecked(aType, aDirPrefId, myInputTypes, sourceList);
	},

	getIMPPLineForCode: function (aCode) {
		var serviceLine = [];
		var myPrefResults = [];
		myPrefResults = cardbookBGPreferences.getAllIMPPs();
		for (var i = 0; i < myPrefResults.length; i++) {
			if (aCode.toLowerCase() == myPrefResults[i][0].toLowerCase()) {
				serviceLine = [myPrefResults[i][0], myPrefResults[i][1], myPrefResults[i][2]];
				break;
			}
		}
		return serviceLine;
	},

	getIMPPLineForProtocol: function (aProtocol) {
		var serviceLine = [];
		var myPrefResults = [];
		myPrefResults = cardbookBGPreferences.getAllIMPPs();
		for (var i = 0; i < myPrefResults.length; i++) {
			if (aProtocol.toLowerCase() == myPrefResults[i][2].toLowerCase()) {
				serviceLine = [myPrefResults[i][0], myPrefResults[i][1], myPrefResults[i][2]];
				break;
			}
		}
		return serviceLine;
	},

	getIMPPCode: function (aInputTypes) {
		var serviceCode = "";
		for (var j = 0; j < aInputTypes.length; j++) {
			serviceCode = aInputTypes[j].replace(/^X-SERVICE-TYPE=/i, "");
			if (serviceCode != aInputTypes[j]) {
				break;
			} else {
				serviceCode = "";
			}
		}
		return serviceCode;
	},

	getIMPPProtocol: function (aCardValue) {
		var serviceProtocol = "";
		if (aCardValue[0].indexOf(":") >= 0) {
			serviceProtocol = aCardValue[0].split(":")[0];
		}
		return serviceProtocol;
	},

	// aTypesList should be escaped
	// TYPE="WORK,VOICE" would be splitted into TYPE=WORK,TYPE=HOME
	// the duplicate types would also be removed
	formatTypes: function (aTypesList) {
		var result = [];
		for (var i = 0; i < aTypesList.length; i++) {
			var myTempString = aTypesList[i].replace(/\"/g,"");
			if ((myTempString.indexOf(",") != -1) && (myTempString.indexOf("TYPE=",0) == 0)) {
				var myTempArray = myTempString.replace(/^TYPE=/, "").split(",");
				for (var j = 0; j < myTempArray.length; j++) {
					result.push("TYPE=" + myTempArray[j]);
				}
			} else if (myTempString && myTempString != "") {
				result.push(myTempString);
			}
		}
		return cardbookHTMLUtils.arrayUnique(result);
	}
};
