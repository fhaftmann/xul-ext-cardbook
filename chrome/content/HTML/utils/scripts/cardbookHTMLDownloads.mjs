import { cardbookHTMLPluralRules } from "./cardbookHTMLPluralRules.mjs"

export var cardbookHTMLDownloads = {

    download: async function (aUrl, aFileName, aActionId, aCode, aNumber) {
        try {
            let downloadingId = await browser.downloads.download({url: aUrl, filename: aFileName, saveAs: true});
            let waitTimer = setInterval( async function() {
                let search = await browser.downloads.search({id: downloadingId});
                if (search.length) {
                    let download = search[0];
                    if (download.state = "complete") {
                        await cardbookHTMLDownloads.finishExport("OK", aCode, aActionId, download.filename, aNumber);
                        clearInterval(waitTimer);
                    } else if (download.state = "interrupted") {
                        await cardbookHTMLDownloads.finishExport("KO", aCode, aActionId, download.filename, aNumber);
                        clearInterval(waitTimer);
                    }
                }
            }, 500);
        } catch(e) {
            if (aActionId)  {
                await messenger.runtime.sendMessage({query: "cardbook.endAction", actionId: aActionId});
            }
            console.log(e)
        }
    },

    finishExport: async function(aStatus, aCode, aActionId, aFilename, aNumber) {
        let alertTitle = "";
        let alertMessage = "";
        let type = "info";
        if (aCode == "cardsExportedToCsv") {
            alertTitle = messenger.i18n.getMessage("exportCardToCSVFileLabel");
            if (aStatus == "OK") {
                alertMessage = cardbookHTMLPluralRules.getPluralMessage("exportCardToFileMessagePF", aNumber, [aNumber, aFilename]);
            } else {
                type = "alert";
                alertMessage = messenger.i18n.getMessage("exportFailed");
            }
        } else if (aCode == "cardsExportedToVcf") {
            alertTitle = messenger.i18n.getMessage("exportCardToVCFFileLabel");
            if (aStatus == "OK") {
                alertMessage = cardbookHTMLPluralRules.getPluralMessage("exportCardToFileMessagePF", aNumber, [aNumber, aFilename]);
            } else {
                type = "alert";
                alertMessage = messenger.i18n.getMessage("exportFailed");
            }
        } else if (aCode == "cardsExportedToZip") {
            alertTitle = messenger.i18n.getMessage("exportCardToZIPFileLabel");
            if (aStatus == "OK") {
                alertMessage = cardbookHTMLPluralRules.getPluralMessage("exportCardToFileMessagePF", aNumber, [aNumber, aFilename]);
            } else {
                type = "alert";
                alertMessage = messenger.i18n.getMessage("exportFailed");
            }
        } else if (aCode == "saveImage") {
            alertTitle = messenger.i18n.getMessage("saveFile");
            if (aStatus == "OK") {
                alertMessage = messenger.i18n.getMessage("imageSavedToFile", [aFilename]);
            } else {
                type = "alert";
                alertMessage = messenger.i18n.getMessage("exportFailed");
            }
        } else if (aCode == "saveTemplate") {
            alertTitle = messenger.i18n.getMessage("saveFile");
            if (aStatus == "OK") {
                alertMessage = messenger.i18n.getMessage("savedToFile", [aFilename]);
            } else {
                type = "alert";
                alertMessage = messenger.i18n.getMessage("exportFailed");
            }
        } else if (aCode == "cardsImagesExported") {
            alertTitle = messenger.i18n.getMessage("exportCardImagesLabel");
            if (aStatus == "OK") {
                alertMessage = cardbookHTMLPluralRules.getPluralMessage("exportImagesToFileMessagePF", aNumber, [aNumber, aFilename]);
            } else {
                type = "alert";
                alertMessage = messenger.i18n.getMessage("exportFailed");
            }
        }
        let error = type == "alert" ? "Error" : "Normal";
        await messenger.runtime.sendMessage({query: "cardbook.updateStatusProgressInformation", string: alertMessage, error});

        if (aActionId) {
            await messenger.runtime.sendMessage({query: "cardbook.endAction", actionId: aActionId});
        }

        let url = "chrome/content/HTML/alertUser/wdw_cardbookAlertUser.html";
		let params = new URLSearchParams();
		params.set("type", type);
		params.set("title", alertTitle);
		params.set("message", alertMessage);
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
												url: `${url}?${params.toString()}`,
												type: "popup"});
	},
}
