class CBInputDropDown extends HTMLDivElement {
	constructor() {
		super();
		this.innerHTML +=
			`<div class="dropdownContainer">
				<input role="combobox" aria-expanded="false"></input>
				<ul role="listbox" tabindex="-1">
				</ul>
				<div class="listitem"></div>
			</div>
			<style>
			:root {
				--color-gray-05: #fafafa;
				--color-gray-10: #f4f4f5;
				--color-gray-40: #a1a1aa;
				--color-gray-60: #52525b;
				--color-gray-80: #27272a;
				--color-gray-90: #18181b;

				--layout-background-1: var(--color-gray-05);
				--layout-color-1: var(--color-gray-90);
				--layout-border-1: var(--color-gray-40);
				--panel-separator-color: rgb(199, 199, 199)
			}
			@media (prefers-color-scheme: dark) {
				:root {
					--layout-background-1: var(--color-gray-80);
					--layout-color-1: var(--color-gray-10);
					--layout-border-1: var(--color-gray-60);
					--panel-separator-color: rgba(249, 249, 250, 0.1);
				}
			}
			@media (prefers-contrast) {
				:root:not(:-moz-lwtheme) {
					--layout-background-1: -moz-Dialog;
					--layout-color-1: -moz-DialogText;
					--layout-border-1: currentColor;
				}
			}
			:root {
				--arrowpanel-background: var(--layout-background-1);
				--arrowpanel-color: var(--layout-color-1);
				--arrowpanel-border-color: var(--layout-border-1);
				--arrowpanel-border-radius: 8px;
			}
			.dropdownContainer {
				position: relative;
			}
			.dropdownContainer > ul {
				display: block;
				width: 27em;
				margin: 0 auto;
				font: menu;
				font-size: inherit;
				text-align: start;
				border: 1px solid var(--arrowpanel-border-color);
				border-radius: var(--arrowpanel-border-radius);
				background-color: var(--arrowpanel-background);
				color: var(--arrowpanel-color);
				margin: 0 4px;
				padding: 4px;
				text-align: start;
			}
			.dropdownContainer > ul {
				position: absolute;
				left: 0;
				right: 0;
				top: 3em;
				max-height: 25em;
				overflow-y: auto;
				list-style-type: none;
				transform: scale(1,0);
				transform-origin: top left;
				transition: all .3s ease-in;
				z-index: 2;
			}
			.dropdownContainer > ul > li.active:before {
				content: url("../../skin/small-icons/validated.svg");
				-moz-context-properties: fill;
				fill: currentColor;
				display: inline-block;
				padding: 0 6px 0 0;
			}
			.dropdownContainer > ul.active {
				transform: scale(1,1);
			}
			.dropdownContainer > ul > li {
				padding: .6rem .5rem;
				transition: all .3s ease-in;
				position: relative;
				height: 10px;
			}
			.dropdownContainer > ul > .dropdownSeparator {
				border-top: 1px solid var(--panel-separator-color);
				margin-top: 1em;
				width: 100%;
			}
			.dropdownContainer > div.listitem {
                position: relative;
			}
			.dropdownContainer > div.listitem > div.item {
				display: inline-block;
				border: var(--multi-input-item-border, 1px solid #ccc);
				border-radius: 2px;
				margin: 5px;
				padding: 2px 25px 2px 5px;
				pointer-events: none;
				position: relative;
				top: -1px;
				white-space: nowrap;
			}
			</style>`;

			this.container = this.querySelector((".dropdownContainer"),);
			this.input = this.querySelector(("[role='combobox']"),);
			this.listitem = this.querySelector((".listitem"),);
			this.dropdown = this.querySelector(("[role='listbox']"),);
			this.options = this.querySelectorAll(("[role='option']"),);

			this.isDropdownOpen = false;
			this.currentOptionIndex = 0;
			this.prefixId = "";

			document.addEventListener("keydown", event => {
				if (event.target.closest("div[is='cb-input-dropdown']")) {
					if (this.isDropdownOpen || event.key != "Escape" ) {
						this._handleKeyPress(event);
						event.stopImmediatePropagation();
					}
				}
			});
			document.addEventListener("click", event => {
				if (event.target.closest("div[is='cb-input-dropdown']")) {
					this._handleDocumentInteraction(event);
					event.stopImmediatePropagation();
				} else if (this.isDropdownOpen) {
					this._toggleDropdown();
				}
			});
	};

	_toggleDropdown () {
		this.dropdown.classList.toggle("active");
		this.isDropdownOpen = !this.isDropdownOpen;
		this.input.setAttribute("aria-expanded", this.isDropdownOpen.toString());
		
		if (this.isDropdownOpen) {
			for (let option of this.options) {
				option.setAttribute("tabindex", "0");
			}
			this._focusCurrentOption(false);
		} else {
			for (let option of this.options) {
				option.removeAttribute("tabindex");
			}
			this.input.focus();
		}
	};

	_handleKeyPress (event) {
		const { key } = event;
		const openKeys = ["ArrowDown", "ArrowUp", "Enter"];
		if (!this.isDropdownOpen && openKeys.includes(key)) {
			this._toggleDropdown();
		} 
		if (this.isDropdownOpen) {
			switch (key) {
				case "Escape":
					this._toggleDropdown();
					event.preventDefault();
					break;
				case "ArrowDown":
					this._moveFocusDown();
					event.preventDefault();
					break;
				case "ArrowUp":
					this._moveFocusUp();
					event.preventDefault();
					break;
				case "Enter":
					this._EnterKey();
					event.preventDefault();
					break;
				default:
					break;
			}
		}
	};

	_handleDocumentInteraction (event) {
		const isClickInsideInput = this.input.contains(event.target);
		const isClickInsideDropdown = this.dropdown.contains(event.target);

		if ((isClickInsideInput && !this.isDropdownOpen) ||
			 (!isClickInsideDropdown && !isClickInsideInput && this.isDropdownOpen)) {
			this._toggleDropdown();
		}

		// Check if the click is on an option
		const clickedOption = event.target.closest("[role='option']");
		if (clickedOption) {
			this._selectOptionByElement(clickedOption);
		}
	};

	_moveFocusDown () {
		if (this.currentOptionIndex < this.options.length - 1) {
			this.currentOptionIndex++;
		} else {
			this.currentOptionIndex = 0;
		}
		this._focusCurrentOption(true);
	};
	  
	_moveFocusUp () {
		if (this.currentOptionIndex > 0) {
			this.currentOptionIndex--;
		} else {
			this.currentOptionIndex = this.options.length - 1;
		}
		this._focusCurrentOption(true);
	};

	_focusCurrentOption (focus) {
		const currentOption = this.options[this.currentOptionIndex];
		const optionLabel = currentOption.textContent;
		
		currentOption.classList.add("current");
		if (focus === true) {
			currentOption.focus();
		}

		// Scroll the current option into view
		currentOption.scrollIntoView({
			block: "nearest",
		});

		this.options.forEach((option, index) => {
			if (option !== currentOption) {
				option.classList.remove("current");
			}
		});
	};

	_toggleOption (optionElement) {
		let value = this._getCategoryName(optionElement.id);
		if (optionElement.classList.contains("active")) {
			optionElement.classList.remove("active");
			optionElement.setAttribute("aria-selected", "false");
			this._deleteDivItem(value);
		} else {
			optionElement.classList.add("active");
			optionElement.setAttribute("aria-selected", "true");
			this._addDivItem(value);
		}
	};

	_selectOptionByElement (optionElement) {
		this._toggleOption(optionElement);

		let index = 0;
		for (let option of this.options) {
			if (option == optionElement) {
				break;
			}
			index++;
		};
		this.currentOptionIndex = index;
		this._focusCurrentOption(false);

		if (optionElement.hasAttribute("type")) {
			this._toggleDropdown();
		}
	};

	_EnterKey() {
		if (document.activeElement.tagName.toUpperCase() == "LI") {
			this._toggleOption(document.activeElement);
		} else {
			if (this.input.value) {
				this._addItem(this.input.value);
				this.input.value = "";
			}
		}
	};

	_addItem(value) {
		let found = Array.from(this.options).map(x => x.textContent.toLowerCase()).filter(x => x == value.toLowerCase());
		if (!found.length) {
			let anchor;
			let index = 0;
			for (let option of this.options) {
				if (option.textContent.toLowerCase().localeCompare(value.toLowerCase()) == 1) {
					anchor = option;
					break;
				}
				index++
			}
			let newOption = document.createElementNS("http://www.w3.org/1999/xhtml", "li");
			newOption.id = `${this.prefixId}${value}`;
			newOption.textContent = value;
			newOption.setAttribute("role", "option");
			newOption.classList.add("active");
			if (anchor) {
				this.dropdown.insertBefore(newOption, anchor);
			} else {
				this.dropdown.appendChild(newOption);

			}
			this.options = this.dropdown.querySelectorAll(("[role='option']"),);	
			this.currentOptionIndex = index;
			this._focusCurrentOption(true);

			this._addDivItem(value);
		} else {
			let optionValue = "";
			for (let option of this.options) {
				if (option.textContent.toLowerCase() == value.toLowerCase()) {
					option.classList.add("active");
					optionValue = option.textContent;
					break;
				}
			}
			this._addDivItem(optionValue);
		}
	};

	_addDivItem(value) {
		let existingItems = Array.from(this.querySelectorAll("div.item")).filter(item => item.textContent.toLowerCase() == value.toLowerCase());
		if (existingItems.length) {
			return
		}
		let item = document.createElementNS("http://www.w3.org/1999/xhtml", "div");
		item.classList.add("item");
		let cleanName = this._formatCategoryForCss(value);
		item.classList.add(`${cleanName}_color`);
		item.textContent = value;
		// insert after input
		let added = false;
		for (let existing of this.querySelectorAll("div.item")) {
			if (existing.textContent.localeCompare(value) > 0) {
				existing.parentNode.insertBefore(item, existing);
				added = true;
				break;
			}
		}
		if (added == false) {
			this.listitem.insertBefore(item, item.nextSibling);
		}
	};

	_deleteDivItem(value) {
		let existingItems = Array.from(this.querySelectorAll("div.item")).filter(item => item.textContent == value);
		if (!existingItems.length) {
			return
		}
		existingItems[0].remove();
	};

	_clear() {
		let element = this.dropdown;
		while (element.hasChildNodes()) {
			element.lastChild.remove();
		}
		let items = this.querySelectorAll("div.item");
		for (let item of items) {
			item.remove();
		}
	};
	
	// needed for the color
	_formatCategoryForCss(label) {
		// avoid starting with number
		return "c" + label.replace(/[!\"#$%&'\(\)\*\+,\.\/:;<=>\?\@\[\\\]\^`\{\|\}~ ]/g, "_");
	};

	_getCategoryName(value) {
		let tmpArray = value.split("::");
		return tmpArray[tmpArray.length-1];
	};

	getInput() {
		return this.input;
	};

	getContainer() {
		return this.container;
	};

	clearOptions() {
		this._clear();
	};

	loadOptions(prefix, options) {
		this._clear();
		this.prefixId = prefix;
		for (let [label, id, checked] of options) {
			let option = document.createElementNS("http://www.w3.org/1999/xhtml", "li");
			option.id = id;
			option.textContent = label;
			option.setAttribute("role", "option");
			if (checked === true) {
				option.classList.add("active");
				this._addDivItem(this._getCategoryName(id));
			}
			let cleanName = this._formatCategoryForCss(label);
			option.classList.add(`${cleanName}_color`);
			this.dropdown.appendChild(option);
		}
		this.options = this.dropdown.querySelectorAll(("[role='option']"),);
	};

	setPlaceholder(message) {
		this.input.setAttribute("placeholder", message);
	};

	setReadOnly() {
		this.input.classList.add("hidden");
	};

	getValues() {
		let items = this.dropdown.querySelectorAll("li.active");
		return Array.from(items).map(x => x.id).map(x => this._getCategoryName(x));
	};

}
customElements.define("cb-input-dropdown", CBInputDropDown, { extends: "div" });
