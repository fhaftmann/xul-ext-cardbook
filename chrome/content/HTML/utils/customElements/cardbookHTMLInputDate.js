class CBInputDate extends HTMLDivElement {
    constructor() {
		super();
		this.innerHTML +=
			`<div class="dateContainer">
                <label id="yearLabel" for="year"></label>
                <input id="year" name="year" type="number" min="1000" max="9999" class="size5"/>
                <select id="month" name="month" class="vcard-month-select">
                    <option id="noMonth" value="" selected="selected"></option>
                </select>
                <select id="day" name="day" class="vcard-day-select">
                    <option id="noDay" value="" selected="selected"></option>
                </select>
                <input id="dateInput" type="date" hidden="true"></input>
                <img id="datePicker"></img>
                <style>
                #yearLabel {
                    display: none;
                }
                #datePicker {
                    -moz-context-properties: fill;
                    content: url("../../skin/icons/calendar.svg");
                    fill: currentColor;
                    margin-right: 10px;
                }
                .dateContainer {
                    display: flex;
                    align-items: center;
                }
                </style>
            </div>`;

            this.defaultYear = "1604";
            this.monthDays = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31};
        
            this.yearInputLabel = this.querySelector("#yearLabel");
            this.yearInputLabel.textContent = messenger.i18n.getMessage("yearLabel");
            this.yearInput = this.querySelector("#year");
            this.yearInput.placeholder = messenger.i18n.getMessage("yearLabel");

            this.monthSelect = this.querySelector("#month");
            this.daySelect = this.querySelector("#day");
            this.noMonthOption = this.querySelector("#noMonth");
            this.noMonthOption.textContent = messenger.i18n.getMessage("monthLabel");
            this.noDayOption = this.querySelector("#noDay");
            this.noDayOption.textContent = messenger.i18n.getMessage("dayLabel");

            this.datePicker = this.querySelector("#datePicker");
            this.dateInput = this.querySelector("#dateInput");

            this.yearInput.addEventListener("input", () => {
                this.fillDayOptions();
            });
            this.monthSelect.addEventListener("change", () => {
                this.fillDayOptions();
            });
            this.datePicker.addEventListener("click", () => {
                this.dateInput.showPicker();
            });
            this.dateInput.addEventListener("input", () => {
                this.validateDatePicker();
            });
            this.fillMonthOptions();
	};

    clearValue() {
        this.yearInput.value = "";
        this.monthSelect.value = "";
        this.fillDayOptions();
        this.daySelect.value = "";
        this.dateInput.value = "";
    };

    setValue(aDay, aMonth, aYear) {
        if (aYear != this.defaultYear){
            this.yearInput.value = parseInt(aYear);
        }
        this.monthSelect.value = parseInt(aMonth);
        this.fillDayOptions();
        this.daySelect.value = parseInt(aDay);
        this.dateInput.value = `${aYear}-${aMonth}-${aDay}`;
    };

    getValue() {
		let paddedDay = this.daySelect.value;
		if (paddedDay.length == 1) {
			paddedDay = ("0" + this.daySelect.value).slice(-2);
		}
		let paddedMonth = this.monthSelect.value;
		if (paddedMonth.length == 1) {
			paddedMonth = ("0" + this.monthSelect.value).slice(-2);
		}
		if (paddedDay && paddedMonth) {
            let year = this.yearInput.value;
            if (year == "") {
                year = this.defaultYear;
            }
            return `${year}${paddedMonth}${paddedDay}`;
		}
        return "";
    };

	validateDatePicker() {
		if (this.dateInput.value) {
			let tmpArray = this.dateInput.value.split("-");
            this.setValue(parseInt(tmpArray[2]), parseInt(tmpArray[1]), parseInt(tmpArray[0]));
		}
	};

    isLeapYear() {
        // If the year is empty, we can't know if it's a leap year so must assume
        // it is. Otherwise year-less dates can't show Feb 29.
        if (!this.yearInput.checkValidity() || this.yearInput.value === "") {
            return true;
        }

        const year = parseInt(this.yearInput.value);
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    };

    fillMonthOptions() {
        const formatter = Intl.DateTimeFormat(undefined, { month: "long" });
        for (let m = 1; m <= 12; m++) {
            const option = document.createElement("option");
            option.setAttribute("value", m);
            option.setAttribute("label", formatter.format(new Date(2000, m - 1, 2)));
            this.monthSelect.appendChild(option);
        }
    };

    fillDayOptions() {
        let prevDay = 0;
        // Save the previously selected day if we have one.
        if (this.daySelect.childNodes.length > 1) {
            prevDay = this.daySelect.value;
        }

        // Always clear old options.
        const defaultOption = document.createElement("option");
        defaultOption.value = "";
        defaultOption.textContent = messenger.i18n.getMessage("dayLabel");
        this.daySelect.replaceChildren(defaultOption);

        const monthValue = this.monthSelect.value || 1;
        // Add a day to February if this is a leap year and we're in February.
        if (monthValue === "2") {
            this.monthDays["2"] = this.isLeapYear() ? 29 : 28;
        }

        const formatter = Intl.DateTimeFormat(undefined, { day: "numeric" });
        for (let d = 1; d <= this.monthDays[monthValue]; d++) {
            const option = document.createElement("option");
            option.setAttribute("value", d);
            option.setAttribute("label", formatter.format(new Date(2000, 0, d)));
            this.daySelect.appendChild(option);
        }
        // Reset the previously selected day, if it's available in the currently
        // selected month.
        this.daySelect.value = prevDay <= this.monthDays[monthValue] ? prevDay : "";
    };
}

customElements.define("cb-input-date", CBInputDate, { extends: "div" });
