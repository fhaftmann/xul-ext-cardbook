class CBButtonDropDown extends HTMLDivElement {
	constructor() {
		super();
		this.innerHTML +=
			`<div class="dropdownContainer">
				<button role="combobox" aria-expanded="false"></button>
				<img role="arrow"></img>
				<ul role="listbox" tabindex="-1">
				</ul>
			</div>
			<style>
			:root {
				--color-gray-05: #fafafa;
				--color-gray-10: #f4f4f5;
				--color-gray-40: #a1a1aa;
				--color-gray-60: #52525b;
				--color-gray-80: #27272a;
				--color-gray-90: #18181b;

				--layout-background-1: var(--color-gray-05);
				--layout-color-1: var(--color-gray-90);
				--layout-border-1: var(--color-gray-40);
				--panel-separator-color: rgb(199, 199, 199)
			}
			@media (prefers-color-scheme: dark) {
				:root {
					--layout-background-1: var(--color-gray-80);
					--layout-color-1: var(--color-gray-10);
					--layout-border-1: var(--color-gray-60);
					--panel-separator-color: rgba(249, 249, 250, 0.1);
				}
			}
			@media (prefers-contrast) {
				:root:not(:-moz-lwtheme) {
					--layout-background-1: -moz-Dialog;
					--layout-color-1: -moz-DialogText;
					--layout-border-1: currentColor;
				}
			}
			:root {
				--arrowpanel-background: var(--layout-background-1);
				--arrowpanel-color: var(--layout-color-1);
				--arrowpanel-border-color: var(--layout-border-1);
				--arrowpanel-border-radius: 8px;
			}
			.dropdownContainer {
				display: flex;
				flex-direction: row;
				position: relative;
			}
			.dropdownContainer > ul {
				display: block;
				width: 27em;
				margin: 0 auto;
				font: menu;
				font-size: inherit;
				text-align: start;
				border: 1px solid var(--arrowpanel-border-color);
				border-radius: var(--arrowpanel-border-radius);
				background-color: var(--arrowpanel-background);
				color: var(--arrowpanel-color);
				margin: 0 4px;
				padding: 4px;
				text-align: start;
			}
			.dropdownContainer > ul {
				position: absolute;
				left: 0;
				right: 0;
				max-height: 25em;
				overflow-y: auto;
				list-style-type: none;
				transform: scale(1,0);
				transition: all .3s ease-in;
				z-index: 2;
			}
			.dropdownContainer.arrow-up > img {
				content: url("../../skin/small-icons/arrow-up.svg");
				-moz-context-properties: fill;
				fill: currentColor;
				display: inline-block;
				padding: 0 6px 0 6px;
			}
			.dropdownContainer.email > button:before {
				content: url("../../skin/icons/newmsg.svg");
				-moz-context-properties: fill, fill-opacity, stroke;
				fill: transparent;
				stroke: currentColor;
				fill-opacity: var(--toolbarbutton-icon-fill-opacity);
				display: inline-block;
				padding: 0 6px 0 6px;
			}
			.dropdownContainer.layout > button:before {
				content: url("../../skin/icons/layout.svg");
				-moz-context-properties: fill, fill-opacity, stroke;
				fill: transparent;
				stroke: currentColor;
				fill-opacity: var(--toolbarbutton-icon-fill-opacity);
				display: inline-block;
				padding: 0 6px 0 6px;
			}
			.dropdownContainer.sync > button:before {
				content: url("../../skin/icons/synchronize.svg");
				-moz-context-properties: fill, fill-opacity, stroke;
				fill: transparent;
				stroke: currentColor;
				fill-opacity: var(--toolbarbutton-icon-fill-opacity);
				display: inline-block;
				padding: 0 6px 0 6px;
			}
			.dropdownContainer.arrow-down > img {
				content: url("../../skin/small-icons/arrow-down.svg");
				-moz-context-properties: fill;
				fill: currentColor;
				display: inline-block;
				padding: 0 6px 0 6px;
			}
			.dropdownContainer.up > ul {
				top: -25em;
				bottom: 3em;
				transform-origin: bottom left;
			}
			.dropdownContainer.down > ul {
				top: 2em;
				transform-origin: top right;
			}
			.dropdownContainer.left > ul {
				left: -320px;
				transform-origin: top left;
			}
			.dropdownContainer.right > ul {
				left: -320px;
				transform-origin: top right;
			}
			.dropdownContainer > ul > li.active:before {
				content: url("../../skin/small-icons/validated.svg");
				-moz-context-properties: fill;
				fill: currentColor;
				display: inline-block;
				padding: 0 6px 0 0;
			}

			.dropdownContainer > ul.active {
				transform: scale(1,1);
			}
			.dropdownContainer > ul > li {
				padding: .6rem .5rem;
				transition: all .3s ease-in;
				position: relative;
				height: 10px;
			}
			.dropdownContainer > ul > .dropdownSeparator {
				border-top: 1px solid var(--panel-separator-color);
				margin-top: 1em;
				width: 100%;
			}
			</style>`;

			this.container = this.querySelector((".dropdownContainer"),);
			this.button = this.querySelector(("[role='combobox']"),);
			this.arrow = this.querySelector(("[role='arrow']"),);
			this.dropdown = this.querySelector(("[role='listbox']"),);
			this.options = this.querySelectorAll(("[role='option']"),);

			this.isDropdownOpen = false;
			this.currentOptionIndex = 0;
			this.lastTypedChar = "";
			this.lastMatchingIndex = 0;
			this.onlyOne = false;

			this.button.addEventListener("keydown", event => {
				this._handleKeyPress(event);
			});
			this.dropdown.addEventListener("keydown", event => {
				this._handleKeyPress(event);
			});
			document.addEventListener("click", event => {
				if (!event.target.closest("div[is='cb-input-dropdown']") && this.isDropdownOpen) {
					this._toggleDropdown();
				}
			});
	};

	_toggleDropdown () {
		this.dropdown.classList.toggle("active");
		this.isDropdownOpen = !this.isDropdownOpen;
		this.button.setAttribute("aria-expanded", this.isDropdownOpen.toString());
		
		if (this.isDropdownOpen) {
			for (let option of this.options) {
				option.setAttribute("tabindex", "0");
			}
			this._focusCurrentOption();
		} else {
			for (let option of this.options) {
				option.removeAttribute("tabindex");
			}
			this.button.focus();
		}
	};
			  
	_handleKeyPress (event) {
		const { key } = event;
		const openKeys = ["ArrowDown", "ArrowUp", "Enter", " "];

		if (!this.isDropdownOpen && openKeys.includes(key)) {
			event.preventDefault();
			this._toggleDropdown();
		} else if (this.isDropdownOpen) {
			switch (key) {
				case "Escape":
					event.preventDefault();
					event.stopImmediatePropagation();
					this._toggleDropdown();
					break;
				case "ArrowDown":
					event.preventDefault();
					this._moveFocusDown();
					break;
				case "ArrowUp":
					event.preventDefault();
					this._moveFocusUp();
					break;
				case "Enter":
				case " ":
					event.preventDefault();
					this._selectCurrentOption();
					return true;
					break;
				default:
					// Handle alphanumeric key presses for mini-search
					this._handleAlphanumericKeyPress(key);
					break;
			}
			return false;
		}
	};
			  
	_handleDocumentInteraction (event) {
		const isClickInsideArrow = this.arrow.contains(event.target);
		const isClickInsideButton = this.button.contains(event.target);
		const isClickInsideDropdown = this.dropdown.contains(event.target);

		if (isClickInsideArrow || isClickInsideButton || (!isClickInsideDropdown && this.isDropdownOpen)) {
			this._toggleDropdown();
		}

		// Check if the click is on an option
		const clickedOption = event.target.closest("[role='option']");
		if (clickedOption) {
			this._selectOptionByElement(clickedOption);
		}
	};
			  
	_moveFocusDown () {
		if (this.currentOptionIndex < this.options.length - 1) {
			this.currentOptionIndex++;
		} else {
			this.currentOptionIndex = 0;
		}
		this._focusCurrentOption();
	};
			  
	_moveFocusUp () {
		if (this.currentOptionIndex > 0) {
			this.currentOptionIndex--;
		} else {
			this.currentOptionIndex = this.options.length - 1;
		}
		this._focusCurrentOption();
	};
			  
	_focusCurrentOption () {
		const currentOption = this.options[this.currentOptionIndex];
		if (currentOption) {
			const optionLabel = currentOption.textContent;
			
			currentOption.classList.add("current");
			currentOption.focus();
			
			// Scroll the current option into view
			currentOption.scrollIntoView({
				block: "nearest",
			});
			
			this.options.forEach((option, index) => {
				if (option !== currentOption) {
					option.classList.remove("current");
				}
			});
		}
	};
			  
	_selectCurrentOption () {
		const selectedOption = this.options[this.currentOptionIndex];
		this._selectOptionByElement(selectedOption);
	};
			  
	_selectOptionByElement (optionElement) {
		const optionValue = optionElement.textContent;

		if (optionElement.hasAttribute("type")) {
			this._toggleDropdown();
		} else {
			if (this.onlyOne == true) {
				for (let option of this.options) {
					if (option == optionElement) {
						continue;
					}
					option.classList.remove("active");
				}
			}
			if (optionElement.classList.contains("active")) {
				optionElement.classList.remove("active");
				optionElement.setAttribute("aria-selected", "false");
			} else {
				optionElement.classList.add("active");
				optionElement.setAttribute("aria-selected", "true");
			}
			if (this.onlyOne == true) {
				this._toggleDropdown();
			}
		}
	};
	
	_handleAlphanumericKeyPress (key) {
		const typedChar = key.toLowerCase();
		
		if (this.lastTypedChar !== typedChar) {
			this.lastMatchingIndex = 0;
		}
		
		const matchingOptions = Array.from(this.options).filter((option) =>
			option.textContent.toLowerCase().startsWith(typedChar)
		);
		
		if (matchingOptions.length) {
			if (this.lastMatchingIndex === matchingOptions.length) {
				this.lastMatchingIndex = 0;
			}
			let value = matchingOptions[this.lastMatchingIndex]
			const index = Array.from(this.options).indexOf(value);
			this.currentOptionIndex = index;
			this._focusCurrentOption();
			this.lastMatchingIndex += 1;
		}
		this.lastTypedChar = typedChar;
	};
		
	_clear() {
		let element = this.dropdown;
		while (element.hasChildNodes()) {
			element.lastChild.remove();
		}
	};

	getButton() {
		return this.button;
	};

	getContainer() {
		return this.container;
	};

	// restEntries : [ { restId, restLabel, restFunction }, ]
	loadOptions(id, options, onclickFunction, restEntries, buttonFunction) {
		this._clear();
		this.button.id = `${id}-Button`;
		this.dropdown.id = `${id}-Ul`;
		for (let [label, id, checked] of options) {
			let option = document.createElementNS("http://www.w3.org/1999/xhtml", "li");
			option.id = id;
			option.textContent = label;
			option.setAttribute("role", "option");
			if (checked === true) {
				option.classList.add("active");
			}
			this.dropdown.appendChild(option);
			option.addEventListener("click", event => {
				this._handleDocumentInteraction(event);
				onclickFunction(event);
				event.stopImmediatePropagation();
			});
			option.addEventListener("keydown", event => {
				let goOn = this._handleKeyPress(event);
				if (goOn) {
					onclickFunction(event);
				}
				event.stopImmediatePropagation();
			});
		}
		if (restEntries.length) {
			let div = document.createElementNS("http://www.w3.org/1999/xhtml", "div");
			div.classList.add("dropdownSeparator");
			this.dropdown.appendChild(div);
		}
		for (let restEntry of restEntries) {
			let option = document.createElementNS("http://www.w3.org/1999/xhtml", "li");
			option.id = restEntry.restId;
			option.textContent = restEntry.restLabel;
			option.setAttribute("role", "option");
			option.setAttribute("type", "rest");
			this.dropdown.appendChild(option);
			option.addEventListener("click", event => {
				this._handleDocumentInteraction(event);
				restEntry.restFunction(event);
				event.stopImmediatePropagation();
			});
			option.addEventListener("keydown", event => {
				let goOn = this._handleKeyPress(event);
				if (goOn) {
					restEntry.restFunction(event);
				}
				event.stopImmediatePropagation();
			});
		}
		this.options = this.dropdown.querySelectorAll(("[role='option']"),);
		this.arrow.addEventListener("click", event => {
			this._handleDocumentInteraction(event);
			event.stopImmediatePropagation();
		});
		this.button.addEventListener("click", event => {
			if (buttonFunction !== null) {
				buttonFunction(event);
			} else {
				this._handleDocumentInteraction(event)
			}
			event.stopImmediatePropagation();
		});
	};

	setButtonLabel(message) {
		this.button.textContent = message;
	};

	setMode(onlyOne = false) {
		this.onlyOne = onlyOne;
	};

	hideButton(position) {
		this.button.classList.add("hidden");
	};

	setClass(classList) {
		for (let class1 of classList) {
			this.container.classList.add(class1);
		}
	};
}
customElements.define("cb-button-dropdown", CBButtonDropDown, { extends: "div" });
