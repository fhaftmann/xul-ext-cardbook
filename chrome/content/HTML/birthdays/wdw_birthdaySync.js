import { cardbookHTMLUtils } from "../utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookHTMLRichContext } from "../utils/scripts/cardbookHTMLRichContext.mjs";
import { cardbookHTMLDates } from "../utils/scripts/cardbookHTMLDates.mjs";
import { cardbookHTMLTools } from "../utils/scripts/cardbookHTMLTools.mjs";

import { cardbookBGPreferences } from "../../BG/utils/cardbookBGPreferences.mjs";

var timer = null;
var birthdayList = [];
var birthdayAccountList = {};
var birthdaysSyncResult = {};
var cleanCalendars = [];

async function syncAllBirthdays () {
    i18n.updateDocument();
    cardbookHTMLRichContext.loadRichContext();

    // button
    document.getElementById("closeEditionLabel").addEventListener("click", event => do_close());

	let calendars = cardbookBGPreferences.getPref("calendarsNameList");
	let calendarsState = await messenger.runtime.sendMessage({query: "cardbook.getWrongBirthdayCalendars", calendars: calendars});

	for (let calendar of calendarsState.exist) {
		let calArray = calendars.split(",");
		let index = calArray.indexOf(calendar);
		if (index > -1) {
			calArray.splice(index, 1);
		}
		let calendarsNameList = calArray.join(",");
		await cardbookBGPreferences.setPref("calendarsNameList", calendarsNameList);
	}

	for (let calendar of calendarsState.writable) {
		let errorTitle = messenger.i18n.getMessage("calendarNotWritableTitle");
		let errorMsg = messenger.i18n.getMessage("calendarNotWritableMessage", [calendar]);
		let url = "chrome/content/HTML/alertUser/wdw_cardbookAlertUser.html";
		let params = new URLSearchParams();
		params.set("type", "alert");
		params.set("title", errorTitle);
		params.set("message", errorMsg);
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
												url: `${url}?${params.toString()}`,
												type: "popup"});
	}

	cleanCalendars = calendarsState.ok;

	let maxDaysUntilNextBirthday = cardbookBGPreferences.getPref("numberOfDaysForWriting");
	[ birthdayList, birthdayAccountList ] = await messenger.runtime.sendMessage({query: "cardbook.getBirthdays", days: maxDaysUntilNextBirthday});

	let calendarsNameList = cardbookBGPreferences.getPref("calendarsNameList");
	let calendarEntryCategories = cardbookBGPreferences.getPref("calendarEntryCategories");
	let repeatingEvent = cardbookBGPreferences.getPref("repeatingEvent");
	let eventEntryWholeDay = cardbookBGPreferences.getPref("eventEntryWholeDay");
	let eventEntryTime = cardbookBGPreferences.getPref("eventEntryTime");
	let calendarEntryAlarm = cardbookBGPreferences.getPref("calendarEntryAlarm");
	let prefs = {calendarsNameList, calendarEntryCategories, repeatingEvent, eventEntryWholeDay, eventEntryTime, calendarEntryAlarm};
	for (let birthday of birthdayList) {
		birthday.push(cardbookHTMLUtils.getUUID());
	}
	messenger.runtime.sendMessage({query: "cardbook.addBirthdaysToCalendar", birthdayList: birthdayList, prefs: prefs, sendResults: true});

	await do_refresh();
	
	timer = setInterval( async () => {
		await do_refresh();
	}, 1000);
};

async function do_close () {
	clearInterval(timer);
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
	cardbookHTMLRichContext.closeWindow();
};

async function do_refresh () {
	let noneFound = document.getElementById("noneFound");
	let resulTable = document.getElementById("syncListTable");
	let maxDaysUntilNextBirthday = cardbookBGPreferences.getPref("numberOfDaysForWriting");
	maxDaysUntilNextBirthday = (maxDaysUntilNextBirthday > 365) ? 365 : maxDaysUntilNextBirthday;

	let birthdaysListLength = birthdayList.length;

	// if there are no birthdays in the configured timespan
	if (birthdaysListLength == 0) {
		noneFound.classList.remove("hidden");
		resulTable.classList.add("hidden");
		let date = new Date();
		let today = new Date(date.getTime() + maxDaysUntilNextBirthday *24*60*60*1000);
		let dateString = cardbookHTMLDates.convertDateToDateString(today, "4.0");
		let longDateString = cardbookHTMLDates.getFormattedDateForDateString(dateString, "0");
		let noBirthdayFoundMessage = messenger.i18n.getMessage("noBirthdayFoundMessage", [longDateString]);
		noneFound.textContent = noBirthdayFoundMessage;
		document.title = messenger.i18n.getMessage("syncListWindowLabelEnded", [0,0]);
	} else 	if (cleanCalendars.length == 0 || (cleanCalendars.length == 1 && cleanCalendars[0] == "")) {
		noneFound.classList.remove("hidden");
		resulTable.classList.add("hidden");
		let noCalendarsFoundMessage = messenger.i18n.getMessage("noCalendarFoundMessage");
		noneFound.textContent = noCalendarsFoundMessage;
		document.title = messenger.i18n.getMessage("syncListWindowLabelEnded", [0,0]);
	} else {
		noneFound.classList.add("hidden");
		resulTable.classList.remove("hidden");

		let totalRecordsToInsert = birthdaysListLength * cleanCalendars.length;
		let birthdaySyncResultGrouped = [];
		let totalRecordsInserted = 0;
		for (let calId in birthdaysSyncResult) {
			let record = birthdaysSyncResult[calId];
            birthdaySyncResultGrouped.push([record.name, record.created, record.updated, record.existing, record.failed]);
			totalRecordsInserted = totalRecordsInserted + record.created + record.updated + record.existing + record.failed;
		}

		let headers = [ "calendarName", "created", "updated", "existing", "failed" ];
		let data = birthdaySyncResultGrouped.map(x => [ x[0], x[1], x[2], x[3], x[4] ]);
		let dataParameters = [];
		let rowParameters = {};
		cardbookHTMLTools.addTreeTable("syncListTable", headers, data, dataParameters, rowParameters);

		if (totalRecordsToInsert != totalRecordsInserted) {
			let lTotalDisplayed = totalRecordsInserted < 0 ? "0" : totalRecordsInserted.toString();
			document.title = messenger.i18n.getMessage("syncListWindowLabelRunning", [lTotalDisplayed, totalRecordsToInsert.toString()]);
		} else {
			document.title = messenger.i18n.getMessage("syncListWindowLabelEnded", [totalRecordsInserted.toString(), totalRecordsToInsert.toString()]);
		}
	}
};

window.addEventListener("beforeunload", async function() {
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
});

messenger.runtime.onMessage.addListener( (info) => {
	switch (info.query) {
		case "cardbook.syncBirthdays.sendResults":
			birthdaysSyncResult = info.results;
			break;
		}
});

await syncAllBirthdays();

