import { cardbookHTMLUtils } from "../utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookHTMLRichContext } from "../utils/scripts/cardbookHTMLRichContext.mjs";

var wdw_cardbookAskPassword = {
	username: "",
	url: "",

	showPassword: function () {
		let passwordInput = document.getElementById("remotePagePassword");
		if (!passwordInput.value) {
			return;
		}
		let passwordInputInfo = document.getElementById("remotePagePasswordInfo");
		if (passwordInput.type == "password") {
			passwordInput.type = "text";
			passwordInputInfo.classList.remove("hiddenIcon");
			passwordInputInfo.classList.add("visibleIcon");
		} else {
			passwordInput.type = "password";
			passwordInputInfo.classList.add("hiddenIcon");
			passwordInputInfo.classList.remove("visibleIcon");
		}
	},

	load: function () {
		let urlParams = new URLSearchParams(window.location.search);
		wdw_cardbookAskPassword.username = urlParams.get("username");
		wdw_cardbookAskPassword.url = urlParams.get("url");

		i18n.updateDocument();
		cardbookHTMLRichContext.loadRichContext();
	
		// button
		document.getElementById("validateButton").addEventListener("click", event => wdw_cardbookAskPassword.validate(event));
		document.getElementById("cancelButton").addEventListener("click", event => wdw_cardbookAskPassword.close(event));
		// image
		document.getElementById("remotePagePasswordInfo").addEventListener("click", event => wdw_cardbookAskPassword.showPassword());

		document.title = messenger.i18n.getMessage("wdw_passwordMissingTitle");
		document.getElementById("infoIcon").classList.add("questionIcon");

		document.getElementById("messageLabel").textContent = messenger.i18n.getMessage("credentials.description", [wdw_cardbookAskPassword.username, wdw_cardbookAskPassword.url]);
		document.getElementById("remotePageUsername").value = wdw_cardbookAskPassword.username;
	},

	validate: async function () {
		let password = document.getElementById("remotePagePassword").value;
		let save = document.getElementById("rememberPasswordCheckbox").checked;
		await messenger.runtime.sendMessage({query: "cardbook.askPassword", username: wdw_cardbookAskPassword.username,
				url: wdw_cardbookAskPassword.url, password: password, save: save});
		let win = await cardbookHTMLRichContext.getWindow();
		await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
		cardbookHTMLRichContext.closeWindow();
	},

	close: async function () {
		await messenger.runtime.sendMessage({query: "cardbook.askPassword", username: wdw_cardbookAskPassword.username, url: wdw_cardbookAskPassword.url, password: ""});
		let win = await cardbookHTMLRichContext.getWindow();
		await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
		cardbookHTMLRichContext.closeWindow();
	}
};

window.addEventListener("beforeunload", async function() {
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
});

wdw_cardbookAskPassword.load();

