import { cardbookBGPreferences } from "../../BG/utils/cardbookBGPreferences.mjs";

import { cardbookHTMLRichContext } from "../utils/scripts/cardbookHTMLRichContext.mjs";
import { cardbookHTMLTools } from "../utils/scripts/cardbookHTMLTools.mjs";
import { cardbookHTMLUtils } from "../utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookHTMLDownloads } from "../utils/scripts/cardbookHTMLDownloads.mjs";

var mode = "";
var columnSeparator = "";
var fields = "";
var includePref = false;
var lineHeader = false;
var filename = "";
var actionId = "";
var source = "";
var foundColumns = "";
var template = [];
var cardbookeditlists = {};
var blankColumn = "";
var resultGiven = false;
var winId = "";
var validated = false;

function getIndexFromName (aName) {
	let tmpArray = aName.split("_");
	return tmpArray[tmpArray.length - 1];
};

function getTableCurrentIndex (aTableName) {
	let selectedList = document.getElementById(aTableName).querySelectorAll("tr[rowselected='true']");
	if (selectedList.length) {
		let tmpArray = selectedList[0].id.split("_");
		return tmpArray[tmpArray.length - 1];
	}
};

function upColumns () {
	let tableName = "addedColumnsTable";
	let currentIndex = getTableCurrentIndex(tableName);
	if (cardbookeditlists[tableName].length && currentIndex) {
		currentIndex = currentIndex*1;
		let temp = [ cardbookeditlists[tableName][currentIndex-1][0], cardbookeditlists[tableName][currentIndex-1][1] ];
		cardbookeditlists[tableName][currentIndex-1] = [ cardbookeditlists[tableName][currentIndex][0], cardbookeditlists[tableName][currentIndex][1] ];
		cardbookeditlists[tableName][currentIndex] = temp;
		displayListTables(tableName);
	}
};

function downColumns () {
	let tableName = "addedColumnsTable";
	let currentIndex = getTableCurrentIndex(tableName);
	if (cardbookeditlists[tableName].length && currentIndex) {
		currentIndex = currentIndex*1;
		let temp = [ cardbookeditlists[tableName][currentIndex+1][0], cardbookeditlists[tableName][currentIndex+1][1] ];
		cardbookeditlists[tableName][currentIndex+1] = [ cardbookeditlists[tableName][currentIndex][0], cardbookeditlists[tableName][currentIndex][1] ];
		cardbookeditlists[tableName][currentIndex] = temp;
		displayListTables(tableName);
	}
};

function clickTree (aEvent) {
	if (aEvent.target.tagName.toLowerCase() == "td") {
		let table = aEvent.target.closest("table");
		let tbody = aEvent.target.closest("tbody");
		let row = aEvent.target.closest("tr");
		if (aEvent.shiftKey) {
			let startIndex = getTableCurrentIndex(table.id) || 0;
			let endIndex = getIndexFromName(row.id);
			let i = 0;
			for (let child of tbody.childNodes) {
				if (i >= startIndex && i <= endIndex) {
					child.setAttribute("rowselected", "true");
				} else {
					child.removeAttribute("rowselected");
				}
				i++;
			}
		} else if (aEvent.ctrlKey) {
			if (row.hasAttribute("rowselected")) {
				row.removeAttribute("rowselected");
			} else {
				row.setAttribute("rowselected", "true");
			}
		} else {
			for (let child of tbody.childNodes) {
				child.removeAttribute("rowselected");
			}
			row.setAttribute("rowselected", "true");
		}
		windowControlShowing();
	}
};

function keyDownTree (aEvent) {
	if (aEvent.ctrlKey && aEvent.key.toUpperCase() == "A") {
		let table = aEvent.target.closest("table");
		if (table) {
			let tbody = table.querySelector("tbody");
			for (let child of tbody.childNodes) {
				child.setAttribute("rowselected", "true");
			}
			aEvent.preventDefault();
		}
	}	
};

function displayListTables (aTableName) {
	let headers = [];
	let data = cardbookeditlists[aTableName].map(x => [ x[1] ]);
	let dataParameters = [];
	let tableParameters = { "events": [ [ "click", clickTree ],
										[ "dblclick", modifyListsFromTable ],
										[ "keydown", keyDownTree ] ] };
	if (aTableName == "foundColumnsTable") {
		tableParameters = { "events": [ [ "click", clickTree ] ] };
    }
	let dataId = 0;
	let dragdrop = { "dragStart": startDrag, "drop": dragCards };
	cardbookHTMLTools.addTreeTable(aTableName, headers, data, dataParameters, null, tableParameters, null, dataId, dragdrop);
	windowControlShowing();
};

function getSelectedColumnsForList (aTableName) {
	let listOfSelected = [];
	let table = document.getElementById(aTableName);
	let selectedRows = table.querySelectorAll("tr[rowselected='true']");
	for (let row of selectedRows) {
		let index = getIndexFromName(row.id);
		listOfSelected.push(index);
	}
	return listOfSelected;
};

function modifyListsFromTable(aEvent) {
	let table = aEvent.target.closest("table");
	modifyLists(table.id);
}

function modifyLists (aMenuOrTable) {
	let addedTablename = "addedColumnsTable";
	let availableTablename = "availableColumnsTable";
	let selectedIndexes = [];
	switch (aMenuOrTable) {
		case "availableColumnsTable":
		case "appendlistavailableColumnsButton":
			selectedIndexes = getSelectedColumnsForList(availableTablename);
			for (let selectedIndex of selectedIndexes) {
				cardbookeditlists[addedTablename] = cardbookeditlists[addedTablename].concat([cardbookeditlists[availableTablename][selectedIndex]]);
			}
			break;
		case "addedColumnsTable":
		case "deletelistaddedColumnsButton":
			selectedIndexes = getSelectedColumnsForList(addedTablename);
			for (let i = selectedIndexes.length-1; i >= 0; i--) {
				cardbookeditlists[addedTablename].splice(selectedIndexes[i], 1);
			}
			break;
		default:
			break;
	}
	displayListTables(addedTablename);
};

async function validateImportColumns () {
	if (cardbookeditlists.foundColumnsTable.length != cardbookeditlists.addedColumnsTable.length && !validated) {
		let confirmTitle = messenger.i18n.getMessage("confirmTitle");
		let confirmMsg = messenger.i18n.getMessage("missingColumnsConfirmMessage");
		let url = "chrome/content/HTML/alertUser/wdw_cardbookAlertUser.html";
		let params = new URLSearchParams();
		params.set("type", "confirm");
		params.set("action", "delete");
		params.set("winId", winId);
		params.set("title", confirmTitle);
		params.set("message", confirmMsg);
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
												url: `${url}?${params.toString()}`,
												type: "popup"});
	} else {
		validated = true;
	}
	return true;
};

function loadFoundColumns () {
	if (foundColumns) {
		cardbookeditlists.foundColumnsTable = [];
		let separator = document.getElementById("fieldDelimiterTextBox").value;
		if (separator == "") {
			separator = ";";
		}
		let tmpArray = foundColumns.split(separator);
		for (let i = 0; i < tmpArray.length; i++) {
			cardbookeditlists.foundColumnsTable.push([i, tmpArray[i]]);
		}
		displayListTables("foundColumnsTable");
	}
};

function startDrag (aEvent) {
	try {
		var listOfUid = [];
		let table = aEvent.target.closest("table");
		let tablename = table.id;
		let selectedRows = table.querySelectorAll("tr[rowselected='true']");
		for (let row of selectedRows) {
			let index = getIndexFromName(row.id);
			listOfUid.push(index + "::" + cardbookeditlists[tablename][index][0]);
		}
		aEvent.dataTransfer.setData("text/plain", listOfUid.join("@@@@@"));
	}
	catch (e) {
		console.debug("startDrag error : " + e, "Error");
	}
};

function dragCards (aEvent) {
	let table;
	let rowIndex = -1;
	// outside the rows
	if (aEvent.target.tagName.toLowerCase() == "table") {
		table = aEvent.target;
	// in the rows
	} else {
		table = aEvent.target.closest("table");
		let row = aEvent.target.closest("tr");
		rowIndex = getIndexFromName(row.id);
	}
	let tablename = table.id;
	let data = aEvent.dataTransfer.getData("text/plain");
	let columns = data.split("@@@@@");

	if (tablename == "availableColumnsTable") {
		for (let i = columns.length-1; i >= 0; i--) {
			let tmpArray = columns[i].split("::");
			cardbookeditlists.addedColumnsTable.splice(tmpArray[0], 1);
		}
	} else if (tablename == "addedColumnsTable") {
		for (let column of columns) {
			let tmpArray = column.split("::");
			let value = tmpArray[1];
            let label = cardbookHTMLUtils.getTranslatedField(value);
			if (rowIndex == -1) {
				cardbookeditlists.addedColumnsTable.push([value, label]);
			} else {
				cardbookeditlists.addedColumnsTable.splice(rowIndex, 0, [value, label]);
				rowIndex++;
			}
		}
	}
	displayListTables("addedColumnsTable");
};

function windowControlShowing () {
	let tableName = "addedColumnsTable";
	let btnDelete = document.getElementById("deletelistaddedColumnsButton");
	let btnUp = document.getElementById("upAddedColumnsButton");
	let btnDown = document.getElementById("downAddedColumnsButton");
	let currentIndex = getTableCurrentIndex(tableName);
	if (cardbookeditlists[tableName].length && currentIndex) {
		currentIndex = currentIndex*1;
		btnDelete.disabled = false;
		if (cardbookeditlists[tableName].length > 1) {
			if (currentIndex == 0) {
				btnUp.disabled = true;
			} else {
				btnUp.disabled = false;
			}
			if (currentIndex == cardbookeditlists[tableName].length-1) {
				btnDown.disabled = true;
			} else {
				btnDown.disabled = false;
			}
		} else {
			btnUp.disabled = true;
			btnDown.disabled = true;
		}
	} else {
		btnDelete.disabled = true;
		btnUp.disabled = true;
		btnDown.disabled = true;
	}
	tableName = "availableColumnsTable";
	let addDelete = document.getElementById("appendlistavailableColumnsButton");
	currentIndex = getTableCurrentIndex(tableName);
	if (cardbookeditlists[tableName].length && currentIndex) {
		addDelete.disabled = false;
	} else {
		addDelete.disabled = true;
	}
};

function guess () {
	let oneFound = false;
	let result = [];
	// search with current locale
	for (let foundColumn of cardbookeditlists.foundColumnsTable) {
		foundColumn = foundColumn[1].replace(/^\"|\"$/g, "").replace(/^\'|\'$/g, "");
		let found = false;
		for (let availableColumn of cardbookeditlists.availableColumnsTable) {
			if (availableColumn[1].toLowerCase() == foundColumn.toLowerCase() || availableColumn[0].toLowerCase() == foundColumn.toLowerCase()) {
				result.push([availableColumn[0], availableColumn[1]]);
				found = true;
				oneFound = true;
				break;
			}
		}
		if (!found) {
			result.push(["blank", blankColumn]);
		}
	}
	if (!oneFound) {
		result = [];
		// search with en-US locale
		for (let foundColumn of cardbookeditlists.foundColumnsTable) {
			foundColumn = foundColumn[1].replace(/^\"|\"$/g, "").replace(/^\'|\'$/g, "");
			let found = false;
			for (let availableColumn of cardbookeditlists.availableColumnsTable) {
                let translatedColumn = cardbookHTMLUtils.getTranslatedField(availableColumn, "locale-US");
				if (translatedColumn.toLowerCase() == foundColumn.toLowerCase()) {
					result.push([availableColumn[0], availableColumn[1]]);
					found = true;
					oneFound = true;
					break;
				}
			}
			if (!found) {
				result.push(["blank", blankColumn]);
			}
		}
	}
	if (oneFound) {
		cardbookeditlists.addedColumnsTable = result;
		displayListTables("addedColumnsTable");
	}
};

async function onLoadDialog () {
	let urlParams = new URLSearchParams(window.location.search);
	mode = urlParams.get("mode");
	columnSeparator = urlParams.get("columnSeparator");
	fields = urlParams.get("fields");
	includePref = urlParams.get("includePref");
	lineHeader = urlParams.get("lineHeader");
	filename = urlParams.get("filename");
	actionId = urlParams.get("actionId");
	source = urlParams.get("source");
	foundColumns = urlParams.get("foundColumns");

	let win = await messenger.windows.getCurrent();
	winId = win.id;

	i18n.updateDocument();
	cardbookHTMLRichContext.loadRichContext();
	document.title = messenger.i18n.getMessage(mode + "MappingTitle");
	document.getElementById("availableColumnsGroupboxLabel").textContent = messenger.i18n.getMessage(`${mode}availableColumnsGroupboxLabel`);
	document.getElementById("addedColumnsGroupboxLabel").textContent = messenger.i18n.getMessage(`${mode}addedColumnsGroupboxLabel`);

	cardbookeditlists.availableColumnsTable = [];
	cardbookeditlists.addedColumnsTable = [];
	
	if (mode == "choice") {
		document.getElementById("foundColumnsTable").classList.add("hidden");
		document.getElementById("fieldDelimiterLabel").classList.add("hidden");
		document.getElementById("fieldDelimiterTextBox").classList.add("hidden");
		document.getElementById("includePrefLabel").classList.add("hidden");
		document.getElementById("includePrefCheckBox").classList.add("hidden");
		document.getElementById("lineHeaderLabel").classList.add("hidden");
		document.getElementById("lineHeaderCheckBox").classList.add("hidden");
		document.getElementById("loadTemplateButton").classList.add("hidden");
		document.getElementById("saveTemplateButton").classList.add("hidden");
		document.getElementById("guesslistavailableColumnsButton").classList.add("hidden");
	} else if (mode == "export") {
		document.getElementById("foundColumnsTable").classList.add("hidden");
		document.getElementById("lineHeaderLabel").classList.add("hidden");
		document.getElementById("lineHeaderCheckBox").classList.add("hidden");
		document.getElementById("guesslistavailableColumnsButton").classList.add("hidden");
		document.getElementById("fieldDelimiterTextBox").value = columnSeparator;
	} else if (mode == "import") {
		if (foundColumns) {
			document.getElementById("foundColumnsTable").classList.add("active");
			document.getElementById("foundColumnsGroupboxLabel").textContent = messenger.i18n.getMessage(mode + "foundColumnsGroupboxLabel");
		} else {
			document.getElementById("foundColumnsTable").classList.add("hidden");
		}
		document.getElementById("includePrefLabel").classList.add("hidden");
		document.getElementById("includePrefCheckBox").classList.add("hidden");
		document.getElementById("lineHeaderCheckBox").checked = true;
		document.getElementById("fieldDelimiterTextBox").value = columnSeparator;
		blankColumn = messenger.i18n.getMessage("importBlankColumn");
		cardbookeditlists.availableColumnsTable.push(["blank", blankColumn]);
	}

	// input
	document.getElementById("fieldDelimiterTextBox").addEventListener("input", event => loadFoundColumns());
	// button
	document.getElementById("appendlistavailableColumnsButton").addEventListener("click", event => modifyLists("appendlistavailableColumnsButton"));
	document.getElementById("deletelistaddedColumnsButton").addEventListener("click", event => modifyLists("deletelistaddedColumnsButton"));
	document.getElementById("upAddedColumnsButton").addEventListener("click", event => upColumns());
	document.getElementById("downAddedColumnsButton").addEventListener("click", event => downColumns());
	document.getElementById("guesslistavailableColumnsButton").addEventListener("click", event => guess());
	document.getElementById("cancelButton").addEventListener("click", onCancelDialog);
	document.getElementById("validateButton").addEventListener("click", onAcceptDialog);
	document.getElementById("loadTemplateButton").addEventListener("click", loadTemplateButton);
	document.getElementById("loadTemplateInput").addEventListener("change", loadTemplateInput);
	document.getElementById("saveTemplateButton").addEventListener("click", saveTemplateButton);

    if (mode == "choice") {
		fields = fields.split("|");
		for (let field of fields) {
			let label = cardbookHTMLUtils.getTranslatedField(field);
			template.push([field, label]);
		}
		cardbookeditlists.addedColumnsTable = template;
		displayListTables("addedColumnsTable");
	}

    let columns = cardbookHTMLUtils.getAllAvailableColumns(mode);
	cardbookHTMLUtils.sortMultipleArrayByString(columns, 1, 1);
	cardbookeditlists.availableColumnsTable = cardbookeditlists.availableColumnsTable.concat(columns);
	displayListTables("availableColumnsTable");

	if (mode == "import") {
		loadFoundColumns();
	}
};

function getTemplate (aFieldList) {
    let fields = aFieldList.split("|");
    let result = [];
    for (let field of fields) {
        let label = cardbookHTMLUtils.getTranslatedField(field);
        result.push([field, label]);
    }
    return result;
};

function getDefaultTemplateName () {
	let defaultTemplateName = filename + ".tpl";
	if (filename.endsWith(".csv")) {
		defaultTemplateName = filename.replace(/\.csv$/, ".tpl");
	} else if (filename.includes(".")) {
		let tmpArray = filename.split(".");
		tmpArray.pop();
		defaultTemplateName = tmpArray.join(".") + ".tpl";
	}
	return defaultTemplateName;
};

async function loadTemplateButton () {
	let filepicker = document.getElementById("loadTemplateInput");
	filepicker.click();
};

async function loadTemplateInput () {
	let filepicker = document.getElementById("loadTemplateInput");
	if (filepicker.files.length == 1) {
		let file = filepicker.files[0];
		let fileReader = new FileReader();
		fileReader.readAsText(file, "UTF-8");
		fileReader.onload = function(event) {
			if (event.target.result) {
				cardbookeditlists.addedColumnsTable = getTemplate(event.target.result);
				displayListTables("addedColumnsTable");
			}
		};
	}
};

async function saveTemplateButton () {
	let content = cardbookeditlists.addedColumnsTable.map(x => x[0]).join("|");
	let file = new Blob([content], { type: "text/plain" });
	let url = URL.createObjectURL(file);
	let filename = getDefaultTemplateName();
	await cardbookHTMLDownloads.download(url, filename, "saveTemplate");
};

async function onAcceptDialog () {
	if (mode == "import" && (!validateImportColumns() || !validated)) {
		return;
	}
	resultGiven = true;
	let urlParams = {};
	urlParams.mode = mode;
	urlParams.columnSeparator = document.getElementById("fieldDelimiterTextBox").value || ";";
	urlParams.includePref = document.getElementById("includePrefCheckBox").checked;
	urlParams.lineHeader = document.getElementById("lineHeaderCheckBox").checked;
	urlParams.actionId = actionId;
	urlParams.fields = cardbookeditlists.addedColumnsTable.map(x => x[0]).join("|");
	urlParams.labels = cardbookeditlists.addedColumnsTable.map(x => x[1]).join("|");
	urlParams.filename = filename;
	urlParams.source = source;
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
	await cardbookBGPreferences.setPref("exportDelimiter", urlParams.columnSeparator);
	if (mode == "choice") {
		await messenger.runtime.sendMessage({query: "cardbook.conf.saveAutocompleteRestrictSearchFields", urlParams: urlParams});
	} else if (mode == "export") {
		await messenger.runtime.sendMessage({query: "cardbook.writeCardsToCSVFile", params: urlParams});
	} else if (mode == "import") {
		await messenger.runtime.sendMessage({query: "cardbook.importCSVFile", params: urlParams});
	}
	cardbookHTMLRichContext.closeWindow();
};

async function onCancelDialog () {
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
	if (resultGiven === false) {
		await messenger.runtime.sendMessage({query: "cardbook.finishCSV", winName: win.name, winState: win.state, actionId: actionId});
	}
	cardbookHTMLRichContext.closeWindow();
};

messenger.runtime.onMessage.addListener( (info) => {
	switch (info.query) {
		case "cardbook.alertUser":
			if (winId == info.winId) {
				if (info.response === true) {
					return new Promise(async (resolve, reject) => {
						let missing = cardbookeditlists.foundColumnsTable.length - cardbookeditlists.addedColumnsTable.length;
						for (let i = 0; i < missing; i++) {
							cardbookeditlists.addedColumnsTable.push(["blank", blankColumn]);
						}
						let more = cardbookeditlists.addedColumnsTable.length - cardbookeditlists.foundColumnsTable.length;
						for (let i = 0; i < more; i++) {
							cardbookeditlists.addedColumnsTable.slice(cardbookeditlists.addedColumnsTable.length, 1);
						}
						validated = true;
						await onAcceptDialog();
					});
				}
			}
			break;
	}
});

// run when clicking on the cross button or with the escape key
window.addEventListener("beforeunload", async function() {
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
	if (resultGiven === false) {
		await messenger.runtime.sendMessage({query: "cardbook.finishCSV", winName: win.name, winState: win.state, actionId: actionId});
	}
});

await onLoadDialog();
