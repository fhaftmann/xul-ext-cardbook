import { cardbookHTMLUtils } from "../utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookHTMLTools } from "../utils/scripts/cardbookHTMLTools.mjs";
import { cardbookHTMLRichContext } from "../utils/scripts/cardbookHTMLRichContext.mjs";

import { cardbookBGPreferences } from "../../BG/utils/cardbookBGPreferences.mjs";

var wdw_cardbookApplyColumns = {
	allAddressbooks: [],
	dirPrefId: "",

	keyDownTree: function (aEvent) {
		let row = aEvent.target.closest("tr");
		let tmpArray = row.id.split("_");
		let x = tmpArray[tmpArray.length -1];
		if (aEvent.keyCode == "38") {
			// up arrow
			if (row.previousElementSibling) {
				let previousRow = row.previousElementSibling;
				let tbody = aEvent.target.closest("tbody");
				for (let child of tbody.childNodes) {
					child.removeAttribute("rowselected");
				}
				previousRow.setAttribute("rowselected", "true");
				previousRow.focus();
			}
		} else if (aEvent.keyCode == "40") {
			// down arrow
			if (row.nextElementSibling) {
				let nextRow = row.nextElementSibling;
				let tbody = aEvent.target.closest("tbody");
				for (let child of tbody.childNodes) {
					child.removeAttribute("rowselected");
				}
				nextRow.setAttribute("rowselected", "true");
				nextRow.focus();
			}
		} else if (aEvent.key == " ") {
			// space
			let data = wdw_cardbookApplyColumns.allAddressbooks;
			let y = 0;
			let checkbox = row.querySelector("input[type='checkbox']");
			checkbox.checked = !data[x][y];
			checkbox.dispatchEvent(new Event("click"));
		}
	},

	loadAddressbooks: function () {
		let tmpArray = [];
		let accounts = cardbookBGPreferences.getAllPrefIds();
		for (let dirPrefId of accounts) {
			let name = cardbookBGPreferences.getName(dirPrefId);
			tmpArray.push([name, dirPrefId]);
		}
		cardbookHTMLUtils.sortMultipleArrayByString(tmpArray,0,1);
		wdw_cardbookApplyColumns.allAddressbooks = tmpArray.map( account => [false, account[0], account[1]] );
	},
	
	sortTable: async function (aTableName) {
		let table = document.getElementById(aTableName);
		let order = table.getAttribute("data-sort-order") == "ascending" ? 1 : -1;
		let data = wdw_cardbookApplyColumns.allAddressbooks;
		if (data && data.length) {
			cardbookHTMLUtils.sortMultipleArrayByString(data, 1, order);
		}

		wdw_cardbookApplyColumns.displayAddressbooks();
	},

	displayAddressbooks: function () {
		let headers = [];
		let data = wdw_cardbookApplyColumns.allAddressbooks.map(x => [ x[0], x[1] ]);
		let dataParameters = [];
		dataParameters[0] = {"events": [ [ "click", wdw_cardbookApplyColumns.enableOrDisableCheckbox ] ] };
		let rowParameters = {};
		let tableParameters = { "events": [ [ "keydown", wdw_cardbookApplyColumns.keyDownTree ] ] };
		cardbookHTMLTools.addTreeTable("addressbooksTable", headers, data, dataParameters, rowParameters, tableParameters);
		wdw_cardbookApplyColumns.changeAddressbooksMainCheckbox();
	},
	
	enableOrDisableCheckbox: function (aEvent) {
		// works only for first column checkboxes
		let tmpArray = aEvent.target.id.split("_");
		if (tmpArray[tmpArray.length -1] == "checkbox") {
			let x = tmpArray[tmpArray.length -3];
			let y = tmpArray[tmpArray.length -2];
			let data = wdw_cardbookApplyColumns.allAddressbooks;
			data[x][y] = !data[x][y];
			wdw_cardbookApplyColumns.changeAddressbooksMainCheckbox();
		}
	},
	
	changeAddressbooksMainCheckbox: function () {
		let totalChecked = 0;
		for (let addressbook of wdw_cardbookApplyColumns.allAddressbooks) {
			if (addressbook[0]) {
				totalChecked++;
			}
		}
		let checkbox = document.getElementById("addressbooksCheckbox");
		if (totalChecked == wdw_cardbookApplyColumns.allAddressbooks.length && totalChecked != 0) {
			checkbox.checked = true;
		} else {
			checkbox.checked = false;
		}
	},

	changedAddressbooksMainCheckbox: function () {
		let checkbox = document.getElementById("addressbooksCheckbox");
		let state = false;
		if (checkbox.checked) {
			state = true;
		}
		let tmpArray = [];
		for (let addressbook of wdw_cardbookApplyColumns.allAddressbooks) {
			tmpArray.push([state, addressbook[1], addressbook[2]]);
		}
		wdw_cardbookApplyColumns.allAddressbooks = JSON.parse(JSON.stringify(tmpArray));
		wdw_cardbookApplyColumns.sortTable("addressbooksTable");
	},
	
	load: function () {
		let urlParams = new URLSearchParams(window.location.search);
		wdw_cardbookApplyColumns.dirPrefId = urlParams.get("dirPrefId");

		i18n.updateDocument();
		cardbookHTMLRichContext.loadRichContext();
	
		// button
		document.getElementById("addressbooksCheckbox").addEventListener("input", event => wdw_cardbookApplyColumns.changedAddressbooksMainCheckbox());
		document.getElementById("validateButton").addEventListener("click", event => wdw_cardbookApplyColumns.validate(event));
		document.getElementById("cancelButton").addEventListener("click", event => wdw_cardbookApplyColumns.close(event));

		document.title = messenger.i18n.getMessage("columnPickerApplyCurrentViewTo");
		wdw_cardbookApplyColumns.loadAddressbooks();
		wdw_cardbookApplyColumns.sortTable("addressbooksTable");
	},

	validate: async function () {
		let win = await cardbookHTMLRichContext.getWindow();
		await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
		let currentColumns = cardbookBGPreferences.getDisplayedColumns(wdw_cardbookApplyColumns.dirPrefId);
		for (let account of wdw_cardbookApplyColumns.allAddressbooks) {
			if (account[0] === true) {
				await cardbookBGPreferences.setDisplayedColumns(account[2], currentColumns);
			}
		}
		cardbookHTMLRichContext.closeWindow();
	},

	close: async function () {
		let win = await cardbookHTMLRichContext.getWindow();
		await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
		cardbookHTMLRichContext.closeWindow();
	}
};

window.addEventListener("beforeunload", async function() {
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
});

// messenger.windows.onFocusChanged.addListener(async (windowId) => {
// 	await wdw_cardbookApplyColumns.close();
// });

wdw_cardbookApplyColumns.load();
