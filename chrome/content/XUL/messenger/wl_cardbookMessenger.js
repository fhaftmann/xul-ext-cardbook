// Import any needed modules.
var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/filters/cardbookFilterRules.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/gloda/cardbookFindEmails.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/findEvents/cardbookFindEvents.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/messenger/cardbookMessenger.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/observers/cardBookMessengerObserver.js", window, "UTF-8");

// called on window load or on add-on activation while window is already open
function onLoad(wasAlreadyOpen) {
	WL.injectCSS("chrome://cardbook/content/skin/mainToolbarButton.css");
	WL.injectCSS("chrome://cardbook/content/skin/cardbookMain.css");
	WL.injectCSS("chrome://cardbook/content/skin/cardbookEmpty.css");
	WL.injectCSS("chrome://cardbook/content/skin/cardbookAddressBooks.css");
	// <!-- for MailList icon -->
	WL.injectCSS("chrome://cardbook/content/skin/cardbookCardsIcon.css");
	// <!-- for the search field -->
	WL.injectCSS("chrome://messenger/skin/searchBox.css");
	// <!-- for the search textbox -->
	WL.injectCSS("chrome://messenger/skin/input-fields.css");
	WL.injectCSS("chrome://cardbook/content/skin/cardbookHTMLSplitter.css");

	WL.injectElements(`
	<!-- horrible hack to have the CardBook keys defined -->
	<!-- <keyset id="viewZoomKeys"> -->
	<key id="CardBookKey" key="__MSG_cardbookMenuItemKey__" modifiers="accel, shift" oncommand="cardbookMessenger.openCBWindow();" insertafter="key_fullZoomReduce"/>
	<key id="CardBookNewContactKey" key="__MSG_newCardBookCardMenuKey__" modifiers="accel, shift" oncommand="cardbookMessenger.newInCardBook();" insertafter="key_fullZoomReduce"/>

	<menupopup id="menu_NewPopup">
		<menuitem id="newCardBookCardMenu" label="__MSG_newCardBookCardMenuLabel__" accesskey="__MSG_newCardBookCardMenuAccesskey__"
			key="CardBookNewContactKey"
			insertafter="menu_newCard" oncommand="cardbookMessenger.newInCardBook();"/>
	</menupopup>

	<menupopup id="menu_FindPopup">
		<menuitem id="newCardBookSearchMenu" label="__MSG_newCardBookSearchMenuLabel__" accesskey="__MSG_newCardBookSearchMenuAccesskey__"
			insertafter="searchAddressesCmd" oncommand="cardbookMessenger.addSearchAddressbook();"/>
	</menupopup>

	<menupopup id="taskPopup">
		<menuitem id="cardbookMenuItem"
			label="__MSG_cardbookMenuItemLabel__" accesskey="__MSG_cardbookMenuItemAccesskey__"
			key="CardBookKey"
			tooltiptext="__MSG_cardbookMenuItemTooltip__"
			oncommand="cardbookMessenger.openCBWindow()"
			insertafter="addressBook"/>
	</menupopup>

	<menupopup id="appmenu_taskPopup">
		<menuitem id="cardbookAppMenuItem"
			label="__MSG_cardbookMenuItemLabel__" accesskey="__MSG_cardbookMenuItemAccesskey__"
			key="CardBookKey"
			tooltiptext="__MSG_cardbookMenuItemTooltip__"
			oncommand="cardbookMessenger.openCBWindow()"
			insertafter="appmenu_addressBook"/>
	</menupopup>

	<!-- Hidden browser so that we have something to print from. When printing, printing-template.html is loaded here. -->
	<browser id="cardbookPrintContent" type="content" remote="true" hidden="true"/>	

	<template xmlns="http://www.w3.org/1999/xhtml" id="cardbookPrintForm">
		<form id="cardbook-print">
			<section class="body-container">
				<section class="section-block">
					<label class="block-label">__MSG_settingsTitlelabel__</label>
					<label class="row cols-2">
						<input type="checkbox" id="displayHeadersCheckBox" checked="checked" autocomplete="off" />
						<span>__MSG_displayHeadersLabel__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="displayFieldNamesCheckBox" checked="checked" autocomplete="off" />
						<span>__MSG_displayFieldNamesLabel__</span>
					</label>
				</section>

				<section class="section-block">
					<label class="block-label">__MSG_fieldsToPrintGroupboxLabel__</label>
					<label class="row cols-2">
						<input type="checkbox" id="displayCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_displayLabel__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="personalCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_personalLabel__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="orgCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_org.print.label__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="customCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_custom.print.label__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="categoriesCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_categories.print.label__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="adrCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_adr.print.label__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="telCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_tel.print.label__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="emailCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_email.print.label__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="imppCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_impp.print.label__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="urlCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_url.print.label__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="eventCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_event.print.label__</span>
					</label>
					<label class="row cols-2">
						<input type="checkbox" id="noteCheckBox" checked="checked" autocomplete="off"/>
						<span>__MSG_note.print.label__</span>
					</label>
				</section>
			</section>
			<hr/>
			<footer class="footer-container" id="print-footer" role="none">
				<section id="button-container" class="section-block">
					<button is="cancel-button"
						type="button">__MSG_closeEditionLabel__</button>
					<button class="primary"
						type="submit"
						showfocus="">__MSG_wizard.next.label__</button>
				</section>
			</footer>
		</form>
	</template>

	`);

	window.cardbookMessenger.load();

	window.cardbookFilterRules.load();

	window.cardBookMessengerObserver.register();
};
