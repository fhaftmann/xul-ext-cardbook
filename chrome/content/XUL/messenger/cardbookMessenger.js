if ("undefined" == typeof(cardbookMessenger)) {
	var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

    var loader = Services.scriptloader;
    loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);
    loader.loadSubScript("chrome://cardbook/content/XUL/utils/CardBookXULNotifyListener.js", this);
    
	var cardbookMessenger = {

        newInCardBook: async function() {
            await notifyTools.notifyBackground({query: "cardbook.createContact"});
        },

        addSearchAddressbook: async function() {
            await notifyTools.notifyBackground({query: "cardbook.addAddressbook", action: "search"});
        },
    
        openCBWindow: async function() {
			await notifyTools.notifyBackground({query: "cardbook.openCBWindow"});
		},

		load: async function() {
            if (document.getElementById("addressBook")) {
                document.getElementById("addressBook").removeAttribute("key");
            }
            if (document.getElementById("appmenu_addressBook")) {
                document.getElementById("appmenu_addressBook").removeAttribute("key");
            }
            if (document.getElementById("key_addressbook")) {
                document.getElementById("key_addressbook").setAttribute("key", "");
            }
            
            // otherwise navigator.clipboard.read() does not exist
            Services.prefs.setBoolPref("dom.events.asyncClipboard.clipboardItem", true);
		},
    };
};
