if ("undefined" == typeof(wdw_cardbookContactsSidebar)) {
	var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;
	var { MailServices } = ChromeUtils.importESModule("resource:///modules/MailServices.sys.mjs");
	var { ExtensionParent } = ChromeUtils.importESModule("resource://gre/modules/ExtensionParent.sys.mjs");
	var { cardbookXULUtils } = ChromeUtils.import("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js");
	
	var loader = Services.scriptloader;
	loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

	var CardBookResultsPaneObserver = {
		onDragStart: function (aEvent) {
			let listOfEmails = wdw_cardbookContactsSidebar.getSelectedEmails().join(", ");
			aEvent.dataTransfer.setData("moz/abcard", listOfEmails);
			aEvent.dataTransfer.setData("moz/abcard", listOfEmails);
			aEvent.dataTransfer.setData("text/x-moz-address", listOfEmails);
			aEvent.dataTransfer.setData("text/unicode", listOfEmails);
			aEvent.dataTransfer.effectAllowed = "copyMove";
			aEvent.dataTransfer.addElement(aEvent.originalTarget);
			aEvent.stopPropagation();
		}
	};
	
	var wdw_cardbookContactsSidebar = {
		mutationObs: null,
		searchResults: [],
		ABInclRestrictions: {},
		ABExclRestrictions: {},
		catInclRestrictions: {},
		catExclRestrictions: {},

		sortTrees: function (aEvent) {
			if (aEvent.button != 0) {
				return;
			}
			var target = aEvent.originalTarget;
			if (target.localName == "treecol") {
				wdw_cardbookContactsSidebar.sortCardsTreeCol(target, "abResultsTree");
			}
		},

		sortCardsTreeCol: function (aColumn, aTreeName) {
			var myTree = document.getElementById(aTreeName);
			if (aColumn) {
				var listOfUid = [];
				var numRanges = myTree.view.selection.getRangeCount();
				var start = new Object();
				var end = new Object();
				for (var i = 0; i < numRanges; i++) {
					myTree.view.selection.getRangeAt(i,start,end);
					for (var j = start.value; j <= end.value; j++){
						listOfUid.push(myTree.view.getCellText(j, myTree.columns.getNamedColumn(aColumn.id)));
					}
				}
			}

			var columnName;
			var columnArray;
			var order = myTree.getAttribute("sortDirection") == "ascending" ? 1 : -1;
			
			// if the column is passed and it's already sorted by that column, reverse sort
			if (aColumn) {
				columnName = aColumn.id;
				if (myTree.getAttribute("sortResource") == columnName) {
					order *= -1;
				}
			} else {
				columnName = myTree.getAttribute("sortResource");
			}
			switch(columnName) {
				case "GeneratedName":
					columnArray=0;
					break;
				case "AB":
					columnArray=1;
					break;
				case "Emails":
					columnArray=2;
					break;
			}
			var myData = wdw_cardbookContactsSidebar.searchResults;

			if (myData && myData.length) {
				cardbookXULUtils.sortMultipleArrayByString(myData,columnArray,order);
			}

			//setting these will make the sort option persist
			myTree.setAttribute("sortDirection", order == 1 ? "ascending" : "descending");
			myTree.setAttribute("sortResource", columnName);
			
			wdw_cardbookContactsSidebar.displaySearchResults();
			
			//set the appropriate attributes to show to indicator
			var cols = myTree.getElementsByTagName("treecol");
			for (var i = 0; i < cols.length; i++) {
				cols[i].removeAttribute("sortDirection");
			}
			document.getElementById(columnName).setAttribute("sortDirection", order == 1 ? "ascending" : "descending");

			// select back
			if (aColumn) {
				for (var i = 0; i < listOfUid.length; i++) {
					for (var j = 0; j < myTree.view.rowCount; j++) {
						if (myTree.view.getCellText(j, myTree.columns.getNamedColumn(aColumn.id)) == listOfUid[i]) {
							myTree.view.selection.rangedSelect(j,j,true);
							break;
						}
					}
				}
			}
		},

		displaySearchResults: function () {
			var abResultsTreeView = {
				get rowCount() { return wdw_cardbookContactsSidebar.searchResults.length; },
				isContainer: function(idx) { return false },
				cycleHeader: function(idx) { return false },
				isEditable: function(idx, column) { return false },
				getCellText: function(idx, column) {
					if (column.id == "GeneratedName") return wdw_cardbookContactsSidebar.searchResults[idx][0];
					else if (column.id == "AB") return wdw_cardbookContactsSidebar.searchResults[idx][1];
					else if (column.id == "Emails") return wdw_cardbookContactsSidebar.searchResults[idx][2];
				},
				getRowProperties: function(idx) {
					if (wdw_cardbookContactsSidebar.searchResults[idx] && wdw_cardbookContactsSidebar.searchResults[idx][3]) {
						return "MailList";
					}
				},
				getColumnProperties: function(column) { return column.id },
				getCellProperties: function(idx, column) { return this.getRowProperties(idx) + " " +  this.getColumnProperties(column)}
			}
			document.getElementById("abResultsTree").view = abResultsTreeView;
		},
		
		isMyCardRestricted: function (aDirPrefId, aCard) {
			if (wdw_cardbookContactsSidebar.catExclRestrictions[aDirPrefId]) {
				let add = true;
				for (let l in wdw_cardbookContactsSidebar.catExclRestrictions[aDirPrefId]) {
					if (aCard.categories.includes(l)) {
						add = false;
						break;
					}
				}
				if (!add) {
					return true;
				}
			}
			if (wdw_cardbookContactsSidebar.catInclRestrictions[aDirPrefId]) {
				let add = false;
				for (let l in wdw_cardbookContactsSidebar.catInclRestrictions[aDirPrefId]) {
					if (aCard.categories.includes(l)) {
						add = true;
						break;
					}
				}
				if (!add) {
					return true;
				}
			}
			return false;
		},
		
		search: async function () {
			if (document.getElementById("cardbookpeopleSearchInput").value == "") {
				document.getElementById("cardbookpeopleSearchInput").placeholder = cardbookXULUtils.localizeMessage("cardbookSearchInputDefault");
			}
			wdw_cardbookContactsSidebar.searchResults = [];
			let composeWindow = Services.wm.getMostRecentWindow("msgcompose");
			let identity = composeWindow.document.getElementById("msgIdentity").selectedItem.getAttribute("identitykey");
			let searchAB = document.getElementById("CardBookABMenulist").value;
			let searchCategory = document.getElementById("categoriesMenulist").value;
			let searchInput = cardbookXULUtils.makeSearchString(document.getElementById("cardbookpeopleSearchInput").value);
			let useOnlyEmail = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "useOnlyEmail"});
			let showNameAs = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "showNameAs"});
            let accounts = await notifyTools.notifyBackground({query: "cardbook.getAccounts"});
			for (let account of accounts) {
				if (account[2]) {
					let dirPrefId = account[1];
                    let dirName = await notifyTools.notifyBackground({query: "cardbook.pref.getName", dirPrefId: dirPrefId});
					if (account[3] != "SEARCH") {
						if (cardbookXULUtils.verifyABRestrictions(dirPrefId, searchAB, wdw_cardbookContactsSidebar.ABExclRestrictions, wdw_cardbookContactsSidebar.ABInclRestrictions)) {
							// All No Only categories
							if ((searchCategory === "allCategories") || (searchCategory === "noCategory") || (searchCategory === "onlyCategories")) {
								if (searchCategory !== "onlyCategories") {
									let cards = await notifyTools.notifyBackground({query: "cardbook.contactssidebar.getCards", dirPrefId: dirPrefId});
									for (let j in cards) {
										if (j.includes(searchInput) || searchInput == "") {
											for (let card of cards[j]) {
												if (wdw_cardbookContactsSidebar.isMyCardRestricted(dirPrefId, card)) {
													continue;
												}
												if (card.isAList) {
													let email = MailServices.headerParser.makeMimeAddress(card.fn, card.fn);
													let conversionList = await notifyTools.notifyBackground({query: "cardbook.getListConversion", email: email, identity: identity});
													let emails = cardbookXULUtils.arrayUnique(conversionList.emailResult).join(", ");
													wdw_cardbookContactsSidebar.searchResults.push([cardbookXULUtils.getName(card, showNameAs), dirName,"", true, "LISTCARDBOOK", card, emails, ""]);
												} else {
													if (card.emails != "") {
														let formattedEmails = cardbookXULUtils.getMimeEmailsFromCards([card], useOnlyEmail);
														wdw_cardbookContactsSidebar.searchResults.push([cardbookXULUtils.getName(card, showNameAs), dirName, card.emails.join(", "), false, "CARDCARDBOOK", card, formattedEmails.join("@@@@@"), ""]);
													}
												}
											}
										}
									}
								}
								if (searchCategory !== "noCategory") {
									let categories = await notifyTools.notifyBackground({query: "cardbook.getAccountsCategories", dirPrefId: dirPrefId});
									for (let category of categories) {
										if (cardbookXULUtils.verifyCatRestrictions(dirPrefId, category, searchInput, wdw_cardbookContactsSidebar.ABExclRestrictions,
																					wdw_cardbookContactsSidebar.catExclRestrictions, wdw_cardbookContactsSidebar.catInclRestrictions)) {
											let emails = [] ;
											let formattedEmails = [];
											let cards = await notifyTools.notifyBackground({query: "cardbook.getDisplayCards", accountId: `${dirPrefId}::categories::${category}`});
											for (let card of cards) {
												if (card.isAList) {
													emails.push(card.fn);
													formattedEmails.push(MailServices.headerParser.makeMimeAddress(card.fn, card.fn));
												} else {
													emails = emails.concat(card.emails);
													formattedEmails = formattedEmails.concat(cardbookXULUtils.getMimeEmailsFromCards([card], useOnlyEmail));
												}
											}
											if (emails != "") {
												wdw_cardbookContactsSidebar.searchResults.push([category, dirName, emails.join(", "), true, "CATCARDBOOK", `${dirPrefId}::categories::${category}`, formattedEmails.join("@@@@@"), ""]);
											}
										}
									}
								}
							// One category
							} else {
								let category = cardbookXULUtils.getNodeName(searchCategory);
								function searchArray(element) {
									return element == category;
								};
								let cards = await notifyTools.notifyBackground({query: "cardbook.contactssidebar.getCards", dirPrefId: dirPrefId});
								for (let j in cards) {
									if (j.includes(searchInput) || searchInput == "") {
										for (let card of cards[j]) {
											let uncat = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "uncategorizedCards"});
											if (((card.categories.find(searchArray) != undefined) && (uncat != category))
												|| ((card.categories.length == 0) && (uncat == category))) {
												if (wdw_cardbookContactsSidebar.catExclRestrictions[dirPrefId]) {
													let add = true;
													for (let l in wdw_cardbookContactsSidebar.catExclRestrictions[dirPrefId]) {
														if (card.categories.includes(l)) {
															add = false;
															break;
														}
													}
													if (!add) {
														continue;
													}
												}
												if (card.isAList) {
													let email = MailServices.headerParser.makeMimeAddress(card.fn, card.fn);
													let conversionList = await notifyTools.notifyBackground({query: "cardbook.getListConversion", email: email, identity: identity});
													let emails = cardbookXULUtils.arrayUnique(conversionList.emailResult).join(", ");
													wdw_cardbookContactsSidebar.searchResults.push([cardbookXULUtils.getName(card, showNameAs), dirName,"", true, "LISTCARDBOOK", card, emails, ""]);
												} else {
													if (card.emails != "") {
														let formattedEmails = cardbookXULUtils.getMimeEmailsFromCards([card], useOnlyEmail);
														wdw_cardbookContactsSidebar.searchResults.push([cardbookXULUtils.getName(card, showNameAs), dirName, card.emails.join(", "), false, "CARDCARDBOOK", card, formattedEmails.join("@@@@@"), ""]);
													}
												}
											}
										}
									}
								}
								if (cardbookXULUtils.makeSearchString(category).includes(searchInput) || searchInput == "") {
									let emails = [] ;
									let formattedEmails = [];
									let cards = await notifyTools.notifyBackground({query: "cardbook.getDisplayCards", accountId: searchCategory});
									for (let card of cards) {
										if (wdw_cardbookContactsSidebar.catExclRestrictions[dirPrefId]) {
											let add = true;
											for (let l in wdw_cardbookContactsSidebar.catExclRestrictions[dirPrefId]) {
												if (card.categories.includes(l)) {
													add = false;
													break;
												}
											}
											if (!add) {
												continue;
											}
										}
										if (card.isAList) {
											emails.push(card.fn);
											formattedEmails.push(MailServices.headerParser.makeMimeAddress(card.fn, card.fn));
										} else {
											emails = emails.concat(card.emails);
											formattedEmails = formattedEmails.concat(cardbookXULUtils.getMimeEmailsFromCards([card], useOnlyEmail));
										}
									}
									if (emails != "") {
										wdw_cardbookContactsSidebar.searchResults.push([category, dirName, emails.join(", "), true, "CATCARDBOOK", searchCategory, formattedEmails.join("@@@@@"), ""]);
									}
								}
							}
						}
					// complex searches
					} else if ((account[3] === "SEARCH") && ((searchAB == dirPrefId))) {
						if (cardbookXULUtils.verifyABRestrictions(dirPrefId, searchAB, wdw_cardbookContactsSidebar.ABExclRestrictions, wdw_cardbookContactsSidebar.ABInclRestrictions)) {
							// first add cards
							if ((searchCategory === "allCategories") || (searchCategory === "noCategory") || (searchCategory === "onlyCategories")) {
								let cards = await notifyTools.notifyBackground({query: "cardbook.getDisplayCards", accountId: dirPrefId});
								for (let card of cards) {
									// All No categories
									if (searchCategory !== "onlyCategories") {
										let dirName = await notifyTools.notifyBackground({query: "cardbook.pref.getName", dirPrefId: card.dirPrefId});
										let searchString = await notifyTools.notifyBackground({query: "cardbook.getLongSearchString", card: card});
										if (searchString.includes(searchInput) || searchInput == "") {
											if (card.isAList) {
												let email = MailServices.headerParser.makeMimeAddress(card.fn, card.fn);
												let conversionList = await notifyTools.notifyBackground({query: "cardbook.getListConversion", email: email, identity: identity});
												let emails = cardbookXULUtils.arrayUnique(conversionList.emailResult).join(", ");
												wdw_cardbookContactsSidebar.searchResults.push([cardbookXULUtils.getName(card, showNameAs), dirName,"", true, "LISTCARDBOOK", card, emails, ""]);
											} else {
												if (card.emails != "") {
													let formattedEmails = [];
													formattedEmails = cardbookXULUtils.getMimeEmailsFromCards([card], useOnlyEmail);
													wdw_cardbookContactsSidebar.searchResults.push([cardbookXULUtils.getName(card, showNameAs), dirName, card.emails.join(", "), false, "CARDCARDBOOK", card, formattedEmails.join("@@@@@"), ""]);
												}
											}
										}
									}
								}
							// one category
							} else {
								let category = cardbookXULUtils.getNodeName(searchCategory);
								let cards = await notifyTools.notifyBackground({query: "cardbook.getDisplayCards", accountId: `${dirPrefId}::categories::${category}`});
								for (let card of cards) {
									let dirName = await notifyTools.notifyBackground({query: "cardbook.pref.getName", dirPrefId: card.dirPrefId});
									// All No categories
									if (searchCategory !== "onlyCategories") {
										let searchString = await notifyTools.notifyBackground({query: "cardbook.getLongSearchString", card: card});
										if (searchString.includes(searchInput) || searchInput == "") {
											if (card.isAList) {
												let email = MailServices.headerParser.makeMimeAddress(card.fn, card.fn);
												let conversionList = await notifyTools.notifyBackground({query: "cardbook.getListConversion", email: email, identity: identity});
												let emails = cardbookXULUtils.arrayUnique(conversionList.emailResult).join(", ");
												wdw_cardbookContactsSidebar.searchResults.push([cardbookXULUtils.getName(card, showNameAs), dirName,"", true, "LISTCARDBOOK", card, emails, ""]);
											} else {
												if (card.emails != "") {
													let formattedEmails = [];
													formattedEmails = cardbookXULUtils.getMimeEmailsFromCards([card], useOnlyEmail);
													wdw_cardbookContactsSidebar.searchResults.push([cardbookXULUtils.getName(card, showNameAs), dirName, card.emails.join(", "), false, "CARDCARDBOOK", card, formattedEmails.join("@@@@@"), ""]);
												}
											}
										}
									}
								}
							}
						}
						// then add categories
						if (searchCategory !== "noCategory") {
							let categories = await notifyTools.notifyBackground({query: "cardbook.getAccountsCategories", dirPrefId: dirPrefId});
							for (let category of categories) {
								if (cardbookXULUtils.verifyCatRestrictions(dirPrefId, category, searchInput, wdw_cardbookContactsSidebar.ABExclRestrictions,
																			wdw_cardbookContactsSidebar.catExclRestrictions, wdw_cardbookContactsSidebar.catInclRestrictions)) {
									if ((searchCategory !== "allCategories") && (searchCategory !== "noCategory") && (searchCategory !== "onlyCategories")) {
										if (`${dirPrefId}::categories::${category}` != searchCategory) {
											continue;
										}
									}
									let emails = [] ;
									let formattedEmails = [];
									let cards = await notifyTools.notifyBackground({query: "cardbook.getDisplayCards", accountId: `${dirPrefId}::categories::${category}`});
									for (let card of cards) {
										if (card.isAList) {
											emails.push(card.fn);
											formattedEmails.push(MailServices.headerParser.makeMimeAddress(card.fn, card.fn));
										} else {
											emails = emails.concat(card.emails);
											formattedEmails = formattedEmails.concat(cardbookXULUtils.getMimeEmailsFromCards([card], useOnlyEmail));
										}
									}
									if (emails != "") {
										wdw_cardbookContactsSidebar.searchResults.push([category, dirName, emails.join(", "), true, "CATCARDBOOK", `${dirPrefId}::categories::${category}`, formattedEmails.join("@@@@@"), ""]);
									}
								}
							}
						}
						break;
					}
				}
			}
			let exclusive = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "exclusive"});
			if (!exclusive) {
				let lastnamefirst = Services.prefs.getIntPref("mail.addr_book.lastnamefirst");
				for (let addrbook of MailServices.ab.directories) {
					if (cardbookXULUtils.verifyABRestrictions(addrbook.dirPrefId, searchAB, wdw_cardbookContactsSidebar.ABExclRestrictions, wdw_cardbookContactsSidebar.ABInclRestrictions)) {
						for (let myABCard of addrbook.childCards) {					
							let emails = myABCard.getProperty("PrimaryEmail","");
							let myDisplayName = myABCard.generateName(lastnamefirst);
							if (!myABCard.isMailList) {
								if (emails != "") {
									let lSearchString = myABCard.getProperty("FirstName","") + myABCard.getProperty("LastName","") + myDisplayName + myABCard.getProperty("NickName","") + emails;
									lSearchString = cardbookXULUtils.makeSearchString(lSearchString);
									if (lSearchString.includes(searchInput) || searchInput == "") {
										if (myDisplayName == "") {
											let delim = emails.indexOf("@",0);
											myDisplayName = emails.substr(0,delim);
										}
										if (useOnlyEmail) {
											wdw_cardbookContactsSidebar.searchResults.push([myDisplayName, addrbook.dirName, emails, false, "CARDCORE", myABCard, emails, addrbook.dirPrefId]);
										} else {
											wdw_cardbookContactsSidebar.searchResults.push([myDisplayName, addrbook.dirName, emails, false, "CARDCORE", myABCard, MailServices.headerParser.makeMimeAddress(myDisplayName, emails), addrbook.dirPrefId]);
										}
									}
								}
							} else {
								let myABList = MailServices.ab.getDirectory(myABCard.mailListURI);
								let lSearchString = myDisplayName + myABList.listNickName + myABList.description;
								lSearchString = cardbookXULUtils.makeSearchString(lSearchString);
								if (lSearchString.includes(searchInput) || searchInput == "") {
										wdw_cardbookContactsSidebar.searchResults.push([myDisplayName, addrbook.dirName, "", true, "LISTCORE", myABCard, MailServices.headerParser.makeMimeAddress(myDisplayName, myDisplayName), addrbook.dirPrefId]);
								}
							}
						}
					}
				}
			}
			wdw_cardbookContactsSidebar.sortCardsTreeCol(null, "abResultsTree");
		},

		addEmails: function (aType) {
			let listOfEmails = wdw_cardbookContactsSidebar.getSelectedEmails();
			parent.addressRowAddRecipientsArray(parent.document.querySelector(`.address-row[data-recipienttype="${aType}"]`), listOfEmails);
		},

		getSelectedEmails: function () {
			let myTree = document.getElementById("abResultsTree");
			let listOfEmails = [];
			let numRanges = myTree.view.selection.getRangeCount();
			let start = new Object();
			let end = new Object();
			for (let i = 0; i < numRanges; i++) {
				myTree.view.selection.getRangeAt(i,start,end);
				for (let j = start.value; j <= end.value; j++){
					let allEmails = wdw_cardbookContactsSidebar.searchResults[j][6].split("@@@@@");
					for (let k = 0; k < allEmails.length; k++) {
						listOfEmails.push(allEmails[k]);
					}
				}
			}
			return listOfEmails;
		},

		getSelectedCards: function () {
			let myTree = document.getElementById("abResultsTree");
			let listOfUid = [];
			let numRanges = myTree.view.selection.getRangeCount();
			let start = new Object();
			let end = new Object();
			for (let i = 0; i < numRanges; i++) {
				myTree.view.selection.getRangeAt(i,start,end);
				for (let j = start.value; j <= end.value; j++){
					listOfUid.push([wdw_cardbookContactsSidebar.searchResults[j][4], wdw_cardbookContactsSidebar.searchResults[j][5], wdw_cardbookContactsSidebar.searchResults[j][7]]);
				}
			}
			return listOfUid;
		},

		doubleClickCards: function (aEvent) {
			let myTree = document.getElementById("abResultsTree");
			if (myTree.currentIndex != -1) {
				let cell = myTree.getCellAt(aEvent.clientX, aEvent.clientY);
				if (cell.row != -1) {
					wdw_cardbookContactsSidebar.addEmails("addr_to");
				}
			}
		},

		deleteCard: async function () {
			let listOfUid = wdw_cardbookContactsSidebar.getSelectedCards();
			let AB = MailServices.ab.getDirectoryFromId(listOfUid[0][2]);
			for (let i = 0; i < listOfUid.length; i++) {
				if (listOfUid[i][0] === "CARDCARDBOOK" || listOfUid[i][0] === "LISTCARDBOOK") {
					await notifyTools.notifyBackground({query: "cardbook.deleteCardsAndValidate", listOfCardsId: [listOfUid[i][1].cbid]});
				} else if (listOfUid[i][0] === "CATCARDBOOK") {
					await notifyTools.notifyBackground({query: "cardbook.removeNode", node: listOfUid[i][1]});
				} else if (listOfUid[i][0] === "LISTCORE") {
					gAddressBookBundle = document.getElementById("bundle_addressBook");
					let card = listOfUid[i][1];
					AbDeleteDirectory(card.mailListURI);
				} else if (listOfUid[i][0] === "CARDCORE") {
					gAddressBookBundle = document.getElementById("bundle_addressBook");
					let card = listOfUid[i][1];
					try {
						var confirmDeleteMessage = gAddressBookBundle.getString("confirmDeleteContact");
						var confirmDeleteTitle = null;
					}
					// for new Thunderbird versions
					catch (e) {
						var confirmDeleteMessage = gAddressBookBundle.getString("confirmDeleteThisContact");
						confirmDeleteMessage = confirmDeleteMessage.replace("#1", card.displayName);
						var confirmDeleteTitle = gAddressBookBundle.getString("confirmDeleteThisContactTitle");
					}
					if (Services.prompt.confirm(window, confirmDeleteTitle, confirmDeleteMessage)) {
						let cardArray = Components.classes["@mozilla.org/array;1"].createInstance(Components.interfaces.nsIMutableArray);
						cardArray.appendElement(card, false);
						AB.deleteCards(cardArray);
					}
				}
			}
			await wdw_cardbookContactsSidebar.search();
		},

		editCard: async function () {
			let listOfUid = wdw_cardbookContactsSidebar.getSelectedCards();
			if (listOfUid[0][0] === "CARDCARDBOOK" || listOfUid[0][0] === "LISTCARDBOOK") {
				let card = listOfUid[0][1];
				await notifyTools.notifyBackground({query: "cardbook.editOrViewContact", card: card});
			} else if (listOfUid[0][0] === "CARDCORE") {
				let AB =  MailServices.ab.getDirectoryFromId(listOfUid[0][2]);
				let card = listOfUid[0][1];
				goEditCardDialog(AB.URI, card);
			} else if (listOfUid[0][0] === "LISTCORE") {
				let card = listOfUid[0][1];
				try {
					goEditListDialog(card, card.mailListURI);
				}
				catch (e) {
				}
			} else if (listOfUid[0][0] === "CATCARDBOOK") {
				await notifyTools.notifyBackground({query: "cardbook.renameNode", node: listOfUid[0][1]});
			}
			await wdw_cardbookContactsSidebar.search();
		},

		newCard: async function () {
			let dirPrefId = document.getElementById("CardBookABMenulist").value
			await notifyTools.notifyBackground({query: "cardbook.createContact", dirPrefId: dirPrefId});
		},

		newList: async function () {
			let dirPrefId = document.getElementById("CardBookABMenulist").value
			await notifyTools.notifyBackground({query: "cardbook.createList", dirPrefId: dirPrefId});
		},

		selectAllKey: function () {
			var myTree = document.getElementById("abResultsTree");
			myTree.view.selection.selectAll();
		},

		cardPropertiesMenuContextShowing: async function (aEvent) {
			if (cardbookXULUElementTools.displayColumnsPicker(aEvent)) {
				await wdw_cardbookContactsSidebar.cardPropertiesMenuContextShowingNext();
				return true;
			} else {
				return false;
			}
		},

		cardPropertiesMenuContextShowingNext: async function () {
			var listOfUid = wdw_cardbookContactsSidebar.getSelectedCards();
			if (listOfUid.length != 0) {
				if (listOfUid.length != 1) {
					document.getElementById("editCard").disabled=true;
				} else {
					if (listOfUid[0][0] == "CATCARDBOOK") {
						let dirPrefId = cardbookXULUtils.getAccountId(listOfUid[0][1]);
						let uncat = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "uncategorizedCards"});
						let readOnly = await notifyTools.notifyBackground({query: "cardbook.pref.getReadOnly", dirPrefId: dirPrefId});
						if (readOnly) {
							wdw_cardbookContactsSidebar.disableCardModification();
						} else if (listOfUid[0][1] == `${dirPrefId}::categories::${uncat}`) {
							wdw_cardbookContactsSidebar.disableCardDeletion();
						} else {
							wdw_cardbookContactsSidebar.enableCardModification();
						}
					} else if (listOfUid[0][0] == "CARDCARDBOOK" || listOfUid[0][0] == "LISTCARDBOOK") {
						let dirPrefId = listOfUid[0][1].dirPrefId;
						let readOnly = await notifyTools.notifyBackground({query: "cardbook.pref.getReadOnly", dirPrefId: dirPrefId});
						if (readOnly) {
							wdw_cardbookContactsSidebar.disableCardModification();
						} else {
							wdw_cardbookContactsSidebar.enableCardModification();
						}
					} else {
						wdw_cardbookContactsSidebar.enableCardModification();
					}
				}
				document.getElementById("toEmail").disabled=false;
				document.getElementById("ccEmail").disabled=false;
				document.getElementById("bccEmail").disabled=false;
				document.getElementById("replytoEmail").disabled=false;
			} else {
				document.getElementById("toEmail").disabled=true;
				document.getElementById("ccEmail").disabled=true;
				document.getElementById("bccEmail").disabled=true;
				document.getElementById("replytoEmail").disabled=true;
				wdw_cardbookContactsSidebar.disableCardModification();
			}
		},

		disableCardDeletion: function () {
			document.getElementById("editCard").disabled=false;
			document.getElementById("deleteCard").disabled=true;
		},
		
		disableCardModification: function () {
			document.getElementById("editCard").disabled=true;
			document.getElementById("deleteCard").disabled=true;
		},
		
		enableCardModification: function () {
			document.getElementById("editCard").disabled=false;
			document.getElementById("deleteCard").disabled=false;
		},
		
		loadPanel: async function () {
			let extension = ExtensionParent.GlobalManager.getExtension("cardbook@vigneau.philippe");
			i18n.updateDocument({ extension: extension });
			if (location.search == "?focus") {
				document.getElementById("cardbookpeopleSearchInput").focus();
			}
			document.getElementById("abContextMenuButton").hidden=false;
			cardBookContactsSideBarObserver.register();
			document.title = parent.document.getElementById("contactsTitle").value;
			await wdw_cardbookContactsSidebar.loadAB();
		},
		
		unloadPanel: function () {
			cardBookContactsSideBarObserver.unregister();
		},
		
		loadRestrictions: async function () {
			let composeWindow = Services.wm.getMostRecentWindow("msgcompose");
			let identityId = composeWindow.document.getElementById("msgIdentity").selectedItem.getAttribute("identitykey");
			let result = await notifyTools.notifyBackground({query: "cardbook.pref.getAllRestrictions"});
			wdw_cardbookContactsSidebar.ABInclRestrictions = {};
			wdw_cardbookContactsSidebar.ABExclRestrictions = {};
			wdw_cardbookContactsSidebar.catInclRestrictions = {};
			wdw_cardbookContactsSidebar.catExclRestrictions = {};
			for (var i = 0; i < result.length; i++) {
				var resultArray = result[i];
				if ((resultArray[0] == "true") && (resultArray[3] != "") && ((resultArray[2] == identityId) || (resultArray[2] == "allMailAccounts"))) {
					if (resultArray[1] == "include") {
						wdw_cardbookContactsSidebar.ABInclRestrictions[resultArray[3]] = 1;
						if (resultArray[4]) {
							if (!(wdw_cardbookContactsSidebar.catInclRestrictions[resultArray[3]])) {
								wdw_cardbookContactsSidebar.catInclRestrictions[resultArray[3]] = {};
							}
							wdw_cardbookContactsSidebar.catInclRestrictions[resultArray[3]][resultArray[4]] = 1;
						}
					} else {
						if (resultArray[4]) {
							if (!(wdw_cardbookContactsSidebar.catExclRestrictions[resultArray[3]])) {
								wdw_cardbookContactsSidebar.catExclRestrictions[resultArray[3]] = {};
							}
							wdw_cardbookContactsSidebar.catExclRestrictions[resultArray[3]][resultArray[4]] = 1;
						} else {
							wdw_cardbookContactsSidebar.ABExclRestrictions[resultArray[3]] = 1;
						}
					}
				}
			}
			wdw_cardbookContactsSidebar.ABInclRestrictions["length"] = cardbookXULUtils.sumElements(wdw_cardbookContactsSidebar.ABInclRestrictions);
		},
		
		loadAB: async function (aParams) {
			wdw_cardbookContactsSidebar.loadRestrictions();
			var ABList = document.getElementById("CardBookABMenulist");
			var ABPopup = document.getElementById("CardBookABMenupopup");
			if (ABList.value) {
				var ABDefaultValue = ABList.value;
			} else {
				var ABDefaultValue = 0;
			}
			let exclusive = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "exclusive"});
            let accounts = await notifyTools.notifyBackground({query: "cardbook.getAccounts"});
			cardbookXULUElementTools.loadAddressBooks(accounts, ABPopup, ABList, ABDefaultValue, exclusive, true, true, true, false,
													wdw_cardbookContactsSidebar.ABInclRestrictions, wdw_cardbookContactsSidebar.ABExclRestrictions);
			await wdw_cardbookContactsSidebar.onABChange(aParams);
			
			document.getElementById("cardbookpeopleSearchInput").placeholder = cardbookXULUtils.localizeMessage("cardbookSearchInputDefault");
		},
		
		onABChange: async function (aParams) {
			// no need to refresh cards for others syncing dirprefid
			let syncCondition = true;
			if (aParams) {
				let ABValue = document.getElementById("CardBookABMenulist").value;
				let dirPrefId = cardbookXULUtils.getAccountId(aParams);
				syncCondition = (ABValue == dirPrefId || ABValue == "allAddressBooks");
			}
			if (syncCondition) {
				let ABList = document.getElementById("CardBookABMenulist");
				let ABDefaultValue = (ABList.value) ? ABList.value : 0;
				let categoryList = document.getElementById("categoriesMenulist");
				let categoryDefaultValue = (categoryList.value) ? categoryList.value : 0;
				let categories = await notifyTools.notifyBackground({query: "cardbook.getAccountsCategories", dirPrefId: ABDefaultValue});
				cardbookXULUElementTools.loadCategories(categories, "categoriesMenupopup", "categoriesMenulist", ABDefaultValue, categoryDefaultValue, true, true, true, false,
													wdw_cardbookContactsSidebar.catInclRestrictions, wdw_cardbookContactsSidebar.catExclRestrictions);
				
				if (document.getElementById("categoriesMenulist").itemCount == 3) {
					document.getElementById("categoriesPickerLabel").setAttribute("hidden", "true");
					document.getElementById("categoriesMenulist").setAttribute("hidden", "true");
				} else {
					document.getElementById("categoriesPickerLabel").removeAttribute("hidden");
					document.getElementById("categoriesMenulist").removeAttribute("hidden");
				}
			
				await wdw_cardbookContactsSidebar.search();
			}
		},

		onSearchEntered: async function () {
			let ABValue = document.getElementById("CardBookABMenulist").value;
			if (ABValue != "allAddressBooks") {
				if (document.getElementById("CardBookABMenulist").selectedItem.getAttribute("ABtype") == "standard-abook") {
					await wdw_cardbookContactsSidebar.search();
				} else {
					let cached = await notifyTools.notifyBackground({query: "cardbook.pref.getDBCached", dirPrefId: ABValue});
					let remote = await notifyTools.notifyBackground({query: "cardbook.isMyAccountRemote", dirPrefId: ABValue});
					if (!cached && remote) {
						let searchValue = document.getElementById("cardbookpeopleSearchInput").value;
						let syncing = await notifyTools.notifyBackground({query: "cardbook.isMyAccountSyncing", dirPrefId: ABValue});
						if (syncing || searchValue == "") {
							return;
						}
						await notifyTools.notifyBackground({query: "cardbook.contactssidebar.setLastSearch", dirPrefId: ABValue, value: searchValue});
						await notifyTools.notifyBackground({query: "cardbook.contactssidebar.searchRemote", dirPrefId: ABValue, value: searchValue});
					} else {
						await wdw_cardbookContactsSidebar.search();
					}
				}
			} else {
				await wdw_cardbookContactsSidebar.search();
			}
		},

		// works only when the restrictions are changed
		onRestrictionsChanged: async function () {
			await wdw_cardbookContactsSidebar.loadAB();
		},

		onIdentityChanged: async function () {
			await wdw_cardbookContactsSidebar.loadAB();
		},

		onCategoryChange: async function () {
			await wdw_cardbookContactsSidebar.search();
		}

	}
};
