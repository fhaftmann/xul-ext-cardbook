var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

var { cardbookXULUtils } = ChromeUtils.import("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js");

var loader = Services.scriptloader;
loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

if ("undefined" == typeof(cardbookXULUElementTools)) {
    var EXPORTED_SYMBOLS = ["cardbookXULUElementTools"];
	var cardbookXULUElementTools = {

		deleteRows: function (aObjectName) {
			// for anonid this does not work
			try {
				var aListRows = document.getElementById(aObjectName);
				while (aListRows.hasChildNodes()) {
					aListRows.lastChild.remove();
				}
			} catch (e) {}
		},

		loadAddressBooks: function (aAccounts, aPopup, aMenu, aDefaultId, aExclusive, aAddAllABs, aIncludeReadOnly, aIncludeSearch, aIncludeDisabled,
            aInclRestrictionList, aExclRestrictionList) {
            cardbookXULUElementTools.deleteRows(aPopup.id);
            let defaultIndex = 0;
            let j = 0;
            if (aAddAllABs) {
                let menuItem = document.createXULElement("menuitem");
                menuItem.setAttribute("label", cardbookXULUtils.localizeMessage("allAddressBooks"));
                menuItem.setAttribute("value", "allAddressBooks");
                menuItem.setAttribute("class", "menuitem-iconic");
                aPopup.appendChild(menuItem);
                if ("allAddressBooks" == aDefaultId) {
                    defaultIndex=j;
                }
                j++;
            }
            let sortedAddressBooks = [];
            for (let account of aAccounts) {
                if ((aIncludeDisabled || account[2])
                    && (aIncludeReadOnly || !account[4])
                    && (aIncludeSearch || (account[3] !== "SEARCH"))) {
                    if (aExclRestrictionList && aExclRestrictionList[account[1]]) {
                     continue;
                    }
                    if (aInclRestrictionList && aInclRestrictionList.length > 0) {
                        if (aInclRestrictionList[account[1]]) {
                            sortedAddressBooks.push([account[0], account[1], cardbookXULUtils.getABIconType(account[3])]);
                        }
                    } else {
                        sortedAddressBooks.push([account[0], account[1], cardbookXULUtils.getABIconType(account[3])]);
                    }
                }
            }
            if (!aExclusive) {
                for (let addrbook of MailServices.ab.directories) {
                    // remote LDAP directory
                    if (addrbook.isRemote && addrbook.dirType === 0) {
                        continue;
                    }
                    if (aInclRestrictionList && aInclRestrictionList.length > 0) {
                        if (aInclRestrictionList[addrbook.dirPrefId]) {
                            sortedAddressBooks.push([addrbook.dirName, addrbook.dirPrefId, "standard-abook"]);
                        }
                    } else {
                        sortedAddressBooks.push([addrbook.dirName, addrbook.dirPrefId, "standard-abook"]);
                    }
                }
            }
            cardbookXULUtils.sortMultipleArrayByString(sortedAddressBooks,0,1);
            for (let addressbook of sortedAddressBooks) {
                let menuItem = document.createXULElement("menuitem");
                menuItem.setAttribute("label", addressbook[0]);
                menuItem.setAttribute("value", addressbook[1]);
                menuItem.setAttribute("ABtype", addressbook[2]);
                menuItem.setAttribute("class", "menuitem-iconic");
                aPopup.appendChild(menuItem);
                if (addressbook[1] == aDefaultId) {
                    defaultIndex=j;
                }
                j++;
            }
            aMenu.selectedIndex = defaultIndex;
        },

		loadCategories: function (aCategories, aPopupName, aMenuName, aDefaultPrefId, aDefaultCatId, aAddAllCats, aAddOnlyCats, aAddNoCats, aAddEmptyCats, aInclRestrictionList, aExclRestrictionList) {
			let myPopup = document.getElementById(aPopupName);
			cardbookXULUElementTools.deleteRows(aPopupName);
			let defaultIndex = 0;
			let j = 0;
			if (aAddEmptyCats) {
				let menuItem = document.createXULElement("menuitem");
				menuItem.setAttribute("label", "");
				menuItem.setAttribute("value", "");
				menuItem.setAttribute("class", "menuitem-iconic");
				menuItem.setAttribute("type", "radio");
				myPopup.appendChild(menuItem);
				j++;
			}
			if (!(aInclRestrictionList && aInclRestrictionList[aDefaultPrefId])) {
				if (aAddAllCats) {
					let menuItem = document.createXULElement("menuitem");
					menuItem.setAttribute("label", cardbookXULUtils.localizeMessage("allCategories"));
					menuItem.setAttribute("value", "allCategories");
					menuItem.setAttribute("class", "menuitem-iconic");
					menuItem.setAttribute("type", "radio");
					myPopup.appendChild(menuItem);
					if ("allCategories" == aDefaultCatId) {
						defaultIndex=j;
					}
					j++;
				}
				if (aAddOnlyCats) {
					var menuItem = document.createXULElement("menuitem");
					menuItem.setAttribute("label", cardbookXULUtils.localizeMessage("onlyCategories"));
					menuItem.setAttribute("value", "onlyCategories");
					menuItem.setAttribute("class", "menuitem-iconic");
					menuItem.setAttribute("type", "radio");
					myPopup.appendChild(menuItem);
					if ("onlyCategories" == aDefaultCatId) {
						defaultIndex=j;
					}
					j++;
				}
				if (aAddNoCats) {
					var menuItem = document.createXULElement("menuitem");
					menuItem.setAttribute("label", cardbookXULUtils.localizeMessage("noCategory"));
					menuItem.setAttribute("value", "noCategory");
					menuItem.setAttribute("class", "menuitem-iconic");
					menuItem.setAttribute("type", "radio");
					myPopup.appendChild(menuItem);
					if ("noCategory" == aDefaultCatId) {
						defaultIndex=j;
					}
					j++;
				}
			}
			var sortedCategories = [];
            for (let category of aCategories) {
                if (aExclRestrictionList && aExclRestrictionList[aDefaultPrefId] && aExclRestrictionList[aDefaultPrefId][category]) {
                    continue;
                }
                if (aInclRestrictionList && aInclRestrictionList[aDefaultPrefId]) {
                    if (aInclRestrictionList[aDefaultPrefId][category]) {
                        sortedCategories.push([category, `${aDefaultPrefId}::categories::${category}`]);
                    }
                } else {
                    sortedCategories.push([category, `${aDefaultPrefId}::categories::${category}`]);
                }
            }
			cardbookXULUtils.sortMultipleArrayByString(sortedCategories,0,1);
			for (var i = 0; i < sortedCategories.length; i++) {
				var menuItem = document.createXULElement("menuitem");
				menuItem.setAttribute("label", sortedCategories[i][0]);
				menuItem.setAttribute("value", sortedCategories[i][1]);
				menuItem.setAttribute("class", "menuitem-iconic");
				menuItem.setAttribute("type", "radio");
				myPopup.appendChild(menuItem);
				if (sortedCategories[i][1] == aDefaultCatId) {
					defaultIndex=j;
				}
				j++;
			}
			document.getElementById(aMenuName).selectedIndex = defaultIndex;
			document.getElementById(aMenuName).selectedItem.setAttribute("checked", "true");
		},

		displayColumnsPicker: function (aEvent) {
			let target = aEvent.explicitOriginalTarget;
			if (target.localName == "treecol") {
				let treeColPicker = target.parentNode.querySelector("treecolpicker");
				let popup = treeColPicker.querySelector(`menupopup[anonid="popup"]`);
				treeColPicker.buildPopup(popup);
				popup.openPopup(target, "after_start", 0, 0, true);
				return false;
			}
			return true;
		},
	}
};
