var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

var loader = Services.scriptloader;
loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

if ("undefined" == typeof(cardbookXULCSS)) {
    var EXPORTED_SYMBOLS = ["cardbookXULCSS"];
	var cardbookXULCSS = {

		getTextColorFromBackgroundColor: function (aHexBackgroundColor) {
			function hexToRgb(aColor) {
				let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(aColor);
				return result ? {r: parseInt(result[1], 16), g: parseInt(result[2], 16), b: parseInt(result[3], 16)} : null;
			}
	
			let rgbColor = hexToRgb(aHexBackgroundColor);
			// http://www.w3.org/TR/AERT#color-contrast
			let o = Math.round(((parseInt(rgbColor.r) * 299) + (parseInt(rgbColor.g) * 587) + (parseInt(rgbColor.b) * 114)) / 1000);
			let fore = (o > 125) ? "black" : "white";
			return fore;
		},

        formatCategoryForCss: function (aCategory) {
            // avoid starting with number
            return "c" + aCategory.replace(/[!\"#$%&'\(\)\*\+,\.\/:;<=>\?\@\[\\\]\^`\{\|\}~ ]/g, "_");
        },

        deleteCssAllRules: function (aStyleSheet) {
            // aStyleSheet.cssRules may not be available
            try {
                while(aStyleSheet.cssRules.length > 0) {
                    aStyleSheet.deleteRule(0);
                }
            } catch(e) {}
    
        },
    
        createMarkerRule: function (aStyleSheet, aStyleSheetRuleName) {
            let ruleString = `.${aStyleSheetRuleName} {}`;
            let ruleIndex = aStyleSheet.insertRule(ruleString, aStyleSheet.cssRules.length);
        },
    
    }
};
