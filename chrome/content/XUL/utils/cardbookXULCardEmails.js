if ("undefined" == typeof(cardbookXULCardEmails)) {
    var EXPORTED_SYMBOLS = ["cardbookXULCardEmails"];
	var cardbookXULCardEmails = {
        emails: {},
        accounts: [],
        uncat: "",

        setUncat: function(aName) {
            cardbookXULCardEmails.uncat = aName;
        },

        addCardToAccounts: function(aAccount) {
            cardbookXULCardEmails.accounts.push(aAccount);
        },

        addCardToEmails: function(aCard) {
            for (var i = 0; i < aCard.email.length; i++) {
                let email = aCard.email[i][0][0].toLowerCase();
                if (email) {
                    if (!cardbookXULCardEmails.emails[aCard.dirPrefId]) {
                        cardbookXULCardEmails.emails[aCard.dirPrefId] = {};
                    }
                    if (!cardbookXULCardEmails.emails[aCard.dirPrefId][email]) {
                        cardbookXULCardEmails.emails[aCard.dirPrefId][email] = [];
                    }
                    cardbookXULCardEmails.emails[aCard.dirPrefId][email].push(aCard);
                }
            }
        },

        removeCardFromEmails: function(aCard) {
            if (cardbookXULCardEmails.emails[aCard.dirPrefId]) {
                for (var i = 0; i < aCard.email.length; i++) {
                    let email = aCard.email[i][0][0].toLowerCase();
                    if (email) {
                        if (cardbookXULCardEmails.emails[aCard.dirPrefId][email]) {
                            if (cardbookXULCardEmails.emails[aCard.dirPrefId][email].length == 1) {
                                delete cardbookXULCardEmails.emails[aCard.dirPrefId][email];
                            } else {
                                function searchCard(element) {
                                    return (element.dirPrefId+"::"+element.uid != aCard.dirPrefId+"::"+aCard.uid);
                                }
                                cardbookXULCardEmails.emails[aCard.dirPrefId][email] = cardbookXULCardEmails.emails[aCard.dirPrefId][email].filter(searchCard);
                            }
                        }
                    }
                }
            }
        },


        isEmailRegistered: function(aEmail) {
            if (aEmail) {
                let email = aEmail.toLowerCase();
                for (let account of cardbookXULCardEmails.accounts) {
                    if (account[2] && (account[3] != "SEARCH")) {
                        let dirPrefId = account[1];
                        if (cardbookXULCardEmails.emails[dirPrefId]) {
                            if (cardbookXULCardEmails.emails[dirPrefId][email]) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        },
    
        // this function is only used by the CardBook filters
        // as mail account restrictions do not apply to filters
        isEmailInPrefIdRegistered: function(aDirPrefId, aEmail) {
            if (aEmail) {
                let checkString = aEmail.toLowerCase();
                // search for a category
                let sepPosition = aDirPrefId.indexOf("::", 0);
                if (sepPosition != -1) {
                    let category = aDirPrefId.substr(sepPosition+2, aDirPrefId.length);
                    aDirPrefId = aDirPrefId.substr(0, sepPosition);
                    if (cardbookXULCardEmails.emails[aDirPrefId]) {
                        if (cardbookXULCardEmails.emails[aDirPrefId][checkString]) {
                            for (let card of cardbookXULCardEmails.emails[aDirPrefId][checkString]) {
                                if (category == cardbookXULCardEmails.uncat) {
                                    if (card.categories == "") {
                                        return true;
                                    }
                                } else {
                                    if (card.categories.includes(category)) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (cardbookXULCardEmails.emails[aDirPrefId]) {
                        if (cardbookXULCardEmails.emails[aDirPrefId][checkString]) {
                            return true;
                        }
                    }
                }
            }
            return false;
        },
    }
};
