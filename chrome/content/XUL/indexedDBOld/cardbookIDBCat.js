var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

var loader = Services.scriptloader;
loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

var cardbookIDBCat = {
	cardbookCatDatabaseVersion: "3",
	cardbookCatDatabaseName: "CardBookCat",
	encryptionEnabled: false,
	db: {},

	openDBForTransfert: function() {
        let openDB = new Promise( async function(resolve, reject) {
			cardbookIDBCat.encryptionEnabled = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "localDataEncryption"});

			var request = indexedDB.open(cardbookIDBCat.cardbookCatDatabaseName, cardbookIDBCat.cardbookCatDatabaseVersion);
			// when version changes
			// for the moment delete all and recreate one new empty
			request.onupgradeneeded = function(e) {
				var db = e.target.result;
				e.target.transaction.onerror = console.log;
				if (db.objectStoreNames.contains("categories")) {
					db.deleteObjectStore("categories");
				}
				var store = db.createObjectStore("categories", {keyPath: "cbid", autoIncrement: false});
				store.createIndex("dirPrefIdIndex", "dirPrefId", { unique: false });
			};
			// when success, call the observer for starting the load cache and maybe the sync
			request.onsuccess = function(e) {
				cardbookIDBCat.db = e.target.result;
				resolve();
			};
			// when error, call the observer for starting the load cache and maybe the sync
			request.onerror = function(e) {
				console.log(e);
				reject();
			};
		});
		return openDB;
	},

	getDataForTransfert: function() {
        let getDataForTransfert = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBCat.db.transaction(["categories"], "readonly");
			var store = transaction.objectStore("categories");
			var cursorRequest = store.getAll();
			var finalResult = [];
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var category of result) {
						category = await cardbookIDBCat.checkCategory(category);
						finalResult.push(category);
					}
				}
				cardbookIDBCat.db.close();
				resolve(finalResult);
			};
			cursorRequest.onerror = (e) => {
				console.log(e);
				cardbookIDBCat.db.close();
				reject(finalResult);
			};
		});
		return getDataForTransfert;
	},

	// check if the category is in a wrong encryption state
	// then decrypt the category if possible
	checkCategory: async function(aCategory) {
		try {
			var stateMismatched = cardbookIDBCat.encryptionEnabled != 'encrypted' in aCategory;
			var versionMismatched = aCategory.encryptionVersion && aCategory.encryptionVersion != cardbookEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ('encrypted' in aCategory) {
					aCategory = await cardbookEncryptor.decryptCategory(aCategory);
				}
			} else {
				if ('encrypted' in aCategory) {
					aCategory = await cardbookEncryptor.decryptCategory(aCategory);
				}
			}
			return aCategory;
		}
		catch(e) {
			throw new Error("failed to decrypt the category : " + e);
		}
	},
};
