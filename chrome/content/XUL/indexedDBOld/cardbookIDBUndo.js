var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

var loader = Services.scriptloader;
loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

var cardbookIDBUndo = {
	cardbookActionsDatabaseVersion: "6",
	cardbookActionsDatabaseName: "CardBookUndo",
	encryptionEnabled: false,
	db: {},

	openDBForTransfert: function() {
        let openDB = new Promise( async function(resolve, reject) {
			cardbookIDBUndo.encryptionEnabled = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "localDataEncryption"});

			var request = indexedDB.open(cardbookIDBUndo.cardbookActionsDatabaseName, cardbookIDBUndo.cardbookActionsDatabaseVersion);
			// when version changes
			// for the moment delete all and recreate one new empty
			request.onupgradeneeded = function(e) {
				var db = e.target.result;
				e.target.transaction.onerror = console.log;
				if (db.objectStoreNames.contains("cardUndos")) {
					db.deleteObjectStore("cardUndos");
				}
				var store = db.createObjectStore("cardUndos", {keyPath: "undoId", autoIncrement: false});
			};
			// when success, call the observer for starting the load cache and maybe the sync
			request.onsuccess = function(e) {
				cardbookIDBUndo.db = e.target.result;
				resolve();
			};
			// when error, call the observer for starting the load cache and maybe the sync
			request.onerror = function(e) {
				console.log(e);
				reject();
			};
		});
		return openDB;
	},

	getDataForTransfert: function() {
        let getDataForTransfert = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBUndo.db.transaction(["cardUndos"], "readonly");
			var store = transaction.objectStore("cardUndos");
			var cursorRequest = store.getAll();
			var finalResult = [];
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var undo of result) {
						undo = await cardbookIDBUndo.checkUndoItem(undo);
						finalResult.push(undo);
					}
				}
				cardbookIDBUndo.db.close();
				resolve(finalResult);
			};
			cursorRequest.onerror = (e) => {
				console.log(e);
				cardbookIDBUndo.db.close();
				reject(finalResult);
			};
		});
		return getDataForTransfert;
	},

	// check if the card is in a wrong encryption state
	// then decrypt the card if possible
	checkUndoItem: async function(aItem) {
		try {
			var stateMismatched = cardbookIDBUndo.encryptionEnabled != 'encrypted' in aItem;
			var versionMismatched = aItem.encryptionVersion && aItem.encryptionVersion != cardbookEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ('encrypted' in aItem) {
					aItem = await cardbookEncryptor.decryptUndoItem(aItem);
				}
			} else {
				if ('encrypted' in aItem) {
					aItem = await cardbookEncryptor.decryptUndoItem(aItem);
				}
			}
			return aItem;
		}
		catch(e) {
			throw new Error("failed to decrypt the undo : " + e);
		}
	},
};
