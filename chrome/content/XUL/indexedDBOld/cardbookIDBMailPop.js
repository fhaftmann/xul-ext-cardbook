var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

var loader = Services.scriptloader;
loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

var cardbookIDBMailPop = {
	cardbookMailPopDatabaseVersion: "1",
	cardbookMailPopDatabaseName: "CardBookMailPop",
	encryptionEnabled: false,
	db: {},

	openDBForTransfert: function() {
        let openDB = new Promise( async function(resolve, reject) {
			cardbookIDBMailPop.encryptionEnabled = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "localDataEncryption"});

			var request = indexedDB.open(cardbookIDBMailPop.cardbookMailPopDatabaseName, cardbookIDBMailPop.cardbookMailPopDatabaseVersion);
			// when version changes
			// for the moment delete all and recreate one new empty
			request.onupgradeneeded = function(e) {
				var db = e.target.result;
				e.target.transaction.onerror = console.log;
				if (e.oldVersion < 1) {
					if (db.objectStoreNames.contains("mailPop")) {
						db.deleteObjectStore("mailPop");
					}
					let store = db.createObjectStore("mailPop", {keyPath: "mailPopId", autoIncrement: true});
				}
			};
			// when success, call the observer for starting the load cache and maybe the sync
			request.onsuccess = function(e) {
				cardbookIDBMailPop.db = e.target.result;
				resolve();
			};
			// when error, call the observer for starting the load cache and maybe the sync
			request.onerror = function(e) {
				console.log(e);
				reject();
			};
		});
		return openDB;
	},

	getDataForTransfert: function() {
        let getDataForTransfert = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBMailPop.db.transaction(["mailPop"], "readonly");
			var store = transaction.objectStore("mailPop");
			var cursorRequest = store.getAll();
			var finalResult = [];
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var mailPop of result) {
						mailPop = await cardbookIDBMailPop.checkMailPop(mailPop);
						finalResult.push(mailPop);
					}
				}
				cardbookIDBMailPop.db.close();
				resolve(finalResult);
			};
			cursorRequest.onerror = (e) => {
				console.log(e);
				cardbookIDBMailPop.db.close();
				reject(finalResult);
			};
		});
		return getDataForTransfert;
	},

	// check if the mail popularity is in a wrong encryption state
	// then decrypt the mail popularity if possible
	checkMailPop: async function(aMailPop) {
		try {
			var stateMismatched = cardbookIDBMailPop.encryptionEnabled != 'encrypted' in aMailPop;
			var versionMismatched = aMailPop.encryptionVersion && aMailPop.encryptionVersion != cardbookEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ('encrypted' in aMailPop) {
					aMailPop = await cardbookEncryptor.decryptMailPop(aMailPop);
				}
			} else {
				if ('encrypted' in aMailPop) {
					aMailPop = await cardbookEncryptor.decryptMailPop(aMailPop);
				}
			}
			return aMailPop;
		}
		catch(e) {
			throw new Error("failed to decrypt the mail popularity : " + e);
		}
	},
};
