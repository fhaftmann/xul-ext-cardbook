var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

var loader = Services.scriptloader;
loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

var cardbookIDBImage = {
	cardbookImageDatabaseVersion: "1",
	cardbookImageDatabaseName: "CardBookImage",
	allMedia: [ 'photo', 'logo', 'sound' ],
	encryptionEnabled: false,
	db: {},

	openDBForTransfert: function() {
        let openDB = new Promise( async function(resolve, reject) {
			cardbookIDBImage.encryptionEnabled = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "localDataEncryption"});
			
			var request = indexedDB.open(cardbookIDBImage.cardbookImageDatabaseName, cardbookIDBImage.cardbookImageDatabaseVersion);
			// when version changes
			// for the moment delete all and recreate one new empty
			request.onupgradeneeded = function(e) {
				var db = e.target.result;
				e.target.transaction.onerror = console.log;
				if (e.oldVersion < 1) {
					for (let media of cardbookIDBImage.allMedia) {
						if (db.objectStoreNames.contains(media)) {
							db.deleteObjectStore(media);
						}
						let store = db.createObjectStore(media, {keyPath: "cbid", autoIncrement: false});
						store.createIndex("dirPrefIdIndex", "dirPrefId", { unique: false });
					}
				}
			};
			// when success, call the observer for starting the load cache and maybe the sync
			request.onsuccess = function(e) {
				cardbookIDBImage.db = e.target.result;
				resolve();
			};
			// when error, call the observer for starting the load cache and maybe the sync
			request.onerror = function(e) {
				console.log(e);
				reject();
			};
		});
		return openDB;
	},

	getDataForTransfert: function() {
		let checkResult = {};
        let getDataForTransfert = new Promise( async function(resolve, reject) {
			for (let media of cardbookIDBImage.allMedia) {
				var transaction = cardbookIDBImage.db.transaction([media], "readonly");
				var store = transaction.objectStore(media);
				var cursorRequest = store.getAll();
				var finalResult = {};
				cursorRequest.onsuccess = async function(e) {
					var result = e.target.result;
					finalResult[media] = [];
					if (result) {
						for (var data of result) {
							data = await cardbookIDBImage.checkImage(data);
							finalResult[media].push(data);
						}
					}
					checkResult[media] = "OK";
					let check = "";
					for (let media1 of cardbookIDBImage.allMedia) {
						if (checkResult[media1]) {
							check = check + checkResult[media1];
						}
					}
					if (check == "OKOKOK") {
						cardbookIDBImage.db.close();
						resolve(finalResult);
					}
				};
				cursorRequest.onerror = (e) => {
					console.log(e);
					cardbookIDBImage.db.close();
					reject(finalResult);
				};
			}
		});
		return getDataForTransfert;
	},

	// check if the image is in a wrong encryption state
	// then decrypt the image if possible
	checkImage: async function(aImage) {
		try {
			var stateMismatched = cardbookIDBImage.encryptionEnabled != 'encrypted' in aImage;
			var versionMismatched = aImage.encryptionVersion && aImage.encryptionVersion != cardbookEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ('encrypted' in aImage) {
					aImage = await cardbookEncryptor.decryptImage(aImage);
				}
			} else {
				if ('encrypted' in aImage) {
					aImage = await cardbookEncryptor.decryptImage(aImage);
				}
			}
			return aImage;
		}
		catch(e) {
			throw new Error("failed to decrypt the image : " + e);
		}
	},
};
