// Import any needed modules.
var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/observers/cardbookAboutMessageObserver.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/attachments/ovl_attachments.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/gloda/cardbookFindEmails.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/findEvents/cardbookFindEvents.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/AboutMessage/cardbookAboutMessage.js", window, "UTF-8");

// called on window load or on add-on activation while window is already open
function onLoad(wasAlreadyOpen) {
	WL.injectCSS("chrome://cardbook/content/skin/cardbookAddressBooks.css");

    WL.injectElements(`
    <menupopup id="emailAddressPopup">
        <menuseparator id="editCardBookSeparator" insertafter="viewContactItem"/>
        <menu id="addToCardBookMenu" class="menuitem-iconic" label="__MSG_addToCardBookMenuLabel__" accesskey="__MSG_addToCardBookMenuAccesskey__" insertafter="editCardBookSeparator">
            <menupopup id="addToCardBookMenuPopup" onpopupshowing="cardbookAboutMessage.addToCardBookMenuSubMenu(this.id, cardbookAboutMessage.addToCardBook)"/>
        </menu>
        <menuitem id="editInCardBookMenu" class="menuitem-iconic" label="__MSG_editInCardBookMenuLabel__" accesskey="__MSG_editInCardBookMenuAccesskey__" insertafter="addToCardBookMenu" onclick="cardbookAboutMessage.editOrViewContact(event.currentTarget.parentNode.headerField.cardDetails.card);"/>
        <menuitem id="deleteInCardBookMenu" class="menuitem-iconic" label="__MSG_deleteInCardBookMenuLabel__" accesskey="__MSG_deleteInCardBookMenuAccesskey__" insertafter="editInCardBookMenu" onclick="cardbookAboutMessage.deleteContact(event.currentTarget.parentNode.headerField.cardDetails.card);"/>
        <menuseparator id="IMPPCardBookSeparator" insertafter="deleteInCardBookMenu"/>
        <menu id="IMPPCards" class="menuitem-iconic" label="__MSG_IMPPMenuLabel__" accesskey="__MSG_IMPPMenuAccesskey__" insertafter="IMPPCardBookSeparator">
            <menupopup id="IMPPCardsMenuPopup"/>
        </menu>
        <menuseparator id="findCardBookSeparator1" insertafter="IMPPCards"/>
        <menuitem id="findEmailsFromEmailMessenger" class="menuitem-iconic" label="__MSG_findEmailsFromEmailMessengerLabel__" accesskey="__MSG_findEmailsFromEmailMessengerAccesskey__"
            oncommand="cardbookFindEmails.findEmailsFromEmail(event.currentTarget.parentNode.headerField);" insertafter="findCardBookSeparator1"/>
        <menuitem id="findAllEmailsFromContactMessenger" class="menuitem-iconic" label="__MSG_findAllEmailsFromContactMessengerLabel__" accesskey="__MSG_findAllEmailsFromContactMessengerAccesskey__"
            oncommand="cardbookFindEmails.findAllEmailsFromContact(event.currentTarget.parentNode.headerField);" insertafter="findEmailsFromEmailMessenger"/>
        <menuitem id="findEventsFromEmailMessenger" class="menuitem-iconic" label="__MSG_findEventsFromEmailMessengerLabel__" accesskey="__MSG_findEventsFromEmailMessengerAccesskey__"
            oncommand="cardbookFindEvents.findEventsFromEmail(event.currentTarget.parentNode.headerField);" insertafter="findAllEmailsFromContactMessenger"/>
        <menuitem id="findAllEventsFromContactMessenger" class="menuitem-iconic" label="__MSG_findAllEventsFromContactMessengerLabel__" accesskey="__MSG_findAllEventsFromContactMessengerAccesskey__"
            oncommand="cardbookFindEvents.findAllEventsFromContact(event.currentTarget.parentNode.headerField);" insertafter="findEventsFromEmailMessenger"/>
        <menuseparator id="findCardBookSeparator2" insertafter="findAllEventsFromContactMessenger"/>
    </menupopup>

	<menupopup id="attachmentSaveAllMultipleMenu">
		<menu id="attachments1CardBookImport" label="__MSG_addAllAttachementsToCardBookMenuLabel__" insertafter="button-saveAllAttachments">
			<menupopup id="attachments1CardBookImportPopup"/>
		</menu>
	</menupopup>

	<menupopup id="attachmentSaveAllSingleMenu">
		<menu id="attachment1CardBookImport" label="__MSG_addAttachementToCardBookMenuLabel__" insertafter="button-saveAttachment">
			<menupopup id="attachment1CardBookImportPopup"/>
		</menu>
	</menupopup>
	`);

    window.cardbookAboutMessage.load();
};

function onUnload(wasAlreadyOpen) {
    window.cardbookAboutMessage.unload();
    window.ovl_attachments.unload();
};
