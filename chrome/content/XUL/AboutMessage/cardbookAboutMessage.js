if ("undefined" == typeof(cardbookAboutMessage)) {
	var { MailServices } = ChromeUtils.importESModule("resource:///modules/MailServices.sys.mjs");
	var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

	var { cardbookXULUtils } = ChromeUtils.import("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js");

	var cardbookAboutMessage = {
		knownContacts: false,
		origFunctions: {},

		load: function() {
			cardbookAboutMessageObserver.register();
		},

		unload: function() {
			cardbookAboutMessageObserver.unregister();
			customElements.get("header-recipient").prototype.addToAddressBook = cardbookAboutMessage.origFunctions.addToAddressBook;
			customElements.get("header-recipient").prototype._updateAvatar = cardbookAboutMessage.origFunctions._updateAvatar;
			gMessageHeader.openEmailAddressPopup = cardbookAboutMessage.origFunctions.openEmailAddressPopup;
			gMessageHeader.showContactEdit = cardbookAboutMessage.origFunctions.showContactEdit;
			gMessageHeader.editContact = cardbookAboutMessage.origFunctions.editContact;
			gMessageHeader.addContact = cardbookAboutMessage.origFunctions.addContact;
		},

		getCardForEmail: async function(aEmail) {
			var standardRV = { book: null, card: null};
			for (let ab of MailServices.ab.directories) {
				try {
					var card = ab.cardForEmailAddress(aEmail);
					if (card) {
						standardRV.book = ab;
						standardRV.card = card;
					}
				} catch (ex) {}
			}
			let CBcard = await notifyTools.notifyBackground({query: "cardbook.getCardFromEmail", email: aEmail});
			if (CBcard) {
				return  { book: standardRV.book, card: CBcard, standardCard: standardRV.card};
			} else {
				return standardRV;
			}
		},

		getIdentityForEmail: function(aEmail) {
			let emailAddress = aEmail.toLowerCase();
			for (let identity of MailServices.accounts.allIdentities) {
				if (!identity.email) {
					continue;
				}
				if (emailAddress == identity.email.toLowerCase()) {
					return identity;
				}
			}
			return null;
		},
		
		getCardBookDisplayNameFromEmail: async function(aEmail, aDefaultDisplay) {
			return await notifyTools.notifyBackground({query: "cardbook.getCardBookDisplayNameFromEmail", email: aEmail, defaultDisplay: aDefaultDisplay});
		},

		getDisplayNameFromEmailAddressNode: function(emailAddressNode) {
			let fullAddress = "";
			if (emailAddressNode && emailAddressNode.fullAddress) {
				fullAddress = emailAddressNode.fullAddress;
			} else if (emailAddressNode && emailAddressNode.email && emailAddressNode.email.title) {
				fullAddress = emailAddressNode.email.title;
			} else if (emailAddressNode && emailAddressNode.email && emailAddressNode.email.textContent) {
				fullAddress = emailAddressNode.email.textContent;
			}
			let address = MailServices.headerParser.parseEncodedHeaderW(fullAddress)[0];
			return address.name;
		},

		getEmailFromEmailAddressNode: function(emailAddressNode) {
			let fullAddress = "";
			if (emailAddressNode && emailAddressNode.fullAddress) {
				fullAddress = emailAddressNode.fullAddress;
			} else if (emailAddressNode && emailAddressNode.email && emailAddressNode.email.title) {
				fullAddress = emailAddressNode.email.title;
			} else if (emailAddressNode && emailAddressNode.email && emailAddressNode.email.textContent) {
				fullAddress = emailAddressNode.email.textContent;
			}
			let address = MailServices.headerParser.parseEncodedHeaderW(fullAddress)[0];
			if (address.email.includes("@")) {
				return address.email.toLowerCase();
			}
			return "";
		},

		getIdentityKey: function() {
			var result = "";
			if (gFolder) {
				try {
					result = MailServices.accounts.getFirstIdentityForServer(gFolder.server).email;
				} catch(e) {}
			}
			return result;
		},

		addToCardBookMenuSubMenu: function(aMenuName, aCallbackFunction) {
			cardbookXULUtils.addToCardBookMenuSubMenu(document, aMenuName, cardbookAboutMessage.getIdentityKey(), aCallbackFunction, {"emailHeaderNode" : "true"});
		},

		addToCardBook: async function(aDirPrefId, aEmailAddress, aDisplayName) {
			try {
				let addEmail = {fn : aDisplayName, email: aEmailAddress.toLowerCase()};
				if (addEmail.fn == "") {
					addEmail.fn = addEmail.email.substr(0, addEmail.email.indexOf("@")).replace("."," ").replace("_"," ");
				}
				let fnArray = addEmail.fn.split(" ");
				if (fnArray.length > 1) {
					addEmail.lastname = fnArray[fnArray.length - 1];
					let removed = fnArray.splice(fnArray.length - 1, 1);
					addEmail.firstname = fnArray.join(" ");
				}
				await notifyTools.notifyBackground({query: "cardbook.addEmailToCardBook", dirPrefId: aDirPrefId, addEmail: addEmail});
			}
			catch (e) {
				var errorTitle = "addToCardBook";
				Services.prompt.alert(null, errorTitle, e);
			}
		},

		editOrViewContact: async function(aCard) {
			await notifyTools.notifyBackground({query: "cardbook.editOrViewContact", card: aCard});
		},

		deleteContact: async function(aCard) {
			let confirmTitle = cardbookXULUtils.localizeMessage("confirmTitle");
			let confirmMsg = PluralForm.get(1, cardbookXULUtils.localizeMessage("selectedCardsDeletionConfirmMessagePF", 1));
			if (Services.prompt.confirm(window, confirmTitle, confirmMsg)) {
				notifyTools.notifyBackground({query: "cardbook.deleteCards", cards: [ aCard ]});
			}
		},

		hideOldAddressbook: function (aExclusive) {
			if (aExclusive) {
				document.getElementById("addToAddressBookItem").setAttribute("hidden", true);
				document.getElementById("editContactItem").setAttribute("hidden", true);
				document.getElementById("viewContactItem").setAttribute("hidden", true);
				document.getElementById("editCardBookSeparator").setAttribute("hidden", true);
			} else {
				document.getElementById("editCardBookSeparator").removeAttribute("hidden");
			}
		},
		
		hideOrShowLightningEntries: function () {
			document.getElementById("findEventsFromEmailMessenger").removeAttribute("hidden");
			if (cardbookAboutMessage.knownContacts) {
				document.getElementById("findAllEventsFromContactMessenger").removeAttribute("hidden");
			}
		},
		
		hideOrShowNewAddressbook: async function (aValue) {
			cardbookAboutMessage.knownContacts = aValue;
			if (aValue) {
				document.getElementById("addToCardBookMenu").setAttribute("hidden", true);
				document.getElementById("editInCardBookMenu").removeAttribute("hidden");
				document.getElementById("deleteInCardBookMenu").removeAttribute("hidden");
				document.getElementById("findAllEmailsFromContactMessenger").removeAttribute("hidden");
			} else {
				let count = 0;
				let accounts = await notifyTools.notifyBackground({query: "cardbook.getAccounts"});
				for (let account of accounts) {
					if (account[2] && (account[3] != "SEARCH") && !account[4]) {
						count++;
					}
				}
				if (count !== 0) {
					document.getElementById("addToCardBookMenu").removeAttribute("hidden");
				} else {
					document.getElementById("addToCardBookMenu").setAttribute("hidden", true);
				}
				document.getElementById("editInCardBookMenu").setAttribute("hidden", true);
				document.getElementById("deleteInCardBookMenu").setAttribute("hidden", true);
				document.getElementById("findAllEmailsFromContactMessenger").setAttribute("hidden", true);
			}

			document.getElementById("findEventsFromEmailMessenger").setAttribute("hidden", true);
			document.getElementById("findAllEventsFromContactMessenger").setAttribute("hidden", true);
			cardbookAboutMessage.hideOrShowLightningEntries();
		}
	};
};

(function() {
	// Keep a reference to the original function.
	cardbookAboutMessage.origFunctions.addToAddressBook = customElements.get("header-recipient").prototype.addToAddressBook;
	
	// Override a function.
	// addToAddressBook
	customElements.get("header-recipient").prototype.addToAddressBook = async function() {
		let dirPrefId = "";
		let accounts = await notifyTools.notifyBackground({query: "cardbook.getAccounts"});
		for (let account of accounts) {
			if (account[2] && (account[3] != "SEARCH") && !account[4]) {
				dirPrefId = account[1];
				break;
			}
		}
		if (dirPrefId) {
			await cardbookAboutMessage.addToCardBook(dirPrefId, this.emailAddress, this.displayName);
		} else {
			cardbookAboutMessage.origFunctions.addToAddressBook.apply(null, arguments);
		}
	};
})();

(function() {
	// Keep a reference to the original function.
	cardbookAboutMessage.origFunctions._updateAvatar = customElements.get("header-recipient").prototype._updateAvatar;
	
	// Override a function.
	// _updateAvatar
	customElements.get("header-recipient").prototype._updateAvatar = async function() {
		// update blue icons node for the from node
		let cardDetails = await cardbookAboutMessage.getCardForEmail(this.emailAddress);
		this.cardDetails = cardDetails;
		let exclusive = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "exclusive"});
		let hasCard = (this.cardDetails.card && this.cardDetails.card.cbid) || (!exclusive && this.cardDetails.card) ;
		this.abIndicator.classList.toggle("in-address-book", hasCard);

		this.avatar.replaceChildren();

		if (!this.cardDetails.card) {
			this._createAvatarPlaceholder();
		} else {
			// We have a card, so let's try to fetch the image.
			let card = this.cardDetails.card;
			if (card.cbid) {
				let image = await notifyTools.notifyBackground({query: "cardbook.getImage", field: "photo", dirName: "", cardId: card.cbid, cardName: card.fn});
				if (image && image.content && image.extension) {
					let file = Services.dirsvc.get("ProfD", Components.interfaces.nsIFile);
					file.append("Photos");
					file.append(card.uid + "." + image.extension);
					await cardbookXULUtils.writeContentToFile(file.path, atob(image.content), "NOUTF8");
					var photoURL = Services.io.newFileURI(file).spec;
				} else {
					var photoURL = "";
				}
			} else {
				var photoURL = card.photoURL;
			}
			if (photoURL) {
				let img = document.createElement("img");
					document.l10n.setAttributes(img, "message-header-recipient-avatar", {
					address: this.emailAddress,
					});
				// TODO: We should fetch a dynamically generated smaller version of the
				// uploaded picture to avoid loading large images that will only be used
				// in smaller format.
				img.src = photoURL;
				this.avatar.appendChild(img);
				this.avatar.classList.add("has-avatar");
			} else {
				this._createAvatarPlaceholder();
			}
		}

		async function colorAvatars() {
			let nodes = document.getElementById("msgHeaderView").querySelectorAll(".header-recipient");
			let exclusive = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "exclusive"});
			for (let node of nodes) {
				if (node.dataset.headerName != "from") {
					let cardDetails = await cardbookAboutMessage.getCardForEmail(node.emailAddress);
					node.cardDetails = cardDetails;
					let hasCard = (node.cardDetails.card && node.cardDetails.card.cbid) || (!exclusive && node.cardDetails.card);
					node.abIndicator.classList.toggle("in-address-book", hasCard);
				}
				if (node.cardDetails?.card?.cbid) {
					let gMessengerBundle = Services.strings.createBundle("chrome://messenger/locale/messenger.properties");
					let displayName1 = gMessengerBundle.GetStringFromName(`header${node.dataset.headerName}FieldMe`);
					let displayName2 = gMessengerBundle.GetStringFromName("headertoFieldMe");
					if (node.email.textContent != displayName1 && node.email.textContent != displayName2) {
						let showCondensedAddress = Services.prefs.getBoolPref("mail.showCondensedAddresses");
						if (showCondensedAddress && node.cardDetails.card.fn) {
							node.email.textContent = node.cardDetails.card.fn;
						}
						if (node.dataset.headerName == "from") {
							if (showCondensedAddress) {
								node.nameLine.textContent = node.cardDetails.card.fn;
							}
						}
					}
				}
			}
		}

		// update blue icons node for others nodes
		setTimeout(async function(){ 
			await colorAvatars();

			// update the MORE button
			let button = document.querySelector("button.show-more-recipients");
			if (button) {
				button.addEventListener("click", async event => {
					await colorAvatars();
				});
			}
		}, 200);
	};
})();

(function() {
	// Keep a reference to the original function.
	cardbookAboutMessage.origFunctions.openEmailAddressPopup = gMessageHeader.openEmailAddressPopup;
	
	// Override a function.
	// gMessageHeader.openEmailAddressPopup
	gMessageHeader.openEmailAddressPopup = async function() {
		// Execute original function.
		let rv = await cardbookAboutMessage.origFunctions.openEmailAddressPopup.apply(null, arguments);

		// Execute some action afterwards.
		let exclusive = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "exclusive"});
		if (arguments[1].cardDetails.card && arguments[1].cardDetails.card.cbid) {
			await cardbookAboutMessage.hideOrShowNewAddressbook(true);
			let card = arguments[1].cardDetails.card;
			document.getElementById("editInCardBookMenu").setAttribute("cardbookId", card.dirPrefId+"::"+card.uid);
			let readOnly = await notifyTools.notifyBackground({query: "cardbook.pref.getReadOnly", dirPrefId: card.dirPrefId});
			if (readOnly) {
				document.getElementById("editInCardBookMenu").label = cardbookXULUtils.localizeMessage("viewInCardBookMenuLabel");
			} else {
				document.getElementById("editInCardBookMenu").label = cardbookXULUtils.localizeMessage("editInCardBookMenuLabel");
			}
			
			await cardbookXULUtils.addCardToIMPPMenuSubMenu(document, card, "IMPPCardsMenuPopup");

			if (arguments[1].cardDetails.standardCard) {
				document.getElementById("addToAddressBookItem").hidden = true;
				document.getElementById("editContactItem").hidden = !arguments[1].cardDetails.standardCard || arguments[1].cardDetails.book?.readOnly;
				document.getElementById("viewContactItem").hidden = !arguments[1].cardDetails.standardCard || !arguments[1].cardDetails.book?.readOnly;
			} else {
				document.getElementById("addToAddressBookItem").hidden = false;
				document.getElementById("editContactItem").hidden = true;
				document.getElementById("viewContactItem").hidden = true;
			}
		} else {
			await cardbookAboutMessage.hideOrShowNewAddressbook(false);
			await cardbookXULUtils.addCardToIMPPMenuSubMenu(document, null, "IMPPCardsMenuPopup");
		}
		cardbookAboutMessage.hideOldAddressbook(exclusive);

		if (document.documentElement.getAttribute("windowtype") == "mail:messageWindow") {
			document.getElementById("findEmailsFromEmailMessenger").setAttribute("hidden", "true");
			document.getElementById("findAllEmailsFromContactMessenger").setAttribute("hidden", "true");
			document.getElementById("findEventsFromEmailMessenger").setAttribute("hidden", "true");
			document.getElementById("findAllEventsFromContactMessenger").setAttribute("hidden", "true");
			document.getElementById("findCardBookSeparator2").setAttribute("hidden", "true");
		} else {
			document.getElementById("findEmailsFromEmailMessenger").removeAttribute("hidden");
			document.getElementById("findAllEmailsFromContactMessenger").removeAttribute("hidden");
			document.getElementById("findEventsFromEmailMessenger").removeAttribute("hidden");
			document.getElementById("findAllEventsFromContactMessenger").removeAttribute("hidden");
			document.getElementById("findCardBookSeparator2").removeAttribute("hidden");
		}
		
		// return the original result
		return rv;
	};
})();

(function() {
	// Keep a reference to the original function.
	cardbookAboutMessage.origFunctions.showContactEdit = gMessageHeader.showContactEdit;
	
	// Override a function.
	// gMessageHeader.showContactEdit
	gMessageHeader.showContactEdit = function() {
		let event = arguments[0];
		if (event.currentTarget.parentNode.headerField.cardDetails.standardCard) {
			gMessageHeader.editContact(event.currentTarget.parentNode.headerField, "standardCard");
		} else {
			gMessageHeader.editContact(event.currentTarget.parentNode.headerField);
		}
	};
})();

(function() {
	// Keep a reference to the original function.
	cardbookAboutMessage.origFunctions.editContact = gMessageHeader.editContact;
	
	// Override a function.
	// gMessageHeader.editContact
	gMessageHeader.editContact = async function() {
		if (arguments[1] == "standardCard") {
			arguments[0].cardDetails.card = arguments[0].cardDetails.standardCard;
			cardbookAboutMessage.origFunctions.editContact(arguments[0]);
		} else if (arguments[0].cardDetails.card.cbid) {
			await cardbookAboutMessage.editOrViewContact(arguments[0].cardDetails.card);
		} else {
			cardbookAboutMessage.origFunctions.editContact(arguments[0]);
		}
	};
})();

(function() {
	// Keep a reference to the original function.
	cardbookAboutMessage.origFunctions.addContact = gMessageHeader.addContact;
	
	// Override a function.
	// gMessageHeader.addContact
	gMessageHeader.addContact = function() {
		let element = arguments[0].currentTarget.parentNode.headerField;
		let card = Components.classes["@mozilla.org/addressbook/cardproperty;1"].createInstance(Components.interfaces.nsIAbCard);
		card.displayName = element.displayName;
		card.primaryEmail = element.emailAddress;
		let addressBook = MailServices.ab.getDirectory("jsaddrbook://abook.sqlite");
		addressBook.addCard(card);
	};
})();
