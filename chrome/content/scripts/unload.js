let JSMs = [
	"chrome://cardbook/content/XUL/messenger/wl_cardbookMessenger.js",
	"chrome://cardbook/content/XUL/messenger/wl_cardbookAboutMessage.js",
	"chrome://cardbook/content/XUL/messenger/wl_cardbookAbout3Pane.js",
	"chrome://cardbook/content/XUL/messenger/wl_calendar.js",
	"chrome://cardbook/content/XUL/messenger/wl_composeMsg.js",
];

for (let jsm of JSMs) {
	console.log("Unloading: " + jsm);
	Cu.unload(jsm);
}
