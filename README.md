
Pragmatic debian packaging for CardBook for Thunderbird
=======================================================


See also
--------

<https://addons.thunderbird.net/thunderbird/addon/cardbook/>


Building
--------

    $ dpkg-buildpackage


Obtaining current upstream version
----------------------------------

    $ git checkout release-dump
    $ ./download-upstream
    $ git commit -m …
